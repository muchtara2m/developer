<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    @auth
    @php 
    
    $god = '|administrator'; 
    // $myrole = Auth::user()->getRoleNames();
    @endphp
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      @role('admin'.$god)
      <li hidden>
        <a href="{{ url('/home') }}">
          <i class="fa fa-home"></i> <span>Dashboard</span>
        </a>
      </li>
      @endrole
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>PBS</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('pbs-create') }}"><i class="fa fa-circle-o"></i> Create PBS</a></li>
          <li><a href="{{ route('index-pbs') }}"><i class="fa fa-circle-o"></i> PBS</a></li>
          <li><a href="{{ route('status-transaksi') }}"><i class="fa fa-circle-o"></i> Status Transaksi</a></li>
          <li><a href="{{ route('inprogress') }}"><i class="fa fa-circle-o"></i> Inprogress</a></li>
          <li><a href="{{ route('selesai') }}"><i class="fa fa-circle-o"></i> Selesai</a></li>
          <li><a href="{{ route('revisi-transaksi') }}"><i class="fa fa-circle-o"></i> Revisi Transaksi</a></li>
          <li><a href="{{ route('kalkulator-pinjaman') }}"><i class="fa fa-circle-o"></i> Kalkulator Pinjaman</a></li>
        </ul>
      </li>
      @php $godspph = $god.'|mgrproc|admin|adminbakn'; @endphp
      @role($godspph)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>SPPH</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('spph-create') }}"><i class="fa fa-circle-o"></i>Create SPPH</a></li>
          <li><a href="{{ url('spph-index') }}"><i class="fa fa-circle-o"></i>List SPPH</a></li>
          <li><a href="{{ url('spph-draft') }}"><i class="fa fa-circle-o"></i>Draft SPPH</a></li>
          <li><a href="{{ url('spph-done') }}"><i class="fa fa-circle-o"></i>Done SPPH</a></li>
        </ul>
      </li>
      @endrole
      @php $godbak = $god.'|mgrproc|avplog|adminbakn'; @endphp
      @role($godbak)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>BAKN</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>

        <ul class="treeview-menu">
          <li hidden><a href="{{ url('ds-bakn') }}"><i class="fa fa-circle-o"></i>Dashboard BAKN</a></li>
          <li><a href="{{ url('bakn-spph') }}"><i class="fa fa-circle-o"></i>Create BAKN</a></li>
          <li><a href="{{ url('list-bakn') }}"><i class="fa fa-circle-o"></i>List BAKN</a></li>
          <li><a href="{{ url('bakn-draft') }}"><i class="fa fa-circle-o"></i>Draft BAKN</a></li>
          <li><a href="{{ url('bakn-done') }}"><i class="fa fa-circle-o"></i>Done BAKN</a></li>
        </ul>
      </li>
      @endrole
      @php
        $godkon = $god.'|admin|mgrlog|mgrlegal|mgrproc|gmproc|vpgs|dirfbs|dirut|avplog|adminkontrak';
        $kon1 = $god.'|mgrlegal';
        $kon2 = $kon1.'|admin|adminkontrak';
        $kon3 = $kon2.'|mgrproc|mgrlog|vpgs|gmproc|dirfbs|dirut|avplog';
        $kon4 = $kon3.'|staff';
      @endphp
      @role($godkon)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>KONTRAK</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @hasrole($kon1)
            <li hidden><a href="{{ url('ds-kontrak') }}"><i class="fa fa-circle-o"></i>Dashboard Kontrak</a></li>
            <li><a href="{{ url('kontrak') }}"><i class="fa fa-circle-o"></i>Kontrak</a></li>
          @endhasrole
          @hasrole($kon2)
            <li><a href="{{ url('kontrak-list') }}"><i class="fa fa-circle-o"></i>List Kontrak</a></li>
            <li><a href="{{ url('kontrak-draft') }}"><i class="fa fa-circle-o"></i>Draft Kontrak</a></li>
            {{-- <li><a href="{{ url('kontrak-return') }}"><i class="fa fa-circle-o"></i>Return Kontrak</a></li> --}}
          @endhasrole
          @hasrole($kon3)
            <li><a href="{{ url('kontrak-status') }}"><i class="fa fa-circle-o"></i>Status Transaksi</a></li>
            <li><a href="{{ url('kontrak-inprogress') }}"><i class="fa fa-circle-o"></i>Inprogress Kontrak</a></li>
          @endhasrole
          @hasrole($kon4)
            <li><a href="{{ url('kontrak-done') }}"><i class="fa fa-circle-o"></i>Done Kontrak</a></li>
          @endhasrole
        </ul>
      </li>
      @endrole
      @role($god)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>MASTER DATA</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('flow') }}"><i class="fa fa-circle-o"></i> Data Flow</a></li>
          <li><a href="{{ route('user.index') }}"><i class="fa fa-circle-o"></i> Data Karyawan</a></li>
          <li><a href="{{ url('mitra') }}"><i class="fa fa-circle-o"></i> Data Mitra</a></li>
           <li><a href="{{ url('pasal') }}"><i class="fa fa-circle-o"></i> Data Pasal</a></li>
          <li><a href="{{ route('chairman.index') }}"><i class="fa fa-circle-o"></i> Data Pimpinan Rapat</a></li>
          <li><a href="{{ route('rpmanage.index') }}"><i class="fa fa-circle-o"></i> Data Role</a></li>
          <li><a href="{{ route('unit.index') }}"><i class="fa fa-circle-o"></i> Data Unit</a></li>
          <li><a href="{{ url('jenispasal') }}"><i class="fa fa-circle-o"></i> Data Jenis Pasal</a></li>
          <li><a href="{{ url('tatacara') }}"><i class="fa fa-circle-o"></i> Data Cara Bayar</a></li>

          {{-- <li><a href="index2.html"><i class="fa fa-circle-o"></i> Data IO</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Data Customer</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Data PBS</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Data Flow</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Barang</a></li> --}}
        </ul>
      </li>
      @endrole
      <li class="treeview" hidden>
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>SYSTEM</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Setting Menu</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> List Icon</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Simulasi Flow</a></li>
        </ul>
      </li>
      <li class="treeview" hidden>
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>LM</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Setting Report</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Report 1</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Report 2</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Report 4</a></li>
        </ul>
      </li>
      <li class="treeview" hidden>
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>REPORTING</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Setting Report</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Report 1</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Report 2</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Report 4</a></li>
        </ul>
      </li>
    </ul>
    @endauth

  </section>
  <!-- /.sidebar -->
</aside>
