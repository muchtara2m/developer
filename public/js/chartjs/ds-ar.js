function chartUnbill(totalnya,unbill,hasil,bagi,bill){
    var max  = 2000000;
    var min = 0;
    
    am4core.ready(function() {
        // declare nilai 
        
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
        
        // create chart
        var chart = am4core.create("chartunbill", am4charts.GaugeChart);
        chart.innerRadius = am4core.percent(88);
        
        /**
        * Normal axis
        */
        
        var axis = chart.xAxes.push(new am4charts.ValueAxis());
        
        axis.min = min;
        axis.max = max; //max percent
        axis.strictMinMax = true;
        axis.renderer.minGridDistance = 400;
        axis.renderer.radius = am4core.percent(86); //membuat garis dalam lingkraan
        axis.renderer.inside = true;
        axis.renderer.line.strokeOpacity = 1;
        axis.renderer.ticks.template.disabled = false
        axis.renderer.ticks.template.strokeOpacity = 1;
        axis.renderer.ticks.template.length = 10;
        axis.renderer.grid.template.disabled = true;
        axis.renderer.labels.template.radius = 80;
        axis.renderer.minGridDistance = 200;
        axis.renderer.labels.template.adapter.add("text", function(text) {
            return "Rp "+ text; //satuan
        })
        
        /**
        * Axis for ranges
        */
        
        var colorSet = new am4core.ColorSet();
        
        var axis2 = chart.xAxes.push(new am4charts.ValueAxis());
        axis2.min = min;
        axis2.max = max; 
        axis2.renderer.innerRadius = 10
        axis2.strictMinMax = true;
        axis2.renderer.minGridDistance = 4000;
        axis2.renderer.labels.template.disabled = true;
        axis2.renderer.ticks.template.disabled = true;
        axis2.renderer.grid.template.disabled = true;
        
        var range0 = axis2.axisRanges.create();
        range0.value = 0;
        range0.endValue = 1000000;
        range0.axisFill.fillOpacity = 1;
        range0.axisFill.fill = colorSet.getIndex(3);
        
        var range1 = axis2.axisRanges.create();
        range1.value = 1000000;
        range1.endValue = max;
        range1.axisFill.fillOpacity = 1;
        range1.axisFill.fill = colorSet.getIndex(5);
        
        /**
        * Label
        */
        
        var label = chart.radarContainer.createChild(am4core.Label);
        label.isMeasured = false;
        label.fontSize = 45;
        label.x = am4core.percent(50);
        label.y = am4core.percent(50);
        label.horizontalCenter = "middle";
        label.verticalCenter = "bottom";
        label.text = "Rp " + unbill.toFixed(2);
        
        
        /**
        * Hand
        */
        
        var hand = chart.hands.push(new am4charts.ClockHand());
        hand.axis = axis2;
        hand.innerRadius = am4core.percent(20);
        hand.startWidth = 10;
        hand.pin.disabled = true;
        hand.value = unbill; 
        
        hand.events.on("propertychanged", function(ev) {
            range0.endValue = ev.target.value;
            range1.value = ev.target.value;
            axis2.invalidate();
        });
       
        var animation = new am4core.Animation(hand, {
            property: "value",
            from: 0,
            to: unbill
        }, 2000, am4core.ease.cubicOut).start();
        
    }); 
    // end am4core.ready() 1    
    
    
    // dashboard bill------------------------------------------------------------------------------------------------------
    am4core.ready(function() {
        
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
        
        // create chart
        var chart = am4core.create("chartbill", am4charts.GaugeChart);
        chart.innerRadius = am4core.percent(88);
        
        /**
        * Normal axis
        */
        
        var axis = chart.xAxes.push(new am4charts.ValueAxis());
        
        axis.min = min;
        axis.max = max; //max percent
        axis.strictMinMax = true;
        axis.renderer.minGridDistance = 400;
        axis.renderer.radius = am4core.percent(86); //membuat garis dalam lingkraan
        axis.renderer.inside = true;
        axis.renderer.line.strokeOpacity = 1;
        axis.renderer.ticks.template.disabled = false
        axis.renderer.ticks.template.strokeOpacity = 1;
        axis.renderer.ticks.template.length = 10;
        axis.renderer.grid.template.disabled = true;
        axis.renderer.labels.template.radius = 80;
        axis.renderer.minGridDistance = 200;
        axis.renderer.labels.template.adapter.add("text", function(text) {
            return "Rp "+text; //satuan
        })
        /**
        * Axis for ranges
        */
        
        var colorSet = new am4core.ColorSet();
        
        var axis2 = chart.xAxes.push(new am4charts.ValueAxis());
        axis2.min = min;
        axis2.max = max; 
        axis2.renderer.innerRadius = 10
        axis2.strictMinMax = true;
        axis2.renderer.minGridDistance = 4000;
        axis2.renderer.labels.template.disabled = true;
        axis2.renderer.ticks.template.disabled = true;
        axis2.renderer.grid.template.disabled = true;
        
        var range0 = axis2.axisRanges.create();
        range0.value = min;
        range0.endValue = 1000000;
        range0.axisFill.fillOpacity = 1;
        range0.axisFill.fill = colorSet.getIndex(3);
        
        var range1 = axis2.axisRanges.create();
        range1.value = 1000000;
        range1.endValue = max;
        range1.axisFill.fillOpacity = 1;
        range1.axisFill.fill = colorSet.getIndex(5);
        
        /**
        * Label
        */
        
        var label = chart.radarContainer.createChild(am4core.Label);
        label.isMeasured = false;
        label.fontSize = 45;
        label.x = am4core.percent(50);
        label.y = am4core.percent(50);
        label.horizontalCenter = "middle";
        label.verticalCenter = "bottom";
        label.text = "Rp " + bill.toFixed(2);  
        
        
        /**
        * Hand
        */
        
        var hand = chart.hands.push(new am4charts.ClockHand());
        hand.axis = axis2;
        hand.innerRadius = am4core.percent(20);
        hand.startWidth = 10;
        hand.pin.disabled = true;
        hand.value = bill; 
        
        hand.events.on("propertychanged", function(ev) {
            range0.endValue = ev.target.value;
            range1.value = ev.target.value;
            axis2.invalidate();
        });
        var animation = new am4core.Animation(hand, {
            property: "value",
            from: 0,
            to: bill
        }, 2000, am4core.ease.cubicOut).start();
       
        
    }); // end am4core.ready()
}