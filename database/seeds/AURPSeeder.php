<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class AURPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');
        
        $permissions = [
            'add' => 'Add data',
            'edit' => 'Edit data',
            'view' => 'View data',
            'delete' => 'Delete data'
        ];
        foreach ($permissions as $perm => $dperm) {
            Permission::create([
                'name' => $perm,
                'display' => $dperm
            ]);
        }

        $roles = [
            'administrator' => 'Administrator',
            'admin' => 'Admin Staff',
            'avpbudget' => 'AVP Budget',
            'avpsales' => 'AVP Sales',
            'dirut' => 'Dir. Utama',
            'dirfbs' => 'Dir. FBS',
            'dirop' => 'Dir. Operation',
            'dirsales' => 'Dir. Sales',
            'gmecom' => 'GM Ecommerce',
            'gmsales' => 'GM Sales',
            'gmsol' => 'GM Solution',
            'mgrsales' => 'Manager Sales',
            'user' => 'User',
            'vpacc' => 'VP Accounting',
            'vpsales' => 'VP Sales',
            'vptrea' => 'VP Treasury'
        ];
        $key = array_keys($roles);
        for ($i=0; $i < count($roles); $i++) { 
            if ($i == 0) {
                $action = Role::create([
                    'name' => $key[$i],
                    'display' => $roles[$key[$i]]
                ]);
                $action->givePermissionTo(Permission::all());
            }
            else {
                Role::create([
                    'name' => $key[$i],
                    'display' => $roles[$key[$i]]
                ]);
            }
        }

        $user = User::create([
            "username" => "agung.fadhil",
            "name" => "Muhammad Agung Fadhil",
            "email" => "agung.fadhil@pins.co.id",
            "level" => 1,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('administrator');

        $user = User::create([
            "username" => "fauzi.hanif",
            "name" => "Ahmad Fauzi Hanif",
            "email" => "fauzi.hanif@pins.co.id",
            "level" => 1,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('administrator');

        $user = User::create([
            "username" => "abdul.muchtar",
            "name" => "Abdul Muchtar Astria",
            "email" => "abdul.muchtar@pins.co.id",
            "level" => 1,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('administrator');

        $user = User::create([
            "username" => "mfirdaus",
            "name" => "Mohammad Firdaus",
            "email" => "mfirdaus@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('dirut');

        $user = User::create([
            "username" => "imam.santoso",
            "name" => "Imam Santoso",
            "email" => "imam.santoso@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('dirop');

        $user = User::create([
            "username" => "rosanti",
            "name" => "Notje Rosanti",
            "email" => "rosanti@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('dirfbs');

        $user = User::create([
            "username" => "benny_artono",
            "name" => "Benny Artono",
            "email" => "benny_artono@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('vpsales');

        $user = User::create([
            "username" => "ernist",
            "name" => "Ernist Rondang Marojahan",
            "email" => "ernist@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('mgrsales');

        $user = User::create([
            "username" => "konang",
            "name" => "Konang Prihandoko",
            "email" => "konang@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('gmecom');

        $user = User::create([
            "username" => "rolan",
            "name" => "Rolan Erick M Panjaitan",
            "email" => "rolan@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('vpacc');

        $user = User::create([
            "username" => "thomas",
            "name" => "Thomas Idi Kurnadi",
            "email" => "thomas@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('vptrea');

        $user = User::create([
            "username" => "sigma",
            "name" => "Sigit Marsono",
            "email" => "sigma@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "suska",
            "name" => "Suska Budiwilopo",
            "email" => "suska@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "sutji_ph",
            "name" => "Sutji Prihyatmoko",
            "email" => "sutji_ph@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "uponco",
            "name" => "Uut Ponco Ari Prabowo",
            "email" => "uponco@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "sams",
            "name" => "Samsu",
            "email" => "sams@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "handoko",
            "name" => "Heribertus Handoko",
            "email" => "handoko@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "iwan",
            "name" => "Iwan Gunawan",
            "email" => "iwan@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "lamhots",
            "name" => "Lamhot Tagor Lindung Simanungkalit",
            "email" => "lamhots@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "rosihan73",
            "name" => "Rosihan Muhammad",
            "email" => "rosihan73@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "bteguhp",
            "name" => "Budi Teguh Prakoso",
            "email" => "bteguhp@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "gati",
            "name" => "Gati Cahyo Handoyo",
            "email" => "gati@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "tasmin",
            "name" => "Tasmin",
            "email" => "tasmin@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "paulus",
            "name" => "Paulus Wibowo Surjo Laksono",
            "email" => "paulus@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "marno",
            "name" => "Sumarno",
            "email" => "marno@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "hernadi.yoga",
            "name" => "Hernadi Yoga Adhitya Tama",
            "email" => "hernadi.yoga@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "rudi.husudungan",
            "name" => "Rudi Hasudungan Napitupulu",
            "email" => "rudi.husudungan@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "hendry@pins.co.id",
            "name" => "Hendry Zulfikar",
            "email" => "hendry@pins.co.id@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "a.yudiarsa",
            "name" => "Andisi Yudiarsa",
            "email" => "a.yudiarsa@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "fatimah",
            "name" => "Fatimah Marjuwah",
            "email" => "fatimah@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "suprastio",
            "name" => "Suprastio",
            "email" => "suprastio@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "yooha",
            "name" => "Yohananto",
            "email" => "yooha@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "edy",
            "name" => "Edy Mulyono",
            "email" => "edy@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "ardi.ismail",
            "name" => "Ardi Ismail",
            "email" => "ardi.ismail@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "hengki.lukito",
            "name" => "Hengki Lukito",
            "email" => "hengki.lukito@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "tebin",
            "name" => "Teguh Bintoro",
            "email" => "tebin@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "ondang",
            "name" => "Rumondang Rosalina Sianturi",
            "email" => "ondang@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "3ads",
            "name" => "Tri Adi Santosa",
            "email" => "3ads@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "dita.hanum",
            "name" => "Dita Sakina Hanum",
            "email" => "dita.hanum@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "robi.cahyono",
            "name" => "Robi Cahyono",
            "email" => "robi.cahyono@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "ronaldo",
            "name" => "Ronaldo Naiborhu",
            "email" => "ronaldo@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "anik",
            "name" => "Ni Nyoman Anik Bagiani",
            "email" => "anik@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "regina",
            "name" => "Regina Lenggogeni",
            "email" => "regina@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "rulliku",
            "name" => "Rulli Kurnia",
            "email" => "rulliku@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "drajad",
            "name" => "Drajad Putrandono",
            "email" => "drajad@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "agus.kamdar",
            "name" => "Agus Kamdar",
            "email" => "agus.kamdar@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "nyoman.aryana",
            "name" => "I Nyoman Aryana",
            "email" => "nyoman.aryana@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "windarto",
            "name" => "Windarto Budi Atmono",
            "email" => "windarto@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "arsad",
            "name" => "Arsad",
            "email" => "arsad@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "hafiz",
            "name" => "Hafiz",
            "email" => "hafiz@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "murtado",
            "name" => "Murtado",
            "email" => "murtado@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "racka",
            "name" => "Racka Raditya Roemanto",
            "email" => "racka@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "gunung",
            "name" => "Gunung Pahala T",
            "email" => "gunung@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "jonggi",
            "name" => "Jonggi Siregar",
            "email" => "jonggi@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "harnindyo",
            "name" => "Harnindyo Khrisnadi",
            "email" => "harnindyo@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "surya.f",
            "name" => "Surya Fachrudiansyah",
            "email" => "surya.f@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "beny_t",
            "name" => "Beny Triantono",
            "email" => "beny_t@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "revi",
            "name" => "Revi Guspa",
            "email" => "revi@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "shopian",
            "name" => "Akhmad Shopian",
            "email" => "shopian@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "fery",
            "name" => "Fery Indrawan",
            "email" => "fery@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "sigit.sumarsono",
            "name" => "Sigit Sumarsono",
            "email" => "sigit.sumarsono@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "iskandar_a",
            "name" => "Iskandar Adiwinoto",
            "email" => "iskandar_a@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "fauzi",
            "name" => "Ahmad Fauzi",
            "email" => "fauzi@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "rizki.firman",
            "name" => "Rizki Firman",
            "email" => "rizki.firman@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "obed",
            "name" => "Obed Christian Alexandermarpaung",
            "email" => "obed@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "dhanang.wardhana",
            "name" => "Dhanang Kusuma Wardhana",
            "email" => "dhanang.wardhana@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "ario.mf",
            "name" => "Ario Muhammad Fanie",
            "email" => "ario.mf@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
        $user->assignRole('user');

        $user = User::create([
            "username" => "jamerkin",
            "name" => "Jamerkin Saragih",
            "email" => "jamerkin@pins.co.id",
            "level" => 3,
            "password" => bcrypt('ijustmadeiteasyforyou')
        ]);
    }
}