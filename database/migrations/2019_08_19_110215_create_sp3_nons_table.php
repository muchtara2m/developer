<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSp3NonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sp3_nons', function (Blueprint $table) {
            // relation to data bakn
            $table->integer('bakn_id')->unsigned();
            $table->foreign('bakn_id')->references('id')->on('bakns')->onDelete('cascade');

            $table->increments('id');
            $table->string('nosp3non');
            $table->date('tglsp3');
            $table->enum('status', ['save_sp3','draft_sp3','done_sp3','disp_sp3']);
            
            $table->string('approval')->nullable();
            $table->integer('queue')->nullable();
            $table->string('endapproval')->nullable();
            //  file
            $table->string('file')->nullable();
            $table->string('title')->nullable();
            $table->dateTime('tglupload')->nullable();

            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp3_nons');
    }
}
