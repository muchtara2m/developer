<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMenuAccessToRoles extends Migration
{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::table('roles', function (Blueprint $table) {
			$table->text('menu_access')->after('display')->nullable();
		});

		Schema::table('permissions', function (Blueprint $table) {
			$table->text('menu_access')->after('display')->nullable();
		});

		Artisan::call( 'db:seed', [
			'--class' => 'ListPermissionsSeeder',
			'--force' => true
			]
		);
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{

	}
}
