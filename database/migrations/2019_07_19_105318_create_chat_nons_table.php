<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatNonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_nons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idKontrakNon')->references('id')->on('kontrak_nons')->onDelete('cascade');
            $table->text('chat');
            $table->integer('queue');
            $table->string('jabatan');
            $table->string('username');
            $table->string('transaksi');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_nons');
    }
}
