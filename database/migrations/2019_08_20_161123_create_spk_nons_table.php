<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpkNonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spk_nons', function (Blueprint $table) {
            $table->integer('bakn_id')->unsigned();
            $table->foreign('bakn_id')->references('id')->on('bakns')->onDelete('cascade');

            $table->increments('id');
            $table->string('nospknon');
            $table->date('tglspk');
            $table->enum('status', ['save_spk','draft_spk','done_spk','disp_spk']);
        
            //  file
            $table->string('file')->nullable();
            $table->string('title')->nullable();
            $table->dateTime('tglupload')->nullable();

            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spk_nons');
    }
}
