<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spks', function (Blueprint $table) {
            // relation to data bakn
            $table->integer('bakn_id')->unsigned();
            $table->foreign('bakn_id')->references('id')->on('bakns')->onDelete('cascade');

            $table->increments('id');
            $table->string('nospk');
            $table->date('tglspk');
            $table->enum('status', ['save_spk','draft_spk','done_spk','disp_spk']);
            $table->text('file')->nullable();
            $table->text('title')->nullable();
            $table->string('approval')->nullable();
            $table->integer('queue')->nullable();
            $table->string('endapproval')->nullable();
            $table->dateTime('tglupload')->nullable();
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spks');
    }
}
