<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblInisiasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_inisiasis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('io');
            $table->integer('am_id');
            $table->integer('portofolio_id');
            $table->integer('segment_id');
            $table->integer('customer_id');
            $table->integer('end_customer_id');
            $table->date('tgl_win');
            $table->double('nilai_project');
            $table->date('tgl_closing');
            $table->string('status');
            $table->integer('kategori_id');
            $table->string('submission_create');
            $table->string('submission_to');
            $table->string('submission_file');
            $table->date('submission_date');
            $table->string('list_submitted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_inisiasis');
    }
}
