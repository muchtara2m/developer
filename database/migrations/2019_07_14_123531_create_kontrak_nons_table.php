<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKontrakNonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kontrak_nons', function (Blueprint $table) {
            $table->integer('sp3_id')->unsigned();
            $table->foreign('sp3_id')->references('id')->on('sp3s')->onDelete('cascade');

            $table->integer('bakn_id')->unsigned();
            $table->foreign('bakn_id')->references('id')->on('bakns')->onDelete('cascade');

            $table->increments('id');
            $table->date('tglkontrak');
            $table->string('handler');
            $table->text('isi');
            $table->string('approval');
            $table->tinyInteger('queue');
            $table->string('endapproval');
            $table->enum('status', ['save_kontrak','draft_kontrak','done_kontrak','disp_kontrak']);
             // data file
             $table->string('file')->nullable();
             $table->string('title')->nullable();
             $table->dateTime('upload')->nullable();

            $table->tinyInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kontrak_nons');
    }
}
