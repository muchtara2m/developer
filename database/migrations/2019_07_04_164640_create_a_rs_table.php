<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateARsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ars', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');

            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')->references('id')->on('mitras')->onDelete('cascade');

            $table->integer('io_id')->unsigned();

            $table->text('uraian');
            $table->text('invoice')->nullable();
            $table->bigInteger('nilai_invoice')->nullable();
            $table->bigInteger('nilai_project');
            $table->bigInteger('nilai_belum_ditagih')->nullable();
            $table->enum('status', ['unbill','bill','readytobill','paid','paid100'])->nullable();

            $table->dateTime('spk')->nullable();
            $table->dateTime('kl')->nullable();
            $table->dateTime('baut')->nullable();
            $table->dateTime('bast')->nullable();
            $table->dateTime('bast2')->nullable();
            $table->dateTime('baop')->nullable();
            $table->dateTime('baso')->nullable();
            $table->dateTime('bapp')->nullable();
            $table->dateTime('npk')->nullable();
            $table->dateTime('lpp')->nullable();
            $table->dateTime('baperub')->nullable();
            $table->dateTime('suratgm')->nullable();
            $table->dateTime('top')->nullable();
            $table->dateTime('barekon')->nullable();
            $table->dateTime('performansi')->nullable();
            $table->dateTime('lkpp')->nullable();
            $table->dateTime('ep')->nullable();
            $table->dateTime('tgl_invoice')->nullable();
            $table->dateTime('tgl_buktibayar')->nullable();
            $table->text('title')->nullable();
            $table->text('file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('a_rs');
    }
}
