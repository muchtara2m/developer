<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spk extends Model
{
    //
    public function bakns()
    {
        return $this->belongsTo('App\Bakn','bakn_id');
    }
}
