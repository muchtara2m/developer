<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sp3 extends Model
{
    //
    public function bakns()
    {
        return $this->belongsTo('App\Bakn','bakn_id');
    }
    public function kontraks()
    {
        return $this->hasMany('App\Kontrak','sp3_id');
    }
    public function kontraksnon()
    {
        return $this->hasMany('App\KontrakNon','sp3_id');
    }
    public function chatsp3(){
        return $this->hasMany('App\ChatSp3','idsp3');
    }
    public function users(){
        return $this->belongsTo('App\User','created_by');
    }
}
