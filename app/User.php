<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name','username','email','password','level','employee_status',
    ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function creator_spph()
    {
        return $this->hasMany('App\Spph', 'created_by');
    }
    public function creator_bakn()
    {
        return $this->hasMany('App\Bakn', 'created_by');
    }
    public function creator_spk()
    {
        return $this->hasMany('App\Spk','created_by');
    }
    public function creator_kontrak()
    {
        return $this->hasMany('App\Kontrak','created_by');
    }
    public function creator_kontraknon()
    {
        return $this->hasMany('App\KontrakNon','created_by');
    }
    public function creator_sp3()
    {
        return $this->hasMany('App\Sp3','created_by');
    }
    public function creator_ar()
    {
        return $this->hasMany('App\AR','created_by');
    }
    public function unitnya()
    {
        return $this->belongsTo('App\Unit', 'id_unit');
    }
    public function creator_chatnon()
    {
        return $this->belongsTo('App\ChatNon','id_user');
    }

}
