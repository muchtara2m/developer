<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatNon extends Model
{
    //
    public function kontraknons()
    {
        return $this->belongsTo('App\KontrakNon','id');
    }
    public function namabikin(){
        return $this->belongsTo('App\User','id');
    }
}
