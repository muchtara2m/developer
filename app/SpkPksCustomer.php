<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpkPksCustomer extends Model
{
    //
    protected $connection = 'mysql2';
    protected $table = 't_spk_pks_customer';

    public function customerSpkPks(){
        return $this->belongsTo('App\CustomerSpkPks','akun_customer');
    }
    public function ioSpkPksCustomer(){
        return $this->belongsTo('App\IoPipeline','no_io');
    }
    public function mappingIoCustomer(){
        return $this->belongsTo('App\MappingIoUbis','no_io');
    }
}
