<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rkap extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'rkap';
    // One to many Rkap to Ubis
    public function rkap_ubis()
    {
        return $this->belongsTo('App\Ubis', 'ubis');
    }
}
