<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortofolioPipeline extends Model
{
    protected $connection = 'mysql2';
    protected $table = 't_portofolio';
    protected $primaryKey = 'id_portofolio';
}
