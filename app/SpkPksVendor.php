<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpkPksVendor extends Model
{
    //
    protected $connection = 'mysql2';
    protected $table = 't_spk_pks_vendor';

    public function akunVendor(){
        return $this->belongsTo('App\Vendor','akun_vendor');
    }
    public function mappingIo(){
        return $this->belongsTo('App\MappingIoUbis','no_io');
    }
    public function ioSpkPksVendor(){
        return $this->belongsTo('App\IoPipeline','no_io');
    }
}
