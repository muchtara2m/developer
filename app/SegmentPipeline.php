<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SegmentPipeline extends Model
{
    protected $connection = 'mysql2';
    protected $table = 't_segment';
    protected $primaryKey = 'id_segmen';
}
