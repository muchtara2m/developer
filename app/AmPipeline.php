<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmPipeline extends Model
{
    //
    protected $connection = 'mysql2';
    protected $table = 't_am' ;
    protected $primaryKey = 'am_id';
}
