<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanBast extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'plan_bast';
    // One to many PlanBast to Ubis
    public function planbast_ubis()
    {
        return $this->belongsTo('App\Ubis', 'ubis');
    }
}
