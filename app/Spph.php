<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Spph extends Model
{
    //
    // use SoftDeletes;

    protected $fillable = [
        'file_title',
        'file'
    ];

    public function bakns(){
        return $this->hasMany("App\Bakn",'spph_id');
    }
    public function creator(){
        return $this->belongsTo('App\User','created_by');
    }
    public function mitras(){
        return $this->belongsTo('App\Mitra', 'mitra');
    }
}
