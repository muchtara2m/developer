<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DshSap extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'dsh_sap';
}
