<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusInisiasi extends Model
{
    //
    protected $connection = 'mysql2';
    protected $table = 't_status_inisiasi';
    protected $primaryKey = 'id_status';
}
