<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpkNon extends Model
{
    //
    public function bakns()
    {
        return $this->belongsTo('App\Bakn','bakn_id');
    }
}
