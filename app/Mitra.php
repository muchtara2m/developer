<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mitra extends Model
{
    // protected $table = 'mitras';
    //
    public function spph(){
        return $this->hasMany('App\Spph','mitra');
    }

    public function ar(){
        return $this->hasMany('App\AR','customer_id');
    }
}
