<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MappingIoUbis extends Model
{
    //
    protected $connection = 'mysql2';
    protected $table = 't_mapping_io_ubis';
    protected $primaryKey = 'no_io';

    public function mappingSpkPksVendor(){
        return $this->hasMany('App\SpkPksVendor','no_io');
    }
    public function ubisMappingVendor(){
        return $this->belongsTo('App\Ubis','ubis_after_to');
    }
    public function mappingSpkPksCustomer(){
        return $this->hasMany('App\SpkPksCustomer','no_io');
    }
    public function ubisMappingCustomer(){
        return $this->belongsTo('App\Ubis','ubis_after_to');
    }
    
}
