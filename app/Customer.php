<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = 'mtr_customer';

    public function ars(){
        return $this->hasMany('App\AR','id');
    }
}
