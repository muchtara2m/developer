<?php

namespace App\Http\Controllers;

use App\AmPipeline;
use App\CustomerInisiasi;
use Illuminate\Http\Request;
use Response;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('home');
        if (Auth::user()) {
            return redirect('home');
        }
        else
            return redirect('login');
    }
    public function notifikasi()
    {
      
        $notif= DB::table('kontraks')
                ->select('status_approval')
                ->where('status_approval', Auth::user()->position)
                ->count('status_approval');

        $list = DB::table('kontraks')
                ->select('judul','handler')
                ->where('status_approval', Auth::user()->position)
                ->get();

        return view('layouts.includes.header', compact('notif','list'));
    }
    public function testingAPI(){
        $am = AmPipeline::all();
        return Response::json(array(
            'status' => 'success',
            'pages' => $am->toArray()),
            200
        );
    }
}
