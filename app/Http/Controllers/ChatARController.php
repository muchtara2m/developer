<?php

namespace App\Http\Controllers;

use App\ChatAR;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatARController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChatAR  $chatAR
     * @return \Illuminate\Http\Response
     */
    public function show(ChatAR $chatAR)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChatAR  $chatAR
     * @return \Illuminate\Http\Response
     */
    public function edit(ChatAR $chatAR)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChatAR  $chatAR
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatAR $chatAR)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChatAR  $chatAR
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatAR $chatAR)
    {
        //
    }
}
