<?php

namespace App\Http\Controllers;

use App\CaraBayar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CaraBayarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $bayar = CaraBayar::all();
        return view('modules.tatacara.index', compact('bayar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('modules.tatacara.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'jenis' => 'required',
            'isi' => 'required'
        ]);
        DB::beginTransaction();

        $crbayar = new CaraBayar();
        $crbayar->jenis = $request->input('jenis');
        $crbayar->isi = $request->input('isi');
        $crbayar->save();
        try {

        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
        DB::commit();

        return redirect('tatacara')->with('Data has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CaraBayar  $caraBayar
     * @return \Illuminate\Http\Response
     */
    public function show(CaraBayar $caraBayar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CaraBayar  $caraBayar
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        //
        $bayar = CaraBayar::find($id);
        return view('modules.tatacara.edit',compact('bayar','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CaraBayar  $caraBayar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        //
        $bayar= \App\CaraBayar::find($id);
        $bayar->jenis = $request->get('jenis');
        $bayar->isi = $request->get('isi');
        $bayar->save();
        return redirect('tatacara')->with('success','Data telah berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CaraBayar  $caraBayar
     * @return \Illuminate\Http\Response
     */
    public function destroy(CaraBayar $id)
    {
        //
        $bayar = CaraBayar::find($id);
        $bayar->delete();

        return redirect('tatacara')->with('success','Data telah dihapus');
    }
}
