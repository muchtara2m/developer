<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mitra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MitraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $mitras=Mitra::all();
        // dd($mitra);
        return view('modules.mitra.index',compact('mitras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('modules.mitra.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $customMessages = [
            'required' => ':Attribute field is required.',
            'unique' => ':Attribute has already be taken',
        ];
        $this->validate($request, [
            'perusahaan' => 'required',
            'alamat' => 'required',
            'direktur' => 'required',
            'pic' => 'required',
            // 'telp' => 'required',
            // 'email' => 'required',
            'telp' => 'required|unique:mitras,telp',
            'email' => 'required|unique:mitras,email',
        ], $customMessages);
            DB::beginTransaction();
        $mitra = new Mitra();
        $mitra->perusahaan = $request->get('perusahaan');
        $mitra->alamat = $request->get('alamat');
        $mitra->direktur = $request->get('direktur');
        $mitra->pic = $request->get('pic');
        $mitra->telp = $request->get('telp');
        $mitra->email = $request->get('email');
        // dd($mitra);
        $mitra->save();
        try {

        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
        DB::commit();
        return redirect('mitra')->with('success', 'Information has been added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $mitra = Mitra::find($id);
        return view('modules.mitra.edit',compact('mitra','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $mitra= Mitra::find($id);
        $mitra->perusahaan = $request->get('perusahaan');
        $mitra->alamat = $request->get('alamat');
        $mitra->direktur = $request->get('direktur');
        $mitra->pic = $request->get('pic');
        $mitra->telp = $request->get('telp');
        $mitra->email = $request->get('email');
        $mitra->save();
        return redirect('mitra');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $mitra = Mitra::find($id);
        $mitra->delete();
        return redirect('mitra')->with('success','Information has been  deleted');
    }
}
