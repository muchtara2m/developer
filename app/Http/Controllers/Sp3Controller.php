<?php

namespace App\Http\Controllers;

use App\Sp3;
use Illuminate\Http\Request;
use App\Bakn;
use Illuminate\Support\Facades\Auth;
use App\Spk;
use DB;
use App\ChatSp3;
use App\User;
use Validator;

class Sp3Controller extends Controller
{
    public function index()
    {
        if(Auth::user()->level == 'administrator'){
            $sp3 = Sp3::where([
                ['approval','CLOSED'],
                ['file',NULL]
                ])->get();
            $spk = Spk::where([
                ['status','save_spk'],
                ['nospk','like','%ECOM%'],
                ['file',NULL]
                ])->get();
            return view('modules.sp3k.index',compact('sp3','spk'));
        }else{
            $sp3 = Sp3::where('approval','CLOSED')->where('created_by', Auth::user()->id)->where('file',NULL)->get();
            $spk = Spk::where('status','save_spk')->where('created_by', Auth::user()->id)->where('file',NULL)->get();
            return view('modules.sp3k.index',compact('sp3','spk'));
        }
        
    }

    public function draft()
    {
        if(Auth::user()->level == 'administrator'){
            $sp3 = Sp3::where('status','draft_sp3')->get();
            $spk = Spk::where('status','draft_spk')->where('nospk','LIKE','%ECOM%')->get();
            return view('modules.sp3k.draft',compact('sp3','spk'));
        }else{
            $sp3 = Sp3::where('status','draft_sp3')->where('created_by', Auth::user()->id)->get();
            $spk = Spk::where('status','draft_spk')->where('nospk','LIKE','%ECOM%')->where('created_by', Auth::user()->id)->get();
            return view('modules.sp3k.draft',compact('sp3','spk'));
        }
        
    }

    public function done()
    {
        if(Auth::user()->level == 'administrator'){
            $sp3 = Sp3::where([
                    ['status','done_sp3'],
                    ['file','!=',NULL]
                ])->get();
            $spk = Spk::where([
                    ['status','done_spk'],
                    ['file','!=',NULL]
                ])->get();
            return view('modules.sp3k.done',compact('sp3','spk'));
        }else{
            $sp3 = Sp3::where([
                ['status','done_sp3'],
                ['file','!=',NULL],
                ['created_by',Auth::user()->id]
            ])->get();
        $spk = Spk::where([
                ['status','done_spk'],
                ['file','!=',NULL],
                ['created_by',Auth::user()->id]
            ])->get();
            return view('modules.sp3k.done',compact('sp3','spk'));
        };
    }

    public function status(){
        if(Auth::user()->level == 'administrator'){
            $sp3 = Sp3::where('approval','!=','CLOSED')->doesntHave('kontraksnon')->get();
            return view('modules.sp3k.status',compact('sp3'));
        }else if(Auth::user()->level == 'adminlkpp'){
            $sp3 = Sp3::where('approval','Return')->where('created_by',Auth::user()->id)->doesntHave('kontraksnon')->get();
            return view('modules.sp3k.status',compact('sp3'));
        }else{
            $sp3 = Sp3::where('approval',Auth::user()->username)->doesntHave('kontraksnon')->get();
            return view('modules.sp3k.status',compact('sp3'));
        }
     
    }

    public function inprogress(){
        if(Auth::user()->level == 'adminlkpp'){
            $sp3 = Sp3::with(['chatsp3' => function($q){
                $q->select('chat_sp3s.*','users.name')
                ->join('users','users.username','chat_sp3s.username')
                ->orderBy('created_at','asc');
            }])
            ->where('created_by',Auth::user()->id)
            ->get();
            return view('modules.sp3k.inprogress',compact('sp3'));
        }else{
            $sp3 = Sp3::with(['chatsp3' => function($q){
                $q->select('chat_sp3s.*','users.name')
                ->join('users','users.username','chat_sp3s.username')
                ->orderBy('created_at','asc');
            }])
            ->get();
            return view('modules.sp3k.inprogress',compact('sp3'));
        }
       
    }

    function databakn(Request $request){
        if($request->ajax() && $request->id != null){
            $spphs = Bakn::with('spph')->where('id', $request->id)->doesntHave('kontraks')->first();
            return response()->json(['options'=>$spphs]);
        }
    }

    public function preview($id)
    {
        $sp3 = Sp3::with('bakns','bakns.spph','bakns.spph.mitras')->where('id', $id)->first();
        $handlers = User::where('id_unit', 59)->get();
        return view('modules.sp3k.preview_sp3k',compact('id','sp3','handlers'));
    }

    public function create()
    {
        $bakn = Bakn::where([
            ['tipe','lkpp'],
            ['handler',NULL],
            ['file','!=',NULL]
        ])
        ->doesntHave('sp3s')
        ->doesntHave('spks')
        ->get();
        return view('modules.sp3k.create',compact('bakn'));
    }

    public function edit($id)
    {
        $data = Sp3::where('id',$id)->first();
        $chat = DB::table('chat_sp3s')
        ->where('idsp3', $id)
        ->where('status','Return')
        ->orderBy('created_at', 'desc')
        ->first();
        return view('modules.sp3k.edit', compact('data','chat'));
    }

    public function destroy($id)
    {
        $sp3 =  DB::delete('delete from sp3s where id = ?',[$id]);
        return back()->with('success','Data telah dihapus');
    }

    public function approve($id)
    {
        $sp3 = Sp3::with('bakns','bakns.spph','bakns.spph.mitras')->where('id', $id)->first();
        $checkbox = explode(",", $sp3->bakns['tipe_rapat']);
        $chats = DB::table('chat_sp3s')
        ->join('users','chat_sp3s.username','=','users.username')
        ->select('chat','jabatan','chat_sp3s.username','users.name','chat_sp3s.created_at')
        ->where('idsp3',$id)
        ->orderBy('chat_sp3s.created_at','asc')
        ->get();
        // dd($sp3, $chats);
        return view('modules.sp3k.approval',compact('id','sp3','chats','checkbox'));
    }


    public function store(Request $request)
    {
        $message = array(
            'required' => ':Attribute field is required'
        );
        $this->validate($request, [
            'baknid' => 'required',
            'nosp3k' => 'required',
            'tglsp3k' => 'required',
        ],$message);
        DB::beginTransaction(); 
        try {
            $sp3 = new Sp3();
            $sp3->bakn_id = $request->input('baknid');
            $sp3->nosp3 = $request->input('nosp3k');
            $sp3->tglsp3 = $request->input('tglsp3k');
            $sp3->created_by = Auth::user()->id;
            $sp3->status = $request->input('status');
            $bakns = Bakn::find($sp3->bakn_id);
            $bakns['lain_lain'] = $request->input('lain_lain');
            $bakns['ruang_lingkup'] = $request->input('ruang_lingkup');
            $bakns['jangka_waktu'] = $request->input('jangka_waktu');
            $bakns['cara_bayar'] = $request->input('cara_bayar');
            $bakns['jangka_waktu'] = $request->input('jangka_waktu');
            $bakns['harga_terbilang'] = $request->input('harga_terbilang');
            $bakns->save();
            $sp3->save();
            if($sp3->status == 'save_sp3'){
                $this->validate($request, [
                    'chat' => 'required',
                ],[
                    'required' => ':Attribute is required'
                ]);
                // insert chat admin
                $komen = new ChatSp3();
                $komen->chat = $request->get('chat');
                $komen->queue = 0;
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idsp3 = $sp3->id;
                $komen->transaksi = 'SP3';
                $komen->status = 'Approve';
                $komen->save();
                $id = $sp3->id;
                $sph = $sp3->nosp3;
                $hrg = $sp3->bakns['harga'];
                $nosp3 = $sp3->nosp3;
                $result=$this->approval($sph,$hrg,$nosp3,$id);
                DB::commit();
                return redirect('sp3-preview/'.$sp3->id)->with('Success','Data telah ditambah');                
            }else if($sp3->status == 'draft_sp3'){
                DB::commit();        
                return redirect('sp3-preview/'.$sp3->id)->with('Success','Data telah ditambah');                
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
    }



    public function update(Request $request, $id)
    {
        $message = array(
            'required' => ':Attribute field is required'
        );
        $this->validate($request, [
            'nosp3k' => 'required',
            'tglsp3k' => 'required',
        ],$message);
        DB::beginTransaction(); 
        try {          
            $sp3 = Sp3::find($id);
            $sp3->nosp3 = $request->input('nosp3k');
            $sp3->tglsp3 = $request->input('tglsp3k');
            $sp3->status = $request->input('status');
            $bakns = Bakn::find($sp3->bakn_id);
            $bakns['lain_lain'] = $request->input('lain_lain');
            $bakns['ruang_lingkup'] = $request->input('ruang_lingkup');
            $bakns['jangka_waktu'] = $request->input('jangka_waktu');
            $bakns['cara_bayar'] = $request->input('cara_bayar');
            $bakns['jangka_waktu'] = $request->input('jangka_waktu');
            $bakns['harga_terbilang'] = $request->input('harga_terbilang');
            // dd($bakns,$sp3);
            $bakns->save();
            $sp3->save();
            if($sp3->status == 'save_sp3'){
                $this->validate($request, [
                    'chat' => 'required',
                ],[
                    'required' => ':Attribute is required'
                ]);
                // insert chat admin
                $komen = new ChatSp3();
                $komen->chat = $request->get('chat');
                $komen->queue = 0;
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idsp3 = $sp3->id;
                $komen->transaksi = 'SP3';
                $komen->status = 'Approve';
                $komen->save();
                $id = $sp3->id;
                $sph = $sp3->nosp3;
                $hrg = $sp3->bakns['harga'];
                $nosp3 = $sp3->nosp3;
                $this->approval($sph,$hrg,$nosp3,$id);
                DB::commit();
            }else if($sp3->status == 'draft_sp3'){
                DB::commit();        
                return redirect('sp3-preview/'.$sp3->id)->with('Success','Data telah ditambah');                
            }
            return redirect('sp3-preview/'.$sp3->id)->with('Success','Data telah ditambah dan Notifikasi sudah dikirim');                
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
    }


    public function approval($sph,$hrg,$sp3,$id){

            $end = DB::table('data_flows')
            ->select('jabatan','username','queue')
            ->where([
                ['min','<=',$hrg],
                ['max','>=',$hrg],
                ['transaksi', 'SP3'],
                ['unit', 'ECOM']
                ])->first();
            $awal = DB::table('data_flows')
            ->select('jabatan','username','queue')
            ->where([['min', '<=', $hrg], ['unit', 'ecom']])
            ->first();
            $insert = DB::table('sp3s')
            ->where('id',$id)
            ->update(
                [
                'queue' => $awal->queue,
                'approval' => $awal->username,
                'endapproval' =>$end->username
                ]
            );
            $this->notifikasi($id,$awal->username);
    }
    public function notifikasi($id,$name)
    {
        $sp3 = Sp3::find($id);
        $usernya = User::where('username',$name)->first();
            // $nomer = substr(str_replace("-","",$usernya->phone),6,13);
            $nomer = '81280295238';
            $data = [
                'phone' => '62'.$nomer, // Receivers phone
                'body' => "Dear ".$usernya->name ."\xA".'File SP3 dengan Nomor '.$sp3->nosp3.' Membutuhkan Approve Anda, silahkan akses pada link berikut'."\xA".'"https://kontrak.pins.co.id/approve/'.$id.'"', //MEssage
            ];
            $json = json_encode($data); // Encode data to JSON
            // URL for request POST /message
            $url = env('API_MAIL');
            // Make a POST request
            $options = stream_context_create(['http' => [
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/json',
                    'content' => $json
                ]
            ]);
            // Send a request
            $result = file_get_contents($url, false, $options);
            $var = json_decode($result, true);
            if($var['sent'] == 'true'){
                return $this;                
            }else{
                return redirect()->back()->with('error', 'Notifikasi ('.$nomer.') tidak terdaftar ');
    
            }
    }

    public function chat(Request $request)
    {
        // get queue username
        $flow =DB::table('data_flows')
        ->where('username', Auth::user()->username)
        ->first();

        $komen = new ChatSp3();
        $komen->chat = $request->get('chat'); //get value chat form
        $komen->queue = $flow->queue; //get value quueu use DB::select before
        $komen->jabatan = Auth::user()->position; //position approval
        $komen->username = Auth::user()->username; //username approval
        $komen->idsp3 = $request->get('idTransaksi'); //get vakue id transaksi in form
        $komen->transaksi = 'SP3'; //set value transaksi for SP3
        $komen->status = $request->get('status'); //get status approve or return
        $komen->save();
        // dd($flow, $komen);

        $idSp3 = $komen->idsp3; //get id form chat approval
        $sp3 = Sp3::find($idSp3); //get all data where id = idsp3
        $trx = 'SP3';
        $skrg = $sp3->queue; //get queue now
        $jabatanskrg = $sp3->approval; //get now approval
        $jabatanend = $sp3->endapproval; //get last approval

        if ($komen->status == 'Approve') {
            // bandingin antara status_approval dengan approval akhir
            if ($jabatanskrg != $jabatanend) {
                $skrg++; //add +1 after approval klik submit
                $cek = DB::table('data_flows')
                ->select('jabatan','username','queue')
                ->where([
                    ['queue', $skrg],
                    ['transaksi', $trx],
                    ['unit', 'ecom']
                    ])
                    ->first(); //get username approval
                $updateapproval = DB::table('sp3s')
                ->where('id', $idSp3)
                ->update([
                    'queue' => $skrg,
                    'approval' => $cek->username
                    ]);
                    $this->notifikasi($idSp3,$cek->username);
                // jika approval == end approval change queue to CLOSED
                }else {
                    // status CLOSED dengan nilai 99
                    $skrg++;
                    $updateapproval = DB::table('sp3s')
                    ->where('id', $idSp3)
                    ->update([
                        'queue' => 99,
                        'approval' => 'CLOSED',
                        'handler' => 'tenny.falentin',
                        ]);
                        $name = 'tenny.falentin';
                    $this->notifikasi($idSp3,$name);
                }
                return redirect('inprogress-sp3k')->with('success','File Has Been Approve & Notificatin has been delivered');
            }else if($komen->status == 'Return'){
                // jika komen return
                $sp3 = Sp3::find($idSp3);
                $updatestatus = DB::table('sp3s')
                ->where('id', $idSp3)
                ->update(
                    ['queue' => NULL, 'approval' =>'Return']
                );
                $name = User::where('id',$sp3->created_by)->first();
                $this->notifikasi($idSp3,$name->username);
                return redirect('inprogress-sp3k')->with('success','File Has Been Return & Notificatin has been delivered');
                // return redirect('inprogress-sp3k');
            }
            return redirect('inprogress-sp3k')->with('success','File Has Been Updated');

        }

        public function upload(Request $request, $id)
        {
            $unit = DB::table('users')
            ->join('units','units.id','users.id_unit')
            ->select('units.nama')
            ->where('units.id',Auth::user()->id_unit)
            ->first();
            // var_dump($unit);
            $path= "public/files/SP3K/".$unit->nama."/".$id;
            if($request->file('file'))
            {
                foreach($request->file('file') as $file)
                {
                    $name=$file->getClientOriginalName();
                    $file->storeAs($path, $name);
                    $namanya[] = $name;
                    $data[] = $path.'/'.$name;
                }
            }
            $file = Sp3::find($id);
            $file->file=json_encode($data);
            $file->title = json_encode($namanya);
            $file->status = 'done_sp3';
            // $file->tgl_upload = date("Y-m-d H:i:s");
            $file->save();

            return redirect('done-sp3k')->withSuccess('File has been uploaded.');
        }

}
