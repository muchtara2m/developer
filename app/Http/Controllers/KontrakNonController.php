<?php

namespace App\Http\Controllers;

use App\KontrakNon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Bakn;
use DB;
use App\User;
use App\Pasal;
use App\ChatNon;
use Illuminate\Support\Facades\Auth;
use App\Sp3;
use function GuzzleHttp\json_encode;
use Unirest;

// for NON LKPP KONTRAK
class KontrakNonController extends Controller
{
    // function status transaksi kontrak
    public function status(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $bakn = KontrakNon::where('approval','!=','CLOSED')
                    ->whereMonth('created_at',$var,$bln)                            
                    ->whereYear('created_at',$thn)
                    ->orderBy('created_at','desc')
                    ->get();
            $sp3 = KontrakNon::where([
                ['approval','!=','CLOSED'],
                ['bakn_id',NULL]
                ])
                ->whereMonth('created_at',$var,$bln)                                
                ->whereYear('created_at',$thn)
                ->orderBy('created_at','desc')
                ->get();
        }else{
            $bakn = KontrakNon::where(function($q){
                $q->where('approval',Auth::user()->username)
                ->where('created_by','!=',Auth::user()->id);
              })->orWhere(function($w){
                  $w->where('approval','Return')
                  ->where('created_by',Auth::user()->id);
              })
              ->whereMonth('created_at',$var,$bln)                                          
              ->whereYear('created_at',$thn)
              ->orderBy('created_at','desc')              
              ->get();
            $sp3 = KontrakNon::where([
                ['approval','Return'],
                ['bakn_id',NULL]
                ])
                ->whereMonth('created_at',$var,$bln)                                
                ->whereYear('created_at',$thn)
                ->orderBy('created_at','desc')
                ->get();
        }
        return view('modules.kontrak_non.status_transaksi',compact('bakn','sp3'));
    }
    // list kontrak hafiz untuk dispatch
    public function index(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
            $sp3 = Sp3::where([
                ['status','done_sp3'],
                ['file','!=',NULL],
                ['handler',NULL]
            ])
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->doesntHave('kontraksnon')
            ->get();
            $bakn = Bakn::where([
                ['status','done_bakn'],
                ['file','!=',NULL],
                ['tipe','non'],
                ['handler',NULL],
                ['harga','>=',50000000],
                // ['jenis_kontrak','HKS']
            ])
            ->orWhere([
                ['jenis_kontrak','KHS'],
                ['status','done_bakn'],
                ['file','!=',NULL],
                ['tipe','non'],
                ['handler',NULL],
                ])
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->doesntHave('kontraksnon')
            ->get();
            return view('modules.kontrak_non.index',compact('bakn','sp3'));
    }
    // function list draft kontrak
    public function draft(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
            if(Auth::user()->level == 'administrator'){
                $claues = 'file';
                $where =NULL;
            }else{
                $claues ='created_by';
                $where = Auth::user()->id;
            }
            $bakn = KontrakNon::with('bakns')
            ->where([
                ['status','draft_kontrak'],
                [$claues,$where]
            ])
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->doesntHave('sp3s')
            ->orderBy('created_at','desc')
            ->get();
            $sp3 = KontrakNon::with('sp3s')
            ->where([
                ['status','draft_kontrak'],
                [$claues,$where],
                ['bakn_id',NULL]
            ])
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->orderBy('created_at','desc')
            ->get();
            return view('modules.kontrak_non.draft',compact('bakn','sp3'));
    }
    // list inprogress
    public function inprogress(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator' || Auth::user()->level != 'adminkontrak'){
            $bakn = KontrakNon::with(['chatnons'=>function ($q){
                $q->select('chat_nons.*','users.name')
                ->join('users','users.username','chat_nons.username')
                ->orderBy('created_at','asc');
            },'bakns','users','bakns.spph'])
            ->where('approval','!=','CLOSED')
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->orderBy('created_at','desc')
            ->get();
            $sp3 = KontrakNon::with(['chatnons'=>function ($q){
                $q->select('chat_nons.*','users.name')
                ->join('users','users.username','chat_nons.username')
                ->orderBy('created_at','asc');
            },'sp3s','users'])
            ->where('approval','!=','CLOSED')
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->orderBy('created_at','desc')
            ->get();
        }else{
            $bakn = KontrakNon::with(['chatnons'=>function ($q){
                $q->select('chat_nons.*','users.name')
                ->join('users','users.username','chat_nons.username')
                ->orderBy('created_at','asc');
            },'bakns','users','bakns.spph'])
            ->where('approval','!=','CLOSED')
            ->where('created_by',Auth::user()->id)
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->orderBy('created_at','desc')
            ->get();
            $sp3 = KontrakNon::with(['chatnons'=>function ($q){
                $q->select('chat_nons.*','users.name')
                ->join('users','users.username','chat_nons.username')
                ->orderBy('created_at','asc');
            },'sp3s','users'])
            ->where('approval','!=','CLOSED')
            ->where('created_by',Auth::user()->id)
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->orderBy('created_at','desc')
            ->get();
        }
        
        return view('modules.kontrak_non.inprogress',compact('bakn','sp3'));
    }
    // list function done
    public function done(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $claues = 'approval';
            $where ='CLOSED';
        }else{
            $claues ='created_by';
            $where = Auth::user()->id;
        }

        $bakn = KontrakNon::where([
            ['status','done_kontrak'],
            ['file','!=',NULL],
            [$claues,$where]
        ])
        ->whereMonth('created_at',$var,$bln)                        
        ->whereYear('created_at',$thn)
        ->doesntHave('sp3s')
        ->orderBy('created_at','desc')
        ->get();
        $sp3 = KontrakNon::where([
            ['status','done_kontrak'],
            ['file','!=',NULL],
            ['bakn_id',NULL],
            [$claues,$where]
        ])
        ->whereMonth('created_at',$var,$bln)                        
        ->whereYear('created_at',$thn)
        ->orderBy('created_at','desc')
        ->get();
        return view('modules.kontrak_non.done',compact('bakn','sp3'));
    }
    // list after dispatch for hafiz
    public function listdispatch(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        $bakn = Bakn::where([
            ['handler','!=',NULL],
            ['tgldisp','!=',NULL],
        ])
        ->whereMonth('created_at',$var,$bln)                        
        ->whereYear('created_at',$thn)
        ->doesntHave('kontraksnon')
        ->doesntHave('sp3s')
        ->orderBy('created_at','desc')
        ->get();
        $sp3 = Sp3::where([
            ['handler','!=',NULL]
        ])
        ->whereMonth('created_at',$var,$bln)                        
        ->whereYear('created_at',$thn)
        ->doesntHave('kontraksnon')
        ->orderBy('created_at','desc')
        ->get();
        return view('modules.kontrak_non.dispatch',compact('bakn','sp3'));
    }
    // function list admin
    public function list(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $sp3 = Sp3::where([
                ['status','done_sp3'],
                ['file','!=',NULL]
            ])
                ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->doesntHave('kontraksnon')
            ->get();
            $bakn = Bakn::where([
                ['status','done_bakn'],
                ['file','!=',NULL],
                ['tipe','non']
            ])
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->doesntHave('kontraksnon')
            ->get();
            return view('modules.kontrak_non.list',compact('bakn','sp3'));
        }else{
            $sp3 = Sp3::where([
                ['handler',Auth::user()->username],
                ['status','done_sp3'],
                ['file','!=',NULL]
            ])
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->doesntHave('kontraksnon')
            ->get();
            $bakn = Bakn::where([
                ['handler',Auth::user()->username],
                ['status','done_bakn'],
                ['file','!=',NULL]
            ])
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->doesntHave('kontraksnon')
            ->get();
            return view('modules.kontrak_non.list',compact('bakn','sp3'));
        }
      
    }
    // tracking document
    public function tracking(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator' || Auth::user()->level != 'adminkontrak'){
            $bakn = KontrakNon::with(['chatnons'=>function ($q){
                $q->select('chat_nons.*','users.name')
                ->join('users','users.username','chat_nons.username')
                ->orderBy('created_at','asc');
            },'bakns','users','bakns.spph'])
                ->whereMonth('created_at',$var,$bln)                            
                ->whereYear('created_at',$thn)
                ->orderBy('created_at','desc')
                ->get();
            $sp3 = KontrakNon::with(['chatnons'=>function ($q){
                $q->select('chat_nons.*','users.name')
                ->join('users','users.username','chat_nons.username')
                ->orderBy('created_at','asc');
            },'sp3s','users'])
                ->whereMonth('created_at',$var,$bln)                            
                ->whereYear('created_at',$thn)
                ->orderBy('created_at','desc') 
                ->get();
            return view('modules.kontrak_non.tracking',compact('bakn','sp3'));
        }else{
            $bakn = KontrakNon::with(['chatnons'=>function ($q){
                $q->select('chat_nons.*','users.name')
                ->join('users','users.username','chat_nons.username')
                ->orderBy('created_at','asc');
            },'bakns','users','bakns.spph'])
                ->where('created_by',Auth::user()->id)
                ->whereMonth('created_at',$var,$bln)                            
                ->whereYear('created_at',$thn)
                ->orderBy('created_at','desc')
                ->get();
            $sp3 = KontrakNon::with(['chatnons'=>function ($q){
                $q->select('chat_nons.*','users.name')
                ->join('users','users.username','chat_nons.username')
                ->orderBy('created_at','asc');
            },'sp3s','users'])
                ->where('created_by',Auth::user()->id)
                ->whereMonth('created_at',$var,$bln)                            
                ->whereYear('created_at',$thn)
                ->orderBy('created_at','desc')
                ->get();
            return view('modules.kontrak_non.tracking',compact('bakn','sp3'));
        }
    }

    public function uploadFile(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $bakn = KontrakNon::where([
                ['approval','CLOSED'],
                ['file',NULL]
                ])
                ->whereMonth('created_at',$bln)
                ->whereYear('created_at',$thn)
                ->doesntHave('sp3s')
                ->orderBy('created_at','desc')
                ->get();
            $sp3 = KontrakNon::where([
                ['approval','CLOSED'],
                ['file',NULL]
                ])
                ->where('bakn_id',NULL)
                ->whereMonth('created_at',$bln)
                ->whereYear('created_at',$thn)
                ->orderBy('created_at','desc')
                ->get();
        }else{
            $bakn = KontrakNon::where([
                ['approval','CLOSED'],
                ['file',NULL]
                ])
                ->where('created_by',Auth::user()->id)
                ->whereMonth('created_at',$bln)
                ->whereYear('created_at',$thn)
                ->doesntHave('sp3s')
                ->orderBy('created_at','desc')
                ->get();
            $sp3 = KontrakNon::where([
                ['approval','CLOSED'],
                ['file',NULL]
                ])
                ->where('created_by',Auth::user()->id)
                ->whereMonth('created_at',$bln)
                ->whereYear('created_at',$thn)
                ->where('bakn_id',NULL)
                ->orderBy('created_at','desc')
                ->get();
        }
        return view ('modules.kontrak_non.upload',compact('bakn','sp3'));
    }

     // function create kontrak
     public function create($id){
        $sp3 = Sp3::with('bakns','bakns.spph','bakns.spph.mitras')->where('id',$id)->first();
        $bakns = Bakn::with('io','spph','spph.mitras')->doesntHave('sp3s')->where('id',$id)->first();
        $pasals = Pasal::all();
        $jenispasal = DB::table('jenis_pasals')->join('bakns','bakns.jenis_kontrak','jenis_pasals.jenis_pasal')->first();
        if($sp3 != NULL){
            $end = DB::table('data_flows')
            ->select('jabatan','data_flows.username','queue','users.name','users.position')
            ->join('users','users.username','=','data_flows.username')
            ->where([
            ['min','<=',$sp3->bakns['harga']],
            ['max','>=',$sp3->bakns['harga']],
            ['transaksi', 'Kontrak'],
            ['unit', 'NON']
            ])->first();
        }else{
            $end = DB::table('data_flows')
            ->select('jabatan','data_flows.username','queue','users.name','users.position')
            ->join('users','users.username','=','data_flows.username')
            ->where([
            ['min','<=',$bakns['harga']],
            ['max','>=',$bakns['harga']],
            ['transaksi', 'Kontrak'],
            ['unit', 'NON']
            ])->first();
        }
        return view('modules.kontrak_non.create',compact('sp3','pasals','bakns','jenispasal','end'));
    } 

    // function to show
    public function preview_status($id)
    {
        $kontrak = KontrakNon::find($id);
        $chats = DB::table('chat_nons')
        ->join('users','chat_nons.username','=','users.username')
        ->select('chat','jabatan','chat_nons.username','users.name','chat_nons.created_at')
        ->where('idKontrakNon',$id)
        ->orderBy('chat_nons.created_at','asc')
        ->get();
        $user = User::all();
        // dd($kontrak);
        $checkbox = explode(",", $kontrak->bakns['tipe_rapat']);
  
        return view('modules.kontrak_non.preview_status',compact('id','kontrak','user','chats','checkbox'));
    } 
     // function print preview kontrak
    public function print_preview($id)
    {
        $kontrak = KontrakNon::with('chatnons','users')->find($id);
        $get = DB::table('chat_nons')
        ->where('status', 'Return')
        ->where('idKontrakNon', $id)
        ->orderBy('created_at', 'desc')
        ->first();
        if($get ==NULL){
            $chat = DB::table('chat_nons')
            ->join('users', 'users.username','chat_nons.username')
            ->select('chat_nons.*', 'users.name')
            ->where('idKontrakNon',$id)
            ->where('transaksi', 'kontrak')
            ->where('status', 'Approve')
            ->orderBy('chat_nons.created_at','asc')
            ->get();
        }else{
            $chat = DB::table('chat_nons')
            ->join('users', 'users.username','chat_nons.username')
            ->select('chat_nons.*', 'users.name')
            ->where('idKontrakNon',$id)
            ->where('transaksi', 'kontrak')
            ->where('status', 'Approve')
            ->where('chat_nons.created_at','>',$get->created_at)
            ->orderBy('chat_nons.created_at','asc')
            ->get();
        }
        $semua = DB::table('chat_nons')
        ->join('users','users.username','=','chat_nons.username')
        ->where('idKontrakNon',$id)
        ->select('chat_nons.*', 'users.name')
        ->orderBy('chat_nons.created_at','asc')
        ->get();
        return view('modules.kontrak_non.print', compact('kontrak','chat','semua'));
    }

    // function edit
    public function edit($id)
    {
        $kontraks = KontrakNon::find($id);
        $pasals = Pasal::all();
        $checkbox = explode(",", $kontraks->pasal);
        $chat = ChatNon::where([
            ['idKontrakNon',$id],
            ['status','Return']
        ])
        ->select('chat_nons.*','users.name')
        ->join('users','users.username','chat_nons.username')
        ->orderBy('created_at','desc')
        ->first();
        // dd($chat);
        return view('modules.kontrak_non.edit', compact('chat','kontraks', 'pasals','checkbox'));
    }
   
    // insert kontrak
    public function store(Request $request)
    {
        $message = array(
            'required' => ':Attribute field is required'
        );
        $this->validate($request, [
            'nokontrak' => 'required',
            'tglkontrak' => 'required',
        ],$message);
        DB::beginTransaction(); 
        try {
            $kontrak = new KontrakNon();
            $kontrak->bakn_id = $request->input('bakn');
            $kontrak->nokontraknon = $request->input('nokontrak');
            $kontrak->tglkontrak = $request->input('tglkontrak');
            $kontrak->created_by = Auth::user()->id;
            $kontrak->handler = Auth::user()->username;
            $kontrak->status = $request->input('status');
            $kontrak->isi = $request->input('isi');
            $kontrak->save();
            $id = $kontrak->id;

            $path= "public/files/KontrakNon/".$id."/lampiran";
            if($request->file('lampiran'))
            {
                foreach($request->file('lampiran') as $file)
                {
                    $name=$file->getClientOriginalName();
                    $file->storeAs($path, $name);
                    $namanya[] = $name;
                    $data[] = $path.'/'.$name;
                }
            }
            $file = KontrakNon::find($id);
            $file->lampiran=json_encode($data);
            $file->title_lampiran  = json_encode($namanya);
            $file->save();
            // dd($kontrak);
            if($kontrak->status == 'save_kontrak'){
                $id = $kontrak->id;
                $this->validate($request, [
                    'chat' => 'required',
                    'lampiran' => 'required',
                ],$message);

                $path= "public/files/KontrakNon/".$id."/lampiran";
                if($request->file('lampiran'))
                {
                    foreach($request->file('lampiran') as $file)
                    {
                        $name=$file->getClientOriginalName();
                        $file->storeAs($path, $name);
                        $namanya[] = $name;
                        $data[] = $path.'/'.$name;
                    }
                }
                $file = KontrakNon::find($id);
                $file->lampiran=json_encode($data);
                $file->title_lampiran  = json_encode($namanya);
                $file->save();
                // insert chat admin
                $komen = new ChatNon();
                $komen->chat = $request->get('chat');
                $komen->queue = 0;
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idKontrakNon = $id;
                $komen->transaksi = 'Kontrak';
                $komen->status = 'Approve';
                $id = $kontrak->id;
                $sph = $kontrak->nokontraknon;
                $hrg = $kontrak->bakns['harga'];
                $komen->save();
                // dd($kontrak,$file,$komen,$hrg);
                $this->approval($sph,$hrg,$id);
                DB::commit();
                return redirect()->route('kontrak-non-status')->with('success', 'Status ('.$kontrak->approval.') was added successfully');                           
            }else if($kontrak->status == 'draft_kontrak'){             
                DB::commit();        
            return redirect('kontrak-non-draft')->with('success', 'Data has been added');            
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
    }

        public function comment($chat, $id){
            // insert chat admin
            $komen = new ChatNon();
            $komen->chat = $chat;
            $komen->queue = 0;
            $komen->jabatan = Auth::user()->position;
            $komen->username = Auth::user()->username;
            $komen->idKontrakNon = $id;
            $komen->transaksi = 'Kontrak';
            $komen->status = 'Approve';
        }
     // function print send data
     public function print(Request $request, $id)
     {
         $kontrak = KontrakNon::find($id);
         $kontrak->tgl_print = date('Y-m-d H:i:s');
         // dd($kontrak);
         $kontrak->save();
         return back()->with('success', 'Your files has been successfully added');
     }
    // function upload file
    public function upload(Request $request, $id)
    {
        $this->validate($request, [
            'file' => 'required',
            ]);
            $path= "public/files/KontrakNon/".$id."/approval";
            if($request->file('file'))
            {
                foreach($request->file('file') as $file)
                {
                    $name=$file->getClientOriginalName();
                    $file->storeAs($path, $name);
                    $namanya[] = $name;
                    $data[] = $path.'/'.$name;
                }
            }
            $file = KontrakNon::find($id);
            $file->upload = date("Y-m-d H:i:s");
            $file->status = "done_kontrak";
            $file->file=json_encode($data);
            $file->title = json_encode($namanya);
            $file->save();
            return back()->with('success', 'Your files has been successfully added');
        }
        // export to word
        public function word($id){
            $kontrak = KontrakNon::find($id);
            $ygpentingbisa = $kontrak->isi;
            header("Content-Type: application/vnd.msword");
            header("Expires: 0");//no-cache
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");//no-cache
            header("content-disposition: attachment;filename=$kontrak->nokontraknon.doc");
            echo "<html>";
            echo "$ygpentingbisa";
            echo "</html>";
        }
   
    
    // function update kontrak
    public function update(Request $request, $id)
    {
        $message = array(
            'required' => ':Attribute field is required'
        );
        $kontrak = KontrakNon::find($id);
        $kontrak->nokontraknon = $request->input('nokontrak');
        $kontrak->isi = $request->input('isi');
        $kontrak->tglkontrak = $request->input('tglkontrak');
        $kontrak->status = $request->input('status');
        $kontrak->save();
        //update status tbl_bakns menjadi dispatch
        DB::beginTransaction();
        try{
            if($kontrak->status == 'save_kontrak' && $kontrak->lampiran == NULL){
                $this->validate($request, [
                    'chat' => 'required',
                    'lampiran' => 'required',
                ],$message);
                $path= "public/files/KontrakNon/".$id."/lampiran";
                if($request->file('lampiran'))
                {
                    foreach($request->file('lampiran') as $file)
                    {
                        $name=$file->getClientOriginalName();
                        $file->storeAs($path, $name);
                        $namanya[] = $name;
                        $data[] = $path.'/'.$name;
                    }
                }
                $file = KontrakNon::find($id);
                $file->lampiran=json_encode($data);
                $file->title_lampiran  = json_encode($namanya);
                $file->save();
                // insert chat admin
                $komen = new ChatNon();
                $komen->chat = $request->get('chat');
                $komen->queue = 0;
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idKontrakNon = $id;
                $komen->transaksi = 'Kontrak';
                $komen->status = 'Approve';
                $id = $kontrak->id;
                $sph = $kontrak->nokontrak;
                $hrg = $kontrak->bakns['harga'];
                $komen->save();

                $this->approval($sph,$hrg,$id);
                DB::commit();
                return redirect()->route('kontrak-non-inprogress')->with('success', 'Status ('.$kontrak->approval.') was added successfully'); 
            }else if($kontrak->status == 'save_kontrak' && $kontrak->lampiran != NULL){
                $this->validate($request, [
                    'chat' => 'required',
                ],$message);
                 // insert chat admin
                $komen = new ChatNon();
                $komen->chat = $request->get('chat');
                $komen->queue = 0;
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idKontrakNon = $id;
                $komen->transaksi = 'Kontrak';
                $komen->status = 'Approve';
                $id = $kontrak->id;
                $sph = $kontrak->nokontrak;
                $hrg = $kontrak->bakns['harga'];
                $komen->save();
                $id = $kontrak->id;
                $sph = $kontrak->nokontrak;
                $hrg = $kontrak->bakns['harga'];
                $this->approval($sph,$hrg,$id);
                DB::commit();
                return redirect()->route('kontrak-non-inprogress')->with('success', 'Status ('.$kontrak->approval.') was added successfully'); 

            }else if($kontrak->status == 'draft_kontrak' && $kontrak->lampiran == NULL  ){
                $path= "public/files/KontrakNon/".$id."/lampiran";
                if($request->file('lampiran'))
                {
                    foreach($request->file('lampiran') as $file)
                    {
                        $name=$file->getClientOriginalName();
                        $file->storeAs($path, $name);
                        $namanya[] = $name;
                        $data[] = $path.'/'.$name;
                    }
                }
                $file = KontrakNon::find($id);
                $file->lampiran=json_encode($data);
                $file->title_lampiran  = json_encode($namanya);
                $file->save();
                DB::commit();        
            return redirect('kontrak-non-draft')->with('success', 'Data has been added');            
            }else{
                DB::commit();        
                return redirect('kontrak-non-draft')->with('success', 'Data has been added');   
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }  

    }
    public function return_approve(Request $request, $id)
    {
        $kontrak = KontrakNon::find($id);
        $kontrak->nokontraknon = $request->input('nokontrak');
        $kontrak->isi = $request->input('isi');
        $kontrak->save();
        //update status tbl_bakns menjadi dispatch
        if($kontrak->sp3_id == NULL){
            $spph = $kontrak->bakns->spph['nomorspph'];
            $hrg = $kontrak->bakns['harga'];
            $id = $kontrak->id;
            $kontrak->save();
        }else{
            $spph = $kontrak->sp3s->bakns->spph['nomorspph'];
            $hrg = $kontrak->sp3s->bakns['harga'];
            $id = $kontrak->id;
            $kontrak->save();
        }
        if ($kontrak->status == 'draft_kontrak') {
            return redirect('kontrak-non-draft')->with('success', 'Data has been added');
        }else{
            $this->validate($request, [
                'chat' => 'required',
                ]);
                DB::beginTransaction();
                // insert chat admin
                $komen = new ChatNon();
                $komen->chat = $request->get('chat');
                $komen->queue = 0;
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idKontrakNon = $kontrak->id;
                $komen->transaksi = 'Kontrak';
                $komen->status = 'Approve';
                $komen->save();
                try {
                } catch (ValidationException $e) {
                    DB::rollback();
                    return redirect()->back()
                    ->withErrors("Something wrong with your form, please check carefully")
                    ->withInput();
                } catch (\Exception $e) {
                    DB::rollback();
                    return redirect()->back()
                    ->withErrors("Something wrong from the server, please check carefully")
                    ->withInput();
                }
                DB::commit();
                // declare harga kontrak
                $result=$this->approval($spph,$hrg,$id);
                return redirect()->route('kontrak-non-status')->with('success', 'Status ('.$kontrak->approval.') was added successfully');
            }
    }
    // function approval
    public function approval( $spph,$hrg,$id)
    {
        $end = DB::table('data_flows')
        ->select('jabatan','username','queue')
        ->where([
            ['min','<=',$hrg],
            ['max','>=',$hrg],
            ['transaksi', 'Kontrak'],
            ['unit', 'NON']
            ])->first();
            $awal = DB::table('data_flows')
            ->select('jabatan','username','queue')
            ->where([
                ['min', '<=', $hrg], 
                ['unit', 'non'],
                ['transaksi','kontrak']
                ])
            ->first();
            $insert = DB::table('kontrak_nons')
            ->where('id',$id)
            ->update(
                [
                    'queue' => $awal->queue,
                    'approval' => $awal->username,
                    'endapproval' =>$end->username
                ]);
        $this->notifikasi($id,$awal->username);
               
        return redirect('kontrak-non-inprogress')->with('Success','Data telah ditambah & Notifikasi telah dikirim ');
    }
    //notifikasi
    public function notifikasi($id,$name)
    {
        
        $kontrak = KontrakNon::find($id);
        $lastnya = ChatNon::where('idKontrakNon',$id)
        ->select('chat_nons.*','users.name')
        ->join('users','users.username','chat_nons.username')
        ->orderBy('created_at','desc')->first();
        $usernya = User::where('username',$name)->first();
        // function shorten url
        if ($kontrak->approval == 'CLOSED') {
            $hp = substr(str_replace("-","",$usernya->phone),6,13);
            $urls = url('kontrak-non-upload');
        }else if($kontrak->approval == 'Return'){
            $hp = substr(str_replace("-","",$usernya->phone),6,13);
            $urls = url('kontrak-non-status');
        }else{
            $hp = substr(str_replace("-","",$usernya->phone),6,13);
            $urls = url('kontrak-non-preview-status/'.$id);
        }

        // $login = "o_6o071p0jnl";
        // $api_key = "R_891d180189384c59a5d498d27a047e57";
        // $url = $urls;
        // $ch = curl_init('http://api.bitly.com/v3/shorten?login='.$login.'&apiKey='.$api_key.'&longUrl='.$url);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // $result = curl_exec($ch);
        // $res = json_decode($result, true);

        if($kontrak->approval == 'CLOSED'){
            $body = 'Bapak/Ibu '.$usernya->name
            ."\xA".'File Kontrak dengan Nomor '.$kontrak->nokontraknon
            ."\xA".'Sudah Selesai di Approve, silahkan askes pada link berikut['
            ."\xA".$urls;
        }else if($kontrak->approval == 'Return'){
            $body = 'Bapak/Ibu '.$usernya->name
            ."\xA".'File Kontrak dengan Nomor '.$kontrak->nokontraknon
            ."\xA".'Di Return oleh '. strtoupper($lastnya->name)
            ."\xA".'Dengan Catatan '
            ."\xA".'" '.$lastnya->chat.' "'
            ."\xA".'Silahkan akses pada link berikut'
            ."\xA".$urls;
        }else{
            $body = 'Bapak/Ibu '.$usernya->name
            ."\xA".'File Kontrak dengan Nomor '.$kontrak->nokontraknon
            ."\xA".'Membutuhkan Approve Anda, silahkan akses pada link berikut '
            ."\xA".$urls;
        }

            // $nomer = substr(str_replace("-","",$usernya->phone),6,13);
            $nomer = '81280295238';
            $data = [
                'phone' => '62'.$hp, // Receivers phone
                'body' => $body,
            ];
            $json = json_encode($data); // Encode data to JSON
            // URL for request POST /message
            $url = 'https://eu43.chat-api.com/instance59109/sendMessage?token=bn0w07mp9572ei4t';
            // Make a POST request
            $options = stream_context_create(['http' => [
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/json',
                    'content' => $json
                ]
            ]);
            // Send a request
            $result = file_get_contents($url, false, $options);
            $var = json_decode($result, true);
            if($var['sent'] == 'true'){
                return $this;                
            }else{
                return redirect()->back()->with('error', 'Notifikasi ('.$nomer.') tidak terdaftar ');
    
            }
    }
    // function chat
    public function chat(Request $request)
    {
        $flow =DB::table('data_flows')
        ->where('username', Auth::user()->username)
        ->first();
        DB::beginTransaction();
        try{
            $komen = new ChatNon();
            $komen->chat = $request->get('chat');
            $komen->queue = $flow->queue;
            $komen->jabatan = Auth::user()->position;
            $komen->username = Auth::user()->username;
            $komen->idKontrakNon = $request->get('idTransaksi');
            $komen->transaksi = 'Kontrak';
            $komen->status = $request->get('status');
            $komen->save();
            if ($komen->status == 'Approve') {
                $idKontrak = $request->get('idKontrak');
                $kontrak = KontrakNon::find($idKontrak);
                $unit = 'NON';
                $trx = 'Kontrak';
                $skrg = $kontrak->queue;
                $jabatanskrg = $kontrak->approval;
                $jabatanend = $kontrak->endapproval;
                // bandingin antara approval dengan approval akhir
                if ($jabatanskrg != $jabatanend) {
                    $skrg++;
                    $cek = DB::table('data_flows')
                    ->select('jabatan','username','queue')
                    ->where([
                        ['queue', $skrg],
                        ['transaksi', $trx],
                        ['unit', $unit]
                        ])
                        ->first();
                        $updateapproval = DB::table('kontrak_nons')
                        ->where('id', $idKontrak)
                        ->update(
                            ['queue' => $skrg, 'approval' => $cek->username]
                        );
                        $this->notifikasi($idKontrak,$cek->username);
                        DB::commit();
                    }else {
                        // status CLOSED dengan nilai 9
                        $skrg++;
                        $updateapproval = DB::table('kontrak_nons')->where('id', $idKontrak)->update(
                            ['queue' => 9, 'approval' => 'CLOSED']
                        );
                        DB::commit();
                    }
                    return redirect('kontrak-non-inprogress')->with('success','Draft Berhasil di Approve');
                }else {
                    // jika komen return
                    $idKontrak = $request->get('idKontrak');
                    $kontrak = KontrakNon::find($idKontrak);
                    $updatestatus = DB::table('kontrak_nons')
                    ->where('id', $idKontrak)
                    ->update(
                        ['queue' => NULL, 'approval' =>'Return']
                    );
                    $name = User::where('id',$kontrak->created_by)->first();
                    $this->notifikasi($idKontrak,$name->username);
                    DB::commit();
                    return redirect('kontrak-non-inprogress')->with('success','Draft Berhasil di Return');
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
    }
  
    
    //CHANGE MODEL OTHER
    // function dispatch from data bakn
    public function dispbakn(Request $request, $id)
    {
        $bakn = Bakn::find($id);
        $bakn->handler = $request->get('handler');
        $bakn->tgldisp = date('Y-m-d H:i:s');
        $bakn->save();
        return redirect('kontrak-non');
    }
    // function dispatch from data bakn
    public function dispsp3(Request $request, $id)
    {
    $sp3 = Sp3::find($id);
    $sp3->handler = $request->get('handler');
    $sp3->tgldisp = date('Y-m-d H:i:s');
    $sp3->save();
    return redirect('kontrak-non');
    }
    public function destroy($id)
    {
        
        $kontrak = KontrakNon::findOrFail($id);
        $kontrak->delete();
        $approval= "public/files/KontrakNon/".$id."/approval";
        $lampiran= "public/files/KontrakNon/".$id."/lampiran";
        Storage::deleteDirectory($lampiran);
        Storage::deleteDirectory($approval);
        return back()->with('success','Anda telah berhasil menghapus data');
    }

}
