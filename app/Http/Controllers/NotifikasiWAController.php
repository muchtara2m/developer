<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotifikasiWAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = [
            'phone' => '6285882868685', // Receivers phone
            'body' => 'Nih yang, ini dari Aplikasi yang aku bikin di kantor dah berhasil buat notifikasi ke WA ', // Message
        ];
        $json = json_encode($data); // Encode data to JSON
        // URL for request POST /message
        // $url = 'https://foo.chat-api.com/message?token=83763g87x';
        $url = 'https://eu62.chat-api.com/instance58018/sendMessage?token=ntuugz8h98jiqeka';
        // Make a POST request
        $options = stream_context_create(['http' => [
                'method'  => 'POST',
                'header'  => 'Content-type: application/json',
                'content' => $json
            ]
        ]);
        // Send a request
        $result = file_get_contents($url, false, $options);
        dd($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'telp' => 'required|max:13|min:11',
            'sms' => 'required'
        ],[
            'max' => 'Nomor terlalu banyak',
            'min' => 'Nomor terlalu dikit'
        ]);
        $data = [
            'phone' => '62'.$request->input('telp'), // Receivers phone
            'body' => $request->sms, // Message
        ];
        $json = json_encode($data); // Encode data to JSON
        // URL for request POST /message
        $url = 'https://eu43.chat-api.com/instance59109/sendMessage?token=bn0w07mp9572ei4t';
        // Make a POST request
        $options = stream_context_create(['http' => [
                'method'  => 'POST',
                'header'  => 'Content-type: application/json',
                'content' => $json
            ]
        ]);
        // Send a request
        $result = file_get_contents($url, false, $options);
        $var = json_decode($result, true);
        if($var['sent'] == 'true'){
            return redirect()->back()->with('success', 'Notifikasi ('.$request->input('telp').') telah dikirim ');
        }else{
            return redirect()->back()->with('error', 'Notifikasi ('.$request->input('telp').') tidak terdaftar ');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function shorten($id){
        // function shorten url
        $login = "o_6o071p0jnl";
        $api_key = "R_891d180189384c59a5d498d27a047e57";
        $url = "http://kontrak.pins.co.id/kontrak-non-preview-status/".$id;
        $ch = curl_init('http://api.bitly.com/v3/shorten?login='.$login.'&apiKey='.$api_key.'&longUrl='.$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $res = json_decode($result, true);
        // dd($res['data']['url']);

        // function to mail
        $data = [
            'phone' => '6281314845016', // Receivers phone
            // 'body' => strtoupper($awal->username).' Terdapat File yg perlu anda approve, silahkan klik link berikut '.$res['data']['url'], // Message
        ];
        $json = json_encode($data); // Encode data to JSON
        // URL for request POST /message
        $url =env('API_MAIL');
        // Make a POST request
        $options = stream_context_create(['http' => [
                'method'  => 'POST',
                'header'  => 'Content-type: application/json',
                'content' => $json
            ]
        ]);
        // Send a request
        $result = file_get_contents($url, false, $options);
        $var = json_decode($result, true);
        if($var['sent'] == 'true'){
            return redirect('kontrak-non-inprogress')->with('success','Data telah ditambah & Notifikasi telah dikirim ');
            
        }else{
            return redirect('kontrak-non-inprogress')->with('error','Data telah ditambah & Notifikasi gagal dikirim ');
        }
    }
}
