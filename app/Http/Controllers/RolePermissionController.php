<?php

namespace App\Http\Controllers;

use App\RolePermission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\Role;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Validator;
use Auth;

class RolePermissionController extends Controller
{

    // public function menus(){
    //     return void;
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // SELECT id, name, display, count(model_id) as tot FROM `roles` r LEFT JOIN `model_has_roles` mhr ON mhr.role_id = r.id group by r.id
        // $roles = Role::all();
        $roles = DB::table('roles')
            ->select('id', 'name', 'display', DB::raw('count(model_id) as total'))
            ->leftJoin('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->groupBy('id')
            ->get();
        $permissions = DB::table('permissions')
            ->select('id', 'name', 'display', DB::raw('count(role_id) as total'))
            ->leftjoin('role_has_permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
            ->groupBy('id')
            ->get();

        return view('modules.rolepermission.index', compact('roles', 'permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createRole()
    {
        $menu_pbs = array(
            array("Create PBS", "pbs_create"),
            array("PBS", "pbs"),
            array("Status Transaksi", "pbs_st_trans"),
            array("Inprogress", "pbs_inp"),
            array("Selesai", "pbs_selesai"),
            array("Revisi Transaksi", "pbs_re_trans"),
            array("Kalkulator Peminjaman", "pbs_kal_pinj")
        );
        $menu_spph = array(
            array("Create SPPH", "spph_create"),
            array("List SPPH", "spph_list"),
            array("Draft SPPH", "spph_draft"),
            array("Done SPPH", "spph_done")
        );
        $menu_bakn = array(
            array("Create BAKN", "bakn_create"),
            array("List BAKN", "bakn_list"),
            array("Draft BAKN", "bakn_draft"),
            array("Done BAKN", "bakn_done")
        );
        $menu_spk = array(
            array("Create SP3/SPK", "spk_create"),
            array("List SP3/SPK", "spk_list"),
            array("Draft SP3/SPK", "spk_draft"),
            array("Status Transaksi", "spk_st_trans"),
            array("Inprogress SP3/SPK", "spk_inp"),
            array("Done SP3/SPK", "spk_done")
        );
        $menu_spk_non = array(
            array("Create SP3/SPK", "spk_create_non"),
            array("List SP3/SPK", "spk_list_non"),
            array("Draft SP3/SPK", "spk_draft_non"),
            array("Status Transaksi", "spk_st_trans_non"),
            array("Inprogress SP3/SPK", "spk_inp_non"),
            array("Done SP3/SPK", "spk_done_non")
        );
        $menu_kontrak = array(
            array("Kontrak", "kontrak"),
            array("List Kontrak", "kontrak_list"),
            array("Draft Kontrak", "kontrak_draft"),
            array("Status Transaksi", "kontrak_st_trans"),
            array("Inprogress Kontrak", "kontrak_inp"),
            array("Done Kontrak", "kontrak_done")
        );
        $menu_mdata = array(
            array("Data Flow", "mdata_dflow"),
            array("Data Karyawan", "mdata_dkaryawan"),
            array("Data Mitra", "mdata_dmitra"),
            array("Data Pasal", "mdata_dpasal"),
            array("Data Pimpinan Rapat", "mdata_dpimrap"),
            array("Data Role", "mdata_drole"),
            array("Data Unit", "mdata_dunit"),
            array("Data Jenis Pasal", "mdata_djenpas"),
            array("Data Cara Bayar", "mdata_dcarbay")
        );
        $menu_ar = array(
            array("Dashboard AR", "ar_dashboard"),
            array("Create", "ar_create"),
            array("Update Nilai", "ar_nilai"),
            array("Unbill", "ar_unbill"),
            array("Bill", "ar_bill"),
            array("Ready To Bill", "ar_readytobill"),
            array("Paid", "ar_paid"),
            array("Paid 100", "ar_paid100"),
            array("Unbill SDV", "unbill-sdv"),
            array("Unbill Operation", "unbill-operation"),
            array("Unbill UBIS", "unbill-ubis")
        );
        $menu_kontrak_non = array(
            array("Kontrak ", "kontrak_non"),
            array("List Kontrak ", "kontrak_non_list"),
            array("Draft Kontrak ", "kontrak_non_draft"),
            array("Status Transaksi ", "kontrak_non_st_trans"),
            array("Inprogress Kontrak ", "kontrak_non_inprogress"),
            array("Upload File ", "kontrak_non_upload"),
            array("Done Kontrak ", "kontrak_non_done"),
            array("List Dispatch Kontrak ", "kontrak_non_listdisp"),
            array("Tracking Document ", "kontrak_non_track"),
        );
        $menu_db = array(
            array("Target Revenue Sales & GP", "target_rev_sal_gp"),
            array("Pencapaian BAST", "pencapaian_bast"),
            array("Revenue SAP", "revenue_sap"),
            array("Pencapaian AM", "pencapaian_am"),
            array("IFRS Monitoring", "ifrs_monitoring")
        );
        $pipeline = array(
            array("CRM", "crm"),
            array("Info PBS/Justifikasi", "info_pbs_justi"),
            array("Legal Vendor", "legal_vendor"),
            array("Legal Customer", "legal_customer"),
            array("Delivery Customer", "delivery_customer"),
            array("Delivery Vendor", "delivery_vendor"),
            array("Report Management", "report_management")
        );

        return view('modules.rolepermission.create_role', compact('menu_pbs', 'menu_spph', 'menu_bakn', 'menu_spk', 'menu_spk_non', 'menu_kontrak', 'menu_mdata', 'menu_ar', 'menu_kontrak_non', 'menu_db', 'pipeline'));
    }

    public function createPermission()
    {
        return view('modules.rolepermission.create_permission');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRole(Request $request)
    {
        Validator::extend('without_space', function ($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });
        $this->validate($request, [
            'roleName' => 'required|without_space',
            'roleDisplay' => 'required'
        ], ['without_space' => "Role name can't contain any space."]);

        $name = $request->input('roleName');
        $display = $request->input('roleDisplay');
        $arr_menu = $request->input('menuAccess');
        $menu_access = implode(",", $arr_menu);

        $assign = Role::create([
            'name' => $name,
            'display' => $display,
            'menu_access' => $menu_access,
            'guard_name' => 'web'
        ]);
        $assign->givePermissionTo([$arr_menu]);

        return redirect()->route('rpmanage.index')->with('success', 'Data was added successfully');
    }

    public function storePermission(Request $request)
    {
        Validator::extend('without_space', function ($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });
        $this->validate($request, [
            'permissionName' => 'required|without_space',
            'permissionDisplay' => 'required'
        ], ['without_space' => "Permission name can't contain any space."]);

        $name = $request->input('permissionName');
        $display = $request->input('permissionDisplay');
        Permission::create([
            'name' => $name,
            'display' => $display
        ]);

        return redirect()->route('rpmanage.index')->with('success', 'Data was added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RolePermission  $rolePermission
     * @return \Illuminate\Http\Response
     */
    public function show(RolePermission $rolePermission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RolePermission  $rolePermission
     * @return \Illuminate\Http\Response
     */
    public function editRole($id)
    {
        $role = Role::find($id);
        $menu_pbs = array(
            array("Create PBS", "pbs_create"),
            array("PBS", "pbs"),
            array("Status Transaksi", "pbs_st_trans"),
            array("Inprogress", "pbs_inp"),
            array("Selesai", "pbs_selesai"),
            array("Revisi Transaksi", "pbs_re_trans"),
            array("Kalkulator Peminjaman", "pbs_kal_pinj")
        );
        $menu_spph = array(
            array("Create SPPH", "spph_create"),
            array("List SPPH", "spph_list"),
            array("Draft SPPH", "spph_draft"),
            array("Done SPPH", "spph_done")
        );
        $menu_bakn = array(
            array("Create BAKN", "bakn_create"),
            array("List BAKN", "bakn_list"),
            array("Draft BAKN", "bakn_draft"),
            array("Done BAKN", "bakn_done")
        );
        $menu_spk = array(
            array("Create SP3/SPK", "spk_create"),
            array("List SP3/SPK", "spk_list"),
            array("Draft SP3/SPK", "spk_draft"),
            array("Status Transaksi", "spk_st_trans"),
            array("Inprogress SP3/SPK", "spk_inp"),
            array("Done SP3/SPK", "spk_done")
        );
        $menu_spk_non = array(
            array("Create SP3/SPK", "spk_create_non"),
            array("List SP3/SPK", "spk_list_non"),
            array("Draft SP3/SPK", "spk_draft_non"),
            array("Status Transaksi", "spk_st_trans_non"),
            array("Inprogress SP3/SPK", "spk_inp_non"),
            array("Done SP3/SPK", "spk_done_non")
        );
        $menu_kontrak = array(
            array("Kontrak", "kontrak"),
            array("List Kontrak", "kontrak_list"),
            array("Draft Kontrak", "kontrak_draft"),
            array("Status Transaksi", "kontrak_st_trans"),
            array("Inprogress Kontrak", "kontrak_inp"),
            array("Done Kontrak", "kontrak_done")
        );
        $menu_mdata = array(
            array("Data Flow", "mdata_dflow"),
            array("Data Karyawan", "mdata_dkaryawan"),
            array("Data Mitra", "mdata_dmitra"),
            array("Data Pasal", "mdata_dpasal"),
            array("Data Pimpinan Rapat", "mdata_dpimrap"),
            array("Data Role", "mdata_drole"),
            array("Data Unit", "mdata_dunit"),
            array("Data Jenis Pasal", "mdata_djenpas"),
            array("Data Cara Bayar", "mdata_dcarbay")
        );
        $menu_ar = array(
            array("Dashboar AR", "ar_dashboard"),
            array("Create", "ar_create"),
            array("Update Nilai", "ar_nilai"),
            array("Unbill", "ar_unbill"),
            array("Bill", "ar_bill"),
            array("Ready To Bill", "ar_readytobill"),
            array("Paid", "ar_paid"),
            array("Paid 100", "ar_paid100"),
            array("Unbill SDV", "unbill-sdv"),
            array("Unbill Operation", "unbill-operation"),
            array("Unbill UBIS", "unbill-ubis")
        );
        $menu_kontrak_non = array(
            array("Kontrak ", "kontrak_non"),
            array("List Kontrak ", "kontrak_non_list"),
            array("Draft Kontrak ", "kontrak_non_draft"),
            array("Status Transaksi ", "kontrak_non_st_trans"),
            array("Inprogress Kontrak ", "kontrak_non_inprogress"),
            array("Upload File ", "kontrak_non_upload"),
            array("Done Kontrak ", "kontrak_non_done"),
            array("List Dispatch Kontrak ", "kontrak_non_listdisp"),
            array("Tracking Document ", "kontrak_non_track"),
        );
        $menu_db = array(
            array("Target Revenue Sales & GP", "target_rev_sal_gp"),
            array("Pencapaian BAST", "pencapaian_bast"),
            array("Revenue SAP", "revenue_sap"),
            array("Pencapaian AM", "pencapaian_am"),
            array("IFRS Monitoring", "ifrs_monitoring")
        );
        $pipeline = array(
            array("CRM", "crm"),
            array("Info PBS/Justifikasi", "info_pbs_justi"),
            array("Legal Vendor", "legal_vendor"),
            array("Legal Customer", "legal_customer"),
            array("Delivery Customer", "delivery_customer"),
            array("Delivery Vendor", "delivery_vendor"),
            array("Report Management", "report_management")
        );
        return view('modules.rolepermission.edit_role', compact('role', 'menu_pbs', 'menu_spph', 'menu_bakn', 'menu_spk', 'menu_spk_non', 'menu_kontrak', 'menu_mdata', 'menu_ar', 'menu_kontrak_non', 'menu_db', 'pipeline'));
    }
    public function editPermission($id)
    {
        $permission = Permission::find($id);

        return view('modules.rolepermission.edit_permission', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RolePermission  $rolePermission
     * @return \Illuminate\Http\Response
     */
    public function updateRole(Request $request, $id)
    {
        Validator::extend('without_space', function ($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });
        $this->validate($request, [
            'roleName' => 'required|without_space',
            'roleDisplay' => 'required'
        ], ['without_space' => "Role name can't contain any space."]);

        $role = Role::find($id);
        $role->name = $request->input('roleName');
        $role->display = $request->input('roleDisplay');
        $arr_menu = $request->input('menuAccess');
        $role->menu_access = implode(",", $arr_menu);
        $role->save();
        $role->syncPermissions($arr_menu);

        return redirect()->route('rpmanage.index')->with('success', "Data has been edited");
    }
    public function updatePermission(Request $request, $id)
    {
        Validator::extend('without_space', function ($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });
        $this->validate($request, [
            'permissionName', 'required|without_space',
            'permissionDisplay', 'required'
        ], ['without_space', "Permission name can't contain any space."]);

        $permission = Permission::find($id);
        $permission->name = $request->input('permissionName');
        $permission->display = $request->input('permissionDisplay');
        $permission->save();

        return redirect()->route('rpmanage.index')->with('success', "Data has been edited");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RolePermission  $rolePermission
     * @return \Illuminate\Http\Response
     */
    public function destroyRole($id)
    {
        $role = Role::find($id);
        $arr_permissions = explode(",", $role->menu_access);
        $role->revokePermissionTo($arr_permissions);
        $role->delete();

        return redirect()->route('rpmanage.index')->with('success', "Data Role has been deleted");
    }
    public function destroyPermission($id)
    {
        $permission = Permission::find($id);
        $permission->delete();

        return redirect()->route('rpmanage.index')->with('success', "Data Permission has been deleted");
    }
}
