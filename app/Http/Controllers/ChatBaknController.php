<?php

namespace App\Http\Controllers;

use App\ChatBakn;
use Illuminate\Http\Request;

class ChatBaknController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChatBakn  $chatBakn
     * @return \Illuminate\Http\Response
     */
    public function show(ChatBakn $chatBakn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChatBakn  $chatBakn
     * @return \Illuminate\Http\Response
     */
    public function edit(ChatBakn $chatBakn)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChatBakn  $chatBakn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatBakn $chatBakn)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChatBakn  $chatBakn
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatBakn $chatBakn)
    {
        //
    }
}
