<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use App\Iodesc;
use App\Mitra;
use App\User;
use App\Bakn;
use DB;
use App\CaraBayar;
use App\Spph;
use App\JenisPasal;
use Validator;
use App\Unit;
use App\ChatBakn;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Chat;

class BaknController extends Controller
{
    //list upload file
    public function index(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $bakns = Bakn::with(['user','spph','io'])
            ->where('status','save_bakn')
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->get();
            return view('modules.bakn.index', compact('bakns'));
        }else{
            $bakns = Bakn::with(['user','spph','io'])
            ->where([
                ['status','save_bakn'],
                ['created_by', Auth::user()->id]
                ])
            ->whereMonth('created_at',$var,$bln)                                
            ->whereYear('created_at',$thn)
            ->orderBy('updated_at','desc')
            ->get();
            return view('modules.bakn.index', compact('bakns'));
            }

        }
        // status bakn approval
        public function status(Request $request)
        {
            if($request->bulan == null){
                $var = '=';
                $bln = date('m');
                $thn = date('Y');
            }else{
                if($request->bulan == 0){
                    $var = '>=';
                    $bln = $request->bulan;
                }else{
                    $var = '=';
                    $bln = $request->bulan;
                }
                $thn = $request->tahun;   
            }
            $bakns = Bakn::where('status','!=','draft_bakn')->where('tipe','non')
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->get();
            // dd($bakns);
            return view('modules.bakn.status',compact('bakns'));
        }

        //list after save
        public function draft(Request $request)
        {
            if($request->bulan == null){
                $var = '=';
                $bln = date('m');
                $thn = date('Y');
            }else{
                if($request->bulan == 0){
                    $var = '>=';
                    $bln = $request->bulan;
                }else{
                    $var = '=';
                    $bln = $request->bulan;
                }
                $thn = $request->tahun;   
            }
            if(Auth::user()->level == 'administrator'){
                $bakns = Bakn::with('user','spph','io')->where('status','draft_bakn')
                ->whereMonth('created_at',$var,$bln)                                
                ->whereYear('created_at',$thn)
                ->get();
                return view('modules.bakn.draft', compact('bakns'));
            }else{
                $bakns = Bakn::with('user','spph','io')
                ->where('status','draft_bakn')
                ->where('created_by',Auth::user()->id)
                ->whereMonth('created_at',$var,$bln)                                
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
                return view('modules.bakn.draft', compact('bakns'));
            }
        }
        function selectUnit(Request $request){
            if($request->ajax() && $request->jns != null){
                if($request->jns == 'ecom'){
                    $bakns = Bakn::with('user','spph','io')->where('status','draft_bakn')->where('tipe','lkpp')->get();
                    return response()->json(['bakns'=>$bakns]);
                }else if($request->jns == 'gs'){
                    $bakns = Bakn::with('user','spph','io')->where('status','draft_bakn')->where('tipe','non')->get();
                    return response()->json(['bakns'=>$bakns]);
                }
            }else{
            }
        }
        //list setelah upload file
        public function done(Request $request)
        {
            if($request->bulan == null){
                $var = '=';
                $bln = date('m');
                $thn = date('Y');
            }else{
                if($request->bulan == 0){
                    $var = '>=';
                    $bln = $request->bulan;
                }else{
                    $var = '=';
                    $bln = $request->bulan;
                }
                $thn = $request->tahun;   
            }
            if(Auth::user()->level == 'administrator'){
                $bakns = Bakn::with(['user','spph','io'])
                ->where('status','done_bakn')
                ->whereMonth('created_at',$var,$bln)                                
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
                return view('modules.bakn.done', compact('bakns'));
            }else{
                $bakns = Bakn::with(['user','spph','io'])
                ->where([
                    ['status','done_bakn'],
                    ['created_by', Auth::user()->id]
                    ])
                ->whereMonth('created_at',$var,$bln)                                    
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
                return view('modules.bakn.done', compact('bakns'));
                }

            }
            // delete data and file
            public function destroy($id){
                // $bakn =  DB::delete('delete from bakns where id = ?',[$id]);
                // $unit = Unit::where('id',Auth::user()->id_unit)->first();
                // $path = "public/files/BAKN/".$unit->nama."/".$id;
                $bakn = Bakn::findOrFail($id);
                $bakn->delete();
                $path = "public/files/SPPH/".$bakn->user->unitnya['nama']."/".$id;
                Storage::deleteDirectory($path);
                return back()->with('success','Data telah dihapus');
            }
            // get data ajax
            function dataspph(Request $request){
                if($request->ajax()){
                    $spphs = Spph::with('mitras')->where('nomorspph', $request->nomorspph)->first();
                    return response()->json(['options'=>$spphs]);
                }
            }
            // preview
            public function preview($id){

                $chat = DB::table('chat_bakns')->where('idBakn',$id)->orderBy('created_at','desc')->first();
                $bakn = Bakn::with(['user','io','spph.mitras'])->where('id',$id)->first();
                $checkbox = explode(",", $bakn->tipe_rapat);
                $handlers = User::where('id_unit', 59)->where('level','adminkontrak')->orWhere('level','mgrlegal')->orWhere('level','adminbakn')->get();
                // dd($bakn->kontraksnon);
                return view('modules.bakn.preview',compact('chat','id','bakn','checkbox','handlers'));
            }

            public function create(){
                if(Auth::user()->id_unit != 59 ){
                    $spph = Spph::where("status", "done_spph")->where('nomorspph','LIKE','%ECOM%')->doesntHave('bakns')->get();
                    $io = Iodesc::all();
                    $jenispasal = JenisPasal::all();
                    $bayar = CaraBayar::all();
                    //query peserta pins
                    $daftarPeserta = DB::select("SELECT * FROM `users` where  (position like 'mgr%' or position like 'avp%' or position like 'gm%' or position like 'vp%') AND id_unit=56" );
                    $peserta = '';
                    foreach ($daftarPeserta as $daftar){
                        $peserta .= $daftar->name.',';
                    }
                    return view('modules.bakn.create', compact('spph','io','jenispasal','daftarPeserta','peserta','bayar'));
                }else{
                    $spph = Spph::where("status", "done_spph")->where('nomorspph', 'NOT LIKE','%ECOM%')->doesntHave('bakns')->get();
                    $io = Iodesc::all();
                    $jenispasal = JenisPasal::all();
                    $bayar = CaraBayar::all();
                    //query peserta pins
                    $daftarPeserta = DB::select("SELECT * FROM `users` where  (position like 'mgr%' or position like 'avp%' or position like 'gm%' or position like 'vp%') AND id_unit=59" );
                    $peserta = '';
                    foreach ($daftarPeserta as $daftar){
                        $peserta .= $daftar->name.',';
                    }

                    return view('modules.bakn.create', compact('spph','io','jenispasal','daftarPeserta','peserta','bayar'));
                }

            }

            public function edit($id)
            {
                $bakns = Bakn::with(['user','io','spph'])->where('id',$id)->first();
                $chat = DB::table('chat_bakns')->where('idBakn',$id)->orderBy('created_at','desc')->first();
                $spph = Spph::with('mitras')->where("status", "done_spph")->get();
                $jenispasal = JenisPasal::all();
                $io = Iodesc::all();
                $jeniskontrak = JenisPasal::all();
                $checkbox = explode(",", $bakns->tipe_rapat);
                return view('modules.bakn.edit', compact('chat','bakns','jeniskontrak','spph','checkbox','jenispasal','io'));

            }

            public function store(Request $request)
            {
                $customMessages = [
                    'required' => ':Attribute field is required.',
                    'jenis_kontrak.required' => 'Jenis Kontrak field is required',
                    'tglbakn.required' => 'Tanggal Bakn field is required',
                    'pimpinan_rapat.required' => 'Pimpinan Rapat field is required',
                    'harga.required' => 'Harga field is required',
                    'lokasi_pekerjaan.required' => 'Lokasi Pekerjaan field is required',
                    'jangka_waktu.required' => 'Jangka Waktu field is required',
                    'tipe_undangan' => 'Tipe Undangan field is required'
                ];
                $this->validate($request, array(
                    'jenis_kontrak' => 'required',
                    'nmrspph' => 'required',
                    'tglbakn' => 'required',
                    'tipe_undangan' => 'required',
                    'pimpinan_rapat' => 'required',
                    'peserta_pins' => 'required',
                    'peserta_mitra' => 'required',
                    'harga' => 'required',
                    // 'io_id' => 'required',

                    'agenda' => 'required',
                    'dasar_pembahasan' => 'required',
                    'ruang_lingkup' => 'required',
                    'lokasi_pekerjaan' => 'required',
                    'jangka_waktu' => 'required',
                    'harga_terbilang' => 'required',
                    'cara_bayar' => 'required',
                    'lain_lain' => 'required',
                ),$customMessages);

                DB::beginTransaction();
                try {
                    $bakn = new Bakn();
                    $bakn->spph_id = $request->input('spphid');
                    $bakn->jenis_kontrak = $request->input('jenis_kontrak');
                    $bakn->tglbakn = $request->input('tglbakn');
                    $bakn->tipe_rapat = implode(",",$request->tipe_undangan);
                    $bakn->pimpinan_rapat = $request->input('pimpinan_rapat');
                    $bakn->peserta_pins = $request->input('peserta_pins');
                    $bakn->peserta_mitra = $request->input('peserta_mitra');
                    $bakn->harga = str_replace(".","",$request->input('harga'));
                    
                    $bakn->io_id = $request->input('io_id');
                    $bakn->agenda = $request->input('agenda');
                    $bakn->dasar_pembahasan = $request->input('dasar_pembahasan');
                    $bakn->ruang_lingkup = $request->input('ruang_lingkup');
                    $bakn->lokasi_pekerjaan = $request->input('lokasi_pekerjaan');
                    $bakn->jangka_waktu = $request->input('jangka_waktu');
                    $bakn->harga_terbilang = $request->input('harga_terbilang');
                    $bakn->cara_bayar = $request->input('cara_bayar');
                    $bakn->lain_lain = $request->input('lain_lain');
                    
                    $bakn->tipe = $request->input('tipe');
                    $bakn->created_by = Auth::user()->id;
                    $bakn->status = $request->input('status');
                    $bakn->save();
                    DB::commit();
                } catch (ValidationException $e) {
                    DB::rollback();
                    return redirect()->back()
                    ->withErrors("Something wrong with your form, please check carefully")
                    ->withInput();
                } catch (\Exception $e) {
                    DB::rollback();
                    return redirect()->back()
                    ->withErrors("Something wrong from the server, please check carefully")
                    ->withInput();
                }
                return redirect('bakn-preview/'.$bakn->id)->with('Success', 'Data telah ditambahkan');

            }


            public function update(Request $request,$id)
            {
                $bakn = Bakn::find($id);

                $bakn->spph_id = $request->input('spphid');
                $bakn->jenis_kontrak = $request->input('jenis_kontrak');
                $bakn->tglbakn = $request->input('tglbakn');
                $bakn->tipe_rapat = implode(",",$request->tipe_undangan);
                $bakn->pimpinan_rapat = $request->input('pimpinan_rapat');
                $bakn->peserta_pins = $request->input('peserta_pins');
                $bakn->peserta_mitra = $request->input('peserta_mitra');
                $bakn->harga = str_replace(".","",$request->input('harga'));

                $bakn->io_id = $request->input('io_id');
                $bakn->agenda = $request->input('agenda');
                $bakn->dasar_pembahasan = $request->input('dasar_pembahasan');
                $bakn->ruang_lingkup = $request->input('ruang_lingkup');
                $bakn->lokasi_pekerjaan = $request->input('lokasi_pekerjaan');
                $bakn->jangka_waktu = $request->input('jangka_waktu');
                $bakn->harga_terbilang = $request->input('harga_terbilang');
                $bakn->cara_bayar = $request->input('cara_bayar');
                $bakn->lain_lain = $request->input('lain_lain');

                $bakn->tipe = $request->input('tipe');
                $bakn->status = $request->input('status');
                $bakn->save();
                // dd($bakn);
                return redirect('bakn-preview/'.$bakn->id)->with('Success', 'Data telah ditambahkan');
            }

            public function upload(Request $request, $id)
            {
                $unit = Unit::where('id',Auth::user()->id_unit)->first();
                // var_dump($unit);
                $path= "public/files/BAKN/".$unit->nama."/".$id;
                if($request->file('file'))
                {
                    foreach($request->file('file') as $file)
                    {
                        $name=$file->getClientOriginalName();
                        $file->storeAs($path, $name);
                        $namanya[] = $name;
                        $data[] = $path.'/'.$name;
                    }
                }
                $file = Bakn::find($id);
                $file->file=json_encode($data);
                $file->title = json_encode($namanya);
                $file->upload = date('Y-m-d H:i:s');
                $file->status = 'done_bakn';
                // $file->tgl_upload = date("Y-m-d H:i:s");
                $file->save();

                return redirect('bakn-done')->withSuccess('File has been uploaded.');
            }

            public function returnbakn(Request $request, $id){

                $return = Bakn::find($id);
                if($request->input('status') == 'approve'){
                    $return->status ='approve_bakn';
                    $return->handler = NULL;
                    $return->tgldisp = NULL;
                    $return->save();
                }else {
                    $return->status ='draft_bakn';
                    $return->handler = NULL;
                    $return->tgldisp = NULL;
                    $return->save();
                }


                if($return->status = 'draft_bakn'){
                    $customMessages = [
                        'required' => ':Attribute field is required.',
                    ];
                    $this->validate($request, array(
                        'chat' => 'required',
                    ),$customMessages);

                    DB::beginTransaction();
                    $chat = new ChatBakn();
                    $chat->chat = $request->input('chat');
                    $chat->idBakn = $id;
                    $chat->jabatan = Auth::user()->position;
                    $chat->username = Auth::user()->username;
                    $chat->transaksi = 'BAKN';
                    $chat->status = 'Return';
                    $chat->save();
                }
                DB::commit();

                return redirect('bakn-draft')->with('success','Data telah di return');
            }


        }
