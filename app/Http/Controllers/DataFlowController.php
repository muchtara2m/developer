<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataFlow;
use App\Unit;
use App\Role;
use App\User;
class DataFlowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dtflow = \App\DataFlow::all();
        $users = User::all();
        return view('modules.flow.index',compact('dtflow', 'users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $units = Unit::all();
        $roles = Role::all();
        $users = User::all();
        $flows = DataFlow::all();
        // $lvldesc = $this->leveldesc();
        return view('modules.flow.create', compact('roles','units','users','flows'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $flow = new DataFlow();
        $flow->jabatan = $request->get('jabatan');
        $flow->username = $request->get('username');
        $flow->min = $request->get('min');
        $flow->max = $request->get('max');
        $flow->queue = $request->get('queue');
        $flow->transaksi = $request->get('transaksi');
        $flow->save();

        return redirect()->route('flow.index')->with('success','Flow ('.$flow->username.' - '.$flow->jabatan.') was added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
