<?php

namespace App\Http\Controllers;

use App\Bakn;
use App\SpkNon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class SpkNonController extends Controller
{
    
    public function index()
    {
        //
        $spk = Spk::where([
            ['status','save_spk'],
            ['tipe','non']
        ])
        ->get();
        return view('modules.sp3k_non.index',compact('spk'));
    }

    public function create()
    {
        //
        $bakn = Bakn::where([
            ['harga','<=',50000000],
            ['status','done_bakn'],
            ['handler',NULL]
        ])
        ->doesntHave('kontraksnon')
        ->get();
        return view('modules.sp3k_non.create',compact('bakn'));
    }

    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'idbakn' => 'required',
            'nospk' => 'required',
            'tglspk' => 'required',
        ],[
            'required' => ':Attribute must be required'
        ]);

        DB::beginTransaction();
        try {
        $spk = new SpkNon();
        $spk->bakn_id = $request->input('idbakn');
        $spk->nospk = $request->input('nospk');
        $spk->tglspk = $request->input('tglspk');
        $spk->created_by = Auth::user()->id;
        $spk->status = $request->input('status');
        $bakns = Bakn::find($spk->bakn_id);
        $bakns->lain_lain = $request->input('lain_lain');
        $bakns->cara_bayar = $request->input('cara_bayar');
        $bakns->jangka_waktu = $request->input('jangka_waktu');
        $bakns->harga_terbilang = $request->input('harga_terbilang');
        $bakns->save();
        $spk->save();
        DB::commit();
            return redirect('spk-non-preview/'.$spk->id)->with('Success','Data dengan nomor '.$spk->nospk.' telah ditambah');
      
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
    }

    public function show($id)
    {
        //
        $spk = SpkNon::find($id);
        return view('modules.sp3k_non.preview',compact('spk'));
    }

    public function edit($id)
    {
        $spk = SpkNon::find($id);
        return view('modules.sp3k_non.edit',compact('spk'));
    }

    public function update(Request $request,$id)
    {
        $spk = SpkNon::find($id);

        if($spk->status == 'save_spk'){
            return view('list-sp3k-non');
        }else{
            return view('list-sp3k-non');            
        }

    }

    public function destroy($id)
    {
        
        $unit = Unit::where('id',Auth::user()->id_unit);
        Spph::where('id', $id)->delete();
        $path= "public/files/SPPH/".$unit->nama."/".$id;
        Storage::deleteDirectory($path);
    }

    public function draft(){
        $spk = SpkNon::where('status','draft_spk')
        ->orderBy('updated_at','desc')
        ->get();
        return view('modules.sp3k_non.draft', compact('spk'));
    }
    
    public function done(){
        $spk = SpkNon::where([
            ['status','done_spk'],
            ['file','!=',NULL]
        ])
        ->orderBy('updated_at','desc')
        ->get();
        return view('modules.sp3k_non.done',compact('spk'));
    }
}
