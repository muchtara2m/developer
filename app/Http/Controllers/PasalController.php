<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pasal;
use DB;

class PasalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pasals=Pasal::all();
        return view('modules.pasal.index',compact('pasals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('modules.pasal.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $pasal = new \App\Pasal();
        $pasal->pasal = $request->get('pasal');
        $pasal->judul = $request->get('judul');
        $pasal->isi = $request->get('isi');
        $pasal->save();

        return redirect('pasal')->with('success', 'Information has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $pasals = Pasal::find($id);
        return view('modules.pasal.edit',compact('pasals','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $pasal= \App\Pasal::find($id);
        $pasal->pasal = $request->get('pasal');
        $pasal->judul = $request->get('judul');
        $pasal->isi = $request->get('isi');
        $pasal->save();

        return redirect('pasal')->with('success', 'Information has been edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pasal = Pasal::find($id);
        $pasal->delete();

        return redirect('pasal')->with('success','Data has been  deleted');
    }
}
