<?php

namespace App\Http\Controllers;

use App\Spk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Bakn;
use DB;

class SpkController extends Controller
{
    
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'baknid' => 'required',
            'nosp3k' => 'required',
            'tglsp3k' => 'required',
            ]);
            
            DB::beginTransaction();
            try {
                $spk = new Spk();
                $spk->bakn_id = $request->input('baknid');
                $spk->nospk = $request->input('nosp3k');
                $spk->tglspk = $request->input('tglsp3k');
                $spk->created_by = Auth::user()->id;
                $spk->status = $request->input('status');
                $bakns = Bakn::find($spk->bakn_id);
                $bakns['lain_lain'] = $request->input('lain_lain');
                $bakns['ruang_lingkup'] = $request->input('ruang_lingkup');
                $bakns['jangka_waktu'] = $request->input('jangka_waktu');
                $bakns['cara_bayar'] = $request->input('cara_bayar');
                $bakns['jangka_waktu'] = $request->input('jangka_waktu');
                $bakns['harga_terbilang'] = $request->input('harga_terbilang');
                $bakns->save();
                $spk->save();
                DB::commit();
                return redirect('spk-preview/'.$spk->id)->with('Success','Data dengan nomor '.$spk->nospk.' telah ditambah');
                
            } catch (ValidationException $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong with your form, please check carefully")
                ->withInput();
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong from the server, please check carefully")
                ->withInput();
            }
            
        }
        
        public function edit($id)
        {
            $datas = Spk::with('bakns','bakns.spph','bakns.spph.mitras')->where('id',$id)->first();
            return view('modules.sp3k.edit', compact('datas'));
        }
        
        
        public function update(Request $request, $id)
        {
            DB::beginTransaction();   
            try {
                $spk = Spk::find($id);
                $spk->nospk = $request->input('nosp3k');
                $spk->tglspk = $request->input('tglsp3k');
                $spk->status = $request->input('status');
                $bakns = Bakn::find($spk->bakn_id);
                $bakns['lain_lain'] = $request->input('lain_lain');
                $bakns['ruang_lingkup'] = $request->input('ruang_lingkup');
                $bakns['jangka_waktu'] = $request->input('jangka_waktu');
                $bakns['cara_bayar'] = $request->input('cara_bayar');
                $bakns['jangka_waktu'] = $request->input('jangka_waktu');
                $bakns['harga_terbilang'] = $request->input('harga_terbilang');
                $bakns->save();
                $spk->save();
                DB::commit();
                return redirect('spk-preview/'.$spk->id)->with('Success','Data dengan nomor '.$spk->nospk.' telah diubah');
            } catch (ValidationException $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong with your form, please check carefully")
                ->withInput();
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong from the server, please check carefully")
                ->withInput();
            }
            DB::commit();
            return redirect('spk-preview/'.$id)->with('Success','Data telah ditambah');
        }
        
        public function destroy($id)
        {
            $spk =  DB::delete('delete from spks where id = ?',[$id]);
            return back()->with('success','Data telah dihapus');
        }
        
        public function preview($id)
        {
            $spk = Spk::with('bakns','bakns.spph','bakns.spph.mitras')->where('id', $id)->first();
            return view('modules.sp3k.preview_sp3k',compact('id','spk'));
        }
        
    }
    