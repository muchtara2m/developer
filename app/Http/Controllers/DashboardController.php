<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bakn;
use App\Kontrak;

class DashboardController extends Controller
{
    public function main()
    {
        return view('modules.dashboard.logopins');
    }
}
