<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use App\Bakn;
use App\Kontrak;
use DB;
use App\Spph;
use App\User;
use App\Pasal;
use App\Chat;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Dompdf\Dompdf;
use App\Sp3;
use App\JenisPasal;

class KontrakController extends Controller
{
    // function status transaksi kontrak
    public function status(){
        if(Auth::user()->level == 'administrator'){
            $sp3 = Kontrak::where('approval','!=','CLOSED')->get();
        }else if(Auth::user()->level != 'adminkontraklkpp'){
            $sp3 = Kontrak::where('approval',Auth::user()->username)->orderBy('updated_at','desc')->get();
        }else{
            $sp3 = Kontrak::where('approval','Return')->where('created_by',Auth::user()->id)->orderBy('updated_at','desc')->get();
        }
        return view('modules.kontrak.status_transaksi', compact('sp3'));
    }

    // function list draft kontrak
    public function draft(){
        $bakn = Kontrak::with('bakns','bakns.spph','bakns.io','bakns.spph.mitras')->doesntHave('sp3s')->where('status','draft_kontrak')->get();
        $sp3 = Kontrak::with('sp3s.bakns','sp3s.bakns.spph','sp3s.bakns.spph.mitras','sp3s.bakns.io','sp3s')->where('status','draft_kontrak')->get();
        return view('modules.kontrak.draft', compact('bakn','sp3'));
    }
    // list function done
    public function done(){
        $bakn = Kontrak::with('bakns','bakns.spph','bakns.io','bakns.spph.mitras')->doesntHave('sp3s')->where('approval','CLOSED')->get();
        $sp3 = Kontrak::with('sp3s.bakns','sp3s.bakns.spph','sp3s.bakns.spph.mitras','sp3s.bakns.io','sp3s')->doesntHave('bakns')->where('approval','CLOSED')->get();
        // dd($bakn, $sp3);
        return view('modules.kontrak.done', compact('bakn','sp3'));
    }
    // function list admin
    public function list(){
        $sp3 = Sp3::doesntHave('kontraks')->where('handler',Auth::user()->username)->get();
        $bakn = Bakn::with('io','spph')->doesntHave('sp3s')->where('handler',Auth::user()->username)->doesntHave('kontraks')->where('tipe','lkpp')->get();
        return view('modules.kontrak.list', compact('bakn','sp3'));
    }
    // function create kontrak
    public function create($id){
        // $sp3 = Sp3::with('bakns','bakns.spph','bakns.spph.mitras')->where('id',$id)->first();
        $sp3 = Sp3::find($id);
        $pasals = Pasal::all();
        $jenispasal = DB::table('jenis_pasals')->join('bakns','bakns.jenis_kontrak','jenis_pasals.jenis_pasal')->first();
            $end = DB::table('data_flows')
            ->select('jabatan','data_flows.username','queue','users.name','users.position')
            ->join('users','users.username','=','data_flows.username')
            ->where([
            ['min','<=',$sp3->bakns['harga']],
            ['max','>=',$sp3->bakns['harga']],
            ['transaksi', 'Kontrak'],
            ['unit', 'ECOM']
            ])->first();
       
        return view('modules.kontrak.create',compact('sp3','pasals','jenispasal','end'));
    }
    // function to show
    public function preview_status($id)
    {
        $kontrak = Kontrak::find($id);
        $checkbox = explode(",", $kontrak->sp3s->bakns['tipe_rapat']);
        $chats = DB::table('chats')
        ->join('users','chats.username','=','users.username')
        ->select('chat','jabatan','chats.username','users.name','chats.created_at')
        ->where('idTransaksi',$id)
        ->orderBy('chats.created_at','asc')
        ->get();
        $pic = DB::table('spphs')
            ->join('users','spphs.pic','=','users.name')
            ->select('spphs.pic', 'users.*')
            ->where('spphs.id',$kontrak->sp3s->bakns->spph['id'])
            ->first();
            $dari =  DB::table('spphs')
            ->join('users','spphs.dari','users.name')
            ->select('spphs.dari', 'users.*')
            ->where('spphs.id', $kontrak->sp3s->bakns->spph['id'])
            ->first();
        $user = User::all();
        return view('modules.kontrak.preview_status',compact('dari','pic','checkbox','id','kontrak','user','chats'));
    }
    // function print preview kontrak
    public function print_preview($id)
    {
        $kontrak = Kontrak::find($id);
        $get = DB::table('chats')
        ->where('status', 'Return')
        ->where('idTransaksi', $id)
        ->orderBy('created_at', 'desc')
        ->first();
        if($get ==NULL){
            $chat = DB::table('chats')
            ->join('users', 'users.username','chats.username')
            ->select('chats.*', 'users.name')
            ->where('idTransaksi',$id)
            ->where('transaksi', 'kontrak')
            ->where('status', 'Approve')
            ->orderBy('chats.created_at','asc')
            ->get();
        }else{
            $chat = DB::table('chats')
            ->join('users', 'users.username','chats.username')
            ->select('chats.*', 'users.name')
            ->where('idTransaksi',$id)
            ->where('transaksi', 'kontrak')
            ->where('status', 'Approve')
            ->where('chats.created_at','>',$get->created_at)
            ->orderBy('chats.created_at','asc')
            ->get();
        }
        // dd($get);
        $semua = DB::table('chats')
        ->join('users','users.username','=','chats.username')
        ->where('idTransaksi',$id)
        ->select('chats.*', 'users.name')
        ->orderBy('chats.created_at','asc')
        ->get();
        // dd($chat);
        return view('modules.kontrak.print', compact('kontrak','chat','semua'));
    }
    // function print send data
    public function print(Request $request, $id)
    {
        $kontrak = Kontrak::find($id);
        $kontrak->tgl_print = date('Y-m-d H:i:s');
        // dd($kontrak);
        $kontrak->save();
        return back()->with('success', 'Your files has been successfully added');
    }
    // FUNCTION EDIT
    public function edit($id)
    {
        $kontraks = Kontrak::find($id);
        $pasals = Pasal::all();
        $checkbox = explode(",", $kontraks->pasal);
        return view('modules.kontrak.edit', compact('kontraks', 'pasals','checkbox'));
    }
    // function upload file
    public function upload(Request $request, $id)
    {
        $this->validate($request, [
            'file' => 'required',
            ]);
            $path= "public/files/KontrakLKPP/".$id;
            if($request->file('file'))
            {
                foreach($request->file('file') as $file)
                {
                    $name=$file->getClientOriginalName();
                    $file->storeAs($path, $name);
                    $namanya[] = $name;
                    $data[] = $path.'/'.$name;
                }
            }
            $file = Kontrak::find($id);
            $file->tgl_upload = date("Y-m-d H:i:s");
            $file->file=json_encode($data);
            $file->title = json_encode($namanya);
            $file->save();
            return back()->with('success', 'Your files has been successfully added');
        }
        // export to word
        public function word($id){
            $kontrak = Kontrak::find($id);
            $ygpentingbisa = $kontrak->isi;
            header("Content-Type: application/vnd.msword");
            header("Expires: 0");//no-cache
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");//no-cache
            header("content-disposition: attachment;filename=$kontrak->nokontrak.doc");
            echo "<html>";
            echo "$ygpentingbisa";
            echo "</html>";
        }
        // inprogress approval
        public function inprogress(){
           if(Auth::user()->level =='administrator' || Auth::user()->level != 'adminkontraklkpp'){
               $sp3 = Kontrak::with(['chatnya' => function($q){
                   $q->select('chats.*','users.name')
                   ->join('users','users.username','chats.username')
                   ->orderBy('updated_at','asc');
               }])->get();
           }else{
            $sp3 = Kontrak::with(['chatnya' => function($q){
                $q->select('chats.*','users.name')
                ->join('users','users.username','chats.username')
                ->orderBy('updated_at','asc');
            }])
            ->where('created_by',Auth::user()->id)
            ->get();
           }
        return view('modules.kontrak.inprogress', compact('sp3'));
        }
        // insert kontrak
    public function store(Request $request)
    {
        $message = array(
            'required' => ':Attribute field is required'
        );
        $this->validate($request, [
            'sp3' => 'required',
            'nokontrak' => 'required',
            'tglkontrak' => 'required',
        ],$message);
        DB::beginTransaction(); 
        try {
            $kontrak = new Kontrak();
            $kontrak->sp3_id = $request->input('sp3');
            $kontrak->nokontrak = $request->input('nokontrak');
            $kontrak->tglkontrak = $request->input('tglkontrak');
            $kontrak->created_by = Auth::user()->id;
            $kontrak->handler = Auth::user()->username;
            $kontrak->status = $request->input('status');
            $kontrak->isi = $request->input('isi');
            $kontrak->save();
            $id = $kontrak->id;
            $path= "public/files/KontrakNon/".$id."/lampiran";
            if($request->file('lampiran'))
            {
                foreach($request->file('lampiran') as $file)
                {
                    $name=$file->getClientOriginalName();
                    $file->storeAs($path, $name);
                    $namanya[] = $name;
                    $data[] = $path.'/'.$name;
                }
            }
            $file = KontrakNon::find($id);
            $file->lampiran=json_encode($data);
            $file->title_lampiran  = json_encode($namanya);
            $file->save();
            if($kontrak->status == 'save_kontrak'){
                $id = $kontrak->id;
                $this->validate($request, [
                    'chat' => 'required',
                    'lampiran' => 'required',
                ],$message);

                $path= "public/files/Kontrak/".$id."/lampiran";
                if($request->file('lampiran'))
                {
                    foreach($request->file('lampiran') as $file)
                    {
                        $name=$file->getClientOriginalName();
                        $file->storeAs($path, $name);
                        $namanya[] = $name;
                        $data[] = $path.'/'.$name;
                    }
                }
                $file = Kontrak::find($id);
                $file->tgl_lampiran = date("Y-m-d H:i:s");
                $file->lampiran=json_encode($data);
                $file->title_lampiran  = json_encode($namanya);
                $file->save();
                // insert chat admin
                $komen = new Chat();
                $komen->chat = $request->get('chat');
                $komen->queue = 0;
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idTransaksi = $kontrak->id;
                $komen->transaksi = 'Kontrak';
                $komen->status = 'Approve';
                $id = $kontrak->id;
                $sph = $kontrak->nokontrak;
                $hrg = $kontrak->sp3s->bakns['harga'];
                $komen->save();

                $this->approval($sph,$hrg,$id);
                DB::commit();
                return redirect()->route('kontrak-status')->with('success', 'Status ('.$kontrak->approval.') was added successfully');                           
            }else if($kontrak->status == 'draft_kontrak'){
                DB::commit();        
            return redirect('kontrak-draft')->with('success', 'Data has been added');            
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
    }
    // function update kontrak
    public function update(Request $request, $id)
    {
        $message = array(
            'required' => ':Attribute field is required'
        );
        $kontrak = Kontrak::find($id);
        $kontrak->nokontrak = $request->input('nokontrak');
        $kontrak->isi = $request->input('isi');
        $kontrak->tglkontrak = $request->input('tglkontrak');
        $kontrak->status = $request->input('status');
        $kontrak->save();
        //update status tbl_bakns menjadi dispatch
        DB::beginTransaction();
        try{
            if($kontrak->status == 'save_kontrak' && $kontrak->lampiran == NULL){
                $this->validate($request, [
                    'chat' => 'required',
                    'lampiran' => 'required',
                ],$message);
                $path= "public/files/Kontrak/".$id."/lampiran";
                if($request->file('lampiran'))
                {
                    foreach($request->file('lampiran') as $file)
                    {
                        $name=$file->getClientOriginalName();
                        $file->storeAs($path, $name);
                        $namanya[] = $name;
                        $data[] = $path.'/'.$name;
                    }
                }
                $file = Kontrak::find($id);
                $file->lampiran=json_encode($data);
                $file->title_lampiran  = json_encode($namanya);
                $file->save();
                // insert chat admin
                $komen = new Chat();
                $komen->chat = $request->get('chat');
                $komen->queue = 0;
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idTransaksi = $id;
                $komen->transaksi = 'Kontrak';
                $komen->status = 'Approve';
                $id = $kontrak->id;
                $sph = $kontrak->nokontrak;
                $hrg = $kontrak->bakns['harga'];
                $komen->save();

                $this->approval($sph,$hrg,$id);
                DB::commit();
                return redirect()->route('kontrak-inprogress')->with('success', 'Status ('.$kontrak->approval.') was added successfully'); 
            }else if($kontrak->status == 'save_kontrak' && $kontrak->lampiran != NULL){
                $this->validate($request, [
                    'chat' => 'required',
                ],$message);
                 // insert chat admin
                $komen = new Chat();
                $komen->chat = $request->get('chat');
                $komen->queue = 0;
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idTransaksi = $id;
                $komen->transaksi = 'Kontrak';
                $komen->status = 'Approve';
                $id = $kontrak->id;
                $sph = $kontrak->nokontrak;
                $hrg = $kontrak->bakns['harga'];
                $komen->save();
                $id = $kontrak->id;
                $sph = $kontrak->nokontrak;
                $hrg = $kontrak->bakns['harga'];
                $this->approval($sph,$hrg,$id);
                DB::commit();
                return redirect()->route('kontrak-inprogress')->with('success', 'Status ('.$kontrak->approval.') was added successfully'); 

            }else if($kontrak->status == 'draft_kontrak' && $kontrak->lampiran == NULL  ){
                $path= "public/files/Kontrak/".$id."/lampiran";
                if($request->file('lampiran'))
                {
                    foreach($request->file('lampiran') as $file)
                    {
                        $name=$file->getClientOriginalName();
                        $file->storeAs($path, $name);
                        $namanya[] = $name;
                        $data[] = $path.'/'.$name;
                    }
                }
                $file = Kontrak::find($id);
                $file->lampiran=json_encode($data);
                $file->title_lampiran  = json_encode($namanya);
                $file->save();
                DB::commit();        
            return redirect('kontrak-draft')->with('success', 'Data has been added');            
            }else{
                DB::commit();        
                return redirect('kontrak-draft')->with('success', 'Data has been added');   
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }  

    }
    public function return_approve(Request $request, $id)
    {
        $kontrak = Kontrak::find($id);
        $kontrak->nokontrak = $request->input('nokontrak');
        $kontrak->isi = $request->input('isi');
        $kontrak->save();
        //update status tbl_bakns menjadi dispatch
        if($kontrak->sp3_id == NULL){
            $kontrak->bakns['status'] = 'disp_bakn';
            $spph = $kontrak->bakns->spph['nomorspph'];
            $hrg = $kontrak->bakns['harga'];
            $id = $kontrak->id;
            $kontrak->save();
        }else{
            $kontrak->sp3s['status'] = 'disp_sp3';
            $spph = $kontrak->sp3s->bakns->spph['nomorspph'];
            $hrg = $kontrak->sp3s->bakns['harga'];
            $id = $kontrak->id;
            $kontrak->save();
        }
        if ($kontrak->status == 'draft_kontrak') {
            return redirect('kontrak-draft')->with('success', 'Data has been added');
        }else{
            $this->validate($request, [
                'chat' => 'required',
                ]);
                DB::beginTransaction();
                // insert chat admin
                $komen = new Chat();
                $komen->chat = $request->get('chat');
                $komen->queue = 0;
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idTransaksi = $kontrak->id;
                $komen->transaksi = 'Kontrak';
                $komen->status = 'Approve';
                $komen->save();
                try {
                } catch (ValidationException $e) {
                    DB::rollback();
                    return redirect()->back()
                    ->withErrors("Something wrong with your form, please check carefully")
                    ->withInput();
                } catch (\Exception $e) {
                    DB::rollback();
                    return redirect()->back()
                    ->withErrors("Something wrong from the server, please check carefully")
                    ->withInput();
                }
                DB::commit();
                // declare harga kontrak
                $result=$this->approval($spph,$hrg,$id);
                return redirect()->route('kontrak-status')->with('success', 'Status ('.$kontrak->approval.') was added successfully');
            }
    }
    // function approval
    public function approval($nokontrak,$hrg, $id)
    {
        $end = DB::table('data_flows')
        ->select('jabatan','username','queue')
        ->where([
            ['min','<=',$hrg],
            ['max','>=',$hrg],
            ['transaksi', 'Kontrak'],
            ['unit', 'ECOM']
            ])->first();
        $awal = DB::table('data_flows')
        ->select('jabatan','username','queue')
        ->where([
            ['min', '<=', $hrg], 
            ['unit', 'ecom'],
            ['transaksi','kontrak']])
        ->first();
        $insert = DB::table('kontraks')
        ->where('id',$id)
        ->update(
            [
            'queue' => $awal->queue,
            'approval' => $awal->username,
            'endapproval' =>$end->username
            ]
        );
        $this->notifikasi($id,$awal->username);
    }
    public function notifikasi($id,$name)
    {
        $kontrak = Kontrak::find($id);
        $lastnya = Chat::where('idKontrakNon',$id)
        ->select('chat_nons.*','users.name')
        ->join('users','users.username','chat_nons.username')
        ->orderBy('created_at','desc')->first();
        $usernya = User::where('username',$name)->first();
        // function shorten url
        if ($kontrak->approval == 'CLOSED') {
            $hp = substr(str_replace("-","",$usernya->phone),6,13);
            $urls = url('kontrak-non-upload');
        }else if($kontrak->approval == 'Return'){
            $hp = substr(str_replace("-","",$usernya->phone),6,13);
            $urls = url('kontrak-non-status');
        }else{
            $hp = substr(str_replace("-","",$usernya->phone),6,13);
            $urls = url('kontrak-non-preview-status/'.$id);
        }

        // $login = "o_6o071p0jnl";
        // $api_key = "R_891d180189384c59a5d498d27a047e57";
        // $url = $urls;
        // $ch = curl_init('http://api.bitly.com/v3/shorten?login='.$login.'&apiKey='.$api_key.'&longUrl='.$url);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // $result = curl_exec($ch);
        // $res = json_decode($result, true);

        if($kontrak->approval == 'CLOSED'){
            $body = 'Bapak/Ibu '.$usernya->name
            ."\xA".'File Kontrak dengan Nomor '.$kontrak->nokontraknon
            ."\xA".'Sudah Selesai di Approve, silahkan askes pada link berikut['
            ."\xA".$urls;
        }else if($kontrak->approval == 'Return'){
            $body = 'Bapak/Ibu '.$usernya->name
            ."\xA".'File Kontrak dengan Nomor '.$kontrak->nokontraknon
            ."\xA".'Di Return oleh '. strtoupper($lastnya->name)
            ."\xA".'Dengan Catatan'
            ."\xA".'" '.$lastnya->chat.' "'
            ."\xA".'Silahkan akses pada link berikut'
            ."\xA".$urls;
        }else{
            $body = 'Bapak/Ibu '.$usernya->name
            ."\xA".'File Kontrak dengan Nomor '.$kontrak->nokontraknon
            ."\xA".'Membutuhkan Approve Anda, silahkan akses pada link berikut '
            ."\xA".$urls;
        }

            // $nomer = substr(str_replace("-","",$usernya->phone),6,13);
            $nomer = '81280295238';
            $data = [
                'phone' => '62'.$hp, // Receivers phone
                'body' => $body,
            ];
            $json = json_encode($data); // Encode data to JSON
            // URL for request POST /message
            $url = 'https://eu43.chat-api.com/instance59109/sendMessage?token=bn0w07mp9572ei4t';
            // Make a POST request
            $options = stream_context_create(['http' => [
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/json',
                    'content' => $json
                ]
            ]);
            // Send a request
            $result = file_get_contents($url, false, $options);
            $var = json_decode($result, true);
            if($var['sent'] == 'true'){
                return $this;                
            }else{
                return redirect()->back()->with('error', 'Notifikasi ('.$nomer.') tidak terdaftar ');
    
            }
    }
    // function chat
    public function chat(Request $request)
    {
        $flow =DB::table('data_flows')
        ->where('username', Auth::user()->username)
        ->first();
        DB::beginTransaction();
        try{
            $komen = new Chat();
        $komen->chat = $request->get('chat');
        $komen->queue = $flow->queue;
        $komen->jabatan = Auth::user()->position;
        $komen->username = Auth::user()->username;
        $komen->idTransaksi = $request->get('idTransaksi');
        $komen->transaksi = 'Kontrak';
        $komen->status = $request->get('status');
        $komen->save();
        if ($komen->status == 'Approve') {
            $idKontrak = $request->get('idKontrak');
            $kontrak = Kontrak::find($idKontrak);
            $unit = 'ECOM';
            $trx = 'Kontrak';
            $skrg = $kontrak->queue;
            $jabatanskrg = $kontrak->approval;
            $jabatanend = $kontrak->endapproval;
            // bandingin antara approval dengan approval akhir
            if ($jabatanskrg != $jabatanend) {
                $skrg++;
                $cek = DB::table('data_flows')
                ->select('jabatan','username','queue')
                ->where([
                    ['queue', $skrg],
                    ['transaksi', $trx],
                    ['unit', $unit]
                    ])
                    ->first();
                    $updateapproval = DB::table('kontraks')
                    ->where('id', $idKontrak)
                    ->update(
                        ['queue' => $skrg, 'approval' => $cek->username]
                    );
                    $this->notifikasi($idKontrak,$cek->username);
                    DB::commit();
                }else {
                    // status CLOSED dengan nilai 9
                    $skrg++;
                    $updateapproval = DB::table('kontraks')->where('id', $idKontrak)->update(
                        ['queue' => 9, 'approval' => 'CLOSED']
                    );
                    DB::commit();
                }
                return redirect('kontrak-inprogress');
            }else {
                // jika komen return
                $idKontrak = $request->get('idKontrak');
                $kontrak = Kontrak::find($idKontrak);
                $updatestatus = DB::table('kontraks')
                ->where('id', $idKontrak)
                ->update(
                    ['queue' => NULL, 'approval' =>'Return']
                );
                $name = User::where('id',$kontrak->created_by)->first();
                $this->notifikasi($idKontrak,$name->username);
                DB::commit();
                return redirect('kontrak-inprogress');
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
       
    }
    // function approval kontrak
    public function approve($id){
        $kontrak = Kontrak::find($id);
        $idnya = $kontrak->id;
        $chats = DB::table('chats')
        ->join('users','chats.username','=','users.username')
        ->select('chat','jabatan','chats.username','users.name','chats.created_at')
        ->where('idTransaksi',$idnya)
        ->orderBy('chats.created_at','asc')
        ->get();
        // dd($chats);
        $nilai = $kontrak->harga;
        $flow = DB::table('data_flows')->select('jabatan','username','queue')
        ->where([
            ['min','<=',$nilai],
            ['transaksi', 'Kontrak'],
            ['unit', Auth::user()->id_unit]
            ])->get();
            // var_dump($flow);
            $pasal=\App\Pasal::all();
            return view('modules.kontrak.approve',compact('id','pasal','kontrak','flow','chats'));
        }
        public function return_form($id)
            {
                $kontrak = Kontrak::find($id);
                $chat = DB::table('chats')
                ->where('idTransaksi', $id)
                ->orderBy('created_at', 'desc')
                ->first();
                return view('modules.kontrak.return_form',compact('id','chat','kontrak'));
            }
}
