<?php

namespace App\Http\Controllers;

use App\FileAR;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileARController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FileAR  $fileAR
     * @return \Illuminate\Http\Response
     */
    public function show(FileAR $fileAR)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FileAR  $fileAR
     * @return \Illuminate\Http\Response
     */
    public function edit(FileAR $fileAR)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FileAR  $fileAR
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FileAR $fileAR)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FileAR  $fileAR
     * @return \Illuminate\Http\Response
     */
    public function destroy(FileAR $fileAR)
    {
        //
    }
}
