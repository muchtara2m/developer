<?php

namespace App\Http\Controllers;

use App\JenisPasal;
use Illuminate\Http\Request;
use DB;


class JenisPasalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pasal=JenisPasal::all();
        return view('modules.jenispasal.index',compact('pasal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('modules.jenispasal.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $jenispasal = new \App\JenisPasal();
        $jenispasal->jenis_pasal = $request->get('jenis_pasal');
        $jenispasal->isi_pasal = $request->get('isi_pasal');
        $jenispasal->save();
        return redirect('jenispasal')->with('success', 'Information has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JenisPasal  $jenisPasal
     * @return \Illuminate\Http\Response
     */
    public function show(JenisPasal $jenisPasal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JenisPasal  $jenisPasal
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        //
        $pasals=JenisPasal::find($id);
        return view('modules.jenispasal.edit',compact('pasals','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JenisPasal  $jenisPasal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        //
        $jenispasal= \App\JenisPasal::find($id);
        $jenispasal->jenis_pasal = $request->get('jenis_pasal');
        $jenispasal->isi_pasal = $request->get('isi_pasal');
        $jenispasal->save();

        return redirect('jenispasal')->with('success', 'Information has been edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JenisPasal  $jenisPasal
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        //
        $pasal = JenisPasal::find($id);
        $pasal->delete();

        return redirect('jenispasal')->with('success','Data has been  deleted');
    }
}
