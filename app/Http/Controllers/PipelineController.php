<?php

namespace App\Http\Controllers;

use App\AmPipeline;
use App\CustomerInisiasi;
use App\Iodesc;
use App\PortofolioPipeline;
use App\SegmentPipeline;
use App\StatusInisiasi;
use App\Unit;
use Illuminate\Http\Request;

class PipelineController extends Controller
{

    // function Entry Update Status Inisiasi
    public function entryUpdate()
    {
        $status_inisiasi = StatusInisiasi::all();
        $io = Iodesc::all();
        $am = AmPipeline::all();
        $customer = CustomerInisiasi::all();
        $ubis = Unit::all();
        $portofolio = PortofolioPipeline::all();
        $segment = SegmentPipeline::all();
        return view('modules.pipeline.entry_update_status',compact('status_inisiasi','io','am','ubis','customer','segment','portofolio'));
    }
    // function Entry Outlook
    public function entryOutlook()
    {
        return view('modules.pipeline.entry_outlook');
    }
    // function create inisiasi
    public function createInisiasi()
    {
        $am = AmPipeline::all();
        $customer = CustomerInisiasi::all();
        $ubis = Unit::all();
        $portofolio = PortofolioPipeline::all();
        $segment = SegmentPipeline::all();
        return view('modules.pipeline.create_inisiasi', compact('am', 'ubis', 'customer', 'segment', 'portofolio'));
    }
    // function edit inisiasi
    public function editInisiasi()
    {
        $am = AmPipeline::all();
        $customer = CustomerInisiasi::all();
        $ubis = Unit::all();
        $portofolio = PortofolioPipeline::all();
        $segment = SegmentPipeline::all();
        return view('modules.pipeline.edit_inisiasi', compact('am', 'ubis', 'customer', 'segment', 'portofolio'));
    }
    // function submit project to sdv status won
    public function submitProject()
    {
        return view('modules.pipeline.submit_project_to_sdv');
    }
}
