<?php

namespace App\Http\Controllers;

use App\Sp3Non;
use App\Bakn;
use App\Spk;
use Illuminate\Http\Request;

class Sp3NonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sp3Non  $sp3Non
     * @return \Illuminate\Http\Response
     */
    public function show(Sp3Non $sp3Non)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sp3Non  $sp3Non
     * @return \Illuminate\Http\Response
     */
    public function edit(Sp3Non $sp3Non)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sp3Non  $sp3Non
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sp3Non $sp3Non)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sp3Non  $sp3Non
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sp3Non $sp3Non)
    {
        //
    }
}
