<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Unit;
use Hash;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Notifications\UserAdded;
use App\Mail\TestUserAdded;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('modules.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $units = Unit::all();
        $roles = Role::all();
        // $lvldesc = $this->leveldesc();
        return view('modules.user.create', compact('roles','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::extend('without_space', function($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });

        $this->validate($request, [
            'name' => 'required|max:100',
            'username' => 'required|unique:users|min:3|without_space',
            'email' => 'required|email|unique:users',
            'level' => 'required',
            'unit' => 'required',
            'phone' => 'nullable|unique:users'
        ],['without_space' => "Username can't contain any space."]);

        DB::beginTransaction();

        $user = new User();
            $user->name = $request->input('name');
            $user->username = $request->input('username');
            $user->password = Hash::make('ijustmadeiteasyforyou');
            $user->email = $request->input('email');
            $user->level = $request->input('level');
            $user->id_unit = $request->input('unit');
            // $user->employee_status = $request->input('employee_status');
            $user->position = $request->input('position');
            $user->band = $request->input('band');
            $user->permanent_status = $request->input('permanent_status');
            // $user->cost_center = $request->input('cost_center');
            $user->phone = $request->input('phone');
            $user->telp = $request->input('telp');
            $user->created_by = $request->input('created_by');

            // $user->notify(new UserAdded());
            // Mail::to($user->email)->send(new TestUserAdded());
            $user->save();
            $user->assignRole($user->level);

        try {
            
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
                ->withErrors("Something wrong with your form, please check carefully")
                ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
                ->withErrors("Something wrong from the server, please check carefully")
                ->withInput();
        }
        DB::commit();
        return redirect()->route('user.index')->with('success', 'User ('.$user->name.') was added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $units = Unit::all();
        $roles = Role::all();
        // $lvldesc = $this->leveldesc();
        
        return view('modules.user.edit', compact('user','units','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::extend('without_space', function($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });

        $this->validate($request, [
            'name' => 'required|max:100',
            'username' => 'required|min:3|without_space|unique:users,username,'.$id,
            'email' => 'required|email|unique:users,email,'.$id,
            'level' => 'required',
            'unit' => 'required',
            'phone' => 'nullable|unique:users,phone,'.$id,
        ],['without_space' => "Username can't contain any space."]);

        $user = User::find($id);

        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->level = $request->input('level');
        $user->id_unit = $request->input('unit');
        // $user->employee_status = $request->input('employee_status');
        $user->position = $request->input('position');
        $user->band = $request->input('band');
        $user->permanent_status = $request->input('permanent_status');
        // $user->cost_center = $request->input('cost_center');
        $user->phone = $request->input('phone');
        $user->telp = $request->input('telp');
        $user->created_by = $request->input('created_by');
        $user->save();
        $user->syncRoles($user->level);

        return redirect()->route('user.index')->with('success','User ('.$user->name.') was updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('user.index')->with('success', 'User ('.$user->name.') was deleted successfully');
    }
}
