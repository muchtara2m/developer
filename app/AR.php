<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AR extends Model
{
    protected $table = 'ars';
    //

    public function dataio()
    {
        return $this->belongsTo('App\Iodesc','io_id');
    }
    public function pembuat(){
        return $this->belongsTo('App\User','created_by');
    }
    public function customer(){
        return $this->belongsTo('App\Customer', 'customer_id');
    }
    public function chatar()
    {
        return $this->hasMany('App\ChatAR','idAR');
    }
}
