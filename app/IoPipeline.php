<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IoPipeline extends Model
{
    //
    protected $connection = 'mysql2';
    protected $table = 't_io';
    protected $primaryKey = 'no_io';


    public function inisasiPip(){
        return $this->hasMany('App\Inisiasi','no_io');
    }
    public function ioSpkPksVendor(){
        return $this->hasMany('App\SpkPksVendor','no_io');
    }
    public function ioSpkPksCustomer(){
        return $this->hasMany('App\SpkPksCustomer','no_io');
    }
}
