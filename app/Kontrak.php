<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kontrak extends Model
{
    //
    public function sp3s()
    {
        return $this->belongsTo('App\Sp3','sp3_id');
    }
    public function bakns()
    {
        return $this->belongsTo('App\Bakn','bakn_id');
    }
    public function chatnya()
    {
        return $this->hasMany('App\Chat','idTransaksi');
    }
    public function users(){
        return $this->belongsTo('App\User','created_by');
    }
}
