<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    //
    protected $connection = 'mysql2';
    protected $table = 't_vendor';
    protected $primaryKey = 'akun_vendor';

    public function vendorSpkPks(){
        return $this->hasMany('App\SpkPksVendor','akun_vendor');
    }
}
