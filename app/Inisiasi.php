<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inisiasi extends Model
{
    //
    protected $connection = 'mysql2';
    protected $table = 't_inisiasi';


    public function iopipe(){
        return $this->belongsTo('App\IoPipeline','no_io');
    }


}
