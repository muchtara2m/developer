<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iodesc extends Model
{
    protected $table = 'mtr_io';
    protected $primaryKey = 'no_io';
    public $incrementing = false;

    public function bakns(){
        return $this->hasMany('App\Bakn','io_id');
    }
    public function ars(){
        return $this->hasMany('App\AR','io_id');
    }
}
