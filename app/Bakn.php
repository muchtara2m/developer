<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bakn extends Model
{
    //
    protected $fillable = [
        'title',
        'file',
        'tgl_upload'
    ];

    public function spph()
    {
        return $this->belongsTo('App\Spph','spph_id');
    }
    public function io()
    {
        return $this->belongsTo('App\Iodesc','io_id');
    }
    public function user(){
        return $this->belongsTo('App\User','created_by');
    }
    public function sp3s()
    {
        return $this->hasMany('App\Sp3','id');
    }
    public function spks()
    {
        return $this->hasMany('App\Spk','bakn_id');
    }
    public function spksnon()
    {
        return $this->hasMany('App\SpkNon','id');
    }
    public function kontraks()
    {
        return $this->hasMany('App\Kontrak','bakn_id');
    }
    public function kontraksnon()
    {
        return $this->hasMany('App\KontrakNon','bakn_id');
    }
    public function chatbakns()
    {
        return $this->hasMany('App\ChatBakn','idBakn');
    }

}
