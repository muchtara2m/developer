<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerInisiasi extends Model
{
    //
    protected $connection = 'mysql2';
    protected $table = 't_customer_inisiasi';
    protected $primaryKey = 'akun_customer';
}
