<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerSpkPks extends Model
{
    //
    protected $connection = 'mysql2';
    protected $table = 't_customer' ;
    protected $primaryKey = 'akun_customer';

    public function SpkPksCustomer(){
        return $this->hasMany('App\SpkPksCustomer','akun_customer');
    }
}
