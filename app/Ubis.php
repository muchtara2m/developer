<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ubis extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'ubis';
    protected $primaryKey = 'id_ubis';
    // One to many Ubis -> Plan_Bast
    public function ubis_outlook()
    {
        return $this->hasMany('App\PlanBast', 'ubis');
    }
    public function ubis_rkap()
    {
        return $this->hasMany('App\Rkap', 'ubis');
    }
    public function ubisVendor(){
        return $this->hasMany('App\MappingIoUbis','id_ubis');
    }
    public function ubisCustomer(){
        return $this->hasMany('App\MappingIoUbis','id_ubis');
    }
}
