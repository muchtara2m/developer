<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatAR extends Model
{
    protected $table = 'chat_ars';
    //
    public function ars(){
        return $this->belongsTo('App\AR','id');
    }
}
