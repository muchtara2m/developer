<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatSp3 extends Model
{
    //
    public function sp3s(){
        return $this->belongsTo('App\Sp3','id');
    }
}
