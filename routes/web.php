<?php

use FontLib\Table\Type\name;
use App\Http\Controllers\ARController;
use App\Http\Controllers\SpkNonController;
use Illuminate\Http\Request;

// use App\Http\Controllers\KontrakController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/////////// AUTH ///////////
/*Route::get('/', function () {
    return view('auth.login');
});*/

Route::get('/', 'HomeController@index');
Auth::routes();
Route::get('/register', function () {
    return abort(404);
});

Route::group(['middleware' => 'auth'], function () {

    // test ldap
    Route::post('test/login', 'PanelController@authLdap')->name('test-login');
    Route::get('/home', 'DashboardController@main');

    //////////// TRANSAKSI ////////
    Route::get('/master', function () {
        return view('master');
    });

    Route::get('/index-pbs', function () {
        return view('modules.transaksi.index-pbs');
    })->name('index-pbs');
    Route::get('/status-transaksi', function () {
        return view('modules.transaksi.status-transaksi');
    })->name('status-transaksi');
    Route::get('/inprogress', function () {
        return view('modules.transaksi.inprogress');
    })->name('inprogress');
    Route::get('/selesai', function () {
        return view('modules.transaksi.selesai');
    })->name('selesai');
    Route::get('/revisi-transaksi', function () {
        return view('modules.transaksi.revisi-transaksi');
    })->name('revisi-transaksi');
    Route::get('/kalkulator-pinjaman', function () {
        return view('modules.transaksi.kalkulator-pinjaman');
    })->name('kalkulator-pinjaman');

    // ------------------- Route DASHBOARD ------------------
    Route::get('db_sap', 'DshSapController@index')->name('db_sap');
    Route::get('db_outlook', 'DshSapController@show');
    Route::get('db_am', 'DshSapController@pencapaianAM');
    Route::get('db-ifrs', 'DshSapController@ifrs')->name('db-ifrs');
    Route::get('ifrs-customer-json', 'DshSapController@jsonIfrsCustomer');
    Route::get('ifrs-vendor-json', 'DshSapController@jsonIfrsVendor');
    Route::post('upload-csv', 'DshSapController@uploadCsv');


    // ------------------- Route PIPELINE -------------------
    Route::get('entry-update', 'PipelineController@entryUpdate')->name('entry-update');
    Route::get('entry-outlook', 'PipelineController@entryOutlook')->name('entry-outlook');
    Route::get('create-inisiasi', 'PipelineController@createInisiasi')->name('create-inisiasi');
    Route::get('edit-inisiasi', 'PipelineController@editInisiasi')->name('edit-inisiasi');
    Route::get('submit-project', 'PipelineController@submitProject')->name('submit-project');


    // ------------------- Route PBS ------------------
    Route::get('pbs-create', 'PbsController@create')->name('pbs-create');
    Route::get('pbs-purchasing', 'PbsController@purchasing')->name('pbs-purchasing');



    // -------------------Route SPPH------------------
    Route::get('spph-index', 'SpphController@index')->name('spph-index');
    Route::get('spph-done', 'SpphController@done')->name('spph-done');
    Route::get('spph-draft', 'SpphController@draft')->name('spph-draft');
    Route::get('spph-create', 'SpphController@create')->name('spph-create');
    Route::post('spph-store', 'SpphController@store')->name('spph-store');
    Route::get('spph-edit/{id}', 'SpphController@edit')->name('spph-edit');
    Route::post('spph-update/{id}', 'SpphController@update')->name('spph-update');
    Route::get('spph-delete/{id}', 'SpphController@destroy');
    Route::get('spph-preview/{id}', 'SpphController@preview')->name('spph-preview');
    Route::post('spph-lampiran/{id}', 'SpphController@lampiran');
    Route::post('select-spph', ['as' => 'select-spph', 'uses' => 'SpphController@selectSpph']);


    // ------------------End Route SPPH---------------

    // Route BAKN
    Route::post('data-spph', ['as' => 'data-spph', 'uses' => 'BaknController@dataspph']);
    Route::get('bakn-spph', 'BaknController@baknspph')->name('bakn-spph');
    Route::get('bakns', function () {
        return view('modules.bakn.index');
    });
    Route::get('bakn-create', 'BaknController@create')->name('bakn-create');
    Route::get('list-bakn', 'BaknController@index')->name('list-bakn');
    Route::post('bakn-store', 'BaknController@store')->name('bakn.store');
    Route::get('bakn-edit/{id}', 'BaknController@edit')->name('bakn.edit');
    Route::post('bakn-update/{id}', 'BaknController@update')->name('bakn.update');
    Route::get('bakn-preview/{id}', 'BaknController@preview')->name('bakn.preview');
    Route::post('bakn-approve', 'BaknController@approve')->name('bakn-approve');
    Route::get('delete/{id}', 'BaknController@destroy');
    Route::get('bakn-draft', 'BaknController@draft')->name('bakn-draft');
    Route::get('bakn-done', 'BaknController@done')->name('bakn-done');
    Route::get('ds-bakn', 'BaknController@dashboard');
    Route::post('bakn-upload/{id}', 'BaknController@upload');
    Route::post('return/{id}', 'BaknController@returnbakn');
    Route::get('bakn-status', 'BaknController@status');
    // Route::get('bakn-view-upload/{id}', 'BaknController@viewUpload');
    Route::post('select-unit', ['as' => 'select-unit', 'uses' => 'BaknController@selectUnit']);


    // ----------------- Route Kontrak LKPP--------------------------

    Route::get('kontrak', 'KontrakController@index')->name('kontrak');
    Route::get('kontrak-list', 'KontrakController@list')->name('kontrak-list');
    Route::get('kontrak-create/{id}', 'KontrakController@create')->name('kontrak-create');
    Route::post('kontrak-store', 'KontrakController@store')->name('kontrak.store');
    Route::post('kontrak-dispbakn/{id}', 'KontrakController@dispbakn');
    Route::post('kontrak-dispsp3/{id}', 'KontrakController@dispsp3');
    Route::get('kontrak-draft', 'KontrakController@draft')->name('kontrak-draft');
    Route::get('kontrak-done', 'KontrakController@done')->name('kontrak-done');
    Route::get('kontrak-preview/{id}', 'KontrakController@preview')->name('kontrak-preview');
    Route::get('deletekontrak/{id}', 'KontrakController@destroy');
    Route::get('kontrak-edit/{id}', 'KontrakController@edit')->name('kontrak-edit');
    Route::post('kontrak-update/{id}', 'KontrakController@update')->name('kontrak-update');
    Route::get('ds-kontrak', 'KontrakController@dashboard');
    Route::get('kontrak-inprogress', 'KontrakController@inprogress')->name('kontrak-inprogress');
    Route::get('kontrak-status', 'KontrakController@status')->name('kontrak-status');
    Route::get('kontrak-approve/{id}', 'KontrakController@approve')->name('kontrak-approve');
    Route::post('kontrak-chat', 'KontrakController@chat')->name('kontrak-chat');
    Route::get('kontrak-return', 'KontrakController@return')->name('kontrak-return');
    Route::get('kontrak-return_form/{id}', 'KontrakController@return_form')->name('kontrak-return_form');
    Route::post('kontrak-return_approve/{id}', 'KontrakController@return_approve')->name('kontrak-return_approve');
    Route::get('kontrak-preview-status/{id}', 'KontrakController@preview_status')->name('kontrak-preview-status');
    Route::get('kontrak-print_preview/{id}', 'KontrakController@print_preview')->name('kontrak-print_preview');
    Route::post('kontrak-upload/{id}', 'KontrakController@upload');
    Route::post('kontrak-print/{id}', 'KontrakController@print');
    Route::post('kontrak-word/{id}', 'KontrakController@word');

    // ----------------- Route Kontrak NON--------------------------
    Route::get('kontrak-non', 'KontrakNonController@index')->name('kontrak-non');
    Route::get('kontrak-non-list', 'KontrakNonController@list')->name('kontrak-non-list');
    Route::get('kontrak-non-create/{id}', 'KontrakNonController@create')->name('kontrak-non-create');
    Route::post('kontrak-non-store', 'KontrakNonController@store')->name('kontrak-non-store');
    Route::post('kontrak-non-dispbakn/{id}', 'KontrakNonController@dispbakn');
    Route::post('kontrak-non-dispsp3/{id}', 'KontrakNonController@dispsp3');
    Route::get('kontrak-non-draft', 'KontrakNonController@draft')->name('kontrak-non-draft');
    Route::get('kontrak-non-done', 'KontrakNonController@done')->name('kontrak-non-done');
    Route::get('kontrak-non-preview/{id}', 'KontrakNonController@preview')->name('kontrak-non-preview');
    Route::get('deletekontrak-non/{id}', 'KontrakNonController@destroy');
    Route::get('kontrak-non-edit/{id}', 'KontrakNonController@edit')->name('kontrak-non-edit');
    Route::post('kontrak-non-update/{id}', 'KontrakNonController@update')->name('kontrak-non-update');
    Route::get('ds-kontrak-non', 'KontrakNonController@dashboard');
    Route::get('kontrak-non-inprogress', 'KontrakNonController@inprogress')->name('kontrak-non-inprogress');
    Route::get('kontrak-non-status', 'KontrakNonController@status')->name('kontrak-non-status');
    Route::get('kontrak-non-approve/{id}', 'KontrakNonController@approve')->name('kontrak-non-approve');
    Route::post('kontrak-non-chat', 'KontrakNonController@chat')->name('kontrak-non-chat');
    Route::get('kontrak-non-return', 'KontrakNonController@return')->name('kontrak-non-return');
    Route::get('kontrak-non-return_form/{id}', 'KontrakNonController@return_form')->name('kontrak-non-return_form');
    Route::post('kontrak-non-return_approve/{id}', 'KontrakNonController@return_approve')->name('kontrak-non-return_approve');
    Route::get('kontrak-non-preview-status/{id}', 'KontrakNonController@preview_status')->name('kontrak-non-preview-status');
    Route::get('kontrak-non-print_preview/{id}', 'KontrakNonController@print_preview')->name('kontrak-non-print_preview');
    Route::post('kontrak-non-upload/{id}', 'KontrakNonController@upload');
    Route::get('kontrak-non-upload', 'KontrakNonController@uploadFile');
    Route::post('kontrak-non-print/{id}', 'KontrakNonController@print');
    Route::post('kontrak-non-word/{id}', 'KontrakNonController@word');
    Route::get('kontrak-non-listdisp', 'KontrakNonController@listdispatch');
    Route::get('kontrak-non-tracking', 'KontrakNonController@tracking')->name('kontrak-non-tracking');
    Route::get('tracking-non-json', 'KontrakNonController@jsonTracking');




    //---------------- RPManage -------------------
    Route::get('create-role', 'RolePermissionController@createRole')->name('rpmanage.createRole');
    Route::get('create-permission', 'RolePermissionController@createPermission')->name('rpmanage.createPermission');
    Route::post('store-role', 'RolePermissionController@storeRole')->name('rpmanage.storeRole');
    Route::post('store-permission', 'RolePermissionController@storePermission')->name('rpmanage.storePermission');
    Route::delete('delete-permission/{id}', 'RolePermissionController@destroyPermission')->name('rpmanage.destroyPermission');
    Route::delete('delete-role/{id}', 'RolePermissionController@destroyRole')->name('rpmanage.destroyRole');
    Route::get('rpmanage/{id}/edit-role', 'RolePermissionController@editRole')->name('rpmanage.editRole');
    Route::get('rpmanage/{id}/edit-permission', 'RolePermissionController@editPermission')->name('rpmanage.editPermission');
    Route::put('rpmanage/{id}/update-role', 'RolePermissionController@updateRole')->name('rpmanage.updateRole');
    Route::put('rpmanage/{id}/update-permission', 'RolePermissionController@updatePermission')->name('rpmanage.updatePermission');


    // Route SP3/SPK
    // ajax sp3
    Route::post('data-bakn', ['as' => 'data-bakn', 'uses' => 'Sp3Controller@databakn']);
    Route::get('create-sp3k', 'Sp3Controller@create')->name('sp3k');
    Route::get('list-sp3k', 'Sp3Controller@index');
    Route::get('draft-sp3k', 'Sp3Controller@draft');
    Route::get('done-sp3k', 'Sp3Controller@done');
    Route::get('edit-sp3/{id}', 'Sp3Controller@edit')->name('edit.sp3');
    Route::get('edit-spk/{id}', 'SpkController@edit')->name('edit.spk');
    Route::post('store-spk', 'SpkController@store');
    Route::post('store-sp3', 'Sp3Controller@store');
    Route::get('list-sp3k', 'Sp3Controller@index');
    Route::get('sp3-preview/{id}', 'Sp3Controller@preview');
    Route::get('spk-preview/{id}', 'SpkController@preview');
    Route::post('update-sp3/{id}', 'Sp3Controller@update')->name('update.sp3');
    Route::post('update-spk/{id}', 'SpkController@update')->name('update.spk');
    Route::get('delete-sp3/{id}', 'Sp3Controller@destroy');
    Route::get('delete-spk/{id}', 'SpKController@destroy');
    Route::get('inprogress-sp3k', 'Sp3Controller@inprogress');
    Route::get('approve-sp3/{id}', 'Sp3Controller@approve');
    Route::post('chat-sp3', 'Sp3Controller@chat')->name('chat-sp3');
    Route::post('sp3-upload/{id}', 'Sp3Controller@upload');
    Route::get('status-sp3k', 'Sp3Controller@status');
    // End route SP3/SPK


    // ------------------Route AR------------------
    Route::post('data-io', ['as' => 'data-io', 'uses' => 'ARController@getdataio']);
    Route::get('create_ar', 'ARController@create');
    Route::get('unbill_ar', 'ARController@index');
    Route::get('edit_ar/{id}', 'ARController@edit')->name('edit_ar');
    Route::get('preview_ar/{id}', 'ARController@show')->name('preview_ar');
    Route::post('insert_ar', 'ARController@store')->name('insert_ar');
    Route::post('update_ar/{id}', 'ARController@update')->name('update_ar');
    Route::get('readytobill', 'ARController@readytobill')->name('readytobill');
    Route::get('bill', 'ARController@bill')->name('bill');
    Route::get('paid', 'ARController@paid')->name('paid');
    Route::get('paid100', 'ARController@paid100')->name('paid100');
    Route::post('update-nilai/{id}', 'ARController@updatenilai')->name('update-nilai');
    Route::post('upload/{id}', 'ARController@upload')->name('upload');
    Route::get('nilai', 'ARController@nilaiproject')->name('nilai');
    Route::get('db_ar', 'ARController@dashboard')->name('db_ar');
    Route::post('db-ar', 'ARController@dashboard');
    Route::get('unbill-sdv', 'ARController@json_sdv')->name('unbill-sdv');
    Route::get('unbill-operation', 'ARController@json_operation')->name('unbill-operation');
    Route::get('unbill-ubis', 'ARController@json_ubis')->name('unbill-ubis');
    Route::post('nilai-project/{id}', 'ARController@updateproject')->name('nilai-project');
    Route::get('live-search', ['as' => 'live-search', 'uses' => 'ARController@action']);

    // SPK NON 
    Route::get('create-spk-non', 'SpkNonController@create');
    Route::post('store-spk-non', 'SpkNonController@store');
    Route::get('list-sp3k-non', 'SpkNonController@index');
    Route::get('preview-spk-non', 'SpkNonController@preview');
    Route::get('edit-spk-non', 'SpkNonController@edit');
    Route::post('update-spk-non/{id}', 'SpkNonController@update');
    Route::post('delete-spk-non/{id}', 'SpkNonController@destroy');
    Route::get('draft-spk-non', 'SpkNonController@draft');
    Route::get('done-spk-non', 'SpkNonController@done');



    // testing notifikasi
    Route::get('telp', function () {
        return view('testing');
    });

    Route::get('test-konek', 'HomeController@konek')->name('test-konek');
    Route::get('test-wa', 'NotifikasiWAController@index')->name('test-wa');
    Route::post('notif', 'NotifikasiWAController@store')->name('notif');
    Route::get('test-api', function () {
        return view('welcome');
    });
    // Route::get('/api-customer-inisiasi','HomeController@testingAPI');
    Route::group(array('prefix' => 'api/v1', 'before' => 'auth.basic'), function () {
        // Route::resource('pages', 'PagesController', array('only' => array('index', 'store', 'show', 'update', 'destroy')));
        // Route::resource('users', 'UsersController');
        Route::get('api-pipeline', 'HomeController@testingAPI');

        // Note: If you are not required authentication for API call, you can remove 'before' => 'auth.basic'
        // Here you can access index, store, show, update and destroy methods from your PagesController.
        // GET http://localhost/project/api/v1/pages // this will call index function
        // POST http://localhost/project/api/v1/pages // this will call store function
        // GET http://localhost/project/api/v1/pages/1 // this will call show method with 1 as arg
        // PUT http://localhost/project/api/v1/pages/1 // this will call update with 1 as arg
        // DELETE http://localhost/project/api/v1/pages/1 // this will call destroy with 1 as arg
    });

    //  -----------------------End Route AR ---------------


    /////////////// MASTER DATA ////////////////
    Route::resources([
        'user' => 'UserController',
        'chairman' => 'ChairmanController',
        'mitra' => 'MitraController',
        'pasal' => 'PasalController',
        'unit' => 'UnitController',
        'flow' => 'DataFlowController',
        'rpmanage' => 'RolePermissionController',
        'jenispasal' => 'JenisPasalController',
        'tatacara' => 'CaraBayarController',
    ]);

    //-----command-----
    Route::get('/clear-cache', function () {
        Artisan::call('cache:clear');
        return "Cache is cleared";
    });
    ///////// playground ////////
    Route::get('bakn_old', function () {
        return view('modules.konbak.bakn_old');
    });
    Route::get('/check-auth', function () {
        return view('modules.auth.login');
    })->name('check-auth');
});
