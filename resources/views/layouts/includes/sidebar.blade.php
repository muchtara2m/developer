<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    @auth
    @php
    // PBS LIST MENU
    $menu_pbs = array(
    array("Create PBS","pbs_create"),
    array("PBS","pbs"),
    array("Status Transaksi","pbs_st_trans"),
    array("Inprogress","pbs_inp"),
    array("Selesai","pbs_selesai"),
    array("Revisi Transaksi","pbs_re_trans"),
    array("Kalkulator Peminjaman","pbs_kal_pinj"));
    // SPPH LIST MENUT
    $menu_spph = array(
    array("Create SPPH","spph_create"),
    array("List SPPH","spph_list"),
    array("Draft SPPH","spph_draft"),
    array("Done SPPH","spph_done"));
    // BAKN LIST MENU
    $menu_bakn = array(
    array("Create BAKN","bakn_create"),
    array("List BAKN","bakn_list"),
    array("Draft BAKN","bakn_draft"),
    array("Done BAKN","bakn_done"));
    //SP3 LIST MENU LKPP
    $menu_spk = array(
    array("Create SP3/SPK","spk_create"),
    array("List SP3/SPK","spk_list"),
    array("Draft SP3/SPK","spk_draft"),
    array("Status Transaksi","spk_st_trans"),
    array("Inprogress SP3/SPK","spk_inp"),
    array("Done SP3/SPK","spk_done"));
    //SP3 LIST MENUNON
    $menu_spk_non = array(
    array("Create SP3/SPK","spk_create_non"),
    array("List SP3/SPK","spk_list_non"),
    array("Draft SP3/SPK","spk_draft_non"),
    array("Status Transaksi","spk_st_trans_non"),
    array("Inprogress SP3/SPK","spk_inp_non"),
    array("Done SP3/SPK","spk_done_non"));
    // KONTRAK LIST MENU
    $menu_kontrak = array(
    array("Kontrak LKPP","kontrak"),
    array("List Kontrak LKPP","kontrak_list"),
    array("Draft Kontrak LKPP","kontrak_draft"),
    array("Status Transaksi LKPP","kontrak_st_trans"),
    array("Inprogress Kontrak LKPP","kontrak_inp"),
    array("Done Kontrak LKPP","kontrak_done"));
    // KONTRAK NON LIST MENU
    $menu_kontrak_non = array(
    array("Kontrak ","kontrak_non"),
    array("List Kontrak ","kontrak_non_list"),
    array("Draft Kontrak ","kontrak_non_draft"),
    array("Status Transaksi ","kontrak_non_st_trans"),
    array("Inprogress Kontrak ","kontrak_non_inprogress"),
    array("Upload File ","kontrak_non_upload"),
    array("Done Kontrak ","kontrak_non_done"),
    array("List Dispatch Kontrak ","kontrak_non_listdisp"),
    array("Tracking Document ","kontrak_non_track"),
    );
    // ADMIN LIST MENU
    $menu_mdata = array(
    array("Data Flow","mdata_dflow"),
    array("Data Karyawan","mdata_dkaryawan"),
    array("Data Mitra","mdata_dmitra"),
    array("Data Pasal","mdata_dpasal"),
    array("Data Pimpinan Rapat","mdata_dpimrap"),
    array("Data Role & Permission","mdata_drole"),
    array("Data Unit","mdata_dunit"),
    array("Data Jenis Pasal","mdata_djenpas"),
    array("Data Cara Bayar","mdata_dcarbay"));
    //  AR LIST MENU
    $menu_ar = array(
    array("Dashboar AR","ar_dashboard"),
    array("Create","ar_create"),
    array("Update Nilai","ar_nilai"),
    array("Unbill","ar_unbill"),
    array("Unbill SDV","unbill-sdv"),
    array("Unbill Operation","unbill-operation"),
    array("Unbill UBIS","unbill-ubis"),
    array("Ready To Bill","ar_readytobill"),
    array("Bill","ar_bill"),
    array("Paid","ar_paid"),
    array("Paid 100","ar_paid100"));
    // DASHBOARD MENU
    $menu_db = array(
    array("Target Revenue Sales & GP","target_rev_sal_gp"),
    array("Pencapaian BAST","pencapaian_bast"),
    array("Revenue SAP","revenue_sap"),
    array("Pencapaian AM","pencapaian_am"),
    array("IFRS Monitoring","ifrs_monitoring"));
    // PIPELINE
    $pipeline = array(
    array("CRM","crm"),
    array("Info PBS/Justifikasi","info_pbs_justi"),
    array("Legal Vendor","legal_vendor"),
    array("Legal Customer","legal_customer"),
    array("Delivery Customer","delivery_customer"),
    array("Delivery Vendor","delivery_vendor"),
    array("Report Management","report_management"));
    $pbs_permissions = $spph_permissions = $bakn_permissions = $spk_permissions = $spk_permissions_non = $kontrak_permissions = $kontrak_non_permissions = $mdata_permissions = $ar_permissions = $menu_db_permissions = $pipeline_permissions = array();
    for ($i=0; $i<count($menu_pbs); $i++){
      array_push($pbs_permissions,$menu_pbs[$i][1]);
    }
    for ($i=0; $i<count($menu_spph); $i++){
      array_push($spph_permissions,$menu_spph[$i][1]);
    }
    for ($i=0; $i<count($menu_bakn); $i++){
      array_push($bakn_permissions,$menu_bakn[$i][1]);
    }
    for ($i=0; $i<count($menu_spk); $i++){
      array_push($spk_permissions,$menu_spk[$i][1]);
    }
    for ($i=0; $i<count($menu_spk_non); $i++){
      array_push($spk_permissions_non,$menu_spk_non[$i][1]);
    }
    for ($i=0; $i<count($menu_kontrak); $i++){
      array_push($kontrak_permissions,$menu_kontrak[$i][1]);
    }
    for ($i=0; $i<count($menu_kontrak_non); $i++){
      array_push($kontrak_non_permissions,$menu_kontrak_non[$i][1]);
    }
    for ($i=0; $i<count($menu_mdata); $i++){
      array_push($mdata_permissions,$menu_mdata[$i][1]);
    }
    for ($i=0; $i<count($menu_ar); $i++){
      array_push($ar_permissions,$menu_ar[$i][1]);
    }
    for ($i=0; $i<count($menu_db); $i++){
      array_push($menu_db_permissions,$menu_db[$i][1]);
    }
    for ($i=0; $i<count($pipeline); $i++){
      array_push($pipeline_permissions,$pipeline[$i][1]);
    }
    @endphp
    
    <ul class="sidebar-menu" style="white-space:normal" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li hidden>
        <a href="{{ url('/home') }}">
          <i class="fa fa-home"></i> <span>Dashboard</span>
        </a>
      </li>
      {{-- @if( Auth::user()->hasAnyPermission([$spph_permissions]) ) --}}
      @canany($menu_db_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>DASHBOARD SALES</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can($menu_db[0][1]) <li><a href="{{ url('db_sap') }}"><i class="fa fa-circle-o"></i>{{ $menu_db[0][0] }}</a></li> @endcan
          @can($menu_db[1][1]) <li><a href="{{ url('db_outlook') }}"><i class="fa fa-circle-o"></i>{{ $menu_db[1][0] }}</a></li> @endcan
          @can($menu_db[2][1]) <li><a href="#"><i class="fa fa-circle-o"></i>{{ $menu_db[2][0] }}</a></li> @endcan
          @can($menu_db[3][1]) <li><a href="{{ url('db_am') }}"><i class="fa fa-circle-o"></i>{{ $menu_db[3][0] }}</a></li> @endcan
          @can($menu_db[4][1]) <li><a href="{{ route('db-ifrs') }}"><i class="fa fa-circle-o"></i>{{ $menu_db[4][0] }}</a></li> @endcan
        </ul>
      </li>
      @endcanany
      
      @canany($pipeline_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>PIPELINE</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="treeview menu-open" style="height: auto;">
            @can($pipeline[0][1])
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> {{ $pipeline[0][0] }}
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('entry-update') }}"><i class="fa fa-circle-o"></i> Entry & Update Status Inisiasi Project</a></li>
                <li><a href="{{ url('entry-outlook') }}"><i class="fa fa-circle-o"></i> Entry Outlook</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Submit Project Won To SDV</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Reporting Outlook & Realisasi</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Report Inisiasi</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Reporting Status Inisiasi</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> List Inisiasi Project status WON</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Reporting Produktivitas AM</a></li>
              </ul>
            </li>
          </li>
          @endcan
          @can($pipeline[1][1])
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i>{{ $pipeline[1][0] }}
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Entry Data PBS</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Entry Data Justifikasi</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Reporting PBS & Kontrak</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List PBS</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List Justifikasi</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> IO - SO</a></li>
            </ul>
          </li> 
          @endcan
          @can($pipeline[2][1])
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i>{{ $pipeline[2][0] }}
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Entry Kontrak Vendor</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List SPK/PKS Vendor</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List KHS Vendor/Create PO</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List PO Vendor</a></li>
            </ul>
          </li>
          @endcan
          @can($pipeline[3][1])
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i>{{ $pipeline[3][0] }}
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Entry Kontrak Customer</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List SPK/PKS Customer</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List KHS Customer/Create PO</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List PO Customer</a></li>
            </ul>
          </li>
          @endcan
          @can($pipeline[4][1])
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i>{{ $pipeline[4][0] }}
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Entry Data Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Update Status Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Update Nilai Pencapaian</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Report Status Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Progress Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Performansi Report</a></li>
            </ul>
          </li>
          @endcan
          @can($pipeline[5][1])
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i>{{ $pipeline[5][0] }}
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Entry Data Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Update Status Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Update Nilai Pencapaian</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Report Status Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Progress Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Performansi Report</a></li>
            </ul>
          </li>
          @endcan
          @can($pipeline[6][1])
          <li class="treeview">
          <a href="#"><i class="fa fa-circle-o"></i>{{ $pipeline[6][0] }}
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i>Reporting Outlook & Realisasi</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Reporting Status Inisiasi</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Report Inisiasi
              <li><a href="#"><i class="fa fa-circle-o"></i>List Inisiasi Project status WON</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Report Legal Pelurusan Project</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>List SPK/PKS Customer</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>List KHS Customer</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>List PO Customer</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>List SPK/PKS Vendor</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>List KHS Vendor</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>List PO Vendor</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Report PBS & Kontrak</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Report Progress Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Report Status Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Report Cash IN</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Report Cash Out</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Report PU Billed</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Report PU Unbilled</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Summarize Cash.IN/OUT dan PU</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Report Residu</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Report Aset Project</a></li>
            </ul>
          </li>
          @endcan
        </ul>
      </<a>
      @endcanany
      
      @canany($pbs_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>PBS</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can($menu_pbs[0][1]) <li><a href="{{ route('pbs-create') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[0][0] }}</a></li> @endcan
          @can($menu_pbs[1][1]) <li><a href="{{ route('index-pbs') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[1][0] }}</a></li> @endcan
          @can($menu_pbs[2][1]) <li><a href="{{ route('status-transaksi') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[2][0] }}</a></li> @endcan
          @can($menu_pbs[3][1]) <li><a href="{{ route('inprogress') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[3][0] }}</a></li> @endcan
          @can($menu_pbs[4][1]) <li><a href="{{ route('selesai') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[4][0] }}</a></li> @endcan
          @can($menu_pbs[5][1]) <li><a href="{{ route('revisi-transaksi') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[5][0] }}</a></li> @endcan
          @can($menu_pbs[6][1]) <li><a href="{{ route('kalkulator-pinjaman') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[6][0] }}</a></li> @endcan
        </ul>
      </li>
      @endcanany
      {{-- @endif --}}
      @canany($spph_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>SPPH</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can($menu_spph[0][1]) <li><a href="{{ url('spph-create') }}"><i class="fa fa-circle-o"></i>{{ $menu_spph[0][0] }}</a></li> @endcan
          @can($menu_spph[1][1]) <li><a href="{{ url('spph-index') }}"><i class="fa fa-circle-o"></i>{{ $menu_spph[1][0] }}</a></li> @endcan
          @can($menu_spph[2][1]) <li><a href="{{ url('spph-draft') }}"><i class="fa fa-circle-o"></i>{{ $menu_spph[2][0] }}</a></li> @endcan
          @can($menu_spph[3][1]) <li><a href="{{ url('spph-done') }}"><i class="fa fa-circle-o"></i>{{ $menu_spph[3][0] }}</a></li> @endcan
        </ul>
      </li>
      @endcanany
      @canany($bakn_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>BAKN</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          {{-- @can($menu_bakn[0][1]) <li hidden><a href="{{ url('ds-bakn') }}"><i class="fa fa-circle-o"></i>Dashboard BAKN</a></li> @endcan --}}
          @can($menu_bakn[0][1]) <li><a href="{{ url('bakn-create') }}"><i class="fa fa-circle-o"></i>{{ $menu_bakn[0][0] }}</a></li> @endcan
          @can($menu_bakn[1][1]) <li><a href="{{ url('list-bakn') }}"><i class="fa fa-circle-o"></i>{{ $menu_bakn[1][0] }}</a></li> @endcan
          @can($menu_bakn[2][1]) <li><a href="{{ url('bakn-draft') }}"><i class="fa fa-circle-o"></i>{{ $menu_bakn[2][0] }}</a></li> @endcan
          @can($menu_bakn[3][1]) <li><a href="{{ url('bakn-done') }}"><i class="fa fa-circle-o"></i>{{ $menu_bakn[3][0] }}</a></li> @endcan
        </ul>
      </li>
      @endcanany
      @canany($spk_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>SP3/SPK LKPP</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can($menu_spk[0][1]) <li><a href="{{ url('create-sp3k') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk[0][0] }}</a></li> @endcan
          @can($menu_spk[1][1]) <li><a href="{{ url('list-sp3k') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk[1][0] }}</a></li> @endcan
          @can($menu_spk[2][1]) <li><a href="{{ url('draft-sp3k') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk[2][0] }}</a></li> @endcan
          @can($menu_spk[3][1]) <li><a href="{{ url('status-sp3k') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk[3][0] }}</a></li> @endcan
          @can($menu_spk[4][1]) <li><a href="{{ url('inprogress-sp3k') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk[4][0] }}</a></li> @endcan
          @can($menu_spk[5][1]) <li><a href="{{ url('done-sp3k') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk[5][0] }}</a></li> @endcan
        </ul>
      </li>
      @endcanany
      @canany($spk_permissions_non)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>SP3/SPK</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can($menu_spk_non[0][1]) <li><a href="{{ url('create-spk-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk_non[0][0] }}</a></li> @endcan
          @can($menu_spk_non[1][1]) <li><a href="{{ url('list-spk-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk_non[1][0] }}</a></li> @endcan
          @can($menu_spk_non[2][1]) <li><a href="{{ url('draft-spk-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk_non[2][0] }}</a></li> @endcan
          @can($menu_spk_non[3][1]) <li><a href="{{ url('status-spk-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk_non[3][0] }}</a></li> @endcan
          @can($menu_spk_non[4][1]) <li><a href="{{ url('inprogress-spk-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk_non[4][0] }}</a></li> @endcan
          @can($menu_spk_non[5][1]) <li><a href="{{ url('done-spk-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk_non[5][0] }}</a></li> @endcan
        </ul>
      </li>
      @endcanany
      @canany($kontrak_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>KONTRAK LKPP</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          {{-- @can($menu_kontrak[0][1]) <li hidden><a href="{{ url('ds-kontrak') }}"><i class="fa fa-circle-o"></i>Dashboard Kontrak</a></li> @endcan --}}
          @can($menu_kontrak[0][1]) <li><a href="{{ url('kontrak') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak[0][0] }}</a></li> @endcan
          @can($menu_kontrak[1][1]) <li><a href="{{ url('kontrak-list') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak[1][0] }}</a></li> @endcan
          @can($menu_kontrak[2][1]) <li><a href="{{ url('kontrak-draft') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak[2][0] }}</a></li> @endcan
          {{-- @can($menu_kontrak[0][1]) <li><a href="{{ url('kontrak-return') }}"><i class="fa fa-circle-o"></i>Return Kontrak</a></li> @endcan --}}
          @can($menu_kontrak[3][1]) <li><a href="{{ url('kontrak-status') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak[3][0] }}</a></li> @endcan
          @can($menu_kontrak[4][1]) <li><a href="{{ url('kontrak-inprogress') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak[4][0] }}</a></li> @endcan
          @can($menu_kontrak[5][1]) <li><a href="{{ url('kontrak-done') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak[5][0] }}</a></li> @endcan
        </ul>
      </li>
      @endcanany
      @canany($kontrak_non_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>KONTRAK</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          {{-- @can($menu_kontrak[0][1]) <li hidden><a href="{{ url('ds-kontrak') }}"><i class="fa fa-circle-o"></i>Dashboard Kontrak</a></li> @endcan --}}
          @can($menu_kontrak_non[0][1]) <li><a href="{{ url('kontrak-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[0][0] }}</a></li> @endcan
          @can($menu_kontrak_non[1][1]) <li><a href="{{ url('kontrak-non-list') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[1][0] }}</a></li> @endcan
          @can($menu_kontrak_non[2][1]) <li><a href="{{ url('kontrak-non-draft') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[2][0] }}</a></li> @endcan
          @can($menu_kontrak_non[3][1]) <li><a href="{{ url('kontrak-non-status') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[3][0] }}</a></li> @endcan
          @can($menu_kontrak_non[4][1]) <li><a href="{{ url('kontrak-non-inprogress') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[4][0] }}</a></li> @endcan
          @can($menu_kontrak_non[5][1]) <li><a href="{{ url('kontrak-non-upload') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[5][0] }}</a></li> @endcan
          @can($menu_kontrak_non[6][1]) <li><a href="{{ url('kontrak-non-done') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[6][0] }}</a></li> @endcan
          @can($menu_kontrak_non[7][1]) <li><a href="{{ url('kontrak-non-listdisp') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[7][0] }}</a></li> @endcan
          @can($menu_kontrak_non[8][1]) <li><a href="{{ url('kontrak-non-tracking') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[8][0] }}</a></li> @endcan
        </ul>
      </li>
      @endcanany
      
      @canany($ar_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>AR</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can($menu_ar[0][1]) <li><a href="{{ route('db_ar') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[0][0] }}</a></li> @endcan
          @can($menu_ar[1][1]) <li><a href="{{ url('create_ar') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[1][0] }}</a></li> @endcan
          @can($menu_ar[2][1]) <li><a href="{{ url('nilai') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[2][0] }}</a></li> @endcan
          @can($menu_ar[3][1]) <li><a href="{{ url('unbill_ar') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[3][0] }}</a></li> @endcan
          @can($menu_ar[4][1]) <li><a href="{{ url('unbill-sdv') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[4][0] }}</a></li> @endcan
          @can($menu_ar[5][1]) <li><a href="{{ url('unbill-operation') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[5][0] }}</a></li> @endcan
          @can($menu_ar[6][1]) <li><a href="{{ url('unbill-ubis') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[6][0] }}</a></li> @endcan
          @can($menu_ar[7][1]) <li><a href="{{ url('readytobill') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[7][0] }}</a></li> @endcan
          @can($menu_ar[8][1]) <li><a href="{{ url('bill') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[8][0] }}</a></li> @endcan
          @can($menu_ar[9][1]) <li><a href="{{ url('paid') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[9][0] }}</a></li> @endcan
          @can($menu_ar[10][1]) <li><a href="{{ url('paid100') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[10][0] }}</a></li> @endcan
          
        </ul>
      </li>
      @endcanany
      @canany($mdata_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>MASTER DATA</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can($menu_mdata[0][1]) <li><a href="{{ url('flow') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[0][0] }}</a></li> @endcan
          @can($menu_mdata[1][1]) <li><a href="{{ route('user.index') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[1][0] }}</a></li> @endcan
          @can($menu_mdata[2][1]) <li><a href="{{ url('mitra') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[2][0] }}</a></li> @endcan
          @can($menu_mdata[3][1]) <li><a href="{{ url('pasal') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[3][0] }}</a></li> @endcan
          @can($menu_mdata[4][1]) <li><a href="{{ route('chairman.index') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[4][0] }}</a></li> @endcan
          @can($menu_mdata[5][1]) <li><a href="{{ route('rpmanage.index') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[5][0] }}</a></li> @endcan
          @can($menu_mdata[6][1]) <li><a href="{{ route('unit.index') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[6][0] }}</a></li> @endcan
          @can($menu_mdata[7][1]) <li><a href="{{ url('jenispasal') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[7][0] }}</a></li> @endcan
          @can($menu_mdata[8][1]) <li><a href="{{ url('tatacara') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[8][0] }}</a></li> @endcan
        </ul>
      </li>
      @endcanany
      <li class="treeview" hidden>
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>LM</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Setting Report</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Report 1</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Report 2</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Report 4</a></li>
        </ul>
      </li>
      <li class="treeview" hidden>
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>REPORTING</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Setting Report</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Report 1</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Report 2</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Report 4</a></li>
        </ul>
      </li>
    </ul>
    @endauth
    
  </section>
  <!-- /.sidebar -->
</aside>
