

<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img class="logo-sidebar" src="{{ asset('images/logo-white-pins.png') }}"></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>SuperSlim</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        @auth
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                {{-- task dispatch --}}

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user fa-lg"></i>
                            <span class="hidden-xs">{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</span>
                            <i class="fa fa-caret-down" style="margin-left: 1em;"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user-header">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{ asset('images/round-logo-pins.png') }}" class="img-circle" alt="User Image">

                                <p>
                                    {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}
                                    <small>PT PINS Indonesia</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <!-- <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                </div>
                            </li> -->
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <a class="btn btn-flat btn-logout" style="color:black" href="{{ route('logout') }}" onClick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign out
                                    <div class="text-center">
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            @endauth

        </nav>
    </header>
