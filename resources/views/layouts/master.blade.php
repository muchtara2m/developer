
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href=" {{ asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href=" {{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href=" {{ asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
    {{-- import pdf --}}
    <script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
    {{-- timeago --}}
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    {{-- <script src="{{ asset('js/jquery.timeago.js') }}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.4.3/jquery.timeago.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
    <script>
        var $j = jQuery.noConflict();
        $j(document).ready(function() {
            $j("time.timeago").timeago();
        });
    </script>
    <style>
        .main-sidebar{
            width: 270px;
        }
    </style>
    @yield('stylesheets')
    
    <!-- Theme style -->
    <link rel="stylesheet" href=" {{ asset('adminlte/dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href=" {{ asset('adminlte/dist/css/skins/_all-skins.min.css') }}">
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
        @yield('customstyle')
        <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/fadstyle.css') }}">
    </head>
    
    <body class="hold-transition skin-blue-light sidebar-mini ">
        <div class="wrapper">
            
            @include('layouts.includes.header')
            @include('layouts.includes.sidebar')
            
            @yield('content')
            
            @include('layouts.includes.footer')
            {{-- @include('layouts.includes.control-sidebar') --}}
            
        </div>
        <!-- ./wrapper -->
        {{-- plugins bahasa indonesia --}}
        
        <!-- jQuery 3 -->
        <script src="{{ asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <!-- FastClick -->
        <script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="{{ asset('adminlte/dist/js/demo.js') }}"></script>
        <!-- Sidebar active menu -->
        <script type="text/javascript">
            var bulan = $('#bulan');
            var tahun  = $('#tahun');
            var urlnya = "";
            var d = new Date();
            var minTahun = (d.getFullYear()-5);
            const months = ['Pilih Bulan','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
            months.forEach(function(i,index){
                bulan.append("<option value="+index+">"+i +"</option>")
            })
            for(let i = d.getFullYear(); i>minTahun ; i--){
                tahun.append('<option value="'+i+'">'+i+'</option>')
            }
            var url = window.location;
            // for sidebar menu but not for treeview submenu
            $('ul.sidebar-menu a').filter(function() {
                return this.href == url;
            }).parent().siblings().removeClass('active').end().addClass('active');
            // for treeview which is like a submenu
            $('ul.treeview-menu a').filter(function() {
                return this.href == url;
            }).parentsUntil(".sidebar-menu > .treeview-menu").siblings().removeClass('active').end().addClass('active');
        </script>
        @yield('scripts')
        
    </body>
    </html>
    