@extends('layouts.master')

@php
$homelink = "/home";
$crmenu = "Notifikasi ";
$crsubmenu = "Notifikasi";
$submenulink = "/unit";
$cract = "Notifikasi";
@endphp

@section('title')
{{ $crsubmenu." | SuperSlim" }}
@endsection

@section('stylesheets')
@endsection

@section('customstyle')
<style type="text/css">
  .form-horizontal .form-group {
    margin-right: unset;
    margin-left: unset;
  }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ $crsubmenu }}
      <!-- <small>Form PBS</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li>{{ $crmenu }}</li>
      <li><a href="{{ $submenulink }}">{{ $crsubmenu }}</a></li>
      <li class="active">{{ $cract }} </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        @if (\Session::has('error'))
        <div class="alert alert-success">
            <p>{{ \Session::get('error') }}</p>
        </div><br />
        @endif

    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-file-text"></i> Testing Notifikasi </h3>
            <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form method="POST" action="{{ url('notif') }}" enctype="multipart/form-data" class="form">
            @csrf
            <div class="box-body">

              @if (count($errors) > 0)
              <div class="alert alert-danger">
                There was a problem, please check your form carefully.
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif

              {{-- <div class="form-group col-md-6 col-md-offset-3">
                <label>No Whatsapp</label>
                <input name="telp" type="text" class="form-control"  placeholder="62xxxxxxxxx" autofocus required>
              </div> --}}
              <div class="form-group col-md-6 col-md-offset-3">
                    <label for="tglbakn">Nomer WA</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                           +62
                        </div>
                        <input type="text" placeholder="812xxxxxxxxxx" name="telp" class="form-control" value="{{ old('telp') }}">
                    </div>
                </div>
                <div class="form-group col-md-6 col-md-offset-3">
                  <label for="tglbakn">Pesan</label>
                  <div class="input-group">
                      <div class="input-group-addon">
                         <i class="fa fa-envelope"></i>
                      </div>
                      <input type="text" placeholder="Assalamualaikum" name="sms" class="form-control" value="{{ old('telp') }}">
                  </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success col-md-3 col-md-offset-8" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
@endsection
