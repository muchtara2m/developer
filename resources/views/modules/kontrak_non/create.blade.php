@extends('layouts.master')

@php
if (isset($sp3)) {
    $nomor = $sp3->id;
    $jeniskontrak = $sp3->bakns['jenis_kontrak'];
    $tanggalbakn = $sp3->bakns['tglbakn'];
    $lastupdate = $sp3->bakns['updated_at'];
    $judul = $sp3->bakns->spph['judul'];
    $nomorspph = $sp3->bakns->spph['nomorspph'];
    $tanggalspph = $sp3->bakns->spph['tglspph'];
    $nomorsph = $sp3->bakns->spph['nomorsph'];
    $tanggalsph = $sp3->bakns->spph['tglsph'];
    $perusahaan = $sp3->bakns->spph->mitras['perusahaan'];
    $alamat = $sp3->bakns->spph->mitras['alamat'];
    $direktur = $sp3->bakns->spph->mitras['direktur'];
    $nomorsp3 = $sp3->nosp3;
    $tanggalsp3 = $sp3->tglsp3;
    $harga = $sp3->bakns['harga'];
}
if (isset($bakns)) {
    $nomor = $bakns->id;
    $jeniskontrak = $bakns->jenis_kontrak;
    $tanggalbakn = $bakns->tglbakn;
    $lastupdate = $bakns->updated_at;
    $judul = $bakns->spph['judul'];
    $nomorspph = $bakns->spph['nomorspph'];
    $tanggalspph = $bakns->spph['tglspph'];
    $nomorsph = $bakns->spph['nomorsph'];
    $tanggalsph = $bakns->spph['tglsph'];
    $perusahaan = $bakns->spph->mitras['perusahaan'];
    $alamat = $bakns->spph->mitras['alamat'];
    $direktur = $bakns->spph->mitras['direktur'];
    $harga = $bakns['harga'];
}
$homelink = "/home";
$crpagename = "Create Kontrak";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
@endsection
@section('customstyle')

<style type="text/css">

    <!-- Froala -->
    hr {
        border-top: 1px solid #000;
    }

    table tr:not(:first-child){
        cursor: pointer;transition: all .25s ease-in-out;
    }
    table tr:not(:first-child):hover{background-color: #ddd;}
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
    .pulse {
        width: 20%;
        --color: #ef6eae;
        --hover: #ef8f6e;
    }
    .pulse:hover,
    .pulse:focus {
        -webkit-animation: pulse 1s;
        animation: pulse 1s;
        box-shadow: 0 0 0 2em rgba(255, 255, 255, 0);
    }

    @-webkit-keyframes pulse {
        0% {
            box-shadow: 0 0 0 0 var(--hover);
        }
    }

    @keyframes pulse {
        0% {
            box-shadow: 0 0 0 0 var(--hover);
        }
    }
    .close:hover,
    .close:focus {
        box-shadow: inset -3.5em 0 0 0 var(--hover), inset 3.5em 0 0 0 var(--hover);
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Kontrak
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Kontrak</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                        <br>
                        <br>
                        <a href="{{ url('bakn-preview', $bakns['id']) }}" target="_blank" class="btn-round pull-right" title="Preview BAKN">Preview BAKN</a>
                        <td class="btn-round pull-right">
                                @php
                                if($bakns->file == NULL){
                                }else{
                                    $title = json_decode($bakns->title, TRUE);
                                    $file = json_decode($bakns->file, TRUE);
                                    $i=1;
                                    foreach ($title as $key => $value) {
                                        echo $i++.'. <a href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';
                                    }
                                }
                                @endphp
                            </td>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <form method="post" action="{{ action('KontrakNonController@store') }}" enctype="multipart/form-data">
                        <input type="hidden" name="idbakn" value="">
                        @csrf
                        <div class="box-body">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                There was a problem, please check your form carefully.
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="box-body">
                                <div class="col-md-12">
                                    <div class="form-group col-md-6 igroup">
                                        <label for="exampleInputEmail1">Nomor Kontrak</label>
                                        <input type="text" class="form-control" name="nokontrak"  id="nmrkontrak" value="{{ old('nokontrak') }}">
                                        @if(isset($sp3))
                                        <input type="hidden" name="sp3" value="{{ $nomor }}">
                                        @else
                                        <input type="hidden" name="bakn" value="{{ $nomor }}">
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6 igroup">
                                        <label for="tglbakn">Tanggal Kontrak</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos" name="tglkontrak" id="enddelivery-date" value="{{ old('tglkontrak') }}">
                                        </div>
                                    </div>
                                </div>

                                @if($jeniskontrak == 'Custom')
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1" style="font-size: 20px;">Pasal</label>
                                        <br>
                                        <input type="checkbox" name="all" id="all" /> <label for='all'>Pilih Semua </label>
                                        <br>
                                        @foreach ($pasals as $list)
                                        <label class="checkbox-inline">
                                            <input type="checkbox" class="dd" id="pilih{{ $list->pasal }}" name="pasal" value="<p><strong>PASAL {{ $list->pasal }}<p>{{ $list->judul }}</strong>{{ $list->isi }}"> {{ $list->judul }}
                                            </label>
                                            @endforeach
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            @php
                                            //date_default_timezone_set('Asia/Jakarta');
                                            setlocale(LC_TIME, "id_ID");

                                            $tglbakn = $tanggalbakn;
                                            $date1 = str_replace('-', '/', $tglbakn);
                                            $tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
                                            $hari = date('l', strtotime($tomorrow));
                                            $bulan = date('F', strtotime($tomorrow));

                                            $dtnow = date('d-m-Y', strtotime($tomorrow));
                                            $tanggal = date('d', strtotime($tomorrow));
                                            $tahun = date('Y', strtotime($tomorrow));
                                            $f = new NumberFormatter("id", NumberFormatter::SPELLOUT);
                                            //echo $f->format($tanggal);
                                            $d = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lastupdate);
                                            $sesok =  $d->addDays(1);

                                            @endphp
                                            <textarea name="isi" id="isi" cols="180" rows="10">
                                                @if ($jeniskontrak == 'KHS')
                                                <p style="text-align: center;"><strong>KONTRAK HARGA SATUAN (KHS)</strong></p>
                                                @else
                                                <p style="text-align: center;"><strong>PERJANJIAN KERJASAMA</strong></p>
                                                @endif
                                                <p style="text-align: center;"><strong>TENTANG</strong></p>
                                                <p style="text-align: center;"><strong>{{ strtoupper($judul) }}</strong></p>
                                                <p style="text-align: center;"><strong>PT. PINS INDONESIA&nbsp;</strong></p>
                                                <p style="text-align: center;"><strong>DENGAN</strong></p>
                                                <p style="text-align: center;"><strong>{{ strtoupper($perusahaan) }}</strong></p>
                                                <p style="text-align: center;">
                                                    <strong>
                                                        <u>___________________________________________________________________________________________</u>
                                                        {{--  <u>_____________________________________________________________________________</u>  --}}
                                                    </strong>
                                                </p>

                                                <p style="text-align: center;"><u><strong id="nmrnya">Nomor :</strong></u></p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>

                                                <div title="Page 1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                                    {{--  <p style="text-align: justify;">Pada hari ini, <strong>{{ Carbon\Carbon::parse($sesok)->formatLocalized('%A') }}</strong>, tanggal <strong>{{ $f->format($tanggal) }}</strong>, bulan <strong>{{ Carbon\Carbon::parse($sesok)->formatLocalized('%B') }}</strong> tahun <strong>{{ $f->format($tahun) }}</strong>, (<strong>{{ $dtnow }}</strong>), bertempat di Jakarta, oleh dan antara pihak-pihak:</p>  --}}
                                                    <p style="text-align: justify;">Pada hari ini, <strong>........................</strong>, tanggal <strong>.........</strong>, bulan <strong>...............</strong> tahun <strong>.............</strong>, (<strong>..........</strong>), bertempat di Jakarta, oleh dan antara pihak-pihak:</p>

                                                    <ol style="list-style-type: upper-roman;">
                                                        <li style="text-align: justify;"><strong>PT. PINS INDONESIA</strong>, perseroan terbatas yang didirikan berdasarkan hukum Negara Republik Indonesia, berkedudukan di Telkom Landmark Tower lantai 42-43, Jl. Gatot Subroto No.Kav. 52, Kuningan Barat, Mampang Prapatan, Kota Jakarta Selatan Daerah Khusus Ibukota Jakarta 12710, dalam perbuatan hukum ini diwakili secara sah oleh <strong>{{ strtoupper($end->name) }}</strong>, Jabatan <strong>{{ strtoupper($end->position) }}</strong>, selanjutnya disebut PINS;
                                                            <br>
                                                            <br>
                                                        </li>
                                                        <li style="text-align: justify;"><strong>{{$perusahaan }}</strong>, perusahaan yang didirikan berdasarkan hukum Negara Republik Indonesia, berkedudukan di {{ $alamat }}, dalam perbuatan hukum ini diwakili secara sah oleh <strong> {{ $direktur }}</strong>, Jabatan <strong> Direktur Utama</strong>, selanjutnya disebut SUPPLIER.
                                                            <br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</li>
                                                        </ol>
                                                        <p style="text-align: justify;"><strong>PINS</strong> dan <strong>SUPPLIER</strong> apabila secara bersama-sama untuk selanjutnya disebut &rdquo;Para Pihak&rdquo;.</p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <p style="text-align: justify;">Dengan terlebih dahulu mempertimbangkan hal-hal sebagai berikut :</p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <ol style="list-style-type: lower-alpha;">
                                                            <li style="text-align: justify;">bahwa PINS bermaksud menjalin kerjasama dengan SUPPLIER dalam Pekerjaan {{ $judul }}.</li>
                                                            <li>
                                                                <p style="text-align: justify;">Bahwa PINS telah menyampaikan kepada SUPPLIER No. {{ $nomorspph }} tanggal {{ date('d F Y', strtotime($tanggalspph)) }} Perihal Surat Permintaan Penawaran Harga;</p>
                                                            </li>
                                                            <li>
                                                                <p style="text-align: justify;">Bahwa SUPPLIER telah menyampaikan kepada PINS surat No. {{ $nomorsph }} tanggal {{ date('d F Y', strtotime($tanggalsph)) }} perihal Penawaran Harga;</p>
                                                            </li>
                                                            <li>
                                                                <p style="text-align: justify;">bahwa Para Pihak telah melaksanakan rapat klarifikasi dan negosiasi pada tanggal {{ date('d F Y', strtotime($tanggalbakn)) }} yang hasilnya dituangkan dalam Berita Acara Klarifikasi dan Negosiasi;</p>
                                                            </li>
                                                            @if(isset($sp3))
                                                            <li>
                                                                <p>bahwa PINS telah menyampaikan kepada SUPPLIER Surat Nomor: {{ $nomorsp3 }} tanggal {{ date('d F Y', strtotime($tanggalsp3)) }} Perihal Surat Penetapan Pelaksanaan Pekerjaan &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                    <br>
                                                                    <br>
                                                                </p>
                                                            </li>
                                                            @endif
                                                        </ol>
                                                        <p style="text-align: justify;">Setelah mempertimbangkan hal-hal tersebut di atas, Para Pihak sepakat untuk mengikatkan diri satu kepada yang lain dan dituangkan dalam Perjanjian Pekerjaan {{ $judul }} (selanjutnya disebut &ldquo;Perjanjian&rdquo;) dengan ketentuan dan syarat-syarat sebagai berikut :</p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <p>
                                                            <br>
                                                        </p>&nbsp; &nbsp;</div>
                                                        @if($jeniskontrak != 'Custom')
                                                        <p>{{ $jenispasal->isi_pasal }}</p>
                                                        @endif
                                                        <p id="akhir"></p>
                                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width: 226pt;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="49.42528735632184%">

                                                                        <p style="text-align:center;"><strong><span style='font-size:15px;font-family:"Calibri",sans-serif;'>PT. PINS INDONESIA</span></strong></p>
                                                                    </td>
                                                                    <td style="width: 231.2pt;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="50.57471264367816%">

                                                                        <p style="text-align:center;"><strong><span style='font-size:15px;font-family:"Calibri",sans-serif;'>PT.&nbsp;</span></strong><strong><span style='font-size:15px;font-family:  "Calibri",sans-serif;'>BISMACINDO PERKASA</span></strong></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 226pt;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="49.42528735632184%">

                                                                        <p style="text-align:center;">
                                                                            <br>
                                                                        </p>

                                                                        <p style="text-align:center;">
                                                                            <br>
                                                                        </p>

                                                                        <p style="text-align:center;">
                                                                            <br>
                                                                        </p>

                                                                        <p style="text-align:center;">
                                                                            <br>
                                                                        </p>

                                                                        <p>
                                                                            <br>
                                                                        </p>

                                                                        <p>
                                                                            <br>
                                                                        </p>

                                                                        <p style="text-align:center;"><strong><u><span style='font-size:15px;font-family:"Calibri",sans-serif;'>{{ $end->name }}</span></u></strong></p>

                                                                        <p style="text-align:center;"><span style='font-size:15px;font-family:"Calibri",sans-serif;'>{{ $end->position }}</span></p>
                                                                    </td>
                                                                    <td style="width: 231.2pt;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="50.57471264367816%">

                                                                        <p style="text-align:center;">
                                                                            <br>
                                                                        </p>

                                                                        <p style="text-align:center;">
                                                                            <br>
                                                                        </p>

                                                                        <p style="text-align:center;">
                                                                            <br>
                                                                        </p>

                                                                        <p style="text-align:center;">
                                                                            <br>
                                                                        </p>

                                                                        <p style="text-align:center;">
                                                                            <br>
                                                                        </p>

                                                                        <p style="text-align:center;">
                                                                            <br>
                                                                        </p>
                                                                        <p style="text-align:center;"><strong><u><span style='font-size:15px;font-family:"Calibri",sans-serif;color:black;'>{{ $direktur }}</span></u></strong></p>
                                                                        <p style="text-align:center;"><span style='font-size:15px;font-family:"Calibri",sans-serif;'>Direktur Utama</span></p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <p>
                                                        </textarea>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="">Lampiran</label>
                                                            <input type="file" name="lampiran[]" id="" class="form-control" multiple="multiple">

                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="">Comment</label>
                                                            <input type="text" name="chat" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <input type="text" name="created_by" value="{{ Auth::id() }}" hidden>
                                                            <button type="submit" class="btn btn-success" name="status" value="draft_kontrak">Save</button>
                                                            <button type="submit" class="btn btn-primary" name="status" value="save_kontrak">Submit</button>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.box-body -->

                                    </form>
                                </div>
                                <!-- /.box -->
                            </div>

                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->
                @endsection

                @section('scripts')

                <!-- Include Editor JS files. -->
                <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
                <script src="{{ asset('froala/js/languages/id.js') }}"></script>

                <!-- date-range-picker -->
                <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
                <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
                <!-- bootstrap datepicker -->
                <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
                <!-- iCheck 1.0.1 -->
                <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
                <!-- clockpicker -->
                <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
                <script>
                    $(function () {
                        $(".datejos").on("change", function () {
                            this.setAttribute(
                            "data-date",
                            moment(this.value, "YYYY-MM-DD")
                            .format(this.getAttribute("data-date-format"))
                            )
                        }).trigger("change")
                    });
                    $('.datejos').datepicker({
                        autoclose: true,
                        orientation: "bottom"
                    });
                    $('#nmrkontrak').change(function() {
                        var nomor = $('#nmrkontrak').val();
                        document.getElementById('nmrnya').innerHTML = "Nomor : "+ nomor;

                        console.log(nomor);
                        // $('#nmrnya').froalaEditor('html.set', nomor);

                    });
                    $(function() {
                        setTarget();
                        setPasal();
                        // $("#judul,#tgl,#handler").change(setTarget);
                        $("#tgl,#handler").change(setTarget);
                        var idnya = [
                        @foreach($pasals as $tes)
                        $("#pilih{{ $tes->pasal }}").change(setPasal),
                        @endforeach
                        ]
                        function setTarget() {
                            var tmp = $("#tgl").val()+" ";
                            tmp += $("#handler").val()+" ";
                        }
                        function setPasal() {
                            $(document).ready(function(){
                                $('input[name="all"],input[name="title"]').bind('click', function(){
                                    var status = $(this).is(':checked');
                                    $('.dd').attr('checked', status);
                                    var isi = "";
                                    @foreach($pasals as $pasal)
                                    isi += $("#pilih{{ $pasal->pasal }}:checked").val() || '';
                                    @endforeach
                                    document.getElementById('akhir').innerHTML = isi;
                                    var isi = "";

                                });
                            });
                            var isi = "";
                            @foreach($pasals as $pasal)
                            isi += $("#pilih{{ $pasal->pasal }}:checked").val() || '';
                            @endforeach
                            document.getElementById('akhir').innerHTML = isi;
                            var isi = "";

                        }
                    });
                    $('textarea').froalaEditor({
                        documentReady: true,
                        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo', 'getPDF'],
                        language: 'id',
                        key: '{{ env("KEY_FROALA") }}',
                    })
                    $('#submit').click(function(e){
                        var helpHtml = $('div#froala-editor').froalaEditor('html.get'); // Froala Editor Inhalt auslesen
                        $.post( "{{ action('KontrakController@store') }}", { helpHtml:helpHtml });
                    });</script>

                    <!-- Data Table -->
                    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
                    <!-- date-range-picker -->
                    <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
                    <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
                    <!-- bootstrap datepicker -->
                    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
                    <!-- iCheck 1.0.1 -->
                    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
                    <!-- clockpicker -->
                    <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
                    <!-- Select2 -->
                    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
                    <!-- CK Editor -->
                    {{-- <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script> --}}

                    @endsection
