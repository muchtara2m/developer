@extends('layouts.master')

@section('title')
Dispatch Kontrak| Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            KONTRAK DISPATCH
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active"> Kontrak Dispatch </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Kontrak</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="nav-tabs-custom">
                        <div style="text-align:center;padding-bottom:10px">
                            <div class="row input-daterange">
                                <form method="get" action="{{ url('kontrak-non-listdisp') }}" enctype="multipart/form-data">
                                    <div class="col-md-4">
                                        <select name="bulan" id="bulan" class="form-control">
                                            @php
                                            if(request()->get('bulan') == null){
                                                $bln = date('m');
                                                $bulan = date('F',strtotime(date('Y-m-d')));
                                                $thn = date('Y');
                                            }else{
                                                $bln = request()->get('bulan');
                                                $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                                $thn = request()->get('tahun');
                                            }
                                            @endphp
                                            <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tahun" id="tahun" class="form-control">
                                            <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                        </select>
                                    </div>
                                    <input type="submit" value="Filter" class="btn btn-primary">
                                    
                                    <div class="col-md-4">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_roles" data-toggle="tab">BAKN</a></li>
                            <li><a href="#tab_permissions" data-toggle="tab">SP3</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="tab_permissions">
                                <div class="box-body table-responsive">
                                    <table id="rolesTable" class="display">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>IO</th>
                                                <th>Deskripsi IO</th>
                                                <th>Nomor SPPH</th>
                                                <th>Judul SPPH</th>
                                                <th>Tangal BAKN</th>
                                                <th>Nomor SP3</th>
                                                <th>Harga</th>
                                                <th>Jenis Kontrak</th>
                                                <th>Handler</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no=1;
                                            @endphp
                                            @foreach ($sp3 as $list)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $list->sp3s->bakns->io['no_io'] }}</td>
                                                <td>{{ $list->sp3s->bakns->io['deskripsi'] }}</td>
                                                <td>{{ $list->sp3s->bakns->spph['nomorspph'] }}</td>
                                                <td>{{ $list->sp3s->bakns->spph['judul'] }}</td>
                                                <td>{{ $list->sp3s->bakns['created_at'] }}</td>
                                                <td>{{ $list->sp3s->nosp3 }}</td>
                                                <td>{{ number_format($list->sp3s->bakns['harga'],0,'.','.') }}</td>
                                                <td>{{ $list->sp3->bakns['jenis_kontrak'] }}</td>
                                                <td>{{ $list->handler }}</td>
                                                <td>
                                                    <a href = {{ url('sp3-preview', $list->id) }}" class="fa fa-fw fa-file-code-o" title="Preview BAKN"></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane active" id="tab_roles">
                                <div class="box-body table-responsive">
                                    <table id="permissionsTable" class="display">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>IO</th>
                                                <th>Deskripsi IO</th>
                                                <th>Nomor SPPH</th>
                                                <th>Judul SPPH</th>
                                                <th>Tangal BAKN</th>
                                                <th>Harga</th>
                                                <th>Jenis Kontrak</th>
                                                <th>Handler</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no=1;
                                            @endphp
                                            @foreach ($bakn as $list)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $list->io['no_io'] }}</td>
                                                <td>{{ $list->io['deskripsi'] }}</td>
                                                <td>{{ $list->spph['nomorspph'] }}</td>
                                                <td>{{ $list->spph['judul'] }}</td>
                                                <td>{{ $list->created_at }}</td>
                                                <td>{{ number_format($list['harga'],0,'.','.') }}</td>
                                                <td>{{ $list->jenis_kontrak }}</td>
                                                <td>{{ $list->handler }}</td>
                                                <td>
                                                    <a href = {{ url('bakn-preview', $list->id) }}" class="fa fa-fw fa-file-code-o" title="Preview BAKN"></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
    $(document).ready( function () {
        $('#rolesTable,#permissionsTable').DataTable({
        });
    } );</script>
    @endsection
    