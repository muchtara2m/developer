@extends('layouts.master')

@section('title')
Status Kontrak | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            STATUS KONTRAK
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active">Status Kontrak </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button href="#" class="close" data-dismiss="alert" aria-label="close">&times;</button>
                            {{ $message }}
                        </div>
                        @endif
                        
                        <h3 class="box-title"><i class="fa fa-ticket"></i>Status Kontrak</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="nav-tabs-custom">
                        <div style="text-align:center;padding-bottom:10px">
                            <div class="row input-daterange">
                                <form method="get" action="{{ url('kontrak-non-status') }}" enctype="multipart/form-data">
                                    <div class="col-md-4">
                                        <select name="bulan" id="bulan" class="form-control">
                                            @php
                                            if(request()->get('bulan') == null){
                                                $bln = date('m');
                                                $bulan = date('F',strtotime(date('Y-m-d')));
                                                $thn = date('Y');
                                            }else{
                                                $bln = request()->get('bulan');
                                                $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                                $thn = request()->get('tahun');
                                            }
                                            @endphp
                                            <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tahun" id="tahun" class="form-control">
                                            <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                        </select>
                                    </div>
                                    <input type="submit" value="Filter" class="btn btn-primary">
                                    
                                    <div class="col-md-4">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_roles" data-toggle="tab">BAKN</a></li>
                            <li><a href="#tab_permissions" data-toggle="tab">SP3</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="tab_permissions">
                                <div class="box-body table-responsive">
                                    <table id="rolesTable" class="display">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>No IO</th>
                                                <th>Deskripsi IO</th>
                                                <th>Nomor SPPH</th>
                                                <th>Nomor Kontrak</th>
                                                <th>Tanggal BAKN</th>
                                                <th>No SP3</th>
                                                <th>Judul</th>
                                                <th>Mitra</th>
                                                <th>Harga</th>
                                                <th>Posisi</th>
                                                <th>Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no=1;
                                            @endphp
                                            @foreach ($sp3 as $list)
                                            <tr style=" white-space: nowrap">
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $list->sp3s->bakns->io['no_io'] }}</td>
                                                <td>{{ $list->sp3s->bakns->io['deskripsi'] }}</td>
                                                <td>{{ $list->sp3s->bakns->spph['nomorspph'] }}</td>
                                                <td>{{ $list->nokontraknon }}</td>
                                                <td>{{ $list->sp3s->bakn['tglbakn'] }}</td>
                                                <td>{{ $list->sp3s->nosp3 }}</td>
                                                <td>{{ $list->sp3s->bakns->spph['judul'] }}</td>
                                                <td>{{ $list->sp3s->bakns->spph->mitras['perusahaan'] }}</td>
                                                <td>{{ number_format($list->sp3s->bakns['harga']) }}</td>
                                                <td>{{ $list->approval }}</td>
                                                <td>
                                                    <a href="{{ url('kontrak-non-preview-status', $list->id)}}" class="fa fa-fw fa-check" title="Approve Kontrak"></a>
                                                    
                                                    
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            
                            <div class="tab-pane active" id="tab_roles">
                                <div class="box-body table-responsive">
                                    <table id="permissionsTable" class="display">
                                        <thead>
                                            <tr style=" white-space: nowrap; text-align:center">
                                                <th style="text-align:center">No</th>
                                                <th style="text-align:center">No IO</th>
                                                <th style="text-align:center">Deskripsi IO</th>
                                                <th style="text-align:center">Nomor SPPH</th>
                                                <th style="text-align:center">Nomor Kontrak</th>
                                                <th style="text-align:center">Tanggal BAKN</th>
                                                <th style="text-align:center">Judul</th>
                                                <th style="text-align:center">Mitra</th>
                                                <th style="text-align:center">Harga</th>
                                                <th style="text-align:center">Pembuat</th>
                                                <th style="text-align:center">Posisi</th>
                                                <th style="text-align:center">Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no=1;
                                            @endphp
                                            @foreach ($bakn as $list)
                                            <tr style=" white-space: nowrap">
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $list->bakns->io['no_io'] }}</td>
                                                <td>{{ $list->bakns->io['deskripsi'] }}</td>
                                                <td>{{ $list->bakns->spph['nomorspph'] }}</td>
                                                <td>{{ $list->nokontraknon }}</td>
                                                <td>{{ $list->bakns['tglbakn'] }}</td>
                                                <td>{{ $list->bakns->spph['judul'] }}</td>
                                                <td>{{ $list->bakns->spph->mitras['perusahaan'] }}</td>
                                                <td>{{ number_format($list->bakns['harga']) }}</td>
                                                <td>{{ $list->users['name'] }}</td>
                                                <td>{{ $list->approval }}</td>
                                                <td>
                                                    <a href="{{ url('kontrak-non-preview-status', $list->id)}}" class="fa fa-fw fa-sticky-note-o" title="Approve Kontrak"></a>
                                                    {{-- <a href="{{ url('kontrak-non-print_preview', $list->id)}}" class="fa fa-file-word-o"></a> --}}
                                                    @if($list->approval == 'Return' && $list->created_by == Auth::user()->id)
                                                    <a href="{{ url('kontrak-non-edit', $list->id)}}" class="fa fa-fw fa-edit" title="Edit Kontrak"></a>
                                                    
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                </div>
                <!-- /.box -->
                <div class="box-body table-responsive">
                    
                </div>
                
                
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
    $(document).ready( function () {
        $('#rolesTable,#permissionsTable').DataTable({
        });
    } );</script>
    @endsection
    