@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "BAKN";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('customstyle')

<style type="text/css">
    table, th, td {
        border: 1px solid black;
    }

    .image-uploader input.main {
        position: absolute;
        right: 0;
        margin: 0;
        opacity: 0;
        -webkit-transform-origin: right;
        -moz-transform-origin: right;
        -ms-transform-origin: right;
        -o-transform-origin: right;
        transform-origin: right;
        -webkit-transform: scale(14);
        -moz-transform: scale(14);
        -ms-transform: scale(14);
        -o-transform: scale(14);
        transform: scale(14);
        font-size: 23px;
        direction: ltr;
        cursor: pointer;
    }

    .image-uploader {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        margin: 1.6em 0;
        position: relative;
        overflow: hidden;
        padding: 55px 60px;
        border: #edece4 3px dashed;
        width: 100%;
        height: auto;
        text-align: center;
        color: #aaa9a2;
        background: #F9F8F5;
    }

    #pelangi:hover {
        -webkit-animation: zomg 0.5s infinite;
        animation: zomg 0.5s infinite;
    }

    @-webkit-keyframes zomg {
        0%, 100% { color: #7ccdea; }
        16%      { color: #0074d9; }
        32%      { color: #2ecc40; }
        48%      { color: #ffdc00; }
        64%      { color: #b10dc9; }
        80%      { color: #ff4136; }
    }
    @keyframes zomg {
        0%, 100% { color: #7ccdea; }
        16%      { color: #0074d9; }
        32%      { color: #2ecc40; }
        48%      { color: #ffdc00; }
        64%      { color: #b10dc9; }
        80%      { color: #ff4136; }
    }
    .class1 tbody tr:nth-child(2n) {
        background: #f9f9f9;
    }

    .class2 thead tr th,
    .class2 tbody tr td {
        border-style: dashed;
    }
    textarea#froala{
        width: -webkit-fill-available;
    }
    .tengah{
        background-color: dimgrey;
        text-align: center;
    }
    div span.fr-counter{
        display:none;
    }
    strong.judul{
        text-align: center
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Preview BAKN</h3>
                        <button onclick="window.history.go(-1); return false;" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                    </div>
                    @if($chat != NULL && Auth::user()->level == 'mgrlegal')
                    <div class="form-group col-md-12" >
                        <label for="exampleInputEmail1">Comment</label>
                        {{ $bakn->id }}
                        <textarea name="" id="chat" cols="30" rows="10">
                            {{ $chat->chat }}
                        </textarea>
                    </div>
                    @endif
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="white-box">
                                {{-- <form method="POST" action="#"  enctype="multipart/form-data"> --}}
                                    <textarea id="froala-editor">
                                        <table class="table" >
                                            <tbody>
                                                <tr>
                                                    <td rowspan="4" colspan="1" style="text-align:center; vertical-align:middle;"><img src="{{ asset('images/pinlogo.png') }}" width="190" /></td>
                                                    <td colspan="3">
                                                        <div style="text-align: center;">
                                                            <strong class="judul">
                                                                <center>BERITA ACARA KLARIFIKASI & NEGOSIASI</br>
                                                                    {{ strtoupper($bakn->spph['judul']) }}</br>
                                                                    ANTARA</br>
                                                                    PT. PINS INDONESIA</br>
                                                                    DENGAN</br>
                                                                    <span id="phk2">{{ strtoupper($bakn->spph->mitras['perusahaan']) }}</span></center>
                                                                </strong>
                                                                <input type="hidden" id="hd_phk2" value="{{ $bakn['secondholder'] }}" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal</td>
                                                        <td colspan="3">{{ Carbon\Carbon::parse($bakn['tglbakn'])->formatLocalized('%d %B %Y') }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Waktu</td>
                                                        <td colspan="3">- WIB</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tempat</td>
                                                        <td colspan="3">Kantor PT. PINS INDONESIA</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="1">Undangan dari</td>
                                                        <td colspan="3">PT. PINS INDONESIA</td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="1">Tipe Rapat</td>
                                                        <td colspan="3">
                                                            <ul class="icheck-list">
                                                                <li style="display: inline;">
                                                                    <input type="checkbox" class="check" id="minimal-checkbox-1" {{ in_array("Review",$checkbox)? "checked":"" }}>
                                                                    <label for="minimal-checkbox-1">Review</label>
                                                                </li>
                                                                <li style="display: inline;">
                                                                    <input type="checkbox" class="check" id="minimal-checkbox-2" {{ in_array("Coordination",$checkbox)? "checked":"" }}>
                                                                    <label for="minimal-checkbox-2">Coordination</label>
                                                                </li>
                                                                <li style="display: inline;">
                                                                    <input type="checkbox" class="check" id="minimal-checkbox-3" {{ in_array("Briefing",$checkbox)? "checked":"" }}>
                                                                    <label for="minimal-checkbox-disabled">Briefing</label>
                                                                </li>
                                                                <li style="display: inline;">
                                                                    <input type="checkbox" class="check" id="minimal-checkbox-4" {{ in_array("Decision Marking",$checkbox)? "checked":"" }}>
                                                                    <label for="minimal-checkbox-disabled-checked">Decision
                                                                        Making</label>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="1">Pimpinan Rapat</td>
                                                            <td colspan="3">{{ $bakn['pimpinan_rapat'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="1">Peserta</td>
                                                            <td colspan="2"><strong>{{ strtoupper($bakn->spph->mitras['perusahaan'])}}:</strong>
                                                                <ol>
                                                                    {{--  @php
                                                                        $datas = json_decode($bakn['peserta_mitra'], true);
                                                                        for ($i=0; $i<count($datas); $i++) {
                                                                            echo '<li>'.$datas[$i]["value"].'</li>';
                                                                        }
                                                                        @endphp  --}}
                                                                        {{ $bakn->peserta_mitra }}
                                                                    </ol>
                                                                </td>
                                                                <td colspan="2">
                                                                    <strong>PT. PINS INDONESIA :</strong>
                                                                    <ol>
                                                                        @php
                                                                        $datas = json_decode($bakn['peserta_pins'], true);
                                                                        for ($i=0; $i<count($datas); $i++) {
                                                                            echo '<li>'.$datas[$i]["value"].'</li>';
                                                                        }
                                                                        @endphp
                                                                    </ol>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" class="tengah" style="background-color: rgb(204, 204, 204);">
                                                                    <strong>
                                                                        <center>AGENDA</center>
                                                                    </strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" >
                                                                    <center>
                                                                        {{ $bakn['agenda'] }}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" class="tengah" style="background-color: rgb(204, 204, 204);">
                                                                        <strong>
                                                                            <center>DASAR PEMBAHASAN</center>
                                                                        </strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" >
                                                                        <!-- {{ $bakn }}  -->
                                                                        {{ $bakn['dasar_pembahasan'] }}

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" class="tengah" style="background-color: rgb(204, 204, 204);">
                                                                        <strong>
                                                                            <center>HASIL - HASIL KLARIFIKASI & NEGOSIASI</center>
                                                                        </strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4">
                                                                        SUPPLIER dan PT. PINS Indonesia sepakat melakukan
                                                                        klarifikasi dan negosiasi dengan hasil sebagai berikut
                                                                        :
                                                                    </br>
                                                                    <b>1. Ruang Lingkup :</b>
                                                                    {{ $bakn['ruang_lingkup'] }}
                                                                </br>
                                                                <b>2. Lokasi Pekerjaan/ Pengiriman Barang/ Jasa :</b>
                                                                {{ $bakn['lokasi_pekerjaan'] }}
                                                            </br>
                                                            <b>3. Jangka Waktu Pengiriman Barang :</b>
                                                            {{ $bakn['jangka_waktu'] }}
                                                        </br>
                                                        <br><b>4. Harga :</b>
                                                        {{ $bakn['harga_terbilang'] }}
                                                    </br>
                                                    <b>5. Tatacara Pembayaran :</b>

                                                    {{ $bakn['cara_bayar'] }}
                                                </br>
                                                <b>6. Lain - lain :</b>
                                                {{ $bakn['lain_lain'] }}
                                            </br>
                                        </br>
                                        Demikian Berita Acara Klarifikasi dan Negosiasi
                                        ini dibuat.
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" width="50%">
                                        <p style="text-align:center">{{ $bakn->spph->mitras['perusahaan'] }}</p>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <p style="text-align:center">{{ $bakn->spph->mitras['direktur'] }}</p>

                                    </td>
                                    <td colspan="2" width="50%">
                                        <p style="text-align:center">PT PINS INDONESIA</p>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <p style="text-align:center">{{ $bakn->pimpinan_rapat }}</p>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </textarea>

                    <br>
                    @if($bakn->status == 'done_bakn')
                    <div class="container" style="padding-left:1%;">
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            There was a problem, please check your form carefully.
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        @if ( (Auth::user()->level == 'mgrlegal' || Auth::user()->level == 'administrator') && $bakn->kontraksnon == "[]")
                        <form class="form"  action="{{ action('BaknController@returnbakn',$bakn->id) }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">Comment</label>
                                <textarea name="chat" id="chat" class="form-control"></textarea>
                            </div>
                            <button type="submit" name="status" value="return" class="btn btn-warning" style="width: 7em;"><i class="fa fa-refresh"></i> Return</button>
                            {{-- <button type="submit" name="status" value="approve" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i>Approve</button> --}}
                        </form>
                                <br>
                                <a href="#modal_unit{{ $bakn->id }}"  data-toggle="modal" data-target="#modal-unit_{{ $bakn->id }}" class="btn btn-primary" title="Dispatch Kontrak"><i class="fa fa-user-plus"></i> Dispatch</a>

                                {{-- <a href = "return/{{ $bakn->id }}" class="btn btn-primary" onclick="return confirm('Bener nih mau direturn?')" title="Return BAKN">Return BAKN</a> --}}
                                <div class="modal fade" id="modal-unit_{{ $bakn->id }}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Pilih Tanggung Jawab</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="post" action="{{action('KontrakNonController@dispbakn', $bakn->id)}}">
                                                        @csrf
                                                        {{-- @method('PATCH') --}}
                                                        <input type="text" value="{{ $bakn->id }}" hidden>
                                                        <br>
                                                        <label for="handler">Penangung Jawab</label>

                                                        <select name="handler"  class="form-control">
                                                            <option selected disabled>Pilih Penanggung Jawab</option>
                                                            @foreach ($handlers as $handler)
                                                            <option value="{{ $handler->username }}">{{ $handler->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <br>
                                                        <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i>
                                                            Submit</button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                        @endif
                                    </div>
                                    @endif
                                    <br>

                                    {{-- </form> --}}
                                    {{-- form --}}
                                </div>
                                {{-- div.col --}}
                            </div>
                            {{-- div.row --}}
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        @endsection

        @section('scripts')
        <!-- Include Editor JS files. -->
        <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
        {{-- bahasa indonesia froala --}}
        <script src="{{ asset('froala/js/languages/id.js') }}"></script>

        <!-- Initialize the editor. -->
        <script>
            $('textarea#froala-editor').froalaEditor({
                // Define new table cell styles.
                toolbarButtons: ['print','html'],
                charCounterCount: false,
                fullPage: true,
                toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'inlineClass', 'clearFormatting', '|', 'emoticons', 'fontAwesome', 'specialCharacters', 'paragraphFormat', 'lineHeight', 'paragraphStyle', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '-', 'insertHR', 'selectAll', 'help', 'html', 'fullscreen', '|', 'undo', 'redo', 'getPDF', 'print'],
                key: '{{ env("KEY_FROALA") }}',

            })
            $('#chat').froalaEditor({
                key: '{{ env("KEY_FROALA") }}',
                height: 70,
                toolbarButtons:['help'],
                charCounterCount: false,
                // language: 'id',

            })
        </script>
        {{-- signature --}}
        <script src="{{ asset('signature/jSignature.min.js') }}"></script>
        <!-- date-range-picker -->
        <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <!-- bootstrap datepicker -->
        <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
        <!-- iCheck 1.0.1 -->
        <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
        <!-- clockpicker -->
        <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
        <!-- Select2 -->
        <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

        @endsection
