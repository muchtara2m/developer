@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Edit BAKN";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{--  Tagify  --}}
<link rel="stylesheet" href="{{ asset('tagify/dist/tagify.css') }}">
@endsection

@section('customstyle')

<style type="text/css">
    table tr:not(:first-child){
        cursor: pointer;transition: all .25s ease-in-out;
    }
    table tr:not(:first-child):hover{background-color: #ddd;}
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Edit BAKN</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if($chat != NULL)
                    <div class="form-group col-md-12" >
                        <label for="exampleInputEmail1">Comment</label>
                        {{ $bakns->id }}
                        <textarea name="" id="chat" cols="30" rows="10">
                            {{ $chat->chat }}
                        </textarea>
                    </div>
                    @endif

                    {{-- start form  --}}
                    <form method="post" action="{{ route('bakn.update',$bakns->id) }}" enctype="multipart/form-data" id="form">
                        <input type="hidden" name="spphid" value="{{ $bakns->spph_id }}">
                        @csrf

                        <div class="box-body">

                            <div class="form-group col-md-12 igroup">
                                <label for="nomorspph">Select SPPH</label><a id="spphpreview" target="_blank" href="#"><i class="fa fa-fw fa-file-code-o"></i></a>
                                <div class="input-group col-md-12">
                                    <select name="nospph" id="nospph" class="form-control">
                                        <option disabled selected>{{ $bakns->spph['nomorspph'] }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12 igroup">
                                <label for="jenis_kontrak">Jenis PKS</label>
                                <div class="input-group col-md-12">
                                    <select name="jenis_kontrak" id="jenis_kontrak" class="form-control">
                                        <option disabled selected>Pilih Jenis Kontrak</option>
                                        @foreach ($jenispasal as $item)
                                        <option value="{{ $item->jenis_pasal}}"{{   ($bakns->jenis_kontrak == $item->jenis_pasal)? 'selected':'' }}>{{ $item->jenis_pasal }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="tglbakn">Tanggal BAKN</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos" name="tglbakn" id="enddelivery-date" value="{{ $bakns->tglbakn }}">
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="harga">Harga</label>
                                <div class="input-group col-md-12">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control" name="harga" onkeyup="formatAngka(this,'.')" placeholder="1000000" id="harganum" value="{{ number_format($bakns->harga,0,'.','.') }}" >
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="pimpinanrapat">Pimpinan Rapat</label>
                                <div class="input-group col-md-12">
                                    <select name="pimpinan_rapat" id="pimpinan" class="form-control" title="Pimpinan">
                                        <option selected value="{{ $bakns->pimpinan_rapat }}">{{ $bakns->pimpinan_rapat }}</option>
                                        <option value="Revi Guspa">VP General Support</option>
                                        <option value="Sigit Sumarsono">GM Regional Timur</option>
                                        <option value="Hernadi Yoga Adhitya Tama">GM E-Commerce</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="noio">No IO</label>
                                <div class="input-group col-md-12">
                                    <input type="hidden" name="io_id" id="io_id" value="{{ $bakns->io_id }}">
                                    <input type="text" name="noio" id="noio" class="form-control" title="Nomor IO" readonly value="{{ $bakns->io['no_io'] }}">
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="desio">Deskripsi IO</label>
                                <div class="input-group col-md-12">
                                    <input type="text" name="desio" id="desio" class="form-control" title="Deskripsi" data-toggle="modal" data-target="#modal-unit" value="{{ $bakns->io['deskripsi'] }}" readonly placeholder="Pilih IO">
                                </div>
                            </div>

                            <div class="form-group col-md-4 igroup">
                                <label class="control-label">Tipe Undangan</label>
                                <br>
                                <div class="col-md-6">
                                    <ul class="no-bullet">
                                        <li>
                                            <input type="checkbox" name="tipe_undangan[]" value="Review" id="invit1" {{ in_array("Review",$checkbox)? "checked":"" }}>
                                            <label for="minimal-checkbox-1">Review</label>
                                        </li>
                                        <li >
                                            <input type="checkbox" name="tipe_undangan[]" value="Coordination" id="invit2" {{ in_array("Coordination",$checkbox)? "checked":"" }}>
                                            <label for="minimal-checkbox-2">Coordination</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="no-bullet">
                                        <li>
                                            <input type="checkbox" name="tipe_undangan[]" value="Briefing" id="invit3" {{ in_array("Briefing",$checkbox)? "checked":"" }}>
                                            <label for="minimal-checkbox-disabled">Briefing</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" name="tipe_undangan[]" value="Decision Marking" id="invit2" {{ in_array("Decision Marking",$checkbox)? "checked":"" }}>
                                            <label for="minimal-checkbox-disabled-checked">Decision
                                                Making</label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="pesertarapat">Peserta Rapat</label>
                                    <div class="form-group">
                                        <div class="input-group-addon select-addon">
                                            <span>PT PINS INDONESIA</span>
                                        </div>
                                        <input name="peserta_pins" class=" input-group tagify"
                                        id="tags-pins" placeholder="Pilih Peserta" value="{{ $bakns->peserta_pins }}">
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="pesertarapat">Peserta Rapat</label>
                                    <div class="form-group" id="mtr">
                                        <div class="input-group-addon select-addon">
                                            <span id="mitra">MITRA</span>
                                        </div>
                                        {{-- <input name="peserta_mitra" class=" input-group tagify"
                                        id="direktur" placeholder="Pilih Peserta">--}}
                                        <input name="peserta_mitra" class=" input-group form-control" id="direktur" placeholder="Pilih Peserta" value="{{ $bakns->peserta_mitra }}">
                                    </div>
                                </div>

                                {{-- hasil pembahasan --}}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <h4 class="divider-title">HASIL PEMBAHASAN</h4>
                                        <hr>
                                    </div>
                                    <div class="form-group">
                                        <label for="agenda">Agenda</label>
                                        <textarea id="agenda" rows="10" cols="80" name="agenda" placeholder="Type Something" class="bakn">
                                            {{ $bakns->agenda }}
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="dasarpembahasan">Dasar Pembahasan</label>
                                        <textarea  rows="10" cols="80" id="dasarpembahasan" name="dasar_pembahasan" placeholder="Type Something" class="bakn">
                                            {{ $bakns->dasar_pembahasan }}
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="ruanglingkip">Ruang Lingkup</label>
                                        <textarea id="ruanglingkup" rows="10" cols="80" name="ruang_lingkup" placeholder="Type Something" class="bakn">
                                            {{ $bakns->ruang_lingkup }}
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="lokasipekerjaan">Lokasi Pekerjaan/Pengiriman Barang/Jasa</label>
                                        <textarea id="cke-lokasi-pekerjaan" rows="10" cols="80" name="lokasi_pekerjaan" placeholder="Type Something" class="bakn">
                                            {{ $bakns->lokasi_pekerjaan }}
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="jangkawaktu">Jangka waktu Pengiriman Barang</label>
                                        <textarea id="cke-jangka-waktu" rows="10" cols="80" name="jangka_waktu" placeholder="Type Something" class="bakn">
                                            {{ $bakns->jangka_waktu }}
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="harga">Harga</label>
                                        <textarea id="nilai" name="harga_terbilang" class="bakn">
                                            {{ $bakns->harga_terbilang }}
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="tatacara">Tata Cara Pembayaran</label>

                                        <textarea  rows="10" cols="80" name="cara_bayar" class="bakn"  id="tatacara">
                                            {{ $bakns->cara_bayar }}
                                            {{-- <p id="carakasar"></p> --}}
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Lain - lain</label>
                                        <textarea rows="10" cols="80" class="bakn" name="lain_lain">
                                            {{ $bakns->lain_lain }}
                                        </textarea>
                                    </div>
                                    <input type="hidden" name="tipe" value="{{ $bakns->tipe }}">
                                </div>
                                {{-- end hasil pembahasan --}}
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="status" value="draft_bakn" class="btn btn-primary" style="width: 7em;"><i class="fa fa-check"></i> Save</button>
                            <button type="submit" name="status" value="save_bakn" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- modal -->
                    <div class="modal fade" id="modal-unit">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Data Unit</h4>
                                    </div>
                                    <div class="modal-body">
                                        <table id="io" class="display table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>IO</th>
                                                    <th>Description</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($io as $ios)
                                                <tr>
                                                    <td>{{ $ios['id'] }}</td>
                                                    <td>{{$ios['no_io']}}</td>
                                                    <td>{{$ios['deskripsi']}}</td>
                                                    <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @endsection

    @section('scripts')
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
    {{-- bahasa indonesia froala --}}
    <script src="{{ asset('froala/js/languages/id.js') }}"></script>

    <!-- Data Table -->
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- clockpicker -->
    <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Tagify -->
    <script src="{{ asset('tagify/dist/jQuery.tagify.min.js') }}"></script>
    <script src="{{ asset('tagify/dist/tagify.min.js') }}"></script>
    <script src="{{ asset('js/bakn.js') }}"></script>
    <script>
        $('.bakn').froalaEditor({
            placeholderText: '',
            language: 'id',
            charCounterCount: false,
            key: '{{ env("KEY_FROALA") }}',
        });
        $('#chat').froalaEditor({
            key: '{{ env("KEY_FROALA") }}',
            height: 70,
            toolbarButtons:['help'],
            charCounterCount: false,
            key: '{{ env("KEY_FROALA") }}',

            // language: 'id',

        })
        $(document).ready(function(){
            $(document).change(function(){
                var nomorspph=$('#nospph').val();
                var token = $("input[name='_token']").val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'post',
                    url:"{{ route('data-spph') }}",
                    data: {nomorspph:nomorspph, _token:token},
                    dataType:'json',//return data will be json
                    success:function(data){

                        var nmr = data.options['nomorspph'];
                        var options = document.forms['form']['jenis_kontrak'].options;
                        for (var i=0, iLen=[options.length-1]; i<iLen; i++) {
                            options[i].disabled = false;
                        }
                        if (nmr.includes("ECOM")) {
                            var options = document.forms['form']['jenis_kontrak'].options;
                            for (var i=0, iLen=[options.length-1]; i<iLen; i++) {
                                options[i].disabled = true;
                            }
                            $('#tipe_bakn').val('LKPP');

                        }else{
                            $('#tipe_bakn').val('NON');
                        }
                        $('[name=spphid]').val(data.options['id']);
                        $("#spphpreview").attr("href","{{ url('spph-preview') }}/"+data.options['id']);
                        $('#mitra').html(data.options['mitras']['perusahaan']);
                        $('#direktur').val(data.options['mitras']['direktur']);
                        const datespph = new Date(data.options['tglspph']);
                        const monthspph = datespph.toLocaleString('id-ID', {month:'long'});
                        const tglspph = datespph.getDate()+' '+monthspph+' '+datespph.getFullYear()
                        const datesph = new Date(data.options['tglsph']);
                        const monthsph = datesph.toLocaleString('id-ID', { month: 'long' });
                        const tglsph = datesph.getDate()+' '+monthsph+' '+datesph.getFullYear();
                        var isinya2 = '<ol style="margin-bottom:0cm;">';
                            isinya2 +='<li><span>Surat Permintaan Penawaran Harga (SPPH) nomor: '+ data.options['nomorspph']+ ' tanggal '+tglspph+' perihal permintaan penawaran harga;</span></li>';
                            isinya2 +='<li><span>Surat dari SUPPLIER nomor: '+data.options['nomorsph'] +' tanggal '+tglsph+' perihal Penawaran Harga.</span></li>';
                            isinya2 +='</ol>';
                            $('#dasarpembahasan').froalaEditor('html.set', isinya2);
                            $('#agenda').froalaEditor('html.set', "Klarifikasi & Negosiasi Penawaran Harga dari SUPPLIER tentang "+data.options['judul']+".");
                            $('#ruanglingkup').froalaEditor('html.set', data.options['judul']+' dengan rincian sebagaimana tercantum pada point nomor 4.');
                        },
                        error:function(){

                        }
                    });


                });
            });</script>
            @endsection
