@extends('layouts.master')

@php
$homelink = "/home";
$crmenu = "Master Data";
$crsubmenu = "Data Unit";
$submenulink = "/unit";
$cract = "Add Data Unit";
@endphp

@section('title')
{{ $crsubmenu." | SuperSlim" }}
@endsection

@section('stylesheets')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endsection

@section('customstyle')
<style type="text/css">
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ $crsubmenu }}
      <!-- <small>Form PBS</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li>{{ $crmenu }}</li>
      <li><a href="{{ $submenulink }}">{{ $crsubmenu }}</a></li>
      <li class="active">{{ $cract }} </li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-file-text"></i> Form Data Unit</h3>
            <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form method="POST" action="{{ action('BaknController@upload', $bakn->id) }}" enctype="multipart/form-data" class="form">
            @csrf
            @method('POST')
            <div class="box-body">
              
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                There was a problem, please check your form carefully.
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif

              @if ($bakn->io == '0')
              <div class="form-group col-md-6 col-md-offset-3">
                <label for="exampleInputEmail1">Nama Projek & IO</label>

                <div class="input-group">
                    <div class="col-md-6 hidden-sm hidden-xs">
                        <input type="text" class="form-control" id="desio" name="deskripsi_io" placeholder="Deskripsi IO" readonly value="{{ old('deskripsi_io') }}" required>
                    </div>
                    <div class=" col-md-6">
                        <input type="text" class="form-control" id="noio" name="io" placeholder="Nomor IO" readonly value="{{ old('io') }}" required>
                    </div>

                    <!-- browse button -->
                    <span class="input-group-btn">
                        <label class="btn btn-primary" title="Browse" data-toggle="modal" data-target="#modal-io">
                            <span class="fa fa-search"></span>
                            <span class="hidden-xs">Browse</span>
                        </label>
                    </span>
                </div>
              </div>
              @endif
              <div class="form-group col-md-6 col-md-offset-3">
                <label>Upload File*</label>
                <input type="hidden" name="title[]" class="form-control">
                <br>
                <input type="file" name="file[]" id="" class="form-control" multiple="multiple">
                <br>
              </div>           

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success col-md-3 col-md-offset-8" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
            </div>
            <!-- /.box-footer -->
          </form>
          <!-- modal -->
          <div class="modal fade" id="modal-io">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Data Unit</h4>
                        </div>
                        <div class="modal-body">
                            <table id="tbl_io" class="display table-responsive">
                                <thead>
                                    <tr>
                                        <th>IO</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($ios as $io)
                                    <tr>
                                        <td>{{$io['no_io']}}</td>
                                        <td>{{$io['deskripsi']}}</td>
                                        <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            {{--  <button type="button" class="btn btn-primary">Save changes</button>  --}}
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

        </div>
        <!-- /.box -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
  var table = document.getElementById('tbl_io');
  $(document).ready( function () {
      $('#tbl_io').DataTable();
  } )
  for(var i = 1; i < table.rows.length; i++)
  {
      table.rows[i].onclick = function()
      {
          //  rIndex = this.rowIndex;
          document.getElementById("noio").value = this.cells[0].innerHTML;
          document.getElementById("desio").value = this.cells[1].innerHTML;

          //document.getElementById("iodes").innerHTML = show;
          // document.getElementById("iodes").value = this.cells[1].innerHTML;

      };
  };
</script>
@endsection