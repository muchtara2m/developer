@extends('layouts.master')

@section('title')
CRM | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
<style type="text/css">
  .form-horizontal .form-group {
    margin-right: unset;
    margin-left: unset;
  }
  tfoot {
    display: table-header-group;
  }
</style>
@endsection

@section('content')

@php 
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Inisiasi
      <!-- <small>Form PBS</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li><a href="#">Transaksi</a></li>
      <li class="active">Status Inprogress</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <!-- Custom Tabs -->
        <div class="box box-info">
         
          <div class="box-body table-responsive">
            <table id="pbsTable"  class="display responsive nowrap" width="100%">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>No Inisiasi</th>
                  <th>No IO</th>
                  <th>AM</th>
                  <th>UBIS</th>
                  <th>Portofolio</th>
                  <th>Segment</th>
                  <th>Nama End Customer</th>
                  <th>Desc Project</th>
                  <th>Nilai Project</th>
                  <th>Status</th>
                  <th>Kategori</th>
                  <th>Tgl Target Win</th>
                  <th>Tgl Closing</th>
                  <th>Entry Outlook</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th></th>
                  <th class="testing">No Inisiasi</th>
                  <th class="testing">No IO</th>
                  <th class="testing">AM</th>
                  <th class="testing">UBIS</th>
                  <th class="testing">Nama End Customer</th>
                  <th class="testing">Desc Project</th>
                  <th class="testing">Nilai Project</th>
                  <th class="testing">Status</th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </tfoot>
            </table>
          </div>
      <!-- /.box -->
    </div>
    <!--/.col (right) -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script>
  var bln = 8;
  var thn = 2019;
  dataEntry(bln, thn);
  $('#pbsTable tfoot .testing').each( function () {
    var title = $(this).text();
    $(this).html( '<input type="text" style="text-align:center;"placeholder="'+title+'" />' );
  } );
  $.extend( $.fn.dataTable.defaults, {
    responsive: true
} );
  function dataEntry(bln = '', thn = ''){
    var table = $('#pbsTable').DataTable({
        responsive:true,
      processing: true,
      serverSide: true,
      ajax: {
        url:'{{ url("unbill_ar") }}',
        data:{bln:bln, thn:thn}
      },
      columns: [
      { data: 'action'},
      { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
      { data: 'dataio.no_io','defaultContent': ""},
      { data: 'dataio.deskripsi','defaultContent': ""},
      { data: 'customer.nama_customer','defaultContent': ""},
      { data: 'pembuat.unitnya.nama','defaultContent': ""},            
      { data: 'uraian','defaultContent': ""},
      { data: 'nilai_project', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},
      { 
        data: 'spk', 
        render: function(data) { 
          if(data ==  '1111-11-11 11:11:11') {
            return '<i class="fa fa-minus"></i>';
          } else if(data != '1111-11-11 11:11:11' && data !==null){
            return '<i class="fa fa-check"></i>';
          }else if(data === null){
            return '<i class="fa fa-close"></i>';
          }
        },
        defaultContent: ''
      },
      { 
        data: 'kb', 
        render: function(data) { 
          if(data ==  '1111-11-11 11:11:11') {
            return '<i class="fa fa-minus"></i>';
          } else if(data != '1111-11-11 11:11:11' && data !==null){
            return '<i class="fa fa-check"></i>';
          }else if(data === null){
            return '<i class="fa fa-close"></i>';
          }
        },
        defaultContent: ''
      },
      { 
        data: 'kl', 
        render: function(data) { 
          if(data ==  '1111-11-11 11:11:11') {
            return '<i class="fa fa-minus"></i>';
          } else if(data != '1111-11-11 11:11:11' && data !==null){
            return '<i class="fa fa-check"></i>';
          }else if(data === null){
            return '<i class="fa fa-close"></i>';
          }
        },
        defaultContent: ''
      },
      { 
        data: 'baut', 
        render: function(data) { 
          if(data ==  '1111-11-11 11:11:11') {
            return '<i class="fa fa-minus"></i>';
          } else if(data != '1111-11-11 11:11:11' && data !==null){
            return '<i class="fa fa-check"></i>';
          }else if(data === null){
            return '<i class="fa fa-close"></i>';
          }
        },
        defaultContent: ''
      },
      { 
        data: 'baut', 
        render: function(data) { 
          if(data ==  '1111-11-11 11:11:11') {
            return '<i class="fa fa-minus"></i>';
          } else if(data != '1111-11-11 11:11:11' && data !==null){
            return '<i class="fa fa-check"></i>';
          }else if(data === null){
            return '<i class="fa fa-close"></i>';
          }
        },
        defaultContent: ''
      },
      { 
        data: 'baut', 
        render: function(data) { 
          if(data ==  '1111-11-11 11:11:11') {
            return '<i class="fa fa-minus"></i>';
          } else if(data != '1111-11-11 11:11:11' && data !==null){
            return '<i class="fa fa-check"></i>';
          }else if(data === null){
            return '<i class="fa fa-close"></i>';
          }
        },
        defaultContent: ''
      },
      { 
        data: 'baut', 
        render: function(data) { 
          if(data ==  '1111-11-11 11:11:11') {
            return '<i class="fa fa-minus"></i>';
          } else if(data != '1111-11-11 11:11:11' && data !==null){
            return '<i class="fa fa-check"></i>';
          }else if(data === null){
            return '<i class="fa fa-close"></i>';
          }
        },
        defaultContent: ''
      },
      
      ],
      
    });
  } 
  $('#pbsTable').DataTable().columns().every( function () {
      var that = this;
      $( 'input', this.footer() ).on( 'keyup change', function () {
        if ( that.search() !== this.value ) {
          that
          .search( this.value )
          .draw();
        }
      } );
    } );
</script>
@endsection