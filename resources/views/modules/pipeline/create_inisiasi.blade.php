@extends('layouts.master')

@section('title')
Create Inisiasi | Superslim
@endsection

@section('stylesheets')
{{-- date picker css --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
{{-- select2 css --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />

@endsection

@section('content')
{{-- content wrapper --}}
<div class="content-wrapper">
    {{-- section content-header --}}
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ "/home" }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Pipeline</a></li>
            <li><a href="{{ url('entry-update') }}">CRM</a></li>
            <li class="active">Create Inisiasi</li>
        </ol>
    </section> <!--section content-header-->
</br>
{{-- main section --}}
<section class="content">
    {{-- section row --}}
    <div class="row">
        {{-- section col-md-12 --}}
        <div class="col-md-12">
            {{-- section box box-info --}}
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Create Inisiasi</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="am" class="col-sm-2 control-label">AM - No Telp</label>
                            
                            <div class="col-sm-10">
                                <select name="am" class="form-control" id="" data-placeholder="Pilih AM">
                                    <option value=""></option>
                                    @foreach ($am as $item)
                                    <option value="{{ $item->am_id }}">{{ $item->nama.' - '.$item->no_telp }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ubis" class="col-sm-2 control-label">UBIS</label>
                            
                            <div class="col-sm-10">
                                <select name="ubis" id="" class="form-control" data-placeholder="Pilih UBIS">
                                    <option value=""></option>
                                    @foreach ($ubis as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="portofolio" class="col-sm-2 control-label">Portofolio</label>
                            
                            <div class="col-sm-10">
                                <select name="portofolio" id="" class="form-control" data-placeholder="Pilih Portofolio"> 
                                    <option value=""></option>
                                    @foreach ($portofolio as $item)
                                    <option value="{{ $item->id_portofolio }}">{{ $item->portofolio }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="segment" class="col-sm-2 control-label">Segmen</label>
                            
                            <div class="col-sm-10">
                                <select name="segment" id="" class="form-control" data-placeholder="Pilih Segmen">
                                    <option value=""></option>
                                    @foreach ($segment as $item)
                                    <option value="{{ $item->id_segmen }}">{{ $item->segmen }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="source" class="col-sm-2 control-label">Source</label>
                            
                            <div class="col-sm-10">
                                <input type="text" name="source" id="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="customer" class="col-sm-2 control-label">Customer</label>
                            
                            <div class="col-sm-10">
                                <select class="form-control select2 " name="customer" data-placeholder="Pilih Customer">
                                    <option value=""></option>
                                    @foreach ($customer as $item)
                                    <option value="{{ $item->akun_customer }}" >{{ $item->deskripsi_customer }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="endcustomer" class="col-sm-2 control-label">End Customer</label>
                            
                            <div class="col-sm-10">
                                <input type="text" name="endcustomer" id="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="descproject" class="col-sm-2 control-label">Desc Project</label>
                            
                            <div class="col-sm-10">
                                <input type="text" name="descproject" id="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tglwin" class="col-sm-2 control-label">Tanggal Target Win</label>
                            
                            <div class="col-sm-10">
                                <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datepick" name="tglwin" id="enddelivery-date">                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="kategori" class="col-sm-2 control-label">Kategori</label>
                            
                            <div class="col-sm-10">
                                <select name="kategori" id="" class="form-control" data-placeholder="Pilih Kategori">
                                    <option value=""></option>
                                    <option value="1">Disruptive</option>
                                    <option value="2">Smart</option>
                                    <option value="3">Ordinari</option>
                                    <option value="4">Unknown</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" name="status" value="draft_bakn" class="btn btn-info" style="width: 7em;"><i class="fa fa-check"></i> Save</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div> <!--div col-md-12-->
        </div> <!-- div row -->
    </section> <!-- main section -->
</div> <!--content-wrapper-->


@endsection
@section('scripts')
{{-- datepicker --}}
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
{{-- select2 --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
    // script datepicker
    $(function () {
        $(".datepick").on("change", function () {
            this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format(this.getAttribute("data-date-format"))
            )
        }).trigger("change")
    });
    $('.datepick').datepicker({
        autoclose: true,
        orientation: "bottom"
    });
    // select2
    $('select').select2({
        placeholder: function(){
            $(this).data('placeholder');
        }
    });
</script>
@endsection