 {{-- modal update status --}}
 <div class="modal fade" id="modal-update-status" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Update Status</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            {{-- <form method="post" action="" enctype="multipart/form-data"> --}}
              <label>Pilih Status :</label>
              <select name="" class="form-control" id="update_status_inisiasi">
                @foreach ($status_inisiasi as $item)
                <option value="{{ $item->nama_status }}">{{ $item->nama_status }}</option>
                @endforeach
              </select>
              <br>
              <button type="submit" class="btn btn-success" id="btn-status-inisiasi">Submit</button>
              {{-- </form> --}}
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    
    {{-- end modal update status --}}