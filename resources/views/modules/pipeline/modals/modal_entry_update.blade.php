 {{-- mapping edit data --}}
 <div class="modal fade" id="modal-edit-data-inisiasi">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Data Inisiasi</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="am" class="col-sm-2 control-label">AM - No Telp</label>
                                
                                <div class="col-sm-10">
                                    <select name="am" class="form-control select2" id="" data-placeholder="Pilih AM" style="width:100%;">
                                        <option value=""></option>
                                        @foreach ($am as $item)
                                        <option value="{{ $item->am_id }}">{{ $item->nama.' - '.$item->no_telp }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ubis" class="col-sm-2 control-label">UBIS</label>
                                
                                <div class="col-sm-10">
                                    <select name="ubis" id="" class="form-control select2" data-placeholder="Pilih UBIS" style="width:100%;">
                                        <option value=""></option>
                                        @foreach ($ubis as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="portofolio" class="col-sm-2 control-label">Portofolio</label>
                                
                                <div class="col-sm-10">
                                    <select name="portofolio" id="" class="form-control select2" data-placeholder="Pilih Portofolio" style="width:100%;"> 
                                        <option value=""></option>
                                        @foreach ($portofolio as $item)
                                        <option value="{{ $item->id_portofolio }}">{{ $item->portofolio }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="segment" class="col-sm-2 control-label">Segmen</label>
                                
                                <div class="col-sm-10">
                                    <select name="segment" id="" class="form-control select2" data-placeholder="Pilih Segmen" style="width:100%;">
                                        <option value=""></option>
                                        @foreach ($segment as $item)
                                        <option value="{{ $item->id_segmen }}">{{ $item->segmen }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="source" class="col-sm-2 control-label">Source</label>
                                
                                <div class="col-sm-10">
                                    <input type="text" name="source" id="" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="customer" class="col-sm-2 control-label">Customer</label>
                                
                                <div class="col-sm-10">
                                    <select class="form-control select2 " name="customer" data-placeholder="Pilih Customer" style="width:100%;">
                                        <option value=""></option>
                                        @foreach ($customer as $item)
                                        <option value="{{ $item->akun_customer }}" >{{ $item->deskripsi_customer }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="endcustomer" class="col-sm-2 control-label">End Customer</label>
                                
                                <div class="col-sm-10">
                                    <input type="text" name="endcustomer" id="" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="descproject" class="col-sm-2 control-label">Desc Project</label>
                                
                                <div class="col-sm-10">
                                    <input type="text" name="descproject" id="" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tglwin" class="col-sm-2 control-label">Tanggal Target Win</label>
                                
                                <div class="col-sm-10">
                                    <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datepick" name="tglwin" id="enddelivery-date">                                
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kategori" class="col-sm-2 control-label">Kategori</label>
                                
                                <div class="col-sm-10">
                                    <select name="kategori" id="" class="form-control" data-placeholder="Pilih Kategori" style="width:100%;">
                                        <option value=""></option>
                                        <option value="1">Disruptive</option>
                                        <option value="2">Smart</option>
                                        <option value="3">Ordinari</option>
                                        <option value="4">Unknown</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="status" value="draft_bakn" class="btn btn-info" style="width: 7em;"><i class="fa fa-check"></i> Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>    
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{-- end mapping io --}}