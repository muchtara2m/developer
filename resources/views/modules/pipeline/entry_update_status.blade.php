@extends('layouts.master')

@section('title')
Entry Update Status Inisiasi | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->

<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
{{-- date picker css --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
{{-- select2 css --}}
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}" />
<style type="text/css">
  .form-horizontal .form-group {
    margin-right: unset;
    margin-left: unset;
  }
  table{
    white-space: nowrap;
  }
  .tosca{
    background-color: #26B99A;
    color: white;
  }
  tfoot {
    display: table-header-group;
  }
  .center{
    text-align: center;
  }
  td.details-control {
    background: url('{{ asset('images/details_open.png') }}') no-repeat center center;
    cursor: pointer;
  }
  tr.details td.details-control {
    background: url('{{ asset('images/details_close.png') }}') no-repeat center center;
  }
</style>
@endsection

@section('content')

@php 
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Inisiasi
      <!-- <small>Form PBS</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li><a href="#">Transaksi</a></li>
      <li class="active">Status Inprogress</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body table-responsive">
            <ul class="nav" style="margin-bottom:10px">
              <li class="pull-right">
                <div>
                  <a href="{{ route('create-inisiasi') }}">
                    <button class="btn tosca m-r-dot5em" style="padding-bottom:4px"><i class="fa fa-plus"></i> Create Inisiasi</button>
                  </a>
                </div>
              </li>
            </ul>
            <table id="pbsTable" class="display">
              <thead class="text-center">
                <tr>
                  <th>Aksi</th>
                  <th>No.</th>
                  <th>No Inisiasi</th>
                  <th>No IO</th>
                  <th>AM</th>
                  <th>UBIS</th>
                  <th>Portofolio</th>
                  <th>Segment</th>
                  <th>Nama End Customer</th>
                  <th>Desc Project</th>
                  <th>Nilai Project</th>
                  <th>Status</th>
                  <th>Kategori</th>
                  <th>Tanggal Target Win</th>
                  <th>Tanggal Closing</th>
               
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th class="testing"></th>  
                  <th class="testing"></th>
                  <th class="testing"></th>
                  <th class="testing"></th>
                  <th class="testing"></th>
                  <th class="testing"></th>
                  <th class="testing"></th>
                  <th class="testing"></th>
                  <th class="testing"></th>
                  <th class="testing"></th>
                  <th class="testing"></th>
                  <th class="testing"></th>
                  <th class="testing"></th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
      @include('modules.pipeline.modals.modal_entry_update')
      @include('modules.pipeline.modals.modal_io_update_status')
      @include('modules.pipeline.modals.modal_update_status_inisiasi')
      @include('modules.pipeline.modals.modal_entry_tgl_closing')
      @include('modules.pipeline.modals.modal_entry_submission')
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  @endsection
  
  @section('scripts')
  <!-- DataTables -->
  <script>
      var url ='{{ url("unbill_ar") }}';
    </script>
  <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
  <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('js/ajax/entry_inisiasi.js') }}"></script>
  <script src="{{ asset('js/entry_update.js') }}"></script>
  
  @endsection