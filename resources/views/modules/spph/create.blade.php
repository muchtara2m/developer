@extends('layouts.master')

@php
$homelink = "/home";
$crmenu = "SPPH";
$crsubmenu = "Create SPPH";
$submenulink = "/unit";
// $cract = "Add Data Unit";
@endphp

@section('title')
{{ $crsubmenu." | SuperSlim" }}
@endsection

@section('stylesheets')
@endsection

@section('customstyle')
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
</style>
@endsection
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />
{{-- css datatable --}}
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
{{-- ajax script src --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{-- css froala editor --}}
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $crsubmenu }}
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li>{{ $crmenu }}</li>
            <li><a href="{{ $submenulink }}">{{ $crsubmenu }}</a></li>
            {{-- <li class="active">{{ $cract }} </li> --}}
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form SPPH</h3>
                        <div style="text-align:-webkit-center">
                            <input type="radio" name="style" value="ecom" class="pilih" > E-Commerce
                            <input type="radio" name="style" value="gs" class="pilih" > General Support
                        </div>
                        {{-- <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button> --}}
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                      {{-- peringatan jika field tidak diisi     --}}
                      @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          There was a problem, please check your form carefully.
                          <ul>
                              @foreach($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                      @endif
                    <form method="POST" action="{{ route('spph-store') }}" enctype="multipart/form-data" class="form" id="form">
                        @csrf
                        @method('POST')
                        <div class="box-body">

                            <div class="form-group col-sm-6">
                                <label for="tglspph">Tanggal SPPH</label>
                                <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos" name="tglspph" id="tglspph" placeholder="Tanggal SPPH" value="{{ old('tglspph') }}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="nomorspph">Nomor SPPH</label>
                                <input type="text" name="nomorspph" class="form-control " id="nomorspph" placeholder="Nomor SPPH" value="{{ old('nomorspph') }}" >
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="tglsph">Tanggal SPH</label>
                                <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datepick @error('title') is-invalid @enderror" name="tglsph" id="datetimepicker_dark" placeholder="Tanggal SPH" value="{{ old('tglsph') }}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="kepada">Kepada</label>
                                <input type="hidden" name="kepada" id="idmitra">
                                <input type="text" class="form-control" placeholder="Kepada" id="kepada" data-toggle="modal" data-target="#modal-unit" value="{{ old('kepada') }}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="judul">Judul</label>
                                <input type="text" name="judul" class="form-control" placeholder="Judul" value="{{ old('judul') }}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="pic">Pembuat</label>
                                <select name="pembuat" id="pic" class="form-control">
                                    <option disabled selected>Pilih Pembuat</option>

                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="dari">Penanggung Jawab</label>
                                <select name="handler" id="dari" class="form-control">

                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="tembusan">Tembusan</label>
                                <select id="role-cm_role" class="form-control" name="" multiple size="4" onchange="getRoles(this.value)">
                                    @foreach ($tembusan as $item)
                                    <option value="{{ $item->name }}" {{  (old('tembusan') == $item->name) ? 'selected' : ''}}>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" id="role-cm_role_text" class="form-control" name="tembusan">

                            </div>
                            <div class="form-group col-md-12">
                                <label for="perihal">Perihal</label>
                                <input type="text" name="perihal" class="form-control" value="Surat Permintaan Penawaran Harga (SPPH)" readonly>
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="status" value="draft_spph" class="btn btn-success col-md-3" style="width: 7em;">Save</button>
                            <button type="submit" name="status" value="save_spph" class="btn btn-primary col-md-3 pull-right" style="width: 7em;">Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>

                </div>
            </div>

        </div>
        <!-- /.box -->
        <!-- modal -->
        <div class="modal fade" id="modal-unit">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Data Mitra</h4>
                        </div>
                        <div class="modal-body">
                            <table id="mitra" class="display table-responsive">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Perusahaan</th>
                                        <th>Alamat</th>
                                        <th>Direktur</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($mitras as $ios)
                                    <tr>
                                        <td>{{ $ios['id'] }}</td>
                                        <td>{{$ios['perusahaan']}}</td>
                                        <td>{{$ios['alamat']}}</td>
                                        <td>{{$ios['direktur']}}</td>
                                        <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            {{--  <button type="button" class="btn btn-primary">Save changes</button>  --}}
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>
{{-- script href datatable --}}
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
{{-- script function froala --}}
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
{{-- script function date picker --}}
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
{{-- select2 --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


<script>
    $('#isi')
    .froalaEditor({
        documentReady: true,
        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo', 'getPDF'],
        language: 'id',
        key: '{{ env("KEY_FROALA") }}',
    })</script>
    {{-- milih jabatan username --}}
    <script>
        $(document).ready( function () {
            $('#mitra').DataTable();
        } )</script>
        <script>

            $('select').select2();
            function getRoles(val) {
                $('#role-cm_role_text').val('');
                var data = $('#role-cm_role').select2('data').map(function(elem){ return elem.text} );
                console.log(data);
                $('#role-cm_role_text').val(data);
                $('#role-cm_role').on('select2:unselecting', function (e) {
                    $('#role-cm_role_text').val('');
                });
            }</script>
            {{-- input format tanggal --}}
            <script>
                $(function(){
                    $(".datejos").on("change", function() {
                        this.setAttribute(
                        "data-date",
                        moment(this.value, "YYYY-MM-DD")
                        .format( this.getAttribute("data-date-format") )
                        )
                    }).trigger("change")
                    $('.datejos').datepicker({
                        autoclose: true,
                        orientation: "bottom"
                    })
                })</script>
                <script type="text/javascript">
                    $.datetimepicker.setLocale('id');
                    $('.datepick').datetimepicker({theme:'dark'})

                    $('.timepick').datetimepicker({
                        datepicker:false,
                        format:'H:i',
                        step:5
                    });</script>

                    {{-- select mitra  --}}
                    <script>
                        var table = document.getElementById('mitra');

                        for(var i = 1; i < table.rows.length; i++)
                        {
                            table.rows[i].onclick = function()
                            {
                                //  rIndex = this.rowIndex;
                                document.getElementById('idmitra').value = this.cells[0].innerHTML;
                                document.getElementById("kepada").value = this.cells[1].innerHTML;

                            };
                        };</script>
                        {{-- pilih pengadaan --}}
                        <script>
                            // $("#form").find(':input').prop('disabled', true);
                            $('.pilih').click(function(){
                                var btn = $(this).val();
                                var token = $("input[name='_token']").val();
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });
                                $.ajax({
                                    type:'post',
                                    url:"{{ route('select-spph') }}",
                                    data: {jns:btn, _token:token},
                                    dataType:'json',//return data will be json
                                    success:function(data){
                                        // console.log(data.handler);
                                        var pic = $('#pic');
                                        var dari = $('#dari');
                                        pic.empty();
                                        dari.empty();
                                        pic.append('<option selected value="{{ Auth::user()->name }}"> {{ Auth::user()->name }}</option>')
                                        data.pembuat.forEach(function(elem, index, array) {
                                            pic.append('<option value="'+elem['name']+'">'+elem['name']+'</option>')
                                        });
                                        data.handler.forEach(function(elem, index, array) {
                                            dari.append('<option value="'+elem['name']+'">'+elem['name']+'</option>')
                                        });
                                    },
                                    error:function(){
                                    }
                                });
                                if(btn !== ''){
                                    document.getElementById('tglspph').value='';
                                    $("#form").find(':input').prop('disabled', false);
                                    // console.log(btn);
                                    if(btn == 'gs'){
                                        var jenis ='';
                                    }else if(btn = 'ecom'){
                                        var jenis ='ECOM/';
                                    }
                                    document.getElementById('nomorspph').value = "/LG.220/"+jenis+"PIN.00.00/";

                                }else{
                                    // console.log(btn);
                                }
                                var gettglspph = document.getElementById('tglspph');

                                function useValue() {
                                    var NameValue = gettglspph.value.split('-');
                                    var year=gettglspph[2];
                                    console.log(NameValue[0]); // just to show the new value
                                    // use it
                                    document.getElementById('nomorspph').value = "/LG.220/"+jenis+"PIN.00.00/"+NameValue[0];


                                }
                                gettglspph.onchange = useValue;
                                gettglspph.onblur = useValue;

                            });</script>

                            @endsection
