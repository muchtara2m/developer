@extends('layouts.master')

@section('title')
Preview SPPH| Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
{{-- css froala editor --}}
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
<style>
    div.froala{
        font-size: 12px;
        font-family: 'Roboto', sans-serif;
    }
</style>
@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            PREVIEW SPPH

            <!-- <small>Form PBS</small> -->
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">SPPH</a></li>
            <li class="active"> Preview SPPH </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">

            <!-- right column -->
            <div class="col-md-12">

                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Preview SPPH</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <textarea class="froala" style="">
                            <!DOCTYPE html>
                            <html>
                            <head>
                                <title></title>
                            </head>
                            <body>

                                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Nomor &nbsp;:&nbsp;</span><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{   $preview->nomorspph }}&nbsp;</span></p>

                                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                    <br>
                                </p>

                                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Jakarta,&nbsp;</span><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ Carbon\Carbon::parse($preview->tglspph)->formatLocalized('%d %B %Y') }}</p>


                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                        <br>
                                    </p>

                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Kepada Yth.</span></strong></p>

                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Direktur {{ $mitra->perusahaan }}</span></strong></p>

                                    {{-- <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Di Tempat&nbsp;</span></strong></p> --}}
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ $mitra->alamat }}&nbsp;</span></strong></p>
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ $mitra->telp }}&nbsp;</span></strong></p>

                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                        <br>
                                    </p>

                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                        <br>
                                    </p>

                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;margin-left:63.0pt;text-align:justify;text-indent:-63.0pt;'><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Perihal &nbsp; &nbsp; &nbsp; &nbsp;:&nbsp;&nbsp;</span><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ $preview->perihal }}&nbsp;</span></strong></p>

                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;margin-left:63.0pt;text-align:justify;text-indent:-63.0pt;'>
                                        <br>
                                    </p>

                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Dengan hormat,</span></p>

                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                        <br>
                                    </p>

                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                        <span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Diinformasikan untuk memenuhi kebutuhan</span>
                                        <span style='font-size:13px;font-family:"Tahoma",sans-serif;'>PT. PINS Indonesia</span>
                                        <span style='font-size:13px;font-family:"Tahoma",sans-serif;'>akan</span>
                                        <strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ $preview->judul }},</span></strong>
                                        <span style='font-size:13px;font-family:"Tahoma",sans-serif;'>dengan rincian sebagaimana tertera pada SPPH ini.</span>
                                        <span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Kami mengundang Perusahaan Saudara untuk berpartisipasi dalam memenuhi kebutuhan</span>
                                        <span style='font-size:13px;font-family:"Tahoma",sans-serif;'>sebagaimana <strong><em>terlampir</em></strong></span><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>.</span></p>

                                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                            <br>
                                        </p>

                                        <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Sehubungan dengan hal tersebut di atas, kami mohon agar Saudara mengirimkan Surat Penawaran Harga (SPH) kepada kami selambat-lambatnya hari</span>
                                            <strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ Carbon\Carbon::parse($preview->tglsph)->formatLocalized('%A') }}</span></strong>
                                            <span style='font-size:13px;font-family:"Tahoma",sans-serif;'>tanggal</span>
                                            <strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ Carbon\Carbon::parse($preview->tglsph)->formatLocalized('%d %B %Y') }}</span></strong>
                                            <span style='font-size:13px;font-family:"Tahoma",sans-serif;'>jam</span>
                                            <strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ Carbon\Carbon::parse($preview->tglsph)->formatLocalized('%H:%M') }} WIB</span></strong>
                                            <span style='font-size:13px;font-family:"Tahoma",sans-serif;'>dan ditujukan kepada:</span><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>&nbsp;</span></p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;text-indent:36.0pt;'>
                                                <br>
                                            </p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;text-indent:36.0pt;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ $dari->position }}</span></strong></p>
                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;text-indent:36.0pt;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>PT.&nbsp;</span></strong><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>PINS Indonesia</span></strong></p>
                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;text-indent:36.0pt;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>The Telkom HUB &nbsp;</span></strong></p>
                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;text-indent:36.0pt;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Telkom Landmark Tower lantai 42-43 &nbsp;</span></strong></p>
                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;text-indent:36.0pt;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Jl. Gatot Subroto No.Kav. 52, Kuningan Barat,</span></strong></p>
                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;text-indent:36.0pt;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Mampang Prapatan, Kota Jakarta Selatan</span></strong></p>
                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;text-indent:36.0pt;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Daerah Khusus Ibukota Jakarta 12710</span></strong></p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                                <br>
                                            </p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Apabila ada hal-hal yang kurang jelas, dapat ditanyakan langsung kepada :</span></p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;margin-left:36.0pt;text-align:justify;'>
                                                <br>
                                            </p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;margin-left:36.0pt;text-align:justify;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Sdr. {{ $pic->name }}</span></strong></p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;margin-left:36.0pt;text-align:justify;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ $pic->position }}&nbsp;</span></strong></p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;margin-left:36.0pt;text-align:justify;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Email. <a href="mailto:{{ $pic->email }}">{{ $pic->email }}</a></span></strong></p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;margin-left:36.0pt;text-align:justify;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Tlp. 021 &ndash; 5202560&nbsp;</span></strong></p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                                <br>
                                            </p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Demikian disampaikan, terimakasih atas perhatian dan kerjasamanya.&nbsp;</span></p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                                <br>
                                            </p>



                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Hormat Kami,</span></p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                                <br>
                                            </p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                                <br>
                                            </p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                                <br>
                                            </p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                                <br>
                                            </p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><strong><u><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ $preview->dari }}</span></u></strong></p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><strong><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ $preview->position }}</span></strong></p>
                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'>
                                                    <br>
                                                </p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>Tembusan :</span></p>

                                            <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Tahoma",serif;text-align:justify;'><span style='font-size:13px;font-family:"Tahoma",sans-serif;'>{{ $preview->tembusan }}</span></p>
                                        </body>
                                        </html>


                                    </textarea>
                                </div>


                            </div>
                            <!-- /.box -->
                        </div>
                        <!--/.col (right) -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            @endsection

            @section('scripts')
            <!-- DataTables -->
            <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            {{-- script function froala --}}
            <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
            <script src="{{ asset('froala/js/languages/id.js') }}"></script>
            <script type="text/javascript">

                $('textarea').froalaEditor({
                    fullPage: true,
                    // toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo', 'getPDF'],
                    toolbarButtons :['print', 'html','getPDF'],
                    charCounterCount: false,
                    language: 'id',
                    key: '{{ env("KEY_FROALA") }}',
                })</script>
                @endsection
