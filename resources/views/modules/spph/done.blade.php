@extends('layouts.master')

@section('title')
Done SPPH| Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
    /* .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    } */
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            DONE SPPH
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">SPPH</a></li>
            <li class="active"> Done SPPH </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Done SPPH</h3>
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                    <div style="text-align:center;padding-bottom:10px">
                        <div class="row input-daterange">
                            <form method="get" action="{{ url('spph-done') }}" enctype="multipart/form-data">
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" class="form-control">
                                        @php
                                        if(request()->get('bulan') == null){
                                            $bln = date('m');
                                            $bulan = date('F',strtotime(date('Y-m-d')));
                                            $thn = date('Y');
                                        }else{
                                            $bln = request()->get('bulan');
                                            $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                            $thn = request()->get('tahun');
                                        }
                                        @endphp
                                        <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="tahun" id="tahun" class="form-control">
                                        <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                    </select>
                                </div>
                                <input type="submit" value="Filter" class="btn btn-primary">
                                
                                <div class="col-md-4">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="display">
                            <thead>
                                <tr style="white-space:nowrap">
                                    <th>No</th>
                                    <th>Nomor SPPH</th>
                                    <th>Nomor SPH</th>
                                    <th>Mitra</th>
                                    <th>Judul</th>
                                    <th>Perihal</th>
                                    <th>Penanggung Jawab</th>
                                    <th>Tanggal SPPH </th>
                                    <th>Tanggal SPH</th>
                                    <th>Pembuat</th>
                                    <th>Lampiran</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no=1;
                                @endphp
                                @foreach ($done as $item)
                                <tr style="white-space:nowrap">
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->nomorspph }}</td>
                                    <td>{{ $item->nomorsph }}</td>
                                    <td>{{ $item->mitras['perusahaan'] }}</td>
                                    <td>{{ $item->judul }}</td>
                                    <td>{{ $item->perihal }}</td>
                                    <td>{{ $item->pic }}</td>
                                    <td>{{ date('d.F.Y', strtotime($item->created_at)) }}</td>                                                
                                    <td>{{ date('d.F.Y', strtotime($item->tglsph)) }}</td>    
                                    <td>{{ $item->creator['name'] }}</td>
                                    <td>
                                        @php                    
                                        if($item->file == NULL){                    
                                        }else{
                                            $title = json_decode($item->title, TRUE);
                                            $file = json_decode($item->file, TRUE);
                                            $i=1;
                                            foreach ($title as $key => $value) {
                                                echo $i++.'. <a target="_blank" href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';      
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <td style="white-space:nowrap">
                                        @if($item->created_by == Auth::user()->username)
                                        @if($item->file==NULL )
                                        <a href="#modal_unit{{ $item->id }}"  data-toggle="modal" data-target="#modal-unit_{{ $item->id }}" class="fa fa-fw fa-file"></a>
                                        <div class="modal fade" id="modal-unit_{{ $item->id }}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title">Upload File</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form method="post" action="{{action('SpphController@lampiran', $item->id)}}" enctype="multipart/form-data">
                                                                @csrf
                                                                {{-- @method('PATCH') --}}
                                                                <input type="hidden" name="title" class="form-control">
                                                                <br>
                                                                <input type="file" name="file[]" id="" class="form-control" multiple="multiple">
                                                                <br>
                                                                <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i>
                                                                    Submit</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <!-- /.modal -->
                                                @endif
                                                @endif
                                                @if(Auth::user()->level == 'administrator')
                                                <a href = "spph-delete/{{ $item->id }}" class="fa fa-fw fa-trash" onclick="return confirm('Bener nih mau dihapus?')" title="Delete File"></a>
                                                @endif
                                                <a href="{{ url('spph-preview',$item->id) }}" class="fa fa-fw fa-file-code-o" title="Preview SPPH"></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            
                            
                        </div>
                        <!-- /.box -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        
        @endsection
        
        @section('scripts')
        <!-- DataTables -->
        <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript">
            // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
            $(document).ready( function () {
                $('#pbsTable').DataTable({
                    
                });
            } );
        </script>
        @endsection
        