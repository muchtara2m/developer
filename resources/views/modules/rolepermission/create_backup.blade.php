@extends('layouts.master')

@php
$homelink = "/home";
$crmenu = "Master Data";
$crsubmenu = "Create Role";
$submenulink = "/rpmanage";
$cract = "Add Role and Permission";
@endphp

@section('title')
{{ $crsubmenu." | SuperSlim" }}
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('customstyle')
<style type="text/css">
  .form-horizontal .form-group {
    margin-right: unset;
    margin-left: unset;
  }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ $crsubmenu }}
      <!-- <small>Form PBS</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li>{{ $crmenu }}</li>
      <li><a href="{{ $submenulink }}">{{ $crsubmenu }}</a></li>
      <li class="active">{{ $cract }} </li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-file-text"></i> Form Data Role and Permission</h3>
            <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form method="POST" action="{{ route('rpmanage.store') }}" enctype="multipart/form-data" class="form">
            @csrf
            @method('POST')
            <div class="box-body">
              
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                There was a problem, please check your form carefully.
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif
              
              <div class="form-group col-md-6 col-md-offset-3">
                <label>Role name*</label>
                <input name="roleName" type="text" class="form-control" value="{{ old('roleName') }}" autofocus required>                
              </div>
              <div class="form-group col-md-6 col-md-offset-3">
                <label>Role display*</label>
                <input name="roleDisplay" type="text" class="form-control" value="{{ old('roleDisplay') }}" autofocus required>
              </div>
              <div class="form-group col-md-6 col-md-offset-3">
                <ul class="unstyled-list">
                    <li><div class="pretty p-icon p-round p-jelly">
                        <input type="checkbox" id="masterData_p"/>
                        <div class="state p-primary">
                        <i class="icon fa fa-check"></i>
                        <label>Master Data</label>
                        </div>
                    </div></li>
                    <ul class="unstyled-list ckbox-child">
                        <li><div class="pretty p-icon p-round p-jelly">
                            <input type="checkbox" name="menuAccess[]" value="data_flow" class="masterData_c" />
                            <div class="state p-primary">
                            <i class="icon fa fa-check"></i>
                            <label id="testlabel">Data Flow</label>
                            </div>
                        </div></li>
                        <li><div class="pretty p-icon p-round p-jelly">
                            <input type="checkbox" name="menuAccess[]" value="data_karyawan" class="masterData_c" />
                            <div class="state p-primary">
                            <i class="icon fa fa-check"></i>
                            <label>Data Karyawan</label>
                            </div>
                        </div></li>
                        <li><div class="pretty p-icon p-round p-jelly">
                            <input type="checkbox" name="menuAccess[]" value="data_mitra" class="masterData_c"/>
                            <div class="state p-primary">
                            <i class="icon fa fa-check"></i>
                            <label>Data Mitra</label>
                            </div>
                        </div></li>
                    </ul>
                </ul>
                
              </div>
              
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success col-md-3 col-md-offset-8" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script type="text/javascript">
	$("#masterData_p").click(function () {
		let items = $('.masterData_c');
    items.not(this).prop('checked', this.checked);
	});
</script>
@endsection