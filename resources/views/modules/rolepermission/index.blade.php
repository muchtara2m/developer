@extends('layouts.master')

@section('title')
Role Permission Management | {{ env('APP_NAME') }}
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css">
<style type="text/css">
  .form-horizontal .form-group {
    margin-right: unset;
    margin-left: unset;
  }
  tfoot input {
    width: 100%;
    padding: 3px;
    box-sizing: border-box;
  }
</style>
@endsection

@section('content')

@php $homelink = "/home"; @endphp
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  
  @guest
  <div class="container container-table">
    <div class="row middle-center">
      <div class="session-ended bg-danger">
        Maaf Anda belum login / Session Anda telah habis.
      </div>
    </div>
  </div>
  
  @else
  <section class="content-header">
    <h1>
      DATA ROLE AND PERMISSION
      <!-- <small>Form PBS</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li><a href="#">Master Data</a></li>
      <li class="active"> Data Role Permission Management</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-primary">
          <div class="box-header with-border">
            
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
              <button href="#" class="close" data-dismiss="alert" aria-label="close">&times;</button>
              {{ $message }}
            </div>
            @endif
            
            <h3 class="box-title"><i class="fa fa-book"></i> Role and Permission Index</h3>
            {{-- <a href="{{ route('rpmanage.createRole') }}">
              <button class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add Role</button>
            </a>
            <a href="{{ route('rpmanage.createPermission') }}">
              <button class="btn btn-primary pull-right m-r-1em"><i class="fa fa-plus"></i> Add Permission</button>
            </a> --}}
            {{-- <label class="btn btn-primary pull-right" title="Add Data" data-toggle="modal" data-target="#modal-add">
              <span class="fa fa-plus"></span>
              <span class="hidden-xs">Add Data</span>
            </label> --}}
            
          </div>
          <!-- /.box-header -->
          
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_roles" data-toggle="tab">Roles</a></li>
              <li><a href="#tab_permissions" data-toggle="tab">Permission</a></li>
              <li class="pull-right">
                <div>
                  <a href="{{ route('rpmanage.createRole') }}">
                    <button class="btn btn-primary m-r-dot5em"><i class="fa fa-plus"></i> Add Role</button>
                  </a>
                  <a href="{{ route('rpmanage.createPermission') }}">
                    <button class="btn btn-primary"><i class="fa fa-plus"></i> Add Permission</button>
                  </a>
                </div>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_roles">
                <div class="box-body table-responsive">
                  <table id="rolesTable" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Display</th>
                        <th>Related Users</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($roles as $role)
                      <tr>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->display }}</td>
                        <td class="text-center"><strong>{{ $role->total }}</strong></td>
                        <td class="text-center">
                          <div class="btn-group  btn-group-sm">
                            <a href="{{ route('rpmanage.editRole', $role->id) }}">
                              <button class="btn btn-success btn-xs" type="button"><i class="fa fa-pencil"  title="Edit Data"></i></button>
                            </a>
                            <form action="{{ route('rpmanage.destroyRole', $role->id) }}" method="POST" style="display: inline;">
                              @csrf
                              @method('DELETE')
                              <button type="submit" onclick="return confirm('Are you sure want to delete it?')" id="delete-btn" class="btn btn-danger btn-xs click-hand" title="Delete Data"><i class="fa fa-trash"></i>
                              </button>
                            </form>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_permissions">
                <div class="box-body table-responsive">
                  <table id="permissionsTable" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Display</th>
                        <th>Related Roles</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($permissions as $permission)
                      <tr>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $permission->display }}</td>
                        <td class="text-center"><strong>{{ $permission->total }}</strong></td>
                        <td class="text-center">
                          <div class="btn-group  btn-group-sm">
                            <a href="{{ route('rpmanage.editPermission', $permission->id) }}">
                              <button class="btn btn-success btn-xs" type="button"><i class="fa fa-pencil"  title="Edit Data"></i></button>
                            </a>
                            <form action="{{ route('rpmanage.destroyPermission', $permission->id) }}" method="POST" style="display: inline;">
                              @csrf
                              @method('DELETE')
                              <button type="submit" onclick="return confirm('Are you sure want to delete it?')" id="delete-btn" class="btn btn-danger btn-xs click-hand" title="Delete Data"><i class="fa fa-trash"></i>
                              </button>
                            </form>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          
          
          {{-- @include('modules.rolepermission.create_modal') --}}
          
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  
  @endguest  
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>
<script type="text/javascript">
  // document.getElementById("unitTable_wrapper").style.overflow = "auto";
  $(document).ready( function () {
    $('#rolesTable,#permissionsTable').DataTable({
    });
  } );
</script>
@endsection