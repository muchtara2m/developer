@extends('layouts.master')

@php
$homelink = "/home";
$crmenu = "Master Data";
$crsubmenu = "Create Permission";
$submenulink = "/rpmanage";
$cract = "Add Permission";
@endphp

@section('title')
{{ $crsubmenu." | SuperSlim" }}
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('customstyle')
<style type="text/css">
  .form-horizontal .form-group {
    margin-right: unset;
    margin-left: unset;
  }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ $crsubmenu }}
      <!-- <small>Form PBS</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li>{{ $crmenu }}</li>
      <li><a href="{{ $submenulink }}">{{ $crsubmenu }}</a></li>
      <li class="active">{{ $cract }} </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-file-text"></i> Form Data Permission</h3>
            <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form method="POST" action="{{ route('rpmanage.storePermission') }}" enctype="multipart/form-data" class="form">
            @csrf
            @method('POST')
            <div class="box-body">

              @if (count($errors) > 0)
              <div class="alert alert-danger">
                There was a problem, please check your form carefully.
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif

              <div class="form-group col-md-6 col-md-offset-3">
                <label>Permission name*</label>
                <input name="permissionName" type="text" class="form-control" autofocus required>
              </div>
              <div class="form-group col-md-6 col-md-offset-3">
                <label>Permission display*</label>
                <input name="permissionDisplay" type="text" class="form-control" required>
              </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success col-md-3 col-md-offset-8" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script type="text/javascript">
  $("#menu-pbs-p").click(function () {
    let items = $('.menu-pbs-c');
    items.not(this).prop('checked', this.checked);
  });
  $("#menu-spph-p").click(function () {
    let items = $('.menu-spph-c');
    items.not(this).prop('checked', this.checked);
  });
  $("#menu-bakn-p").click(function () {
    let items = $('.menu-bakn-c');
    items.not(this).prop('checked', this.checked);
  });
  $("#menu-kontrak-p").click(function () {
    let items = $('.menu-kontrak-c');
    items.not(this).prop('checked', this.checked);
  });
  $("#menu-kontrak-non-p").click(function () {
    let items = $('.menu-kontrak-non-c');
    items.not(this).prop('checked', this.checked);
  });
  $("#menu-mdata-p").click(function () {
    let items = $('.menu-mdata-c');
    items.not(this).prop('checked', this.checked);
  });
  $("#menu-ar-p").click(function () {
    let items = $('.menu-ar-c');
    items.not(this).prop('checked', this.checked);
  });
</script>
@endsection
