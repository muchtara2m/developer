@php
if (isset($sp3)) {
    $res = $sp3;
    $refno = $sp3->nosp3;
    $docdate = $sp3->tglsp3;
    $doctitle = "surat penetapan pelaksanaan pekerjaan (sppp)";
}
if (isset($spk)) {
    $res = $spk;
    $refno = $spk->nospk;
    $docdate = $spk->tglspk;
    $doctitle = "surat perintah kerja (spk)";
}
@endphp
<textarea id="froala-editor">
    <p>Ref. No &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp; &nbsp;{{ $refno }}</p>
    <p>Tanggal (<em>date</em>) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: &nbsp; &nbsp;{{ Carbon\Carbon::parse($docdate)->formatLocalized('%d %B %Y') }}</p>
    <p style="text-align: center; text-transform:uppercase"><strong>{{ $doctitle }}</strong></p>
    <br/>
    <span style="display:inline-block; width:50%; vertical-align:top;"><em>Referensi:</em></span>
    <span style="display:inline-block; width:50%; vertical-align:top;">Kepada (<em>to</em>):</span>
    <span style="display:inline-block; width:50%; vertical-align:top;">Berita Acara Klarifikasi & Negosiasi Instalasi
        {{ $res->bakns->spph['judul']}} tanggal {{ Carbon\Carbon::parse($res->bakns['tglbakn'])->formatLocalized('%d %B %Y') }}</span>
        <span style="display:inline-block; width:50%; vertical-align:top;">Direktur {{ $res->bakns->spph->mitras['perusahaan'] }}<br/>{{ $res->bakns->spph->mitras['alamat'] }}
        </span>

        <p><br/></p>
        <b>1. Ruang Lingkup :</b>
        &nbsp; &nbsp; {{ $res->bakns['ruang_lingkup'] }}
    </br>
    <b>2. Lokasi Pekerjaan/ Pengiriman Barang/ Jasa :</b>
    &nbsp; &nbsp; {{ $res->bakns['lokasi_pekerjaan'] }}
</br>
<b>3. Jangka Waktu Pengiriman Barang :</b>
&nbsp; &nbsp; {{ $res->bakns['jangka_waktu'] }}
</br>
<br><b>4. Harga :</b>
&nbsp; &nbsp; {{ $res->bakns['harga_terbilang'] }}
</br>
<b>5. Tatacara Pembayaran :</b>
&nbsp; &nbsp; {{ $res->bakns['cara_bayar'] }}
</br>
<b>6. Lain - lain :</b>
&nbsp; &nbsp; {{ $res->bakns['lain_lain'] }}
</br>
<p><em>Demikian Surat Penetapan ini dibuat sebagai dasar pelaksanaan pekerjaan sebelum ditandatanganinya Perjanjian/Kontrak.</em></p>
<span style="display:inline-block; width:50%; vertical-align:top;">
    <p style="text-align:center">{{ $res->bakns->spph->mitras['perusahaan'] }}</p>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <p style="text-align:center">{{ $res->bakns->spph->mitras['direktur'] }}</p>
</span>
<span style="display:inline-block; width:50%; vertical-align:top;">
    <p style="text-align:center">PT. PINS Indonesia</p>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <p style="text-align:center">{{ $res->bakns['pimpinan_rapat'] }}</p>
</span>
</textarea>

