@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Edit SP3 - SPK";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{--  Tagify  --}}
<link rel="stylesheet" href="{{ asset('tagify/dist/tagify.css') }}">
@endsection

@section('customstyle')

<style type="text/css">
    table tr:not(:first-child){
        cursor: pointer;transition: all .25s ease-in-out;
    }
    table tr:not(:first-child):hover{background-color: #ddd;}
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SP3 - SPK
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">SP3/SPK</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Edit SP3/SPK</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="container">
                        @if(isset($data))
                        @if($chat != NULL)
                        <textarea name="" id="chat" cols="30" rows="10">
                            {{ $chat->chat }}
                        </textarea>
                        @endif
                        @endif
                        <br>
                        <div class="col-md">
                            <label for="nomorspph">Select SPPH</label><a target="_blank" href="#"><i class="fa fa-fw fa-file-code-o"></i></a>
                            <select id="nospph" class="form-control">
                                @if(isset($datas))
                                <option disabled selected >{{ $datas->bakns->spph['nomorspph'].' - '.$datas->bakns->spph['judul'].' - '.$datas->bakns->spph->mitras['perusahaan'] }}</option>
                                @else
                                <option disabled selected >{{ $data->bakns->spph['nomorspph'].' - '.$data->bakns->spph['judul'].' - '.$data->bakns->spph->mitras['perusahaan'] }}</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    {{-- start form  --}}
                    @if(isset($datas))
                    <form method="post" action="{{ url('update-spk',$datas->id) }}" enctype="multipart/form-data" id="form">
                        @else
                        <form method="post" action="{{ url('update-sp3',$data->id) }}" enctype="multipart/form-data" id="form">
                            @endif
                            @csrf
                            <input type="hidden" name="baknid">
                            <div class="box-body">
                                <div class="form-group col-md-6 igroup">
                                    <label for="jenis_kontrak">No SP3/SPK</label>
                                    <div class="input-group col-md-12">
                                        @if(isset($datas))
                                        <input type="text" name="nosp3k" class="form-control" id="nosp3k" value="{{ $datas->nospk }}">
                                        @else
                                        <input type="text" name="nosp3k" class="form-control" id="nosp3k" value="{{ $data->nosp3 }}">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col-md-6 igroup">
                                    <label for="tglspk">Tanggal SP3/SPK</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        @if(isset($datas))
                                        <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos" name="tglsp3k" id="tglspk" value="{{ $datas->tglspk }}">
                                        @else
                                        <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos" name="tglsp3k" id="tglspk" value="{{ $data->tglsp3 }}">
                                        @endif
                                    </div>
                                </div>
                                {{-- hasil pembahasan --}}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        @if(isset($datas))
                                        <h4 id="jenis" class="divider-title" align="center">SURAT PERINTAH KERJA (SPK)</h4>
                                        @else
                                        <h4 id="jenis" class="divider-title" align="center">SURAT PENETAPAN PELAKSANAAN PEKERJAAN (SPPP)</h4>
                                        @endif
                                        <hr>
                                    </div>
                                </div>
                                {{--  isinya  --}}
                                <div class="form-group">
                                    <label for="ruanglingkip">Ruang Lingkup</label>
                                    <textarea id="ruanglingkup" rows="10" cols="80" name="ruang_lingkup" class="all">
                                        @if(isset($datas))
                                        {{ $datas->bakns['ruang_lingkup'] }}
                                        @else
                                        {{ $data->bakns['ruang_lingkup'] }}
                                        @endif
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="lokasipekerjaan">Lokasi Pekerjaan/Pengiriman Barang/Jasa :</label>
                                    <textarea id="lokasipekerjaan" rows="10" cols="80" name="lokasi_pekerjaan" class="all">
                                        @if(isset($datas))
                                        {{ $datas->bakns['lokasi_pekerjaan'] }}
                                        @else
                                        {{ $data->bakns['lokasi_pekerjaan'] }}
                                        @endif
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="jangkawaktu">Jangka Waktu Pengiriman Barang : </label>
                                    <textarea id="jangkawaktu" rows="10" cols="80" name="jangka_waktu" class="all">
                                        @if(isset($datas))
                                        {{ $datas->bakns['jangka_waktu'] }}
                                        @else
                                        {{ $data->bakns['jangka_waktu'] }}
                                        @endif
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="harga">Harga Pekerjaan :</label>
                                    <textarea id="hargaterbilang" name="harga_terbilang" class="all">
                                        @if(isset($datas))
                                        {{ $datas->bakns['harga_terbilang'] }}
                                        @else
                                        {{ $data->bakns['harga_terbilang'] }}
                                        @endif
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="tatacara">Tata Cara Pembayaran :</label>
                                    <textarea  rows="10" class="all" cols="80" name="cara_bayar"  id="carabayar">
                                        @if(isset($datas))
                                        {{ $datas->bakns['cara_bayar'] }}
                                        @else
                                        {{ $data->bakns['cara_bayar'] }}
                                        @endif
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Lain - lain : </label>
                                    <textarea rows="10" class="all" cols="80" name="lain_lain" id="lainlain">
                                        @if(isset($datas))
                                        {{ $datas->bakns['lain_lain'] }}
                                        @else
                                        {{ $data->bakns['lain_lain'] }}
                                        @endif
                                    </textarea>
                                </div>
                                {{--  end isinya  --}}

                                {{-- end hasil pembahasan --}}
                                @if(isset($data))
                                <div class="form-group">
                                    <label for="">Comment</label>
                                    <input type="text" name="chat" class="form-control">
                                </div>
                                @endif
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" name="status" @if(isset($datas)) value="draft_spk" @else value="draft_sp3" @endif class="btn btn-primary" style="width: 7em;"><i class="fa fa-check"></i> Save</button>
                                <button type="submit" name="status" @if(isset($datas)) value="save_spk" @else value="save_sp3" @endif class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                        {{-- end form --}}
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @endsection

    @section('scripts')
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
    {{-- bahasa indonesia froala --}}
    <script src="{{ asset('froala/js/languages/id.js') }}"></script>

    <!-- Data Table -->
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- clockpicker -->
    <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Tagify -->
    <script src="{{ asset('tagify/dist/jQuery.tagify.min.js') }}"></script>
    <script src="{{ asset('tagify/dist/tagify.min.js') }}"></script>
    <script>
        // $(document).ready(function(){
            //     $("textarea").froalaEditor("edit.off");
            // });
            // aktif froala
            $('.all').froalaEditor({
                placeholderText: '',
                language: 'id',
                charCounterCount: false,
                key: '{{ env("KEY_FROALA") }}',
            });
            $('#chat').froalaEditor({
                key: '{{ env("KEY_FROALA") }}',
                height: 70,
                toolbarButtons:['help'],
                charCounterCount: false,
                // language: 'id',

            })
        </script>
        {{-- input format tanggal --}}
        <script>
            $(function(){
                $(".datejos").on("change", function() {
                    this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                    .format( this.getAttribute("data-date-format") )
                    )
                }).trigger("change")
                $('.datejos').datepicker({
                    autoclose: true,
                    orientation: "bottom"
                })
            })</script>
            @endsection
