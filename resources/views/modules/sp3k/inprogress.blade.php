@extends('layouts.master')

@section('title')
SP3 - SPK Inprogress | Super Slim
@endsection
@section('stylesheets')
<!-- DataTables -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    /* tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    } */
    tfoot {
        display: table-header-group;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SP3 - SPK
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">SP3 - SPK</a></li>
            <li class="active"> SP3 - SPK Inprogress </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> SP3 - SPK INPROGRESS</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_roles" data-toggle="tab">SP3</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_roles">
                                <div class="box-body table-responsive">
                                    <table name="item" id="item" class="display" width="100%" cellspacing="0">
                                        <thead align="center">
                                            <tr style=" white-space: nowrap">
                                                <th rowspan="2" style="text-align:center">No</th>
                                                <th rowspan="2" style="text-align:center">IO</th>
                                                <th rowspan="2" style="text-align:center">Nomor SPPH</th>
                                                <th rowspan="2" style="text-align:center">Judul SPPH</th>
                                                <th rowspan="2" style="text-align:center">Nomor SP3</th>
                                                <th rowspan="2" style="text-align:center">Mitra</th>
                                                <th rowspan="2" style="text-align:center">Tanggal SPPH</th>
                                                <th rowspan="2" style="text-align:center">Tanggal Upload File SPPH</th>
                                                <th rowspan="2" style="text-align:center">Tanggal BAKN</th>
                                                <th rowspan="2" style="text-align:center">Tanggal Upload BAKN</th>
                                                <th rowspan="2" style="text-align:center">Tanggal SP3</th>
                                                <th rowspan="2" style="text-align:center">Tanggal Buat</th>
                                                <th rowspan="2" style="text-align:center">Pembuat</th>
                                                <th rowspan="2" style="text-align:center">Harga</th>
                                                <th colspan="3" style="text-align:center">Approval</th>
                                                <th rowspan="2" style="text-align:center">Posisi</th>                                                
                                                <th rowspan="2" style="text-align:center">Preview</th>                                                
                                                <th rowspan="2" style="text-align:center">Action</th>
                                            </tr>
                                            <tr>
                                                <th style="text-align:center">Staff</th>
                                                <th style="text-align:center">Manager</th>
                                                <th style="text-align:center">GM</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th ></th>
                                                <th class="testing" >No. IO</th>
                                                <th class="testing" >No. SPH</th>
                                                <th class="testing" >Judul SPPH</th>
                                                <th class="testing" >No. SP3</th>
                                                <th class="testing" >Mitra</th>
                                                <th class="testing" >Tanggal SPPH</th>
                                                <th></th>
                                                <th class="testing" >Tanggal BAKN</th>
                                                <th></th>
                                                <th class="testing" >Tanggal SP3</th>
                                                <th></th>
                                                <th class="testing" >Pembuat</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @php
                                            $no=1;
                                            @endphp
                                            @foreach ($sp3 as $item => $value)
                                            <tr style="white-space:nowrap">
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $value->bakns->io['no_io'] }}</td>
                                                <td>{{ $value->bakns->spph['nomorspph'] }}</td>
                                                <td>{{ $value->bakns->spph['judul'] }}</td>
                                                <td>{{ $value->nosp3 }}</td>
                                                <td>{{ $value->bakns->spph->mitras['perusahaan'] }}</td>
                                                <td>{{ $value->bakns->spph['created_at'] }}</td>
                                                <td>{{ $value->bakns->spph['upload'] }}</td>
                                                <td>{{ $value->bakns['created_at'] }}</td>
                                                <td>{{ $value->bakns['upload'] }}</td>
                                                <td>{{ $value->tglsp3 }}</td>
                                                <td>{{ $value->created_at }}</td>
                                                <td>{{ $value->users['name'] }}</td>
                                                <td>{{ number_format($value->bakns['harga'],0,'.','.') }}</td>
                                                <td style="white-space:nowrap">
                                                    @php
                                                    $data[$value->idnya]['id']= $value->id; //idkontrak
                                                    for ($i=0; $i < count($value->chatsp3); $i++) {
                                                        if ($value->chatsp3[$i]['queue']== 0) {
                                                            echo '<br>('.date('d.F.Y H:i', strtotime($value->chatsp3[$i]['created_at'])).' - '.$value->chatsp3[$i]['status'].')<br>'.$value->chatsp3[$i]['name'];
                                                        }
                                                    }
                                                    @endphp
                                                </td>
                                                <td style="white-space:nowrap">
                                                    @php
                                                    $data[$value->idnya]['id']= $value->id; //idkontrak
                                                    for ($i=0; $i < count($value->chatsp3); $i++) {
                                                        if ($value->chatsp3[$i]['queue']== 1) {
                                                            echo '<br>('.date('d.F.Y H:i', strtotime($value->chatsp3[$i]['created_at'])).' - '.$value->chatsp3[$i]['status'].')<br>'.$value->chatsp3[$i]['name'];
                                                        }
                                                    }
                                                    @endphp
                                                </td>
                                                <td style="white-space:nowrap">
                                                    @php
                                                    $data[$value->idnya]['id']= $value->id; //idkontrak
                                                    for ($i=0; $i < count($value->chatsp3); $i++) {
                                                        if ($value->chatsp3[$i]['queue']== 2) {
                                                            echo '<br>('.date('d.F.Y H:i', strtotime($value->chatsp3[$i]['created_at'])).' - '.$value->chatsp3[$i]['status'].')<br>'.$value->chatsp3[$i]['name'];
                                                        }
                                                    }
                                                    @endphp
                                                </td>
                                                @if ($value->status == 'draft_sp3' || $value->approval == 'Return')
                                                    <td>{{ $value->users['name'] }}</td>
                                                @else
                                                    <td>{{ $value->approval }}</td>
                                                @endif
                                                <td>
                                                    <a href="{{ url('spph-preview/'.$value->bakns->spph['id']) }}">Preview SPPH</a><br>
                                                    <a href="{{ url('bakn-preview/'.$value->bakns['id']) }}">Preview BAKN</a><br>
                                                </td>
                                                <td>
                                                    <a href="{{ url('sp3-preview/'.$value->id) }}">Preview SP3</a><br>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>

 <script type="text/javascript">
    // Setup - add a text input to each footer cell
    $('#item tfoot .testing').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
    
    // DataTable
    var otable = $('.display').DataTable();
    
    // Apply the search
    otable.columns().every( function () {
        
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                .search( this.value )
                .draw();
            }
        } );
    } );
</script>
@endsection
