@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Approve SP3";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{-- smartwizard --}}
{{-- <link href="{{ asset('smartwizard/dist/css/smart_wizard.css') }}" rel="stylesheet" type="text/css" /> --}}
{{-- <link href="{{ asset('smartwizard/dist/css/smart_wizard_theme_circles.css') }}" rel="stylesheet" type="text/css" /> --}}
<link href="{{ asset('smartwizard/dist/css/smart_wizard_theme_circles.min.css') }}" rel="stylesheet" type="text/css" />

{{-- end smartwizard --}}
@endsection
@section('customstyle')

<style type="text/css">
    table tr:not(:first-child){
        cursor: pointer;transition: all .25s ease-in-out;
    }
    table tr:not(:first-child):hover{background-color: #ddd;}
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
    .pulse {
        width: 20%;
        --color: #ef6eae;
        --hover: #ef8f6e;
    }
    .pulse:hover,
    .pulse:focus {
        -webkit-animation: pulse 1s;
        animation: pulse 1s;
        box-shadow: 0 0 0 2em rgba(255, 255, 255, 0);
    }

    @-webkit-keyframes pulse {
        0% {
            box-shadow: 0 0 0 0 var(--hover);
        }
    }

    @keyframes pulse {
        0% {
            box-shadow: 0 0 0 0 var(--hover);
        }
    }
    .close:hover,
    .close:focus {
        box-shadow: inset -3.5em 0 0 0 var(--hover), inset 3.5em 0 0 0 var(--hover);
    }
    .fr-wrapper {
        margin-bottom: 1em;
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Approve SP3
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">SP3 - SPK</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Approve SP3</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>

                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {{-- <form method="post" action="" enctype="multipart/form-data"> --}}
                        {{-- @csrf --}}
                        <div class="box-body">
                            <div class="box-body">
                                <div class="form-group" hidden>

                                </div>
                                <div class="form-groupp">
                                    <div id="smartwizard">
                                        <ul>
                                            <li><a href="#step-1">Step 1<br /><small>Draft BAKN</small></a></li>
                                            <li><a href="#step-2">Step 1<br /><small>Draft SP3</small></a></li>
                                            <li><a href="#step-3">Step 2<br /><small>Approval</small></a></li>
                                        </ul>
                                        <div>
                                            <div id="step-1" class="">
                                                    <textarea id="froala-editor">
                                                            <table class="table" >
                                                                <tbody>
                                                                    <tr>
                                                                        <td rowspan="4" colspan="1" style="text-align:center; vertical-align:middle;"><img src="{{ asset('images/pinlogo.png') }}" width="190" /></td>
                                                                        <td colspan="3">
                                                                            <div style="text-align: center;">
                                                                                <strong class="judul">
                                                                                    <center>BERITA ACARA KLARIFIKASI & NEGOSIASI</br>
                                                                                        {{ strtoupper($sp3->bakns->spph['judul']) }}</br>
                                                                                        ANTARA</br>
                                                                                        PT. PINS INDONESIA</br>
                                                                                        DENGAN</br>
                                                                                        <span id="phk2">{{ strtoupper($sp3->bakns->spph->mitras['perusahaan']) }}</span></center>
                                                                                    </strong>
                                                                                    <input type="hidden" id="hd_phk2" value="{{ $sp3->bakns['secondholder'] }}" />
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Tanggal</td>
                                                                            <td colspan="3">{{ Carbon\Carbon::parse($sp3->bakns['tglbakn'])->formatLocalized('%d %B %Y') }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Waktu</td>
                                                                            <td colspan="3">- WIB</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Tempat</td>
                                                                            <td colspan="3">Kantor PT. PINS INDONESIA</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="1">Undangan dari</td>
                                                                            <td colspan="3">PT. PINS INDONESIA</td>
                                                                        </tr>
                    
                                                                        <tr>
                                                                            <td colspan="1">Tipe Rapat</td>
                                                                            <td colspan="3">
                                                                                <ul class="icheck-list">
                                                                                    <li style="display: inline;">
                                                                                        <input type="checkbox" class="check" id="minimal-checkbox-1" {{ in_array("Review",$checkbox)? "checked":"" }}>
                                                                                        <label for="minimal-checkbox-1">Review</label>
                                                                                    </li>
                                                                                    <li style="display: inline;">
                                                                                        <input type="checkbox" class="check" id="minimal-checkbox-2" {{ in_array("Coordination",$checkbox)? "checked":"" }}>
                                                                                        <label for="minimal-checkbox-2">Coordination</label>
                                                                                    </li>
                                                                                    <li style="display: inline;">
                                                                                        <input type="checkbox" class="check" id="minimal-checkbox-3" {{ in_array("Briefing",$checkbox)? "checked":"" }}>
                                                                                        <label for="minimal-checkbox-disabled">Briefing</label>
                                                                                    </li>
                                                                                    <li style="display: inline;">
                                                                                        <input type="checkbox" class="check" id="minimal-checkbox-4" {{ in_array("Decision Marking",$checkbox)? "checked":"" }}>
                                                                                        <label for="minimal-checkbox-disabled-checked">Decision
                                                                                            Making</label>
                                                                                        </li>
                                                                                    </ul>
                                                                                </td>
                                                                            </tr>
                    
                                                                            <tr>
                                                                                <td colspan="1">Pimpinan Rapat</td>
                                                                                <td colspan="3">{{ $sp3->bakns['pimpinan_rapat'] }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="1">Peserta</td>
                                                                                <td colspan="2"><strong>{{ strtoupper($sp3->bakns->spph->mitras['perusahaan'])}}:</strong>
                                                                                    <ol>
                                                                                        {{--  @php
                                                                                            $datas = json_decode($sp3->bakns['peserta_mitra'], true);
                                                                                            for ($i=0; $i<count($datas); $i++) {
                                                                                                echo '<li>'.$datas[$i]["value"].'</li>';
                                                                                            }
                                                                                            @endphp  --}}
                                                                                            {{ $sp3->bakns->peserta_mitra }}
                                                                                        </ol>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <strong>PT. PINS INDONESIA :</strong>
                                                                                        <ol>
                                                                                            @php
                                                                                            $datas = json_decode($sp3->bakns['peserta_pins'], true);
                                                                                            for ($i=0; $i<count($datas); $i++) {
                                                                                                echo '<li>'.$datas[$i]["value"].'</li>';
                                                                                            }
                                                                                            @endphp
                                                                                        </ol>
                    
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4" class="tengah" style="background-color: rgb(204, 204, 204);">
                                                                                        <strong>
                                                                                            <center>AGENDA</center>
                                                                                        </strong>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4" >
                                                                                        <center>
                                                                                            {{ $sp3->bakns['agenda'] }}
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" class="tengah" style="background-color: rgb(204, 204, 204);">
                                                                                            <strong>
                                                                                                <center>DASAR PEMBAHASAN</center>
                                                                                            </strong>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" >
                                                                                            <!-- {{ $sp3->bakns }}  -->
                                                                                            {{ $sp3->bakns['dasar_pembahasan'] }}
                    
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4" class="tengah" style="background-color: rgb(204, 204, 204);">
                                                                                            <strong>
                                                                                                <center>HASIL - HASIL KLARIFIKASI & NEGOSIASI</center>
                                                                                            </strong>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="4">
                                                                                            SUPPLIER dan PT. PINS Indonesia sepakat melakukan
                                                                                            klarifikasi dan negosiasi dengan hasil sebagai berikut
                                                                                            :
                                                                                        </br>
                                                                                        <b>1. Ruang Lingkup :</b>
                                                                                        {{ $sp3->bakns['ruang_lingkup'] }}
                                                                                    </br>
                                                                                    <b>2. Lokasi Pekerjaan/ Pengiriman Barang/ Jasa :</b>
                                                                                    {{ $sp3->bakns['lokasi_pekerjaan'] }}
                                                                                </br>
                                                                                <b>3. Jangka Waktu Pengiriman Barang :</b>
                                                                                {{ $sp3->bakns['jangka_waktu'] }}
                                                                            </br>
                                                                            <br><b>4. Harga :</b>
                                                                            {{ $sp3->bakns['harga_terbilang'] }}
                                                                        </br>
                                                                        <b>5. Tatacara Pembayaran :</b>
                    
                                                                        {{ $sp3->bakns['cara_bayar'] }}
                                                                    </br>
                                                                    <b>6. Lain - lain :</b>
                                                                    {{ $sp3->bakns['lain_lain'] }}
                                                                </br>
                                                            </br>
                                                            Demikian Berita Acara Klarifikasi dan Negosiasi
                                                            ini dibuat.
                                                        </td>
                                                    </tr>
                    
                                                    <tr>
                                                        <td colspan="2" width="50%">
                                                            <p style="text-align:center">{{ $sp3->bakns->spph->mitras['perusahaan'] }}</p>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <p style="text-align:center">{{ $sp3->bakns->spph->mitras['direktur'] }}</p>
                    
                                                        </td>
                                                        <td colspan="2" width="50%">
                                                            <p style="text-align:center">PT PINS INDONESIA</p>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <p style="text-align:center">{{ $sp3->bakns->pimpinan_rapat }}</p>
                    
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </textarea>
                                            </div>
                                            <div id="step-2" class="">
                                                @include('modules.sp3k.inc_preview')
                                            </div>
                                            <div id="step-3" class="">
                                                <h4>Coment and Approve</h4>
                                                <ul class="timeline">
                                                    @foreach ($chats as $isi)
                                                    <!-- timeline time label -->
                                                    <li class="time-label">
                                                        <span class="bg-green">
                                                            {{ date('d M.Y', strtotime($isi->created_at)) }}
                                                        </span>
                                                    </li>
                                                    <!-- /.timeline-label -->
                                                    <!-- timeline item -->
                                                    <li>
                                                        <!-- timeline icon -->
                                                        <i class="fa fa-user bg-aqua"></i>
                                                        <div class="timeline-item">
                                                            <span class="time">
                                                                <i class="fa fa-clock-o"></i>
                                                                {{ date('H:i:s', strtotime($isi->created_at)) }}
                                                            </span>
                                                            <h3 class="timeline-header"><a href="#">{{ $isi->jabatan.' - '.$isi->name }}</a></h3>
                                                            <div class="timeline-body">
                                                                {{ $isi->chat }}
                                                            </div>

                                                            {{-- <div class="timeline-footer">
                                                                <a class="btn btn-primary btn-xs">...</a>
                                                            </div> --}}
                                                        </div>
                                                    </li>

                                                    <!-- END timeline item -->
                                                    @endforeach
                                                    <li>
                                                        <i class="fa fa-clock-o bg-gray"></i>
                                                    </li>
                                                </ul>
                                                <br>
                                                {{-- start form chat --}}
                                                @if($sp3->approval == Auth::user()->username || Auth::user()->level == 'administrator')
                                                <form action="{{url('chat-sp3') }}" class="form-group" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                    <input type="hidden" value="{{ $sp3->id }}" name="idsp3">
                                                    <div class="col-md-12 pad-0">
                                                        <div class="col-md-6 pad-0">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Comment</label>
                                                                <input type="text" class="form-control" name="chat"  id="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6" hidden>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">ID Transaksi</label>
                                                                <input type="text" class="form-control" name="idTransaksi" value="{{ $sp3->id }}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mar-paginate pad-0">
                                                            <button type="submit" class="btn btn-danger" name="status" value="Return">Return</button>
                                                            <button type="submit" class="btn btn-primary" name="status"  value="Approve">Approve</button>
                                                        </div>
                                                    </div>
                                                </form>
                                                @endif
                                                {{-- end form --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    {{-- </form> --}}
                </div>
                <!-- /.box -->

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @endsection

    @section('scripts')
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
    <!-- Initialize the editor. -->
    <script src="{{ asset('froala/js/languages/id.js') }}"></script>

    <script>
        $('textarea').froalaEditor({
            // toolbarButtons: ['container'],
            // toolbarButtons: ['getPDF', 'print'],
            toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'inlineClass', 'clearFormatting', '|', 'emoticons', 'fontAwesome', 'specialCharacters', 'paragraphFormat', 'lineHeight', 'paragraphStyle', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', 'insertHR', 'selectAll', 'help', 'html', 'fullscreen', '|', 'undo', 'redo', 'getPDF', 'print'],
            placeholderText: '',
            documentReady: true,
            language: 'id',
            key: '{{ env("KEY_FROALA") }}',
        })</script>
        {{-- smart wizard --}}

        <script type="text/javascript" src="{{ asset('smartwizard/dist/js/jquery.smartWizard.min.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('div#smartwizard').smartWizard();
                $('div#smartwizardcircle').smartWizard();
            });</script>
            <!-- Data Table -->
            <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            <!-- date-range-picker -->
            <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
            <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
            <!-- bootstrap datepicker -->
            <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
            <!-- iCheck 1.0.1 -->
            <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
            <!-- clockpicker -->
            <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
            <!-- Select2 -->
            <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
            <!-- CK Editor -->
            {{-- <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script> --}}

            @endsection
