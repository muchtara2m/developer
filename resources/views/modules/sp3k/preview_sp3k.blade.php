@extends('layouts.master')
@php
if (isset($sp3)) {
    $res = $sp3;
    $refno = $sp3->nosp3;
    $docdate = $sp3->tglsp3;
    $doctitle = "surat penetapan pelaksanaan pekerjaan (sppp)";
}
if (isset($spk)) {
    $res = $spk;
    $refno = $spk->nospk;
    $docdate = $spk->tglspk;
    $doctitle = "surat perintah kerja (spk)";
}
@endphp
@php
$homelink = "/home";
$crpagename = "Preview SP3/SPK";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('customstyle')

<style type="text/css">
    table, th, td {
        border: 1px solid black;
    }
    
    .image-uploader input.main {
        position: absolute;
        right: 0;
        margin: 0;
        opacity: 0;
        -webkit-transform-origin: right;
        -moz-transform-origin: right;
        -ms-transform-origin: right;
        -o-transform-origin: right;
        transform-origin: right;
        -webkit-transform: scale(14);
        -moz-transform: scale(14);
        -ms-transform: scale(14);
        -o-transform: scale(14);
        transform: scale(14);
        font-size: 23px;
        direction: ltr;
        cursor: pointer;
    }
    
    .image-uploader {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        margin: 1.6em 0;
        position: relative;
        overflow: hidden;
        padding: 55px 60px;
        border: #edece4 3px dashed;
        width: 100%;
        height: auto;
        text-align: center;
        color: #aaa9a2;
        background: #F9F8F5;
    }
    
    #pelangi:hover {
        -webkit-animation: zomg 0.5s infinite;
        animation: zomg 0.5s infinite;
    }
    
    @-webkit-keyframes zomg {
        0%, 100% { color: #7ccdea; }
        16%      { color: #0074d9; }
        32%      { color: #2ecc40; }
        48%      { color: #ffdc00; }
        64%      { color: #b10dc9; }
        80%      { color: #ff4136; }
    }
    @keyframes zomg {
        0%, 100% { color: #7ccdea; }
        16%      { color: #0074d9; }
        32%      { color: #2ecc40; }
        48%      { color: #ffdc00; }
        64%      { color: #b10dc9; }
        80%      { color: #ff4136; }
    }
    .class1 tbody tr:nth-child(2n) {
        background: #f9f9f9;
    }
    
    .class2 thead tr th,
    .class2 tbody tr td {
        border-style: dashed;
    }
    textarea#froala{
        width: -webkit-fill-available;
    }
    div span.fr-counter{
        display:none;
    }
    strong.judul{
        text-align: center
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Preview SP3/SPK
            <!-- <small>Form PBS</small> -->
        </h1>
        @if (\Session::has('Success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('Success') }}</p>
        </div><br />
        @endif
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Preview SP3/SPK</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Preview SP3/SPK</h3>
                        <button onclick="window.history.go(-1); return false;" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="white-box">
                                <textarea id="froala-editor">
                                    <p>Ref. No &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp; &nbsp;{{ $refno }}</p>
                                    <p>Tanggal (<em>date</em>) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: &nbsp; &nbsp;{{ Carbon\Carbon::parse($docdate)->formatLocalized('%d %B %Y') }}</p>
                                    <p style="text-align: center; text-transform:uppercase"><strong>{{ $doctitle }}</strong></p>
                                    <br/>
                                    <span style="display:inline-block; width:50%; vertical-align:top;"><em>Referensi:</em></span>
                                    <span style="display:inline-block; width:50%; vertical-align:top;">Kepada (<em>to</em>):</span>
                                    <span style="display:inline-block; width:50%; vertical-align:top;">Berita Acara Klarifikasi & Negosiasi Instalasi
                                        {{ $res->bakns->spph['judul']}} tanggal {{ Carbon\Carbon::parse($res->bakns['tglbakn'])->formatLocalized('%d %B %Y') }}</span>
                                        <span style="display:inline-block; width:50%; vertical-align:top;">Direktur {{ $res->bakns->spph->mitras['perusahaan'] }}<br/>{{ $res->bakns->spph->mitras['alamat'] }}
                                        </span>
                                        
                                        <p><br/></p>
                                        <b>1. Ruang Lingkup :</b>
                                        &nbsp; &nbsp; {{ $res->bakns['ruang_lingkup'] }}
                                    </br>
                                    <b>2. Lokasi Pekerjaan/ Pengiriman Barang/ Jasa :</b>
                                    &nbsp; &nbsp; {{ $res->bakns['lokasi_pekerjaan'] }}
                                </br>
                                <b>3. Jangka Waktu Pengiriman Barang :</b>
                                &nbsp; &nbsp; {{ $res->bakns['jangka_waktu'] }}
                            </br>
                            <br><b>4. Harga :</b>
                            &nbsp; &nbsp; {{ $res->bakns['harga_terbilang'] }}
                        </br>
                        <b>5. Tatacara Pembayaran :</b>
                        &nbsp; &nbsp; {{ $res->bakns['cara_bayar'] }}
                    </br>
                    <b>6. Lain - lain :</b>
                    &nbsp; &nbsp; {{ $res->bakns['lain_lain'] }}
                </br>
                <p><em>Demikian Surat Penetapan ini dibuat sebagai dasar pelaksanaan pekerjaan sebelum ditandatanganinya Perjanjian/Kontrak.</em></p>
                <span style="display:inline-block; width:50%; vertical-align:top;">
                    <p style="text-align:center">{{ $res->bakns->spph->mitras['perusahaan'] }}</p>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <p style="text-align:center">{{ $res->bakns->spph->mitras['direktur'] }}</p>
                </span>
                <span style="display:inline-block; width:50%; vertical-align:top;">
                    <p style="text-align:center">PT. PINS Indonesia</p>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <p style="text-align:center">{{ $res->bakns['pimpinan_rapat'] }}</p>
                </span>
            </textarea>
        </div>
    </div>
</div>
<div class="box box-footer">
    <div class="container" style="padding-left:1%;">
        @if (Auth::user()->level == 'mgrlegal')
        <form class="btn-form" action="{{ action('BaknController@returnbakn',$res->id) }}" method="post">
            @csrf
            <button type="submit" class="btn btn-warning" style="width: 7em;"><i class="fa fa-refresh"></i>
                Return</button>
            </form>
            <a href="#modal_unit{{ $res->id }}"  data-toggle="modal" data-target="#modal-unit_{{ $res->id }}" class="btn btn-primary" title="Dispatch Kontrak"><i class="fa fa-user-plus"></i> Dispatch</a>
            {{-- <a href = "return/{{ $bakn->id }}" class="btn btn-primary" onclick="return confirm('Bener nih mau direturn?')" title="Return BAKN">Return BAKN</a> --}}
            <div class="modal fade" id="modal-unit_{{ $sp3->id }}">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Pilih Penanggung Jawab</h4>
                            </div>
                            <div class="modal-body">
                                <form method="post" class="form" action="{{action('KontrakController@dispsp3', $res->id)}}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" value="{{ $res->id }}" hidden>
                                        <label for="handler">Penanggung Jawab</label>
                                        <select name="handler" id="handler" class="form-control">
                                            <option disabled selected>Pilih Penanggung Jawab</option>
                                            @foreach ($handlers as $item)
                                            <option value="{{ $item->username }}" {{ (old('handler') == $item->name) ? "selected" : "" }} >{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                        
                                    </div>
                                    <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i>
                                        Submit</button>
                                        
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    @endif
                </div>
            </div>
        </div>
    </div>
    
</div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
{{-- bahasa indonesia froala --}}
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
{{-- select2 --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<!-- Initialize the editor. -->
<script>
    $('textarea#froala-editor').froalaEditor({
        // Define new table cell styles.
        toolbarButtons: ['print','html'],
        tabSpaces: 4,
        charCounterCount: false,
        fullPage: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'inlineClass', 'clearFormatting', '|', 'emoticons', 'fontAwesome', 'specialCharacters', 'paragraphFormat', 'lineHeight', 'paragraphStyle', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '-', 'insertHR', 'selectAll', 'help', 'html', 'fullscreen', '|', 'undo', 'redo', 'getPDF', 'print'],
        key: '{{ env("KEY_FROALA") }}',
    })</script>
    <script>
        $('select').select2();
        function getRoles(val) {
            $('#role-cm_role_text').val('');
            var data = $('#role-cm_role').select2('data').map(function(elem){ return elem.text} );
            console.log(data);
            $('#role-cm_role_text').val(data);
            $('#role-cm_role').on('select2:unselecting', function (e) {
                $('#role-cm_role_text').val('');
            });
        }</script>
        {{-- signature --}}
        <script src="{{ asset('signature/jSignature.min.js') }}"></script>
        <!-- date-range-picker -->
        <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <!-- bootstrap datepicker -->
        <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
        <!-- iCheck 1.0.1 -->
        <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
        <!-- clockpicker -->
        <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
        <!-- Select2 -->
        <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
        
        @endsection
        