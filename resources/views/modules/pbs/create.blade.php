@extends('layouts.master')

@section('title')
Create PBS | Super Slim
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
</style>
@endsection

@section('content')

@php ( $homelink = "/home")

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            PBS
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Transaksi</a></li>
            <li class="active">Create PBS </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> FORM PBS</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="">
                        @csrf
                        <div class="box-body">
                            <div class="form-group col-md-12">
                                <div class="col-md-2">
                                    <label for="inputEmail3" class="control-label">Unit*</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="kodeunit" class="form-control" id="kodeunit" placeholder="Kode Unit" readonly>
                                </div>
                                <div class="col-md-7">
                                    <div class="input-group">
                                        <input type="text" name="unit" class="form-control" id="unit" placeholder="Unit" readonly>
                                        <span class="input-group-btn">
                                            <label class="btn btn-primary" title="Browse" data-toggle="modal" data-target="#modal-unit">
                                                <span class="fa fa-search"></span>
                                                <span class="hidden-xs">Browse</span>
                                            </label>
                                        </span>
                                    </div>
                                </div>
                                <!-- modal -->
                                <div class="modal fade" id="modal-unit">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Data Unit</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Kode Unit</th>
                                                                <th>Unit</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @php($i =1)
                                                            @foreach ($units as $item)
                                                            <tr>
                                                                <td>{{ $i++ }}</td>
                                                                <td>{{ $item->kode }}</td>
                                                                <td>{{ $item->nama }}</td>
                                                                <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-md-2">
                                        <label for="inputEmail3" class="control-label">IO*</label>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" name="io" class="form-control" id="io" placeholder="IO" readonly>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <input type="text" name="deskripsi" class="form-control" id="deskripsi" placeholder="Deskripsi" readonly>
                                            <span class="input-group-btn">
                                                <label class="btn btn-primary" title="Browse" data-toggle="modal" data-target="#modal-io">
                                                    <span class="fa fa-search"></span>
                                                    <span class="hidden-xs">Browse</span>
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- modal -->
                                    <div class="modal fade" id="modal-io">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Data IO</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>IO</th>
                                                                    <th>Deskripsi</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @php($i =1)
                                                                @foreach ($ios as $item)
                                                                <tr>
                                                                    <td>{{ $i++ }}</td>
                                                                    <td>{{ $item->no_io }}</td>
                                                                    <td>{{ $item->deskripsi }}</td>
                                                                    <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                        <button type="button" class="btn btn-primary">Save changes</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="col-md-2">
                                            <label for="inputEmail3" class="control-label">Tgl Dokumen*</label>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="tgldokumen" class="form-control pull-right" id="document-date">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="control-label">E-commerce</label>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-control" name="bisnischannel">
                                                <option value="Y" selected>E-Commerce</option>
                                                <option value="N" >Non-Ecommerce</option>
                                                <option value="BI" >Business Inovation</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="col-md-2">
                                            <label for="mitra" class="control-label">Mitra*</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="kode" id="nomormitra" placeholder="Kode Mitra" readonly>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="mitra" id="mitra" placeholder="Mitra" readonly>
                                                <span class="input-group-btn">
                                                    <label class="btn btn-primary" title="Browse" data-toggle="modal" data-target="#modal-customer">
                                                        <span class="fa fa-search"></span>
                                                        <span class="hidden-xs">Browse</span>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                        <!-- modal -->
                                        <div class="modal fade" id="modal-customer">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title">Data Customer</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                                <table class="table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>No</th>
                                                                                <th>Kode Mitra</th>
                                                                                <th>Mitra</th>
                                                                                <th>Action</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @php($i =1)
                                                                            @foreach ($mitras as $item)
                                                                            <tr>
                                                                                <td>{{ $i++ }}</td>
                                                                                <td>{{ $item->direktur }}</td>
                                                                                <td>{{ $item->perusahaan }}</td>
                                                                                <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                                                            </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save changes</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="col-md-2">
                                                <label for="inputEmail3" class="control-label">Deskripsi*</label>
                                            </div>
                                            <div class="col-md-10">
                                                <textarea class="form-control" placeholder="Deskripsi"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="col-md-2">
                                                <label for="inputEmail3" class="control-label">End User*</label>
                                            </div>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" placeholder="End User">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="col-md-2">
                                                <label for="inputEmail3" class="control-label">Tgl End Delivery*</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" id="enddelivery-date">
                                                </div>
                                            </div>
                                                    <div class="col-md-2">
                                                        <label for="Type Product" class="control-label">Type Product*</label>
                                                    </div>
                                                    <div class="col-md-4">
                                                       <select name="" id="" class="form-control">
                                                            <option value="3">router</option>
                                                            <option value="4">switch</option>
                                                            <option value="5">Access Point</option>
                                                            <option value="6">Jaringan LAN</option>
                                                            <option value="7">Seat Management</option>
                                                            <option value="8">IP Camera</option>
                                                            <option value="9">Smart Building</option>
                                                            <option value="10">Smart Home</option>
                                                            <option value="11">Mobility</option>
                                                            <option value="12">Wifi.id</option>
                                                            <option value="13">GPON</option>
                                                            <option value="14">Panic Button</option>
                                                            <option value="15">Fiber Optic</option>
                                                            <option value="2">-</option>
                                                            <option value="1">-</option>
                                                            </select>
                                                       </select>
                                                    </div>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <div class="col-md-2">
                                                <label for="inputEmail3" class="control-label">Layanan (Date Range)*</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" id="layanan-date-range">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="control-label">Garansi (Date Range)*</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" id="garansi-date-range">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="col-md-2">
                                                <label for="inputEmail3" class="control-label">Doc BOQ*</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="doc-boq" readonly>
                                                    <span class="input-group-btn">
                                                        <label class="btn btn-primary" title="Browse">
                                                            <span class="fa fa-search"></span>
                                                            <span class="hidden-xs">Browse</span>
                                                            <input type="file" id="file-boq" class="{{ $errors->has('file-boq') ? ' is-invalid' : '' }}" style="display: none;" onchange="document.getElementById('doc-boq').value = this.files[0].name">
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="control-label">Doc Customer*</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                        <select name="" class="form-control" id="">
                                                                <option value="BAKN">BAKN</option>
                                                                <option value="MoM">MoM</option>
                                                                <option value="SP">SP</option>
                                                                <option value="NDE">NDE</option>
                                                        </select>
                                                        <br>
                                                        <input type="file" name="docmitra"  id="">
                                                    {{-- <input type="hidden" class="form-control" id="doc-customer" readonly>
                                                    <span class="input-group-btn">
                                                        <label class="btn btn-primary" title="Browse">
                                                            <span class="fa fa-search"></span>
                                                            <span class="hidden-xs">Browse</span>
                                                            <input type="file" id="file-customer" class="{{ $errors->has('file-customer') ? ' is-invalid' : '' }}" style="display: none;" onchange="document.getElementById('doc-customer').value = this.files[0].name">
                                                        </label>
                                                    </span> --}}

                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary" style="width: 7em;">Next</button>
                                        <a href="{{ url('pbs-purchasing') }}" >PBS Purchasing Sementara</a>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>
                            <!-- /.box -->
                        </div>
                        <!--/.col (right) -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            @endsection

            @section('scripts')
            <!-- date-range-picker -->
            <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
            <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
            <!-- bootstrap datepicker -->
            <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
            <!-- iCheck 1.0.1 -->
            <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
            <script type="text/javascript">
                //Date picker
                $('#document-date').datepicker({
                    autoclose: true,
                    orientation: "bottom"
                })
                $('#enddelivery-date').datepicker({
                    autoclose: true
                })
                //Date range picker
                $('#layanan-date-range').daterangepicker()
                $('#garansi-date-range').daterangepicker()
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass   : 'iradio_flat-green'
                })
            </script>
            @endsection
