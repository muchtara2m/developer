@extends('layouts.master')

@php
  $homelink = "/home";
  $crpagename = "BAKN";
@endphp

@section('title')
  {{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <!-- Clockpicker -->
  <link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
@endsection
@section('customstyle')
  <style type="text/css">
  	.form-horizontal .form-group {
  		margin-right: unset;
  		margin-left: unset;
  	}
  	.example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }
    .example-modal .modal {
      background: transparent !important;
    }
    .no-bullet {
    	padding-left: 0;
    	list-style-type: none;
    }
  </style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      BAKN
      <!-- <small>Form PBS</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li><a href="#">Kontrak & BAKN</a></li>
      <li class="active">{{ $crpagename }} </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-file-text"></i> Form BAKN</h3>
            <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form method="post" action="{{action('MitraController@update', $id)}}">
                @csrf
                @method('PATCH')
            <div class="box-body">
	          <div class="form-group col-md-12">
	          	 <div class="form-group">
	          	   <label for="exampleInputEmail1">Nama Perusahaan</label>
                     <input type="text" class="form-control" name="perusahaan" value="{{$mitra->perusahaan}}">
	             </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Alamat Perusahaan</label>
                 <input type="text" class="form-control" name="alamat" value="{{$mitra->alamat}}">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Nama Direktur</label>
                 <input type="text" class="form-control" name="direktur" value="{{$mitra->direktur}}">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Nama PIC</label>
                 <input type="text" class="form-control" name="pic" value="{{$mitra->pic}}">
               </div>
	          </div>
            <div class="form-group col-md-6">
              <label for="exampleInputEmail1">Email</label>
              <input type="text" class="form-control" name="email" value="{{$mitra->email}}">
            </div>
            <div class="form-group col-md-6">
              <label for="exampleInputEmail1">Telepon</label>
              <input type="text" class="form-control" name="telp" value="{{$mitra->telp}}">
            </div>
          </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
  <!-- date-range-picker -->
  <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
  <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- bootstrap datepicker -->
  <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <!-- iCheck 1.0.1 -->
  <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
  <!-- clockpicker -->
  <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
  <!-- Select2 -->
  <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

@endsection
