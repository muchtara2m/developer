@extends('layouts.master')

@section('title')
  Create PBS | Super Slim
@endsection

@section('stylesheets')
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <style type="text/css">
    .form-horizontal .form-group {
      margin-right: unset;
      margin-left: unset;
    }
  </style>
@endsection

@section('content')

@php ( $homelink = "/home" )

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      TRANSAKSI
      <!-- <small>Form PBS</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li><a href="#">Transaksi</a></li>
      <li class="active">Kalkulator Pinjaman</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-calculator"></i> SIMULASI KREDIT</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form class="form-horizontal">
            <div class="box-body">
            <div class="form-group col-md-12">
               <div class="col-md-2">
                 <label for="inputEmail3" class="control-label">Pinjaman</label>
               </div>
               <div class="col-md-4">
                 <input type="text" class="form-control" id="inputEmail3" placeholder="Email">
               </div>
               <div class="col-md-2">
                 <label for="inputEmail3" class="control-label">Lama Pinjaman</label>
               </div>
               <div class="col-md-4">
                 <input type="text" class="form-control" id="inputEmail3" placeholder="Email">
               </div>
            </div>
            <div class="form-group col-md-12">
               <div class="col-md-2">
                 <label for="inputEmail3" class="control-label">Bunga Pinjaman/Tahun</label>
               </div>
               <div class="col-md-4">
                 <input type="text" class="form-control" id="inputEmail3" placeholder="Email">
               </div>
               <div class="col-md-2">
                 <label for="inputEmail3" class="control-label">Perhitungan Bunga</label>
               </div>
               <div class="col-md-4">
                 <select class="form-control">
                  <option selected>Flat</option>
                  <option>Efektif</option>
                  <option>Anuitas</option>
                 </select>
               </div>
            </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-block btn-primary">Kalkulasi</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
  <!-- date-range-picker -->
  <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
  <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <!-- bootstrap datepicker -->
  <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <!-- iCheck 1.0.1 -->
  <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
  <script type="text/javascript">
    //Date picker
    $('#document-date').datepicker({
      autoclose: true
    })
    $('#enddelivery-date').datepicker({
      autoclose: true
    })
    //Date range picker
    $('#layanan-date-range').daterangepicker()
    $('#garansi-date-range').daterangepicker()
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
  </script>
@endsection