@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Data Pimpinan Rapat";
$crmenuname = "Master Data"
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
@endsection

@section('customstyle')
<style type="text/css">
  .form-horizontal .form-group {
    margin-right: unset;
    margin-left: unset;
  }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ $crpagename }}
      <!-- <small>Form PBS</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li><a href="#">{{ $crmenuname }}</a></li>
      <li class="active">{{ $crpagename }} </li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-file-text"></i> Form Data Karyawan</h3>
            <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form method="POST" action="{{ route('chairman.update', $chairman->id) }}" enctype="multipart/form-data" class="form">
            @csrf
            @method('PATCH')
            <div class="box-body">
              
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                There was a problem, please check your form carefully.
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif
              
              <div class="form-group col-md-6 col-md-offset-3">
                <label>Jabatan*</label>
                <input name="jabatan" type="text" class="form-control" value="{{ $chairman->jabatan }}" autofocus required>
              </div>
              <div class="form-group col-md-6 col-md-offset-3">
                <label>Nama*</label>
                <input name="nama" type="text" class="form-control" value="{{ $chairman->nama }}" required>
              </div>
              <input type="hidden" name="updated_by" value="{{ Auth::id() }}">
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success col-md-3 col-md-offset-8" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
@endsection