@extends('layouts.master')

@section('title')
Index Chairmen | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css">
<style type="text/css">
  .form-horizontal .form-group {
    margin-right: unset;
    margin-left: unset;
  }
  tfoot input {
    width: 100%;
    padding: 3px;
    box-sizing: border-box;
  }
</style>
@endsection

@section('content')

@php 
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  
  @guest
  <div class="container container-table">
    <div class="row middle-center">
      <div class="session-ended bg-danger">
        Maaf Anda belum login / Session Anda telah habis.
      </div>
    </div>
  </div>
  
  @else
  <section class="content-header">
    <h1>
      DATA PIMPINAN RAPAT
      <!-- <small>Form</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li><a href="#">Master Data</a></li>
      <li class="active"> Data Pimpinan Rapat</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-primary">
          <div class="box-header with-border">
            
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
              <button href="#" class="close" data-dismiss="alert" aria-label="close">&times;</button>
              {{ $message }}
            </div>
            @endif
            
            <h3 class="box-title"><i class="fa fa-book"></i> Daftar Pimpinan Rapat</h3>
            <a href="{{ route('chairman.create') }}">
              <button class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah Data</button>
            </a>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
            <table id="chairmanTable" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Jabatan</th>
                  <th>Nama</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($chairmen as $key => $chairman)
                <tr>
                  <td>{{ $chairman->jabatan }}</td>
                  <td>{{ $chairman->nama }}</td>
                  <td class="text-center">
                    <div class="btn-group  btn-group-sm">
                      <a href="{{ route('chairman.edit', $chairman->id) }}">
                        <button class="btn btn-success btn-xs" type="button"><i class="fa fa-pencil"  title="Edit Data"></i></button></a>
                        
                        <form action="{{ route('chairman.destroy', $chairman->id) }}" method="POST" style="display: inline;">
                          @csrf
                          @method('DELETE')
                          <button type="submit" onclick="return confirm('Are you sure want to delete it?')" id="delete-btn" class="btn btn-danger btn-xs click-hand" title="Delete Data"><i class="fa fa-trash"></i>
                          </button>
                        </form>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    
    @endguest  
  </div>
  <!-- /.content-wrapper -->
  
  @endsection
  
  @section('scripts')
  <!-- DataTables -->
  <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>
  <script type="text/javascript">
    // document.getElementById("chairmanTable_wrapper").style.overflow = "auto";
    $(document).ready( function () {
      $('#chairmanTable').DataTable({
      });
    } );
  </script>
  @endsection