@extends('layouts.master')

@section('title')
Print Preview | Super Slim
@endsection
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('stylesheets')

<!-- DataTables -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>

    // printDivCSS = new String('<style>header, footer, aside, nav, form, iframe, .menu, .hero, .adslot {display: none;} body{margin: 30mm 0mm 100mm 0mm;}</style>');
    printDivCSS = new String('<style>@media print{@page {size: A4;margin :20mm 20mm 20mm 20mm;}.table {width: 100%;max-width: 100%;margin-bottom: 20px;border-collapse: collapse;}}</style>');
    // printDivCSS = new String('<style>@page { size: A4; margin :20mm 20mm 20mm 20mm;}</style>');

    function printDiv(divId) {
        window.frames["print_frame"].document.body.innerHTML = printDivCSS + document.getElementById(divId).innerHTML;
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();
        var today = new Date();
        var ss = String(today.getSeconds()).padStart(2, '0');
        var mm = String(today.getMinutes()).padStart(2, '0');
        var hh = String(today.getHours()).padStart(2, '0');
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy + '-' + mm + '-' + dd + ' '+hh+':'+mm+':'+ss;
        console.log(today);
        console.log(divId);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type : "POST",  //type of method
            url  : "/kontrak-print/{{ $kontrak->id }}",  //your page
            // data : { tgl_print : today,  },// passing the values
            success: function(res){
                //do what you want here...
                console.log('masukdong');
            }
        });

    }

</script>

<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    #div1{
        border:1px solid black;
        margin: 0cm 2cm 2cm 2cm;
    }
    #isikon{
        margin: 0cm 2cm 2cm 2cm;
    }
</style>
<style media="print">
    #isikon{
        table th, table td {
            text-align : center;
        }
        table {
            background-color : red;
        }
    </style>
    @endsection

    @section('content')

    @php
    $homelink = "/home";
    @endphp
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                PRINT PREVIEW
                <!-- <small>Form PBS</small> -->
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
                <li><a href="#">Kontrak</a></li>
                <li class="active"> Print Preview </li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- right column -->
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-ticket"></i> Print Preview</h3>
                            <button  onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>

                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <h1><b><center>Print Preview</center></b>
                                <hr color=#00cc00 width=95%>
                            </h1>
                            <form align="center" action="{{ url('kontrak-word',$kontrak->id) }}" method="post">
                                @csrf
                                <input type="submit"  value="Export to Word" class="btn btn-primary" />
                            </form>
                            <h3 align="center"><b><a href="javascript:printDiv('div1')" target="_blank">PRINT</a><br></b></h3>
                            <div id="div1" >

                                {{-- isi kontrak --}}
                                <div id="isikon" align="justify">
                                    @php
                                   $str = <<<'HEYWHATTHEHECK'

HEYWHATTHEHECK
;                                   echo $kontrak->isi;
                                    @endphp
                                </div>
                                {{-- // isi approver --}}
                                <hr>
                                <div style="page-break-before: always">
                                    <p align="left">
                                        PT. PINS INDONESIA<br>
                                        Plaza Kuningan, Annex Building, Lantai 7<br>
                                        Jl. H.R. Rasuna Said Kav . C11- C14 Jakarta 12940<br>
                                        Tel. 021 520 2560; Fax. 021 5292 0156<br>
                                    </p>
                                    <table class="display table">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;">No</th>
                                                <th style="text-align: center;">I/R/A</th>
                                                <th style="text-align: center;">NAMA & JABATAN</th>
                                                <th style="text-align: center;">TANDA TANGAN</th>
                                                <th style="text-align: center;">TANGGAL</th>
                                                <th style="text-align: center;">KET/SARAN/KOMENTAR</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $i = 1;
                                            @endphp
                                            @foreach($chat as $item)
                                            @php

                                            $isiawal = '';
                                            if($i ==1){
                                                $isiawal = 'INITIATOR/KONSEPTOR *)';
                                            }else{
                                                $isiawal = 'APPROVER *)';
                                            }
                                            @endphp
                                            <tr>
                                                <td style="text-align: center;">{{ $i++ }}</td>
                                                <td style="text-align: center;">{{ $isiawal }}</td>
                                                <td style="text-align: center;"><small>{{ strtoupper($item->name) }}</small><br>{{ strtoupper($item->jabatan) }}</td>
                                                <td style="text-align: center;">{{ $item->status }}</td>
                                                <td style="text-align: center;">{{ $item->created_at }}</td>
                                                <td style="text-align: center;"></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{-- // isi komen --}}
                                <hr>
                                <div style="page-break-before: always">
                                    <p align="left">
                                        PT. PINS INDONESIA<br>
                                        Plaza Kuningan, Annex Building, Lantai 7<br>
                                        Jl. H.R. Rasuna Said Kav . C11- C14 Jakarta 12940<br>
                                        Tel. 021 520 2560; Fax. 021 5292 0156<br>
                                    </p>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;">No</th>
                                                <th style="text-align: center;">Nama</th>
                                                <th style="text-align: center;">Keterangan</th>
                                                <th style="text-align: center;">Tanggal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $i = 1;
                                            @endphp
                                            @foreach($semua as $item)

                                            <tr>
                                                <td style="text-align: center;">{{ $i++ }}</td>
                                                <td style="text-align: center;">{{ strtoupper($item->name) }}</td>
                                                <td style="text-align: center;">{{ $item->chat }}</td>
                                                <td style="text-align: center;">{{ $item->created_at }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div hidden>
                                <br><br>
                                <b>Div 2:</b> <a href="javascript:printDiv('div2')">Print</a><br>
                                <div id="div2" hidden>

                                </div>
                                <br><br>
                                <div hidden>
                                    <b>Div 3:</b> <a href="javascript:printDiv('div3')">Print</a><br>
                                    <div id="div3" hidden>

                                    </div>
                                </div>

                            </div>
                            <iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank"></iframe>
                        </div>


                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @endsection

    @section('scripts')

    @endsection
