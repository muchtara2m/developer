@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Approve Kontrak";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{-- smartwizard --}}
{{-- <link href="{{ asset('smartwizard/dist/css/smart_wizard.css') }}" rel="stylesheet" type="text/css" /> --}}
{{-- <link href="{{ asset('smartwizard/dist/css/smart_wizard_theme_circles.css') }}" rel="stylesheet" type="text/css" /> --}}
<link href="{{ asset('smartwizard/dist/css/smart_wizard_theme_circles.min.css') }}" rel="stylesheet" type="text/css" />

{{-- end smartwizard --}}
@endsection
@section('customstyle')

<style type="text/css">
    table tr:not(:first-child){
        cursor: pointer;transition: all .25s ease-in-out;
    }
    table tr:not(:first-child):hover{background-color: #ddd;}
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
    .pulse {
        width: 20%;
        --color: #ef6eae;
        --hover: #ef8f6e;
    }
    .pulse:hover,
    .pulse:focus {
        -webkit-animation: pulse 1s;
        animation: pulse 1s;
        box-shadow: 0 0 0 2em rgba(255, 255, 255, 0);
    }

    @-webkit-keyframes pulse {
        0% {
            box-shadow: 0 0 0 0 var(--hover);
        }
    }

    @keyframes pulse {
        0% {
            box-shadow: 0 0 0 0 var(--hover);
        }
    }
    .close:hover,
    .close:focus {
        box-shadow: inset -3.5em 0 0 0 var(--hover), inset 3.5em 0 0 0 var(--hover);
    }
    .fr-wrapper {
        margin-bottom: 1em;
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Approve Kontrak
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Approve Kontrak</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>

                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {{-- <form method="post" action="{{ action('KontrakController@store', $kontrak->id) }}" enctype="multipart/form-data"> --}}
                        {{-- @csrf --}}
                        <div class="box-body">
                            <div class="box-body">
                                <div class="form-group" hidden>

                                </div>
                                <div class="form-groupp">
                                    <div id="smartwizard">
                                        <ul>
                                            <li><a href="#step-1">Step 1<br /><small>Draft Kontrak</small></a></li>
                                            <li><a href="#step-3">Step 2<br /><small>Approval</small></a></li>
                                        </ul>
                                        <div>
                                            <div id="step-1" class="">
                                                <textarea name="isi"  cols="180" rows="10">
                                                    {{ $kontrak->isi }}
                                                </textarea>
                                            </div>
                                            <div id="step-3" class="">
                                                <h4>Coment and Approve</h4>
                                                <ul class="timeline">
                                                    @foreach ($chats as $isi)
                                                    <!-- timeline time label -->
                                                    <li class="time-label">
                                                        <span class="bg-green">
                                                            {{ date('d M.Y', strtotime($isi->created_at)) }}
                                                        </span>
                                                    </li>
                                                    <!-- /.timeline-label -->
                                                    <!-- timeline item -->
                                                    <li>
                                                        <!-- timeline icon -->
                                                        <i class="fa fa-user bg-aqua"></i>
                                                        <div class="timeline-item">
                                                            <span class="time">
                                                                <i class="fa fa-clock-o"></i>
                                                                {{ date('H:i:s', strtotime($isi->created_at)) }}
                                                            </span>

                                                            <h3 class="timeline-header"><a href="#">{{ $isi->jabatan.' - '.$isi->name }}</a></h3>

                                                            <div class="timeline-body">
                                                                {{ $isi->chat }}
                                                            </div>

                                                            <div class="timeline-footer">
                                                                {{-- <a class="btn btn-primary btn-xs">...</a> --}}
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <!-- END timeline item -->
                                                    @endforeach
                                                    <li>
                                                        <i class="fa fa-clock-o bg-gray"></i>
                                                    </li>
                                                </ul>
                                                <br>
                                                    <form action="{{ action('KontrakController@chat') }}" class="form-group" method="post">
                                                        @csrf
                                                        <input type="hidden" value={{ $kontrak->id }} name="idKontrak">
                                                        <div class="col-md-12 pad-0">
                                                            <div class="col-md-6 pad-0">
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Comment</label>
                                                                    <input type="text" class="form-control" name="chat"  id="">
                                                                    {{-- <input type="text" class="form-control" name="chat"  id="" value="{{ $flow->queue }}"> --}}
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" hidden>
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">ID Transaksi</label>
                                                                    <input type="text" class="form-control" name="idTransaksi" value="{{ $kontrak->id }}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 mar-paginate pad-0">
                                                                <button type="submit" class="btn btn-danger" name="status" value="Return">Return</button>
                                                                <button type="submit" class="btn btn-primary" name="status" id="submit" value="Approve">Approve</button>
                                                            </div>
                                                        </div>
                                                    </form>

                                                    <br>
                                            </div>
                                            {{-- <div id="step-4" class="">
                                                Testing Approval 3
                                            </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    {{-- </form> --}}
                </div>
                <!-- /.box -->

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @endsection

    @section('scripts')
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
    <!-- Initialize the editor. -->
    <script src="{{ asset('froala/js/languages/id.js') }}"></script>

    <script>
        $('textarea').froalaEditor({
            // toolbarButtons: ['container'],
            // toolbarButtons: ['getPDF', 'print'],
            toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'inlineClass', 'clearFormatting', '|', 'emoticons', 'fontAwesome', 'specialCharacters', 'paragraphFormat', 'lineHeight', 'paragraphStyle', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', 'insertHR', 'selectAll', 'help', 'html', 'fullscreen', '|', 'undo', 'redo', 'getPDF', 'print'],
            placeholderText: '',
            documentReady: true,
            language: 'id',
            key: '{{ env("KEY_FROALA") }}',
        })</script>
        {{-- smart wizard --}}

        <script type="text/javascript" src="{{ asset('smartwizard/dist/js/jquery.smartWizard.min.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('div#smartwizard').smartWizard();
                $('div#smartwizardcircle').smartWizard();
            });</script>
            <!-- Data Table -->
            <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            <!-- date-range-picker -->
            <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
            <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
            <!-- bootstrap datepicker -->
            <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
            <!-- iCheck 1.0.1 -->
            <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
            <!-- clockpicker -->
            <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
            <!-- Select2 -->
            <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
            <!-- CK Editor -->
            {{-- <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script> --}}

            @endsection
