@extends('layouts.master')

@section('title')
Kontrak | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    /* tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    } */
    tfoot {
        display: table-header-group;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            KONTRAK
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active"> Kontrak </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Kontrak</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- /.box-header -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_roles" data-toggle="tab">SP3</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_roles">
                                <div class="box-body table-responsive">
                                    <table name="item" id="item" class="display" width="100%" cellspacing="0">
                                        <thead>
                                            <tr id="kepala">
                                                <th rowspan="2" style="text-align:center">No</th>
                                                <th rowspan="2" style="text-align:center">No. IO</th>
                                                <th rowspan="2" style="text-align:center">No. SPH</th>
                                                <th rowspan="2" style="text-align:center">No. SPPH</th>
                                                <th rowspan="2" style="text-align:center">No. Kontrak</th>
                                                <th rowspan="2" style="text-align:center">Judul</th>
                                                <th rowspan="2" style="text-align:center">Mitra</th>
                                                <th rowspan="2" style="text-align:center">Tanggal BAKN</th>
                                                <th rowspan="2" style="text-align:center">Tanggal Unggah BAKN</th>
                                                <th rowspan="2" style="text-align:center">Tanggal SP3</th>
                                                <th rowspan="2" style="text-align:center">Pembuat</th>
                                                <th rowspan="2" style="text-align:center">Tanggal Buat</th>
                                                <th rowspan="2" style="text-align:center">Harga</th>
                                                <th colspan="5" style="text-align:center">Approval</th>
                                                <th rowspan="2" style="text-align:center">Posisi</th>
                                                <th rowspan="2" style="text-align:center">Lampiran</th>
                                                <th rowspan="2" style="text-align:center">Tanggal Upload File</th>
                                                <th rowspan="2" style="text-align:center">Preview</th>
                                            </tr>
                                            <tr style=" white-space: nowrap">
                                                <th style="text-align:center">Admin Kontrak LKPP</th>
                                                <th style="text-align:center">Manager Catalogue & Partnership Management</th>
                                                <th style="text-align:center">GM E-Commerce</th>
                                                <th style="text-align:center">DIR Operation</th>
                                                <th style="text-align:center">DIR Utama</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th ></th>
                                                <th class="testing" >No. IO</th>
                                                <th class="testing" >No. SPH</th>
                                                <th class="testing" >No. SPPH</th>
                                                <th class="testing" >No. Kontrak</th>
                                                <th class="testing" >Judul</th>
                                                <th class="testing" >Mitra</th>
                                                <th class="testing" >Tanggal BAKN</th>
                                                <th></th>
                                                <th class="testing" >Tanggal SP3</th>
                                                <th class="testing" >Pembuat</th>
                                                <th></th>
                                                <th ></th>
                                                <th ></th>
                                                <th ></th>
                                                <th ></th>
                                                <th ></th>
                                                <th ></th>
                                                <th ></th>
                                                <th ></th>
                                                <th ></th>
                                                <th ></th>
                                            </tr>
                                        </tfoot>
                                        <tbody style="text-align:center">
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach ($sp3 as $item => $value)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $value->sp3s->bakns->io['no_io'] }}</td>
                                                    <td>{{ $value->sp3s->bakns->spph['nomorsph'] }}</td>
                                                    <td>{{ $value->sp3s->bakns->spph['nomorspph'] }}</td>
                                                    <td>{{ $value->nokontrak }}</td>
                                                    <td>{{ $value->sp3s->bakns->spph['judul'] }}</td>
                                                    <td>{{ $value->sp3s->bakns->spph->mitras['perusahaan'] }}</td>
                                                    <td>{{ $value->sp3s->bakns['created_at'] }}</td>
                                                    <td>{{ $value->sp3s->bakns['upload'] }}</td>
                                                    <td>{{ $value->sp3s['tglsp3'] }}</td>
                                                    <td>{{ $value->sp3s->users['name'] }}</td>
                                                    <td>{{ $value->sp3s['created_at'] }}</td>
                                                    <td>{{ number_format($value->sp3s->bakns['harga']) }}</td>
                                                    <td style="white-space:nowrap">
                                                            @php
                                                            $data[$value->idnya]['id']= $value->id; //idkontrak
                                                            for ($i=0; $i < count($value->chatnya); $i++) {
                                                                if ($value->chatnya[$i]['queue']== 0) {
                                                                    echo '<br>('.date('d.F.Y H:i', strtotime($value->chatnya[$i]['created_at'])).' - '.$value->chatnya[$i]['status'].')<br>'.$value->chatnya[$i]['name'];
                                                                }
                                                            }
                                                            @endphp
                                                    </td>
                                                    <td style="white-space:nowrap">
                                                            @php
                                                            $data[$value->idnya]['id']= $value->id; //idkontrak
                                                            for ($i=0; $i < count($value->chatnya); $i++) {
                                                                if ($value->chatnya[$i]['queue']== 1) {
                                                                    echo '<br>('.date('d.F.Y H:i', strtotime($value->chatnya[$i]['created_at'])).' - '.$value->chatnya[$i]['status'].')<br>'.$value->chatnya[$i]['name'];
                                                                }
                                                            }
                                                            @endphp
                                                    </td>
                                                    <td style="white-space:nowrap">
                                                            @php
                                                            $data[$value->idnya]['id']= $value->id; //idkontrak
                                                            for ($i=0; $i < count($value->chatnya); $i++) {
                                                                if ($value->chatnya[$i]['queue']== 2) {
                                                                    echo '<br>('.date('d.F.Y H:i', strtotime($value->chatnya[$i]['created_at'])).' - '.$value->chatnya[$i]['status'].')<br>'.$value->chatnya[$i]['name'];
                                                                }
                                                            }
                                                            @endphp
                                                    </td>
                                                    <td style="white-space:nowrap">
                                                            @php
                                                            $data[$value->idnya]['id']= $value->id; //idkontrak
                                                            for ($i=0; $i < count($value->chatnya); $i++) {
                                                                if ($value->chatnya[$i]['queue']== 3) {
                                                                    echo '<br>('.date('d.F.Y H:i', strtotime($value->chatnya[$i]['created_at'])).' - '.$value->chatnya[$i]['status'].')<br>'.$value->chatnya[$i]['name'];
                                                                }
                                                            }
                                                            @endphp
                                                    </td>
                                                    <td style="white-space:nowrap">
                                                            @php
                                                            $data[$value->idnya]['id']= $value->id; //idkontrak
                                                            for ($i=0; $i < count($value->chatnya); $i++) {
                                                                if ($value->chatnya[$i]['queue']== 4) {
                                                                    echo '<br>('.date('d.F.Y H:i', strtotime($value->chatnya[$i]['created_at'])).' - '.$value->chatnya[$i]['status'].')<br>'.$value->chatnya[$i]['name'];
                                                                }
                                                            }
                                                            @endphp
                                                    </td>
                                                    <td>{{ $value->approval }}</td>
                                                    @php
                                                        $flampiran = json_decode($value->lampiran, TRUE);
                                                        $tlampiran = json_decode($value->title_lampiran, TRUE);
                                                    @endphp
                                                    <td style="white-space:nowrap">
                                                            @php
                                                            $x=1;
                                                            @endphp
                                                            @if($flampiran != NULL)
                                                            @foreach ($tlampiran as $keylampiran =>$vallampiran)
                                                            {{ $i++.'. ' }}<a href="{{ Storage::url($flampiran[$keylampiran]) }}">{{ $tlampiran[$keylampiran] }}</a><br>
                                                            @endforeach
                                                            @endif
                                                            
                                                        </td>
                                                    <td>{{ $value->upload }}</td>
                                                    <td style="white-space:nowrap">
                                                            <a href="{{ url('spph-preview/'.$value->sp3s->bakns->spph['id']) }}">Preview SPPH</a><br>
                                                            <a href="{{ url('bakn-preview/'.$value->sp3s->bakns['id']) }}">Preview BAKN</a><br>
                                                            <a href="{{ url('sp3-preview/'.$value->sp3s['id']) }}">Preview SP3</a><br>
                                                            <a href="{{ url('kontrak-preview/'.$value->id) }}">Preview Kontrak</a><br>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                </div>
                <!-- /.box -->
                <div class="box-body table-responsive">
                    
                </div>
                
                
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    // Setup - add a text input to each footer cell
    $('#item tfoot .testing').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
    
    // DataTable
    var otable = $('.display').DataTable();
    
    // Apply the search
    otable.columns().every( function () {
        
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                .search( this.value )
                .draw();
            }
        } );
    } );
    
    $(document).on("click", ".upload", function () {
        var idbro = $(this).data('id');
        // $(".modal-body #idnya").val( idbro );
        $('form').attr('action',"{{ url('kontrak-non-upload')}}/"+idbro);
    }); </script>
    @endsection
    