@extends('layouts.master')

@section('title')
Kontrak Done| Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            KONTRAK DONE
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active"> Kontrak Done </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Kontrak</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_roles" data-toggle="tab">SP3</a></li>
                            <li><a href="#tab_permissions" data-toggle="tab">BAKN</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_roles">
                                <div class="box-body table-responsive">
                                    <table id="rolesTable" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>IO</th>
                                                <th>Deskripsi IO</th>
                                                <th>Nomor SPPH</th>
                                                <th>Nomor Kontrak</th>
                                                <th>Judul SPPH</th>
                                                <th>Tangal BAKN</th>
                                                <th>Nomor SP3</th>
                                                <th>Harga</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no=1;
                                            @endphp
                                            @foreach ($sp3 as $list)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $list->sp3s->bakns->io['no_io'] }}</td>
                                                <td>{{ $list->sp3s->bakns->io['deskripsi'] }}</td>
                                                <td>{{ $list->sp3s->bakns->spph['nomorspph'] }}</td>
                                                <td>{{ $list->nokontrak }}</td>
                                                <td>{{ $list->sp3s->bakns->spph['judul'] }}</td>
                                                <td>{{ $list->sp3s->bakns['created_at'] }}</td>
                                                <td>{{ $list->sp3s->nosp3 }}</td>
                                                <td>{{ number_format($list->sp3s->bakns['harga'],0,'.','.') }}</td>
                                                <td>
                                                        <a href="{{ url('kontrak-print_preview', $list->id)}}" class="fa fa-fw fa-edit"></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_permissions">
                                <div class="box-body table-responsive">
                                    <table id="permissionsTable" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>IO</th>
                                                <th>Deskripsi IO</th>
                                                <th>Nomor SPPH</th>
                                                <th>Nomor Kontrak</th>
                                                <th>Judul SPPH</th>
                                                <th>Tangal BAKN</th>
                                                <th>Harga</th>
                                                <th>Lampiran Kontrak</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no=1;
                                            @endphp
                                            @foreach ($bakn as $list)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $list->bakns->io['no_io'] }}</td>
                                                <td>{{ $list->bakns->io['deskripsi'] }}</td>
                                                <td>{{ $list->bakns->spph['nomorspph'] }}</td>
                                                <td>{{ $list->nokontrak }}</td>
                                                <td>{{ $list->bakns->spph['judul'] }}</td>
                                                <td>{{ $list->bakns->created_at }}</td>
                                                <td>{{ number_format($list->bakns['harga'],0,'.','.') }}</td>
                                                <td>
                                                    @php
                                                    if($list->file == NULL){
                                                    }else{
                                                        $title = json_decode($list->title, TRUE);
                                                        $file = json_decode($list->file, TRUE);
                                                        $i=1;
                                                        foreach ($title as $key => $value) {
                                                            echo $i++.'. <a href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';
                                                        }
                                                    }
                                                    @endphp
                                                </td>
                                                <td>
                                                        <a href="{{ url('kontrak-print_preview', $list->id)}}" class="fa fa-fw fa-edit"></a>

                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                </div>
                <!-- /.box -->
                <div class="box-body table-responsive">

                </div>


            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
    $(document).ready( function () {
        $('#rolesTable,#permissionsTable').DataTable({
        });
    } );</script>
    @endsection
