@extends('layouts.master')

@section('title')
Inprogress Kontrak | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
{{-- Icon --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- DataTables -->
<link href="{{ asset('smartwizard/dist/css/smart_wizard.css') }}" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
{{-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css"> --}}
<style type="text/css">
    td.details-control {
        background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('https://cdn.rawgit.com/DataTables/DataTables/6c7ada53ebc228ea9bc28b1b216e793b1825d188/examples/resources/details_close.png') no-repeat center center;
    }
    button:disabled,
    button[disabled]{
        border: 1px solid #999999;
        background-color: #cccccc;
        color: #666666;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Iprogress Kontrak
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active"> Inprogress Kontrak </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Inprogress Transaksi</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <!-- Table starts here -->
                        <table  id="status" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="text-align:center">No</th>
                                    <th style="text-align:center">Nomor SPPH</th>
                                    <th style="text-align:center">Judul</th>
                                    <th style="text-align:center">Tanggal BAKN</th>
                                    <th style="text-align:center">Tanggal Unggah</th>
                                    <th style="text-align:center">Tanggal Disposisi</th>
                                    <th style="text-align:center">Pembuat</th>
                                    <th style="text-align:center">Tanggal Bikin</th>
                                    <th style="text-align:center">ADMIN</th>
                                    <th style="text-align:center">HAFIZ</th>
                                    <th style="text-align:center">BUNYAMIN</th>
                                    <th style="text-align:center">REVI GUSPA</th>
                                    <th style="text-align:center">NOTJE ROSANTI</th>
                                    <th style="text-align:center">FIRDAUS</th>
                                    <th style="text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tbody style="text-align:center">
                                @php
                                $no = 1;
                                @endphp
                                @foreach ($testsirkulir as $item)
                                @php
                                $checkId = '';
                                if($checkId == $checkId) continue;
                                @endphp
                                <dd>{{ $checkId }}</dd>

                                <tr>
                                  <td>{{ $no++ }}</td>
                                  <td>{{ $item->nomorBakn}}</td>
                                  <td>{{ $item->judul }}</td>
                                  <td>{{ $item->tglbakn }}</td>
                                  <td>{{ $item->tgl_upload }}</td>
                                  <td>{{ $item->tgldis }}</td>
                                  <td>{{ $item->pembuat }}</td>
                                  <td>{{ $item->created_at }}</td>
                                  <td>{{ $item->ADMIN }}</td>
                                  <td>{{ $item->HAFIZ }}</td>
                                  <td>{{ $item->BUNYAMIN }}</td>
                                  <td>{{ $item->REVI }}</td>
                                  <td>{{ $item->NOTJE }}</td>
                                  <td>{{ $item->FIRDAUS }}</td>
                                    <td>
                                        <a href="{{ url('kontrak-preview-status', $item->id)}}" class="fa fa-fw fa-sticky-note-o" title="Preview Kontrak"></a>
                                    </td>
                                </tr>
                                @php
                                $checkId = $item->id;
                                @endphp
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ asset('smartwizard/dist/js/jquery.smartWizard.min.js') }}"></script>
<script>
    $(document).ready( function () {
        $('#status').DataTable({

        });
    } );
</script>
@endsection
