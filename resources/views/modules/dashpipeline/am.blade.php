@extends('layouts.master')

@section('title')
Pencapaian AM | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
    /* .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    } */
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pencapaian AM
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            {{-- <li><a href="#">SPPH</a></li> --}}
            <li class="active"> Pencapaian AM </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Pencapaian AM</h3>
                        <div class="row" style="text-align: center;padding-left: 41%;">
                            <div class="col-md-2">
                                <label>Bulan</label>
                                <select name="handler" id="dari" class="form-control">
                                    <option value="">Pilih Bulan</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Tahun</label>
                                <select name="handler" id="dari" class="form-control">
                                    <option value="">Pilih Tahun</option>
                                </select>
                            </div>
                        </div>
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="display">
                            <thead>
                                <tr style="white-space:nowrap">
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">Nama</th>
                                    <th rowspan="2">Ubis</th>
                                    <th colspan="3" class="text-center">SALES</th>
                                    <th colspan="3" class="text-center">REVENUE</th>
                                    <th colspan="3" class="text-center">GROSS PROFIT</th>
                                    <th colspan="3" class="text-center">COLLECTION</th>
                                    <th rowspan="2">KINERJA %</th>
                                </tr>
                                <tr style="white-space:nowrap">
                                    <th>TARGET</th>
                                    <th>REAL</th>
                                    <th>ACH %</th>
                                    <th>TARGET</th>
                                    <th>REAL</th>
                                    <th>ACH %</th>
                                    <th>TARGET</th>
                                    <th>REAL</th>
                                    <th>ACH %</th>
                                    <th>TARGET</th>
                                    <th>REAL</th>
                                    <th>ACH %</th>
                                </tr>
                            </thead>
                            <tbody id="datanya">
                                @php
                                $i=1;
                                @endphp
                                @foreach ($data as $item)
                                <tr style="white-space:nowrap">
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $item->nama_am }}</td>
                                    <td>{{ $item->ubis }}</td>
                                    <td>{{ number_format($item->nilai) }}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table> 
                      
                        <div class="col-md-6" hidden>
                                <textarea name="" id="ubis" cols="30" rows="10"></textarea>
                                <textarea name="" id="am" cols="30" rows="10"></textarea>
                            <h4>Testing Upload File</h4>
                            <form id="upload_csv" method="post" enctype="multipart/form-data">  
                                @csrf
                                
                                <input type="file" name="filenya" class="form-control" id="">
                                <input type="submit" id="upload" value="Upload" class="btn btn-primary">
                            </form>
                            
                        </div>
                        
                        <div id="employee_table" hidden>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    getData();
    TableData();
    getTable();
    async function getTable(){
        $.ajax({
            url:"{{ asset('files/amtest.csv') }}",
            dataType:"text",
            success:function(data)
            {
                var employee_data = data.split(/\r?\n|\r/);
                var table_data = '<table class="table table-bordered table-striped">';
                    for(var count = 0; count<employee_data.length; count++)
                    {
                        var cell_data = employee_data[count].split(",");
                        table_data += '<tr>';
                            for(var cell_count=0; cell_count<cell_data.length; cell_count++)
                            {
                                if(count === 0)
                                {
                                    table_data += '<th>'+cell_data[cell_count]+'</th>';
                                }
                                else
                                {
                                    table_data += '<td>'+cell_data[cell_count]+'</td>';
                                }
                            }
                            table_data += '</tr>';
                        }
                        table_data += '</table>';
                        $('#employee_table').html(table_data);
                    }
                });
            }
            async function TableData(){
                $('#pbsTable').DataTable({
                    "aLengthMenu": [[15,30, 50, 75, -1], [15,30, 50, 75, "All"]],
                    "iDisplayLength": 15,
                    
                });
            }
            async function getData(){
                const ubis = [];
                const am = [];
                
                const response = await fetch("{{ asset('files/am.csv') }}");
                const data = await response.text();
                const table = data.split('\n').slice(1);
                
                table.forEach(elt => {
                    const columns = elt.split(';');
                    const test = columns[0];
                    const test1 = columns[1];
                    const test2 = columns[2];
                    const test3 = columns[3];
                    const test4 = columns[4];
                    // console.log();
                    ubis.push(test);
                    am.push(test1);
                    // $('#am').val(am);
                    // $('#ubis').val(ubis);
                    
                })
                return {ubis, am};
            }
        </script>
        <script>
            $(document).ready(function(){  
                $('#upload_csv').on("submit", function(e){  
                    e.preventDefault(); //form will not submitted  
                    $.ajax({  
                        url:"{{ url('upload-csv') }}",  
                        method:"post",  
                        data:new FormData(this),  
                        contentType:false,          // The content type used when sending data to the server.  
                        cache:false,                // To unable request pages to be cached  
                        processData:false,          // To send DOMDocument or non processed data file it is set to false  
                        success: function(data){  
                            console.log(data.DT_RowIndex);
                            
                        }  
                    })  
                });  
            });  </script>
            <script>
                $(document).ready(function(){
                    //  $('#load_data').click(function(){
                        
                        //  });
                        
                    });
                </script>
                
                @endsection
                