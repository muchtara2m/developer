@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Create SP3 - SPK";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{--  Tagify  --}}
<link rel="stylesheet" href="{{ asset('tagify/dist/tagify.css') }}">
@endsection

@section('customstyle')

<style type="text/css">
    table tr:not(:first-child){
        cursor: pointer;transition: all .25s ease-in-out;
    }
    table tr:not(:first-child):hover{background-color: #ddd;}
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SP3 - SPK
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">SP3/SPK</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Create SP3/SPK</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="container">

                        <div class="col-md">
                            <label for="nomorspph">Select SPPH</label><a target="_blank" href="#"><i class="fa fa-fw fa-file-code-o"></i></a>
                            <input type="text" id="nmr" class="form-control" title="SPPH" data-toggle="modal" data-target="#modal-unit" readonly placeholder="Pilih SPPH">
                        </div>
                    </div>

                    {{-- start form  --}}
                    <form method="post" action="store" enctype="multipart/form-data" id="form">
                        @csrf
                        <input type="hidden" id="spph" name="idbakn" value="{{ old('idbakn') }}">                        
                        <div class="box-body">
                            <div class="form-group col-md-6 igroup">
                                <label for="jenis_kontrak">No SP3/SPK</label>
                                <div class="input-group col-md-12">
                                    <input type="text" name="nospk" class="form-control" id="nospk" value="{{ old('nospk') }}">
                                </div>
                            </div>
                            <div class="form-group col-md-6 igroup">
                                <label for="tglspk">Tanggal SP3/SPK</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos" name="tglspk" id="tglspk" value="{{ old('tglspk') }}">
                                </div>
                            </div>
                            {{-- hasil pembahasan --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4 id="jenis" class="divider-title" align="center">JUDUL</h4>
                                    <hr>
                                </div>
                            </div>
                            {{--  isinya  --}}
                            <div class="form-group">
                                <label for="ruanglingkip">Ruang Lingkup</label>
                                <textarea id="ruanglingkup" rows="10" cols="80" name="ruang_lingkup" placeholder="Type Something">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="lokasipekerjaan">Lokasi Pekerjaan/Pengiriman Barang/Jasa :</label>
                                <textarea id="lokasipekerjaan" rows="10" cols="80" name="lokasi_pekerjaan" placeholder="Type Something">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="jangkawaktu">Jangka Waktu Pengiriman Barang : </label>
                                <textarea id="jangkawaktu" rows="10" cols="80" name="jangka_waktu" placeholder="Type Something">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="harga">Harga Pekerjaan :</label>
                                <textarea id="hargaterbilang" name="harga_terbilang" >

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="tatacara">Tata Cara Pembayaran :</label>
                                <textarea  rows="10" cols="80" name="cara_bayar"  id="carabayar">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Lain - lain : </label>
                                <textarea rows="10" cols="80" name="lain_lain" id="lainlain">
                                </textarea>
                            </div>
                            {{--  end isinya  --}}
                            {{-- end hasil pembahasan --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Comment</label>
                                    <input type="text" name="chat" class="form-control">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="status" id="draft" class="btn btn-primary" style="width: 7em;"><i class="fa fa-check"></i> Save</button>
                            <button type="submit" name="status" id="save" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    {{-- end form --}}
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
        <div class="modal fade" id="modal-unit">
            <div class="modal-dialog " style="width:auto">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Data Unit</h4>
                        </div>
                        <div class="modal-body table-responsive">
                            <table id="io" class="display table-responsive">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nomor SPPH</th>
                                        <th>Judul</th>
                                        <th>Harga</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($bakn as $ios)
                                    <tr>
                                        <td>{{ $ios['id'] }}</td>
                                        <td style="white-space:nowrap">{{$ios->spph['nomorspph']}}</td>
                                        <td style="white-space:nowrap">{{$ios->spph['judul']}}</td>
                                        <td>{{ number_format($ios->harga) }}</td>
                                        <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
{{-- bahasa indonesia froala --}}
<script src="{{ asset('froala/js/languages/id.js') }}"></script>

<!-- Data Table -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Tagify -->
<script src="{{ asset('tagify/dist/jQuery.tagify.min.js') }}"></script>
<script src="{{ asset('tagify/dist/tagify.min.js') }}"></script>
<script>
$(document).ready( function () {
        $('#io').DataTable();
    } )
    var table = document.getElementById('io');

    for(var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function()
        {
            //  rIndex = this.rowIndex;
            document.getElementById("spph").value = this.cells[0].innerHTML;
            document.getElementById("nmr").value = this.cells[1].innerHTML;
        };
    };
    $('select').select2();
    function getRoles(val) {
        $('#role-cm_role_text').val('');
        var data = $('#role-cm_role').select2('data').map(function(elem){ return elem.text} );
        console.log(data);
        $('#role-cm_role_text').val(data);
        $('#role-cm_role').on('select2:unselecting', function (e) {
            $('#role-cm_role_text').val('');
        });
    }</script>
    <script>
        // $(document).ready(function(){
            //     $("textarea").froalaEditor("edit.off");
            // });
            // aktif froala
            $('textarea').froalaEditor({
                placeholderText: '',
                language: 'id',
                charCounterCount: false,
                key: '{{ env("KEY_FROALA") }}',
            });

            $(document).ready(function(){
                $(document).change(function(){
                    var id=$('#spph').val();
                    var token = $("input[name='_token']").val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type:'post',
                        url:"{{ route('data-bakn') }}",
                        data: {id:id, _token:token},
                        dataType:'json',//return data will be json
                        async: "false",
                        success:function(data){
                            console.log(data);
                            var nmr = data.options['spph']['nomorspph'];
                            var tglbakn = new Date(data.options['tglbakn']);
                            var thn = tglbakn.getFullYear();
                            $('[name=baknid]').val(data.options['id']);
                            $('#tglspk').attr('value',data.options['tglbakn']);
                            $('#ruanglingkup').froalaEditor('html.set', data.options['ruang_lingkup']);
                            $('#lokasipekerjaan').froalaEditor('html.set', data.options['lokasi_pekerjaan']);
                            $('#jangkawaktu').froalaEditor('html.set', data.options['jangka_waktu']);
                            $('#hargaterbilang').froalaEditor('html.set', data.options['harga_terbilang']);
                            $('#carabayar').froalaEditor('html.set', data.options['cara_bayar']);
                            $('#lainlain').froalaEditor('html.set', data.options['lain_lain']);

                                if(data.options['harga'] >= 50000000){
                                    $('form').attr('action',"{{ url('store-sp3-non')}}");
                                    $('#jenis').html('SURAT PENETAPAN PELAKSANAAN PEKERJAAN (SPPP)');
                                    $('#nosp3k').attr('value','/LG.220/PIN.00.00/'+thn);
                                    $('#save').val('save_sp3');
                                    $('#draft').val('draft_sp3');
                                }else{
                                    $('form').attr('action',"{{ url('store-spk-non')}}");
                                    $('#jenis').html('SURAT PERINTAH KERJA (SPK)');
                                    $('#nospk').attr('value','/HK.810/PIN.00.00/'+thn);
                                    $('#save').val('save_spk');
                                    $('#draft').val('draft_spk');
                                }
                        },
                        error:function(){

                        }
                    });


                });
            });
        </script>
        {{-- input format tanggal --}}
        <script>
            $(function(){
                $(".datejos").on("change", function() {
                    this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                    .format( this.getAttribute("data-date-format") )
                    )
                }).trigger("change")
                $('.datejos').datepicker({
                    autoclose: true,
                    orientation: "bottom"
                })
            })</script>
            @endsection
