
<table id="pbsTable" class="table display table-bordered table-hover">
    @csrf
    <thead>
        <tr style="white-space: nowrap">
            <th>No</th>
            <th>IO</th>
            <th>Deskripsi IO</th>
            <th>Nilai Project</th>
            <th>Action</th>
        </tr>
    </thead>
</table>

@push('scripts')
<script>
    $(function() {
        $('#pbsTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'nilai-json',
            columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
            { data: 'dataio.no_io','defaultContent': ""},
            { data: 'dataio.deskripsi','defaultContent': ""},
            { data: 'nilai_project', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},
            { data: 'action'},
            ]
        });
    });
</script>
@endpush
