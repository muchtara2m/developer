
<table id="pbsTable" class="table display table-bordered table-hover">
        @csrf
        <thead>
                <tr style="white-space: nowrap">
                    <th rowspan="2">No</th>
                    <th rowspan="2">IO</th>
                    <th rowspan="2">Deskripsi IO</th>
                    <th rowspan="2">Customer</th>
                    <th rowspan="2">Uraian</th>
                    <th rowspan="2">Nilai Project</th>
                    <th rowspan="2">Nilai Invoice</th>
                    <th colspan="15" style="text-align: -webkit-center">Status</th>
                    <th rowspan="2">Status Dokumen</th>
                    <th rowspan="2">Pembuat</th>
                    <th rowspan="2">Ubis</th>
                    <th rowspan="2">Tanggal Input</th>
                    <th rowspan="2">Action</th>
                </tr>
                <tr style="white-space: nowrap">
                    <th>SPK</th>
                    <th>KL</th>
                    <th>BAUT</th>
                    <th>BAST</th>
                    <th>BASO</th>
                    <th>BAPP</th>
                    <th>LPP</th>
                    <th>BA Perub JK WKT</th>
                    <th>Surat GM Segmen</th>
                    <th>BA Rekon</th>
                    <th>Performansi</th>
                    <th>LKPP</th>
                    <th>EP</th>
                    <th>NPK</th>
                    <th>PPN</th>
                </tr>
            </thead>
    </table>
 
@push('scripts')
<script>
$(function() {
    $('#pbsTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'unbill-json',
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
            { data: 'dataio.no_io','defaultContent': ""},
            { data: 'dataio.deskripsi','defaultContent': ""},
            { data: 'customer.nama_customer','defaultContent': ""},
            { data: 'uraian','defaultContent': ""},
            { data: 'nilai_project', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},
            { data: 'nilai_invoice', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},
            { 
              data: 'spk', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                }
                else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'kl', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                }else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'baut', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                }else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'bast', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                }else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'baso', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                }else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'bapp', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                }else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'lpp', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                } else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'baperub', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                } else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'suratgm', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                } else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'barekon', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                } else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'performansi', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                } else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'lkpp', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                } else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'ep', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                } else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'npk', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                } else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'top', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                } else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },




            { data: 'status'},
            { data: 'pembuat.name','defaultContent': ""},
            { data: 'pembuat.unitnya.nama','defaultContent': ""},
            { data: 'created_at'},
            { data: 'action'},
        ],
        
    });
});
</script>
@endpush
