
<table id="pbsTable" class="table display table-bordered table-hover">
    @csrf
    <thead>
        <tr style="white-space: nowrap">
            <th>No</th>
            <th>IO</th>
            <th>Deskripsi IO</th>
            <th>Customer</th>
            <th>Deskripsi Project</th>
            <th>Nilai Project</th>
            <th>Nilai Invoice</th>
            <th>Tanggal Dibuat</th>
            <th>Tanggal Invoice</th>
            <th>Tanggal Bukti Bayar</th>
            <th>PIC</th>
            <th>Ubis</th>
            <th>Action</th>
        </tr>
    </thead>
    
</table>

@push('scripts')
<script>
    $(function() {
        $('#pbsTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'paid100-json',
            columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
            { data: 'dataio.no_io','defaultContent': ""},
            { data: 'dataio.deskripsi','defaultContent': ""},
            { data: 'customer.nama_customer','defaultContent': ""},
            { data: 'uraian','defaultContent': ""},
            { data: 'nilai_project', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},
            { data: 'nilai_invoice', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},
            { data: 'created_at'},
            { data: 'tgl_invoice'},
            { data: 'upload'},
            { data: 'pembuat.name','defaultContent': ""},
            { data: 'pembuat.unitnya.nama','defaultContent': ""},
            { data: 'action'},
            ]
        });
    });
</script>
@endpush
