
<table id="pbsTable" class="table display table-bordered table-hover">
        @csrf
        <thead>
                <tr style="white-space: nowrap">
                    <th rowspan="2">No</th>
                    <th rowspan="2">IO</th>
                    <th rowspan="2">Deskripsi IO</th>
                    <th rowspan="2">Customer</th>
                    <th rowspan="2">Uraian</th>
                    <th rowspan="2">Nilai Project</th>
                    <th rowspan="2">Nilai Invoice</th>
                    <th colspan="3" style="text-align: -webkit-center">Status</th>
                    <th rowspan="2">Status Dokumen</th>
                    <th rowspan="2">Pembuat</th>
                    <th rowspan="2">Ubis</th>
                    <th rowspan="2">Tanggal Input</th>
                    <th rowspan="2">Action</th>
                </tr>
                <tr style="white-space: nowrap">
                    <th>LPP</th>
                    <th>BA Rekon</th>
                    <th>Performansi</th>
                </tr>
            </thead>
    </table>
 
@push('scripts')
<script>
$(function() {
    $('#pbsTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'operation-json',
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false}, //no
            { data: 'dataio.no_io','defaultContent': ""},  //No IO
            { data: 'dataio.deskripsi','defaultContent': ""}, // Deskripsi IO
            { data: 'customer.nama_customer','defaultContent': ""},  //Customer
            { data: 'uraian','defaultContent': ""}, //Uraian
            { data: 'nilai_project', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )}, //Nilai project
            { data: 'nilai_invoice', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )}, //Nilai Invoice
            { 
              data: 'lpp', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                } else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'barekon', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                }
                else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { 
              data: 'performansi', 
              render: function(data) { 
                if(data ==  '1111-11-11 11:11:11') {
                  return '<i class="fa fa-close"></i>';
                }
                else if(data != '1111-11-11 11:11:11' && data !==null){
                  return '<i class="fa fa-check"></i>';
                }else if(data === null){
                    return '<i class="fa fa-minus"></i>';
                }
              },
              defaultContent: ''
            },
            { data: 'status'},
            { data: 'pembuat.name','defaultContent': ""},
            { data: 'pembuat.unitnya.nama','defaultContent': ""},
            { data: 'created_at'},
            { data: 'action'},
        ],
        
    });
});
</script>
@endpush
