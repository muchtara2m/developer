@extends('layouts.master')

@section('title')
Dashboard AR | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
{{-- Icon --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- DataTables -->

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
{{-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css"> --}}
<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/material.js"></script>
<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    #chartdiv {
        width: 100%;
        height: 500px;
    }
</style>

@endsection

<!-- Chart code -->

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            DASHBOARD AR
            <!-- <small>Form PBS</small> -->
        </h1>
        <br>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">AR</a></li>
            <li class="active"> Dashboard </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
        <br />
        @endif
        <div class="row">
            <div class="col-md-12" style="padding-bottom:2%">
                <div style="text-align:-webkit-center">
                    <select name="bulan" id="bulan" class="form-group" style="width:20%;height:34px">
                        
                    </select>
                    <select name="tahun" id="tahun" class="form-group" style="width:20%;height:34px">
                        
                    </select>
                    <br>
                    {{-- <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                    <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button> --}}
                </div>
            </div>
            
            <div class="col-md-12" hidden > <!-- Dashboard AR Counting perWeek-->
                <div class="box box-primary">
                    <div class="box-header with-border" style="text-align:center">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="background:#ecf0f5">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3 col-sm-6 col-xs-12" >
                                    <div class="info-box box box-warning" style="text-align:center">
                                        <b><span class="info-box-text">Minggu 1</span></b>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="description-block border>
                                                    <span class="description-percentage "> Jumlah</span>
                                                    <h5 class="description-header">50</h5>
                                                    <span class="description-text">2 September 2019</span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="description-block ">
                                                    <span class="description-percentage "> Jumlah</span>
                                                    <h5 class="description-header">40</h5>
                                                    <span class="description-text">9 September 2019</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="info-box box box-danger" style="text-align:center">
                                        <b><span class="info-box-text">Minggu 2</span></b>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="description-block border>
                                                    <span class="description-percentage "> Jumlah</span>
                                                    <h5 class="description-header">40</h5>
                                                    <span class="description-text">9 September 2019</span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="description-block ">
                                                    <span class="description-percentage "> Jumlah</span>
                                                    <h5 class="description-header">30</h5>
                                                    <span class="description-text">16 September 2019</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 ">
                                    <div class="info-box box box-success " style="text-align:center">
                                        <b><span class="info-box-text">Minggu 3</span></b>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="description-block border>
                                                    <span class="description-percentage "> Jumlah</span>
                                                    <h5 class="description-header">30</h5>
                                                    <span class="description-text">16 September 2019</span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="description-block ">
                                                    <span class="description-percentage "> Jumlah</span>
                                                    <h5 class="description-header">20</h5>
                                                    <span class="description-text">23 September 2019</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.info-box -->
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="info-box box box-info" style="text-align:center">
                                        <b><span class="info-box-text">Minggu 4</span></b>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="description-block border>
                                                    <span class="description-percentage "> Jumlah</span>
                                                    <h5 class="description-header">20</h5>
                                                    <span class="description-text">23 September 2019</span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="description-block ">
                                                    <span class="description-percentage "> Jumlah</span>
                                                    <h5 class="description-header">10</h5>
                                                    <span class="description-text">30 September 2019</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            {{-- Dashboard Gauge AR  --}}
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border" style="text-align:center">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body" style="background:#ecf0f5">
                        <div class="col-md-6"> <!-- Dashboard AR Gauge UNBILL with window mode-->
                            <div class="box box-success">
                                <div class="box-header with-border" style="text-align:center">
                                    <h3 class="box-title">UNBILL</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body" >
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-center">
                                                <strong>1 : 1.000.000</strong>
                                            </p>
                                            <div id="chartunbill" style="height:400px">
                                                
                                            </div>
                                            <!-- /.chart-responsive -->
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <div class="col-md-6"> <!-- Dashboard AR Gauge BILL with window mode-->
                            <div class="box box-info">
                                <div class="box-header with-border" style="text-align:center">
                                    <h3 class="box-title">BILL</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="text-center">
                                                <strong>1 : 1.000.000</strong>
                                            </p>
                                            <div id="chartbill" style="height:400px">
                                                
                                            </div>
                                            <!-- /.chart-responsive -->
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>  
                    </div>
                </div>
            </div>
            {{-- end dashboard gauge ar --}}
            <!-- Dashboard AR BOX with window mode-->
            <div class="col-md-12" >
                <div class="box box-primary">
                    <div class="box-header with-border" style="text-align:center">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="background:#ecf0f5">
                        <div class="row">
                            <div class="col-md-12">
                                {{-- <p class="text-center">
                                    <strong>Dashboard AR</strong>
                                </p> --}}
                                <div class="col-md-3 col-sm-6 col-xs-12" >
                                    <div class="info-box box box-warning">
                                        <span class="info-box-icon bg-yellow"><i class="fa fa-file"></i></span>
                                        
                                        <div class="info-box-content">
                                            <span class="info-box-text">TOTAL AR</span>
                                            <span class="info-box-number" id="totalar"> </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="info-box box box-danger">
                                        <span class="info-box-icon bg-red"><i class="fa fa-file"></i></span>
                                        
                                        <div class="info-box-content">
                                            <span class="info-box-text">UNBILLED</span>
                                            <span class="info-box-number" id="unbill"> </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="info-box box box-success">
                                        <span class="info-box-icon bg-green"><i class="fa fa-file"></i></span>
                                        
                                        <div class="info-box-content">
                                            <span class="info-box-text">Ready To Bill</span>
                                            <span class="info-box-number" id="ready">  </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                </div>
                                <!-- /.info-box -->
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="info-box box box-info">
                                        <span class="info-box-icon bg-aqua"><i class="fa fa-file"></i></span>
                                        
                                        <div class="info-box-content">
                                            <span class="info-box-text">BILLED</span>
                                            <span class="info-box-number" id="billed"><span>
                                            </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-sm-6 col-xs-12" >
                                        <div class="info-box box box-warning" style="text-align:center">
                                            <h4 class="info-box-text" ><b>LAST WEEK</b></h4>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="description-block border">
                                                        {{-- <span class="description-percentage "> Jumlah</span> --}}
                                                        <h5 class="description-header" id="ttlLastWeek"></h5>
                                                        <h4 class="description-text" id="lastWeek"></h4>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="info-box box box-danger" style="text-align:center">
                                            <h4 class="info-box-text" ><b>NOW</b></h4>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="description-block ">
                                                        {{-- <span class="description-percentage ">Jumlah</span> --}}
                                                        <h3 class="description-header" id="ttlNow"></h4>
                                                            <h4 class="description-text" id="skrg"></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.info-box -->
                                        </div>
                                        
                                        <!-- /.info-box -->
                                        
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                    <!-- END Dashboard AR BOX with window mode-->
                    {{-- dashboard counting last week --}}
                    <div class="col-md-12" hidden > <!-- Dashboard AR Counting perWeek-->
                        <div class="box box-primary">
                            <div class="box-header with-border" style="text-align:center">
                                <h3 class="box-title"></h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body" style="background:#ecf0f5">
                                
                                <!-- /.box-footer -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                    {{-- end dashboard counting last week --}}
                    
                </div>
            </div>
            
        </section>
        @endsection
        @section('scripts')
        <!-- DataTables -->
        <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>
        <!-- Chart code -->
        <script src="{{ asset('js/chartjs/ds-ar.js') }}"></script>
        
        <script>
            
            getMessage();
            $('#bulan').change(function(){
                var bln = $('#bulan').val();
                var thn = $('#tahun').val();
                if(bln != '' &&  thn != '')
                {
                    $('#pbsTable').DataTable().destroy();
                    getMessage(bln, thn);
                }
                else
                {
                    alert('Both Date is required');
                }
            });
            $('#tahun').change(function(){
                var bln = $('#bulan').val();
                var thn = $('#tahun').val();
                if(bln != '' &&  thn != '')
                {
                    $('#pbsTable').DataTable().destroy();
                    getMessage(bln, thn);
                }
                else
                {
                    alert('Both Date is required');
                }
            });
            function getMessage(bln='', thn='') {
                var token = $("input[name='_token']").val();
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'post',
                    serverSide: true,
                    url:"{{ url('db-ar') }}",
                    data: {bln:bln,thn:thn, _token:token},
                    dataType:'json',//return data will be json
                    success:function(data){
                        $('#billed').html('Rp ' +parseFloat(data.nilaibill).toLocaleString('id') );
                        $('#totalar').html('Rp ' +(data.total).toLocaleString('id') );
                        $('#ready').html('Rp ' +parseFloat(data.nilaireadytobill).toLocaleString('id') );
                        $('#unbill').html('Rp ' +parseFloat(data.nilaiunbill).toLocaleString('id') );
                        $('#lastWeek').html( data.lastWeek);
                        $('#skrg').html(data.now);
                        $('#ttlNow').html('Dokumen Yang Masih X : '+data.count);
                        $('#ttlLastWeek').html('Dokumen Yang Masih X : '+data.countLast);
                        if(data.total > 0){
                           
                            var totalnya = data.total/1000000;
                            var unbill = data.nilaiunbill/1000000;
                            var bill = data.nilaibill/1000000;
                            var hasil = (totalnya/unbill)*100;
                            var bagi = totalnya/2;
                        }else{
                            var totalnya = 1;
                            var unbill = 0;
                            var bill = 0;
                            var hasil = 0;
                            var bagi = 0;
                        }
                        chartUnbill(totalnya,unbill,hasil,bagi,bill);
                    }
                });
            }
            
        </script>   
        
        
        @endsection
        