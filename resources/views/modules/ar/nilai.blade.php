@extends('layouts.master')

@section('title')
Update Nilai | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
{{-- Icon --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- DataTables -->

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
{{-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css"> --}}
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    /* table thead th tbody td{
        :
    } */
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Update Nilai
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">AR</a></li>
            <li class="active"> Update Nilai</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Update Nilai</h3>
                    </div>
                    <div style="text-align:center">
                        <div class="row input-daterange">
                            <div class="col-md-4">
                                <select name="bulan" id="bulan" class="form-control">
                                    
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select name="tahun" id="tahun" class="form-control">
                                    
                                </select>
                            </div>
                            <div class="col-md-4" hidden>
                                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                                <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="display">
                            @csrf
                            <thead>
                                <tr style="white-space: nowrap">
                                    <th>No</th>
                                    <th>IO</th>
                                    <th>Deskripsi IO</th>
                                    <th>Nilai Revenue</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                        
                    </div>
                    <div class="modal fade" id="modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Update Nilai Project</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="" enctype="multipart/form-data">
                                            @csrf
                                            {{--  @method('PATCH')  --}}
                                            <input type="text" id="nilai" name="nilai_project" onkeyup="formatAngka(this,'.')"  class="form-control">
                                            <br>
                                            <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i>
                                                Submit</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        
        @endsection
        
        @section('scripts')
        <!-- DataTables -->
        <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>
        @stack('scripts')
        <script type="text/javascript">
            // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
            
            $(document).on("click", ".upload", function () {
                var idbro = $(this).data('id');
                var nilai = $(this).data('nilai');
                // $(".modal-body #idnya").val( idbro );
                $('form').attr('action',"{{ url('nilai-project')}}/"+idbro);
                $('#nilai').attr('value',nilai);
            }); </script>
            <script>
                function formatAngka(objek, separator) {
                    a = objek.value;
                    b = a.replace(/[^\d]/g, "");
                    c = "";
                    panjang = b.length;
                    j = 0;
                    for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                            c = b.substr(i - 1, 1) + separator + c;
                        } else {
                            c = b.substr(i - 1, 1) + c;
                            formatAngka
                        }
                    }
                    objek.value = c;
                }
                function clearDot(number)
                {
                    output = number.replace(".", "");
                    return output;
                }
            </script>
            <script>
                dataNilai();
                $('#bulan').change(function(){
                    var bln = $('#bulan').val();
                    var thn = $('#tahun').val();
                    if(bln != '' &&  thn != '')
                    {
                        $('#pbsTable').DataTable().destroy();
                        dataNilai(bln, thn);
                    }
                    else
                    {
                        alert('Both Date is required');
                    }
                });
                $('#tahun').change(function(){
                    var bln = $('#bulan').val();
                    var thn = $('#tahun').val();
                    if(bln != '' &&  thn != '')
                    {
                        $('#pbsTable').DataTable().destroy();
                        dataNilai(bln, thn);
                    }
                    else
                    {
                        alert('Both Date is required');
                    }
                });
                function dataNilai(bln='',thn=''){
                    $('#pbsTable').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: {
                            url:'{{ url("nilai") }}',
                            data:{bln:bln, thn:thn}
                        },
                        columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
                        { data: 'dataio.no_io','defaultContent': ""},
                        { data: 'dataio.deskripsi','defaultContent': ""},
                        { data: 'nilai_project', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},
                        { data: 'action'},
                        ]
                    });
                }</script>
                @endsection
                