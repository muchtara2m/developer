
@php
$no = 1;
@endphp
@foreach($data as $item)
<tr>
        <td style="white-space:nowrap">{{ $no++ }}</td>
        <td style="white-space:nowrap">{{ $item->no_io }}</td>
        <td style="white-space:nowrap">{{ $item->deskripsi }}</td>
        <td style="white-space:nowrap">{{ $item->nama_customer }}</td>
        <td style="white-space:nowrap">{{ $item->uraian }}</td>
        <td style="white-space:nowrap">{{ number_format($item->nilai_project) }}</td>
        <td style="white-space:nowrap">{{ number_format($item->nilai_invoice) }}</td>
        @if($item->spk == '1111-11-11 11:11:11')
        <td id="icon"><i class="fa fa-close"></i></td>
        @elseif($item->spk != '1111-11-11 11:11:11' && $item->spk !=NULL)
        <td id="icon"><i class="fa fa-check"></i></td>
        @elseif($item->spk == NULL)
        <td id="icon"><i class="fa fa-minus"></i></td>
        @endif
        @if($item->kl == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->kl != '1111-11-11 11:11:11' && $item->kl !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->kl == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->baut == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->baut != '1111-11-11 11:11:11' && $item->baut !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->baut == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->bast == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->bast != '1111-11-11 11:11:11' && $item->bast !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->bast == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->baso == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->baso != '1111-11-11 11:11:11' && $item->baso !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->baso == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->bapp == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->bapp != '1111-11-11 11:11:11' && $item->bapp !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->bapp == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->lpp == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->lpp != '1111-11-11 11:11:11' && $item->lpp !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->lpp == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->baperub == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->baperub != '1111-11-11 11:11:11' && $item->baperub !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->baperub == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->suratgm == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->suratgm != '1111-11-11 11:11:11' && $item->suratgm !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->suratgm == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->barekon == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->barekon != '1111-11-11 11:11:11' && $item->barekon !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->barekon == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->performansi == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->performansi != '1111-11-11 11:11:11' && $item->performansi !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->performansi == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->lkpp == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->lkpp != '1111-11-11 11:11:11' && $item->lkpp !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->lkpp == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->ep == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->ep != '1111-11-11 11:11:11' && $item->ep !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->ep == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->npk == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->npk != '1111-11-11 11:11:11' && $item->npk !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->npk == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif

    @if($item->top == '1111-11-11 11:11:11')
    <td id="icon"><i class="fa fa-close"></i></td>
    @elseif($item->top != '1111-11-11 11:11:11' && $item->top !=NULL)
    <td id="icon"><i class="fa fa-check"></i></td>
    @elseif($item->top == NULL)
    <td id="icon"><i class="fa fa-minus"></i></td>
    @endif
    <td style="white-space:nowrap">{{ strtoupper($item->status) }}</td>
    <td style="white-space:nowrap">{{ $item->name }}</td>
    <td style="white-space:nowrap">{{ $item->nama}}</td>
    <td style="white-space:nowrap">{{ $item->created_at }}</td>
    <td style="white-space:nowrap">
        <a href="{{ url('preview_ar/'.$item->id) }}"><i class="fa fa-file"></i></a>
        <a href="{{ url('edit_ar/'.$item->id) }}"><i class="fa fa-edit"></i></a>
        <a href="#"><i class="fa fa-trash"></i></a>
    </td>
</tr>
@endforeach
<tr>
        <td colspan="28" align="left">
                Halaman : {{ $data->currentPage() }} | Jumlah Data : {{ $data->total() }} | 
                Data Per Halaman : {{ $data->perPage() }} |<br/>
                {!! $data->links() !!}
        </td>
</tr>


