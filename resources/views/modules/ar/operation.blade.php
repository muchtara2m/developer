@extends('layouts.master')

@section('title')
Unbill Operation | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
{{-- Icon --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- DataTables -->

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
{{-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css"> --}}
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    #icon{
        text-align:center;
    }
    /* table thead th tbody td{
        :
    } */
    tbody tr{
        white-space: nowrap;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            UNBILL Operation
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">AR</a></li>
            <li class="active"> Unbill Operation</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Unbill</h3>
                    </div>
                    <div style="text-align:center">
                        <div class="row input-daterange">
                            <div class="col-md-4">
                                <select name="bulan" id="bulan" class="form-control">
                                    
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select name="tahun" id="tahun" class="form-control">
                                    
                                </select>
                            </div>
                            <div class="col-md-4" hidden>
                                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                                <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="display">
                            @csrf
                            <thead>
                                <tr style="white-space: nowrap">
                                    <th rowspan="2">Action</th>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">IO</th>
                                    <th rowspan="2">Deskripsi IO</th>
                                    <th rowspan="2">Customer</th>
                                    <th rowspan="2">Ubis</th>
                                    <th rowspan="2">Uraian</th>
                                    <th rowspan="2">Nilai Project</th>
                                    <th rowspan="2">Nilai Invoice</th>
                                    <th colspan="3" style="text-align: -webkit-center">Status</th>
                                    <th rowspan="2">Status Dokumen</th>
                                    <th rowspan="2">Pembuat</th>
                                    <th rowspan="2">Tanggal Input</th>
                                </tr>
                                <tr style="white-space: nowrap">
                                    <th>LPP</th>
                                    <th>BA Rekon</th>
                                    <th>Performansi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>
<script>
    dataBill();
    $('#bulan').change(function(){
        var bln = $('#bulan').val();
        var thn = $('#tahun').val();
        if(bln != '' &&  thn != '')
        {
            $('#pbsTable').DataTable().destroy();
            dataBill(bln, thn);
        }
        else
        {
            alert('Both Date is required');
        }
    });
    $('#tahun').change(function(){
        var bln = $('#bulan').val();
        var thn = $('#tahun').val();
        if(bln != '' &&  thn != '')
        {
            $('#pbsTable').DataTable().destroy();
            dataBill(bln, thn);
        }
        else
        {
            alert('Both Date is required');
        }
    });
    function dataBill(bln = '', thn = ''){
        $('#pbsTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ url("unbill-operation") }}',
                data:{bln:bln, thn:thn}
            },
            columns: [
            { data: 'action'},
            { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false}, //no
            { data: 'dataio.no_io','defaultContent': ""},  //No IO
            { data: 'dataio.deskripsi','defaultContent': ""}, // Deskripsi IO
            { data: 'customer.nama_customer','defaultContent': ""},  //Customer
            { data: 'pembuat.unitnya.nama','defaultContent': ""},
            { data: 'uraian','defaultContent': ""}, //Uraian
            { data: 'nilai_project', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )}, //Nilai project
            { data: 'nilai_invoice', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )}, //Nilai Invoice
            { 
                data: 'lpp', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'barekon', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'performansi', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { data: 'status'},
            { data: 'pembuat.name','defaultContent': ""},
            { data: 'created_at'},
            ],
        });
    };
</script>

@endsection
