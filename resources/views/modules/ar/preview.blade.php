@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Preview AR";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- Data Table -->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for radioes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
{{--  froala  --}}
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />


@endsection
@section('customstyle')
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Preview AR
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">AR</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Preview AR</h3>
                    </div>
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="{{ url('update-nilai/'.$ars->id) }}" id="form" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <h4 class="divider-title">{{ $ars->pembuat['name'].' - '.$ars->pembuat->unitnya['nama'] }}</h4>
                                <hr>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="customer">Customer</label>
                                    <input type="text" class="form-control" title="Customer" value="{{ $ars->customer['nama_customer'] }}" id="customer" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="noio">No. IO</label>
                                    <input type="text" name="desio" id="noio" class="form-control" value="{{ $ars->dataio['no_io'] }}" placeholder="Pilih IO" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="Currency">Deskripsi IO</label>
                                    <input type="text" name="" class="form-control" value="{{ $ars->dataio['deskripsi'] }}" id="" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-4" hidden>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nilai Project</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">IDR</i></span>
                                        <input type="text" onblur="formatAngka(this,'.')" value=" {{ $ars->nilai_project }}" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nilai Invoice</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">IDR</span>
                                        <input type="text" name="nilai_invoice" value="{{ $ars->nilai_invoice }}" onkeyup="formatAngka(this,'.'),tambahHasil()" id="nilai_invoice" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-4" hidden>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nilai Belum Ditagih</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">IDR</span>
                                        <input type="text" value="" class="form-control" id="nilai_belum_ditagih" onkeyup="tambahHasil(),formatAngka(this,'.')" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3" hidden>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nilai Paid</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                        <input type="text" onkeyup="formatAngka(this,'.')" placeholder="1.000.00.000" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Deskripsi Project</label>
                                    <textarea name="uraian" class="form-control">
                                        {{ $ars->uraian }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group col-md-12" hidden>
                                <div class="form-group" >
                                    <label for="exampleInputEmail1">Deskripsi Invoice</label>
                                    <textarea name="invoice" class="form-control">
                                        {{ $ars->invoice }}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <h4 class="divider-title">DOKUMEN</h4>
                                <hr>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-green">
                                                <input type="checkbox" id="spk"  name="spk" {{ ($ars->spk != NULL && $ars->spk != '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" id="spk" value="spk" name="spk" {{ ($ars->spk == '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <input type="text" value="SPK" class="form-control" id="spkin" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="kb"  name="kb" {{ ($ars->kb != NULL && $ars->kb != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="kb" value="kb" name="kb" {{ ( $ars->kb == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="KB" class="form-control" id="kbin" readonly>
                                            </div>
                                        </div>
                                    </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-green">
                                                <input type="checkbox" id="kl"  name="kl" {{ ($ars->kl != NULL && $ars->kl != '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" id="kl" value="kl" name="kl" {{ ( $ars->kl == '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <input type="text" value="KL" class="form-control" id="klin" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-green">
                                                <input type="checkbox" id="baut"  name="baut" {{ ($ars->baut != NULL && $ars->baut != '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" id="baut" value="baut" name="baut" {{ ( $ars->baut == '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <input type="text" value="BAUT" readonly class="form-control" id="bautin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-green">
                                                <input type="checkbox" id="bast"  name="bast" {{ ($ars->bast != NULL && $ars->bast != '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" id="bast" value="bast"  name="bast" {{ ( $ars->bast == '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <input type="text" value="BAST" readonly class="form-control" id="bastin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-green">
                                                <input type="checkbox" id="baso"  name="baso" {{ ($ars->baso != NULL && $ars->baso != '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" id="baso" value="baso" name="baso" {{ ( $ars->baso == '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <input type="text" value="BASO PINS" readonly class="form-control" id="basoin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="basocus"  name="basocus" {{ ($ars->basocus != NULL && $ars->basocus != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="basocus" value="basocus" name="basocus" {{ ( $ars->basocus == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="BASO CUST" readonly class="form-control" id="basoin">
                                            </div>
                                        </div>
                                    </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-green">
                                                <input type="checkbox" id="bapp"  name="bapp" {{ ($ars->bapp != NULL && $ars->bapp != '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" id="bapp" value="bapp" name="bapp" {{ ( $ars->bapp == '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <input type="text" value="BAPP" readonly class="form-control" id="bappin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-green">
                                                <input type="checkbox" id="lpp"  name="lpp" {{ ($ars->lpp != NULL && $ars->lpp != '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" id="lpp" value="lpp" name="lpp" {{ ( $ars->lpp == '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <input type="text" value="LPP" readonly class="form-control" id="lppin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-green">
                                                <input type="checkbox" id="baperub"  name="baperub" {{ ($ars->baperub != NULL && $ars->baperub != '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" id="baperub" value="baperub"  name="baperub" {{ ( $ars->baperub == '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <input type="text" value="BA Perub JK WKT" readonly class="form-control" id="bain">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-green">
                                                <input type="checkbox" id="barekon"  name="barekon" {{ ($ars->barekon != NULL && $ars->barekon != '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" id="barekon" value="barekon"  name="barekon" {{ ( $ars->barekon == '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <input type="text" value="BA Rekon" readonly class="form-control" id="barein">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-green">
                                                <input type="checkbox" id="performansi"  name="performansi" {{ ($ars->performansi != NULL && $ars->performansi != '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" id="performansi" value="performansi" name="performansi" {{ ( $ars->performansi == '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <input type="text" value="Performansi" readonly class="form-control" id="perfomin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-green">
                                                <input type="checkbox" id="npk"  name="npk" {{ ($ars->npk != NULL && $ars->npk != '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" id="npk" value="npk"  name="npk" {{ ( $ars->npk == '1111-11-11 11:11:11')? 'checked':'' }}>
                                            </span>
                                            <input type="text" value="NPK" readonly class="form-control" id="npkin">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <h4 class="divider-title">DOKUMEN</h4>
                                <hr>
                            </div>
                            <div class="form-group">
                                <table class=" display table table-bordered table-striped dataTable" id="dokumen">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Dokumen</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>SPK</td>
                                            @if($ars->spk == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->spk != '1111-11-11 11:11:11' && $ars->spk !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->spk }}</td>
                                            @elseif($ars->spk == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif

                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>KB</td>
                                            @if($ars->kb == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->kb != '1111-11-11 11:11:11' && $ars->kb !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->kb }}</td>
                                            @elseif($ars->kb == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>KL</td>
                                            @if($ars->kl == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->kl != '1111-11-11 11:11:11' && $ars->kl !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->kl }}</td>
                                            @elseif($ars->kl == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif
                                        </tr>
                                         <tr>
                                            <td>4</td>
                                            <td>BAUT</td>
                                            @if($ars->baut == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->baut != '1111-11-11 11:11:11' && $ars->baut !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->baut }}</td>
                                            @elseif($ars->baut == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>BAST</td>
                                            @if($ars->bast == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->bast != '1111-11-11 11:11:11' && $ars->bast !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->bast }}</td>
                                            @elseif($ars->bast == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>BASO PINS</td>
                                            @if($ars->baso == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->baso != '1111-11-11 11:11:11' && $ars->baso !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->baso }}</td>
                                            @elseif($ars->baso == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>BASO CUST</td>
                                            @if($ars->basocus == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->basocus != '1111-11-11 11:11:11' && $ars->basocus !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->basocus }}</td>
                                            @elseif($ars->basocus == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>BAPP</td>
                                            @if($ars->bapp == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->bapp != '1111-11-11 11:11:11' && $ars->bapp !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->bapp }}</td>
                                            @elseif($ars->bapp == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>LPP</td>
                                            @if($ars->lpp == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->lpp != '1111-11-11 11:11:11' && $ars->lpp !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->lpp }}</td>
                                            @elseif($ars->lpp == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>BA Perub JK WKT</td>
                                            @if($ars->baperub == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->baperub != '1111-11-11 11:11:11' && $ars->baperub !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->baperub }}</td>
                                            @elseif($ars->baperub == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>BA Rekon</td>
                                            @if($ars->barekon == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->barekon != '1111-11-11 11:11:11' && $ars->barekon !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->barekon }}</td>
                                            @elseif($ars->barekon == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Performansi</td>
                                            @if($ars->performansi == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->performansi != '1111-11-11 11:11:11' && $ars->performansi !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->performansi }}</td>
                                            @elseif($ars->performansi == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>NPK</td>
                                            @if($ars->npk == '1111-11-11 11:11:11')
                                            <td id="icon"><i class="fa fa-minus"></i></td>
                                            <td></td>
                                            @elseif($ars->npk != '1111-11-11 11:11:11' && $ars->npk !=NULL)
                                            <td id="icon"><i class="fa fa-check"></i></td>
                                            <td>{{ $ars->npk }}</td>
                                            @elseif($ars->npk == NULL)
                                            <td id="icon"><i class="fa fa-close"></i></td>
                                            <td></td>
                                            @endif
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="col-md-12">
                                @if($ars->status == 'readytobill')
                                <div class="form-group">
                                    <label for="">Comment</label>
                                    <input type="text" name="chat" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <button type="submit" name="status" value="bill" class="btn btn-success" style="width: 7em;"> Submit</button>
                                <button type="submit" name="status" value="unbill" class="btn btn-danger" style="width: 7em;"> Return</button>
                            </div>
                            @endif
                        </div>
                        <!-- /.box-footer -->
                    </form>

                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
{{-- bahasa indonesia froala --}}
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
{{--  datatabe  --}}
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
{{--  datatable  --}}
<script>

    $(function(){
        $('textarea').froalaEditor({
            placeholderText: 'Type Something',
            charCounterCount: false,
            key: '{{ env("KEY_FROALA") }}',
        });
        $('#dokumen').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
    function tambahHasil(){
        var nilai_project = {{ $ars->nilai_project }};
        var nilai_invoice = $('#nilai_invoice').val();
        var invoice_hapus_dot = nilai_invoice.split('.').join("");
        var hasil = nilai_project - invoice_hapus_dot;
        $('#nilai_belum_ditagih').val(hasil);

    }
    function formatAngka(objek, separator) {
        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i - 1, 1) + separator + c;
            } else {
                c = b.substr(i - 1, 1) + c;
                formatAngka
            }
        }
        objek.value = c;
    }
    function clearDot(number)
    {
        output = number.replace(".", "");
        return output;
    }
</script>
@endsection
